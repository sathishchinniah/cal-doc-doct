import UIKit

class OnBoardViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var SignButton: UIButton! {
        didSet {
            SignButton.layer.borderColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1.0).cgColor
            SignButton.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var Register: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!

    //MARK:- Variables and Constants
    var titleTextArray = ["Keep your number private!", "Real time communication", "Manage own Doctors", "Add and Search Doctors"]
    var descTextArray = ["Connect with your patients using Chat, Videos or Voice","Do real time Audio/Video calls and text chat with you trusted Doct(or", "Manage your doctors and for your family members", "Add and Search your trusted doctor from Specialities"]
    var timer = Timer()
    // MARK: - Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        self.initializeView()
    }
    
//    override func viewWillAppear(true) {
//        //super.viewWillAppear(true)
//        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
//    }
//    
//    override func viewDidDisappear(true) {
//        //super.viewDidDisappear(true)
//        timer.invalidate()
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- Custom methods
    
    @IBAction func SignInButtonPressed(_ sender: UIButton!) {
        performSegue(withIdentifier: "gotoMain", sender: self)
    }
    
    @IBAction func RegisterButtonPressed(_ sender: UIButton!)    {
        performSegue(withIdentifier: "gotoRegister", sender: self)
    }

    //MARK: UIScrollView Delegate

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let viewWidth: CGFloat = UIScreen.main.bounds.width
        var pageNumber = floor((scrollView.contentOffset.x - viewWidth / 70) / viewWidth) + 1
        if Int(pageNumber) > 3 {
            scrollView.contentOffset.x = 0
            pageNumber = 0
        }
        pageControl.currentPage = Int(pageNumber)
        titleLabel.text = titleTextArray[pageControl.currentPage]
        descLabel.text = descTextArray[pageControl.currentPage]
    }

    // MARK: - Custom Methods
    private func initializeView() {
        self.initializeWalkThrough()
    }

    private func initializeWalkThrough() {
        let pageCount : CGFloat = 4
        scrollView.backgroundColor = UIColor.clear
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width * pageCount, height: scrollView.frame.size.height)
    }
    
    @objc func timerAction() {
        let xOffSet = scrollView.contentOffset.x + UIScreen.main.bounds.width
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.scrollView.contentOffset.x = xOffSet
            }, completion: nil)
        }
    }
}

