//
//  CallViewController.h
//  Doctor
//
//  Created by Manish Verma on 18/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMSoundManager.h"

@class GData;
@class QBRTCSession;

@interface CallViewController : UIViewController

@property (nonatomic,retain) GData *dataPointer;
@property (strong, nonatomic) QBRTCSession *session;

@end
