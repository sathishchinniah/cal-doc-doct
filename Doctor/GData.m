//
//  GData.m
//  Patient
//
//  Created by Manish Verma on 15/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import "GData.h"

static GData *shared = NULL;

@implementation GData

@synthesize theCurrentUser, scanidfor, isPatientFetched, glocationcurrent, glocationdestination,  PatientName, incomingcallUserName, populatedArray, patientslistArray,calledUserName, chatdialogusersindex, chatDialoguserscontext, ischatuserfound, isalreadyloggedin,thecurrentuserID,theSession,dialignTimer;



-(id)init{
    
    self = [super init];
    
    if(self){

        scanidfor=0; // 0 = for physician, =1 for patient
        PatientName=@"Patient Name";
        calledUserName=@"";
        incomingcallUserName = @"";
        isPatientFetched = false;
        populatedArray=[[NSMutableArray alloc]init];
        patientslistArray=[[NSMutableArray alloc]init];
        chatDialoguserscontext=[[NSMutableArray alloc]init];
        theCurrentUser=0;
        chatdialogusersindex = 0;
        ischatuserfound = false;
        isalreadyloggedin = false;
        thecurrentuserID=0;
        theSession = nil;
        dialignTimer = nil;
    }
    return self;
    
}





+ (GData *)sharedData
{
    
    {
        if ( !shared || shared == NULL )
        {
            // allocate the shared instance, because it hasn't been done yet
            shared = [[GData alloc] init];
        }
        
        return shared;
    }
    
}

@end
