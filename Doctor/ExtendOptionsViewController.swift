//
//  ExtendOptionsViewController.swift
//  Doctor
//
//  Created by Kirthika Raukutam on 05/12/18.
//  Copyright © 2018 calldoc. All rights reserved.
//

import UIKit

class ExtendOptionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var extendButton: UIButton!
    var selectedConsult = QBCOCustomObject()
    let extendOptions = ["1 Day", "2 Days", "3 Days"]
    var extendedDuration = 0
    var extendedNoOfDays = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationController?.navigationBar.isHidden = true
        tableView.tableFooterView = UIView()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func extendTapped(_ sender: Any) {
        print("EXTEND CONTROLLER EXTENDBTNTAPPEd")
        self.dismiss(animated: true, completion: nil)
        //let consult = selectedConsult
        //numberofextends
        //selectedConsult.fields.value(forKey: "extendduration")
        var selectedExtendedDuration = selectedConsult.fields.value(forKey: "extendduration") as? Int
        //extendedduration
        //selectedConsult.fields.value(forKey: "noofextends")
        var selectedNoOfExtends = selectedConsult.fields.value(forKey: "noofextends") as? String
        if selectedExtendedDuration == nil {
            selectedExtendedDuration = extendedDuration
        } else {
            selectedExtendedDuration = selectedExtendedDuration! + extendedDuration
        }
        if selectedNoOfExtends == nil {
            selectedNoOfExtends = extendedNoOfDays
        } else {
            selectedNoOfExtends = selectedNoOfExtends! + ", " + extendedNoOfDays
        }
        let object = QBCOCustomObject()
        object.className = "ConsultsTable"
        object.fields["extendduration"] =  selectedExtendedDuration
        object.fields["noofextends"] =  selectedNoOfExtends
        
        let timeRemaining = (selectedExtendedDuration! + 259200) - Int(Date().timeIntervalSince(selectedConsult.createdAt!))
        if timeRemaining <= 0 {
            object.fields["status"] = "CLOSED"
        } else if timeRemaining <= 24 {
            object.fields["status"] = "CLOSING SOON"
        } else {
            object.fields["status"] = "OPEN"
        }
        
        object.id = selectedConsult.id
        QBRequest.update(object, successBlock: { (response, contributors) in
//            let alert = UIAlertController(title: "", message: "Successfully extended the consult", preferredStyle: UIAlertControllerStyle.alert)
//
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
//                //print("Handle Ok logic here")
//                //self.dismiss(animated: true, completion: nil)
//            }))
//
//            self.present(alert, animated: true, completion: nil)
            
        }) { (response) in
            //LoadingIndicatorView.hide()
            print("ConfirmMedicalAdviceBaseViewController:UpdateConsultTableforPrescriptionrID: Response error: \(String(describing: response.error?.description))")
        }
        
    }
    
//    @objc func extendButtonTapped(_ sender: Any) {
//        print("EXTEND CONTROLLER EXTENDBTNTAPPEd")
//    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "extendOptionsCell", for: indexPath)
        cell.textLabel?.text = extendOptions[indexPath.row]
        cell.selectionStyle = .none
        // Configure the cell...
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Code
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        //extendedDuration = 0
        extendedDuration = (indexPath.row + 1) * 86400
        extendedNoOfDays = extendOptions[indexPath.row]
        print("EXTENDTVCTAPPED")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .none
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
