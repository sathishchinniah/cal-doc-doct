import UIKit
import MIBadgeButton_Swift

class PatientDetailsConsultCell: UITableViewCell {

    @IBOutlet var latestDateBtn: MIBadgeButton!
    @IBOutlet var latestDateTimelb: UILabel!
    @IBOutlet var consultstatuslb: UILabel!
    @IBOutlet var billingvaluelb: UILabel!
    @IBOutlet var extendconsultButton: UIButton! {
        didSet {
            extendconsultButton.layer.borderColor = UIColor.lightGray.cgColor
            extendconsultButton.layer.borderWidth = 1
        }
    }

    @IBOutlet var consultstatuslbwithConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor.white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
