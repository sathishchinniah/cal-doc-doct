import UIKit
import KMPlaceholderTextView

class PrimaryDetailsDocTableViewController: UITableViewController, UITextViewDelegate {

    var topView : HeaderViewWithImage?

    @IBOutlet var Emailaddtxt: UITextField!
    @IBOutlet weak var gendertxt: UITextField!
    @IBOutlet weak var experiencetxt: UITextField!
    @IBOutlet weak var MCInumtxt: UITextField!
    @IBOutlet weak var degreetxt: UITextField!
    @IBOutlet weak var degreetxt1: UITextField!
    @IBOutlet weak var textViewDegree: KMPlaceholderTextView!
    @IBOutlet weak var specialisation: UITextField!
    @IBOutlet weak var textviewPracticeAddress: KMPlaceholderTextView!
    @IBOutlet weak var citytxt: UITextField!
    @IBOutlet weak var statetxt: UITextField!
    @IBOutlet weak var pincodetxt: UITextField!
    @IBOutlet weak var citystateactivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var MCIcertificateImg: UIImageView!
    @IBOutlet weak var MCIUploadimg: UIImageView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var activityIndicationMCIimg: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicationsignatureimg: UIActivityIndicatorView!
    @IBOutlet weak var SignatureImageView: UIImageView!
    @IBOutlet weak var SignatureViewDone: UIView!
    @IBOutlet weak var dropDownGenderview: UIView!
    @IBOutlet weak var dropDownexperienceview: UIView!
    @IBOutlet weak var dropDownspecialityview: UIView!
    @IBOutlet weak var Englishbuttonview: UIButton!
    @IBOutlet weak var Hindibuttonview: UIButton!
    @IBOutlet weak var Urdubuttonview: UIButton!
    @IBOutlet weak var Assamesebuttonview: UIButton!
    @IBOutlet weak var Tamilbuttonview: UIButton!
    @IBOutlet weak var Kannadabuttonview: UIButton!
    @IBOutlet weak var Telugubuttonview: UIButton!
    @IBOutlet weak var Oriyabuttonview: UIButton!
    @IBOutlet weak var Malayalambuttonview: UIButton!
    @IBOutlet weak var Panjabibuttonview: UIButton!
    @IBOutlet weak var Gujaratibuttonview: UIButton!
    @IBOutlet weak var Marathibuttonview: UIButton!
    @IBOutlet weak var Bengalibuttonview: UIButton!
    @IBOutlet weak var btnSignature: UIButton!
    @IBOutlet weak var btnMCI: UIButton!
    @IBOutlet weak var signinfoimgview: UIImageView!
    @IBOutlet weak var contraintDegreeBottom: NSLayoutConstraint!

    @IBOutlet weak var iButton: UIButton! {
        didSet {
            iButton.layer.borderColor = UIColor.darkGray.cgColor
            iButton.layer.borderWidth = 1.0
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewDegree.sizeToFit()
        textviewPracticeAddress.sizeToFit()
        let screenHeight  = UIScreen.main.bounds.height
        let totalHeaderHeight = 0.29 * screenHeight
        self.topView = HeaderViewWithImage.instantiateFromNib()
        self.topView?.activityIndicatordocimg.isHidden = true
        self.tableView.setParallaxHeader(self.topView!, mode: .topFill, height: totalHeaderHeight)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: Scroll Delegates

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.tableView.shouldPositionParallaxHeader()
    }

}
