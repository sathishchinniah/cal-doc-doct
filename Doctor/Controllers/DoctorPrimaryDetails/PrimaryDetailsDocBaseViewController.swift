import UIKit
import SwiftSignatureView
import DropDown
import SDWebImage

var isiBtn = true

struct GlobalDocData {
    static var gdocname = ""
    static var guserid = 0
    static var gdocemail = ""
    static var gdocmobile = ""
    static var gusercustomdata = ""
    static var gdocage = ""
    static var gdocgender = ""
    static var gmcinum = ""
    static var gdegree = ""
    static var gdegree1 = ""
    static var gdocspeciality = ""
    static var gprofileURL = ""
    static var gMCIURL = ""
    static var gSignatureURL = ""
    static var gsamplesignatureURL = ""
    static var gaccounttype = ""
    static var firsttimeview = true
    static var firsttimeviewbankdetails = true
    static var firsttimeviewcharges = true
    static var gfullchargeval = 0
    static var ghalfchargeval = 0
    static var boolmytrustedteam = false
    static var gsmsinvitefirstname = ""
    static var gsmsinvitelastname = ""
    static var gwhichdocadded = 0    // this is to tell from my team which doctor has sent sms adding request ie 1,2,3,4,5
    static var cotspecialityselected = "" // this is to tell which speciality doctor want to add other doctors
    static var languagearray:[String] = []
    static var languagecounter:Int8 = 0
    static var maskschedularview = false
    static var languagearrayipad:[String] = []
    static var languagecounteripad:Int8 = 0
    static var gthefinalcalldoccode = ""
    static var regcalldoccode = ""
    static var countrycode = ""
    static var uploadspeciality = ""
    static var displayspeciality = ""
    static var profileVerified = false
}

class PrimaryDetailsDocBaseViewController: UIViewController, UITextFieldDelegate,UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {


    @IBOutlet weak var opacityview: UIView!
    @IBOutlet var bottumBarView: UIView!
    @IBOutlet var Okbuttonview: UIButton!
    @IBOutlet weak var signaturebackgroundview: UIView!
    @IBOutlet weak var SignatureView: SwiftSignatureView!
    @IBOutlet weak var signherelb: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    
    var  primaryDetailsTVC : PrimaryDetailsDocTableViewController?
    var homeviewController = HomeViewController()
    var nav: UINavigationController?

    let GenderDropDown = DropDown()
    let ExperienceDropdown = DropDown()
    let SpecialisationDropDown = DropDown()

    var activeTextField: UITextField?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    var profileimgbool: Bool = false
    var signaturebool: Bool = false
    var mcicertibool: Bool = false
    var allowkbinlanguage: Bool = false
    var expdrodownviewbool: Bool = false
    var genderdropdownonoff: Bool = false
    var experiencedropdownonoff: Bool = false
    var EngbuttonSelected = false
    var HindibuttonSelected = false
    var TamilbuttonSelected = false
    var TelugubuttonSelected = false
    var UrdubuttonSelected = false
    var KannadabuttonSelected = false
    var MalaybuttonSelected = false
    var AssambuttonSelected = false
    var OriyabuttonSelected = false
    var GujbuttonSelected = false
    var BengalibuttonSelected = false
    var PanjbuttonSelected = false
    var MarathibuttonSelected = false

    //for iPad
    var EngbuttonSelectedipad = false
    var HindibuttonSelectedipad = false
    var TamilbuttonSelectedipad = false
    var TelugubuttonSelectedipad = false
    var UrdubuttonSelectedipad = false
    var KannadabuttonSelectedipad = false
    var MalaybuttonSelectedipad = false
    var AssambuttonSelectedipad = false
    var OriyabuttonSelectedipad = false
    var GujbuttonSelectedipad = false
    var BengalibuttonSelectedipad = false
    var PanjbuttonSelectedipad = false
    var MarathibuttonSelectedipad = false

    var theprofileimageUID = ""
    var regCallDocCode = ""
    var CallDocTblName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.opacityview.isHidden = true
        self.signaturebackgroundview.isHidden = true
        navigationController?.isNavigationBarHidden = true
        self.view.layoutIfNeeded()

        bottumBarView.backgroundColor = UIColor(patternImage: UIImage(named: "schbottombarbg")!)

        primaryDetailsTVC?.Emailaddtxt.delegate = self
        primaryDetailsTVC?.gendertxt.delegate = self
        primaryDetailsTVC?.experiencetxt.delegate = self
        primaryDetailsTVC?.MCInumtxt.delegate = self
        primaryDetailsTVC?.specialisation.delegate = self
        primaryDetailsTVC?.textViewDegree.delegate = self
        primaryDetailsTVC?.textViewDegree.textContainer.maximumNumberOfLines = 3
        primaryDetailsTVC?.textviewPracticeAddress.delegate = self
        primaryDetailsTVC?.textviewPracticeAddress.textContainer.maximumNumberOfLines = 6
        primaryDetailsTVC?.citytxt.delegate = self
        primaryDetailsTVC?.statetxt.delegate = self
        primaryDetailsTVC?.pincodetxt.delegate = self
        primaryDetailsTVC?.activityIndicationMCIimg.isHidden = true
        primaryDetailsTVC?.activityIndicationsignatureimg.isHidden = true
        primaryDetailsTVC?.citystateactivityIndicator.isHidden = true

        let layer1 = primaryDetailsTVC?.topView?.line1
        layer1?.alpha = 0.40
        layer1?.layer.borderWidth = 1
        layer1?.layer.borderColor = UIColor.black.cgColor

        let layer2 = primaryDetailsTVC?.topView?.line2
        layer2?.alpha = 0.40
        layer2?.layer.borderWidth = 1
        layer2?.layer.borderColor = UIColor.black.cgColor
        
        if phone && maxLength == 568 {
            self.primaryDetailsTVC?.topView?.profileimageYConstraint.constant = 30.0
            
        }
        if phone && maxLength == 667 {
            self.primaryDetailsTVC?.topView?.profileimageYConstraint.constant = 30.0
            
        }

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(gesture:)))
        view.addGestureRecognizer(tapGesture)

        primaryDetailsTVC?.topView?.profileimgButtonview.addTarget(self, action: #selector(self.ProfileImgButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.btnMCI.addTarget(self, action: #selector(self.MCIButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.btnSignature.addTarget(self, action: #selector(self.SingnatureButtonClicked(_:)), for: UIControlEvents.touchUpInside)

        primaryDetailsTVC?.Englishbuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Hindibuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Urdubuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Assamesebuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Tamilbuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Kannadabuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Telugubuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Oriyabuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Malayalambuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Panjabibuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Gujaratibuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Marathibuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        primaryDetailsTVC?.Bengalibuttonview.addTarget(self, action: #selector(self.LanguageButtonTapped(_:)), for: UIControlEvents.touchUpInside)

        primaryDetailsTVC?.iButton.addTarget(self, action: #selector(self.iButtonTapped(_:)), for: UIControlEvents.touchUpInside)

        //  LoadProfileImage()

        // Check if we are coming to this view controller for the first time
        if GlobalDocData.firsttimeview {
            print( "PrimaryDetailsViewDocController: this is for first time user" )
            // do this for existing user
            if isKeyPresentInUserDefaults(key: "existinguser") {
                // This is first time user from Existing User flow
                print( "PrimaryDetailsViewDocController: This is first time user from Existing User flow" )
                let isexistinguser = UserDefaults.standard.object(forKey: "existinguser") as! Bool

                if(isexistinguser) {

                    if let existingusername = UserDefaults.standard.string(forKey: "existinguserfullname")  {
                        GlobalDocData.gdocname = existingusername
                    }
                    if let existinguseremail = UserDefaults.standard.string(forKey: "existinguseremail")  {
                        GlobalDocData.gdocemail = existinguseremail
                    }
                    if let existinguserphone = UserDefaults.standard.string(forKey: "existinguserphopne")  {
                        GlobalDocData.gdocmobile = existinguserphone
                    }

                    if let existingusercustomdata = UserDefaults.standard.string(forKey: "usercustomdata")  {
                        GlobalDocData.gusercustomdata = existingusercustomdata
                    }
                    self.cancelButton.isHidden = true
                    self.firsttimeuserdataUpdateforExistingUser()
                } else {
                    // as we are coming to this part of code which should be only very first time of Registration flow only as we  generate calldoc code
                    self.GenerateCallDocCode()
                    self.cancelButton.isHidden = true
                    // This is first time user from Registration flow
                    print( "PrimaryDetailsViewDocController:  This is first time user from Registration flow" )
                    if let tmpgdocname = UserDefaults.standard.string(forKey: "fullname")  {
                        GlobalDocData.gdocname = tmpgdocname
                    }

                    if let tmpgdocemail = UserDefaults.standard.string(forKey: "useremail")  {
                        GlobalDocData.gdocemail = tmpgdocemail
                    }

                    if let tmpgdocmobile = UserDefaults.standard.string(forKey: "userphonenumber")  {
                        GlobalDocData.gdocmobile = tmpgdocmobile
                    }

                    if let regcalldoccode = UserDefaults.standard.string(forKey: "regcalldoccode")  {
                        self.regCallDocCode = regcalldoccode
                    }

                    if let calldoctblname = UserDefaults.standard.string(forKey: "calldoccodetblname")  {
                        self.CallDocTblName = calldoctblname
                    }
                    self.firsttimeuserdataUpdate()
                    self.homeviewController.CreateCustomObjectforCOT()
                    self.CreateNewEntrytoDocCountTable()
                }
            } else {
                // Something not right
                print( "PrimaryDetailsViewDocController: Something is not right existinguser key is not found")
            }
        } else {
            // this is for not first time user
            //for Signature
            self.cancelButton.isHidden = false
            if isKeyPresentInUserDefaults(key: "keyforsamplesignatureurl") {
                let defaults: UserDefaults? = UserDefaults.standard
                var fetchPersistantValUrl: URL? = defaults?.url(forKey: "keyforsamplesignatureurl")
                if fetchPersistantValUrl != nil {
                    let theURL: String? = fetchPersistantValUrl?.absoluteString
                    primaryDetailsTVC?.SignatureImageView.isHidden = false
                    primaryDetailsTVC?.SignatureImageView.sd_setImage(with: URL(string: theURL!), placeholderImage: UIImage(named: "Signature"), options: .cacheMemoryOnly)
                    fetchPersistantValUrl = nil
                }
            }
            if isKeyPresentInUserDefaults(key: "keyforMCIcertificateurl") {
                let defaults: UserDefaults? = UserDefaults.standard
                var fetchPersistantValUrl: URL? = defaults?.url(forKey: "keyforMCIcertificateurl")
                if fetchPersistantValUrl != nil {
                    let theURL: String? = fetchPersistantValUrl?.absoluteString

                    self.primaryDetailsTVC?.MCIUploadimg.sd_setImage(with: URL(string: theURL!), placeholderImage: UIImage(named: "certificateimg@2x.png"), options: .cacheMemoryOnly)
                    fetchPersistantValUrl = nil
                }
            }

            ReadPrimaryDetailsFromServer()  // this will execute when user is not coming to this page first time and he is not existing user that means he came from Registration process
        }
        primaryDetailsTVC?.topView?.labelUserName.text = "Dr. " + GlobalDocData.gdocname
        primaryDetailsTVC?.topView?.labelUserRpleType.text = GlobalDocData.displayspeciality
        primaryDetailsTVC?.Emailaddtxt.text = GlobalDocData.gdocemail
    }

    //Generare CallDoc Code
    func GenerateCallDocCode() {
        var thefinalcode = ""
        let myCode = ShortCodeGenerator.getCode(length: 4)
        let thefullname = QBSession.current.currentUser?.fullName
        var components = thefullname?.components(separatedBy: " ")
        if((components?.count)! > 0) {
            let firstName = components?.removeFirst()
            let lastName = components?.joined(separator: " ")
            
            let firstNamefirstchar = firstName?.substring(to:(firstName?.index((firstName?.startIndex)!, offsetBy: 1))!)
            let lastNamefirstchar = lastName?.substring(to:(lastName?.index((lastName?.startIndex)!, offsetBy: 1))!)
            thefinalcode = firstNamefirstchar!  + myCode + lastNamefirstchar!
        }
        let codeuppercase = thefinalcode.uppercased()
        GlobalDocData.gthefinalcalldoccode = codeuppercase
        let updateParameters = QBUpdateUserParameters()
        updateParameters.customData = GlobalDocData.gthefinalcalldoccode
        
        QBRequest.updateCurrentUser(updateParameters, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
            // User updated successfully
        }, errorBlock: {(_ response: QBResponse) -> Void in
            // Handle error
            print("PrimaryDetailsDocViewController: GenerateCallDocCode() Error ()")
        })
    }

    // to check if key is present for persistance key value storage
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }

    @objc func didTapView(gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField

        if (textField == primaryDetailsTVC?.gendertxt) {
            hideKeyboardWhenTappedAround()
            textField.resignFirstResponder()
            textField.endEditing(true)

            GenderDropDown.anchorView = primaryDetailsTVC?.dropDownGenderview
            GenderDropDown.dataSource = ["Male", "Female"]
            GenderDropDown.show()
            GenderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.primaryDetailsTVC?.gendertxt.text = item
                UserDefaults.standard.setValue(item, forKey: "gender")
                GlobalDocData.gdocgender = item
            }
        }

        if (textField == primaryDetailsTVC?.experiencetxt) {
            hideKeyboardWhenTappedAround()
            textField.endEditing(true)

            ExperienceDropdown.anchorView = primaryDetailsTVC?.dropDownexperienceview
            ExperienceDropdown.dataSource = ["0 - 5 years", "5 - 10 years", "10 - 15 years", "15 - 20 years", "20+ years"]
            ExperienceDropdown.show()
            ExperienceDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.primaryDetailsTVC?.experiencetxt.text = item
                UserDefaults.standard.setValue(item, forKey: "age")
                GlobalDocData.gdocage = item
            }
        }

        if (textField == primaryDetailsTVC?.specialisation) {
            hideKeyboardWhenTappedAround()
            textField.endEditing(true)
            SpecialisationDropDown.anchorView = primaryDetailsTVC?.dropDownspecialityview
            SpecialisationDropDown.dataSource = ["Cardiologist", "Dermatologist", "Dietician", "Endocrinologist", "E.N.T.", "G.P.", "Gastroentrologist", "Gynaecologist", "Nephrologist", "Neurologist", "Oncologist", "Orthopaedic", "Paediatrician", "Psychiatrist", "Pulmonoligist", "Sexologist", "Alt Medicine"]
            SpecialisationDropDown.direction = .bottom
            SpecialisationDropDown.show()
            SpecialisationDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Speciality Selected item: \(item) at index: \(index)")
                self.primaryDetailsTVC?.specialisation.text = item
                GlobalDocData.gdocspeciality = item
                var iitem = item
                if iitem == "E.N.T."{
                    iitem = "ent"
                } else if iitem == "G.P." {
                    iitem = "general"
                } else if iitem == "Alt Medicine" {
                    iitem = "altmedcine"
                }else if iitem == "Cardiologist" {
                    iitem = "cardiologist"
                }else if iitem == "Dermatologist" {
                    iitem = "dermatologist"
                }else if iitem == "Dietician" {
                    iitem = "dietician"
                }else if iitem == "Endocrinologist" {
                    iitem = "endocrinologist"
                }else if iitem == "Gastroentrologist" {
                    iitem = "gastroentrologist"
                }else if iitem == "Gynaecologist" {
                    iitem = "gynaecologist"
                }else if iitem == "Nephrologist" {
                    iitem = "nephrologist"
                }else if iitem == "Neurologist" {
                    iitem = "neurologist"
                }else if iitem == "Oncologist" {
                    iitem = "oncologist"
                }else if iitem == "Orthopaedic" {
                    iitem = "orthopaedic"
                }else if iitem == "Paediatrician" {
                    iitem = "paediatrician"
                }else if iitem == "Psychiatrist" {
                    iitem = "psychiatrist"
                }else if iitem == "Pulmonoligist" {
                    iitem = "pulmonoligist"
                }else if iitem == "Sexologist" {
                    iitem = "sexologist"
                }

                GlobalDocData.uploadspeciality =  iitem

                UserDefaults.standard.setValue(item, forKey: "specialisation")
            }
        }

        if (textField == self.primaryDetailsTVC?.pincodetxt) {
            if (textField.text?.count)! < 5 {
                self.primaryDetailsTVC?.cityView.isHidden = true
                self.primaryDetailsTVC?.stateView.isHidden = true
            } else if (textField.text?.count)! >= 6 {
                self.primaryDetailsTVC?.cityView.isHidden = false
                self.primaryDetailsTVC?.stateView.isHidden = false
            }
        }

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        if (textField == self.primaryDetailsTVC?.pincodetxt) {
            if (textField.text?.count)! < 5 {
                self.primaryDetailsTVC?.cityView.isHidden = true
                self.primaryDetailsTVC?.stateView.isHidden = true
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }

    func textViewDidChange(_ textView: UITextView) {
        UIView.performWithoutAnimation {
            self.primaryDetailsTVC?.tableView.beginUpdates()
            self.primaryDetailsTVC?.tableView.endUpdates()
        }
    }


    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }

        if phone && maxLength == 568 {
            if text.count == 0 {
                if textView.text.count != 0 {
                    return true
                }
            } else if textView.text.count > 80 {
                return false
            }
        }

        if phone && maxLength == 736 {
            if text.count == 0 {
                if textView.text.count != 0 {
                    return true
                }
            } else if textView.text.count > 130 {
                return false
            }
        }
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == primaryDetailsTVC?.MCInumtxt {
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }

        if textField == primaryDetailsTVC?.citytxt {
            if phone && maxLength == 568 {
                //iphone 5
                let charmaxLength = 18
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= charmaxLength
            }

            let charmaxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= charmaxLength
        }

        if textField == primaryDetailsTVC?.statetxt {
            if phone && maxLength == 568 {
                //iphone 5
                let charmaxLength = 12
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= charmaxLength
            }

            let charmaxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= charmaxLength
        }

        if textField == self.primaryDetailsTVC?.pincodetxt {

            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)

            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length

            if length == 0 || length == 6 {

                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int

                let index = 0 as Int
                let formattedString = NSMutableString()

                let remainder = decimalString.substring(from: index)
                formattedString.append(remainder)
                textField.text = formattedString as String
                if length == 6 {
                    self.fetchpincodebaseddata(pincode: textField.text!)
                }

                self.primaryDetailsTVC?.pincodetxt.resignFirstResponder()
                return (newLength >= 6) ? false : true
                // return false
            }
            let index = 0 as Int
            let formattedString = NSMutableString()

            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
        } else {
            return true
        }
    }

    // We need to login with registered user when we come on the app for the first time
    func VerifiedUserLoginNow() {

    }

    func LoadProfileImage() {
        self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = (self.primaryDetailsTVC?.topView?.imageViewProfileIcon.frame.size.width)! / 2;
        self.primaryDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true;
        self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 0.2
        self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.lightGray.cgColor
        self.primaryDetailsTVC?.topView?.imageViewProfileIcon.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profile"))
        primaryDetailsTVC?.topView?.activityIndicatordocimg.isHidden = true
        primaryDetailsTVC?.topView?.activityIndicatordocimg.stopAnimating()
    }

    @IBAction func OKButtonClicked(_ sender: UIButton) {
        if ( GlobalDocData.gprofileURL.isEmpty == true) {
            showAlert(msg: "Please select and upload profile image")
            return
        }

        if ( GlobalDocData.gMCIURL.isEmpty == true) {
            showAlert(msg: "Please select and upload MCI Certificate image")
            return
        }

        if ( GlobalDocData.gsamplesignatureURL.isEmpty == true) {
            showAlert(msg: "Please select and upload Signature image")
            return
        }

        if (primaryDetailsTVC?.MCInumtxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter MCI Reg No. details!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.MCInumtxt.text, forKey: "mcinumber")
            UserDefaults.standard.synchronize()
            GlobalDocData.gmcinum = (self.primaryDetailsTVC?.MCInumtxt.text!)!
        }

        if (primaryDetailsTVC?.gendertxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Gender details!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.gendertxt.text, forKey: "gender")
            UserDefaults.standard.synchronize()
            GlobalDocData.gdocgender = (self.primaryDetailsTVC?.gendertxt.text!)!
        }

        if (primaryDetailsTVC?.experiencetxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Age details!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.experiencetxt.text, forKey: "age")
            UserDefaults.standard.synchronize()
            GlobalDocData.gdocage = (self.primaryDetailsTVC?.experiencetxt.text!)!
        }

        if (primaryDetailsTVC?.textViewDegree.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Degree details!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.textViewDegree.text, forKey: "degree")
            UserDefaults.standard.synchronize()
            GlobalDocData.gdegree = (self.primaryDetailsTVC?.textViewDegree.text!)!
        }

        if (self.primaryDetailsTVC?.specialisation.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Specialisation details!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.specialisation.text, forKey: "speciality")
            UserDefaults.standard.synchronize()
            GlobalDocData.displayspeciality = (self.primaryDetailsTVC?.specialisation.text!)!
        }

        //for language
        if ipad && maxLength == 1024 {
            if  GlobalDocData.languagecounteripad < 1 {
                let alert = UIAlertController(title: "Alert", message: "Please Enter Languages details!", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                })
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
                return
            } else {
                // language array is not empty
            }

        } else {
            if  GlobalDocData.languagecounter < 1 {
                let alert = UIAlertController(title: "Alert", message: "Please Enter Languages details!", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                })
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
                return
            } else {
                // language array is not empty
            }
        }

        if (primaryDetailsTVC?.textviewPracticeAddress.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Practice Address details!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.textviewPracticeAddress.text, forKey: "address")
        }

        if (primaryDetailsTVC?.pincodetxt.text?.count)! <= 5 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Area PIN Code !", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.pincodetxt.text, forKey: "pincode")
            UserDefaults.standard.synchronize()
        }

        if (primaryDetailsTVC?.citytxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter City !", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.citytxt.text, forKey: "city")
            UserDefaults.standard.synchronize()
        }

        if (primaryDetailsTVC?.statetxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter State !", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.statetxt.text, forKey: "state")
            UserDefaults.standard.synchronize()
        }

        if GlobalDocData.firsttimeview {
            CreateCustomObjectforDocPrimaryDetails()
        } else {
            UpDateCustomObjectforDocPrimaryDetails()
        }
    }
    
    
    func showAlert(msg: String) {
        let cntrl = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        cntrl.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(cntrl, animated: true, completion: nil)
    }
    

    func CreateCustomObjectforDocPrimaryDetails() {
        // Create Custom Object My Trusted Team here which is empty table with ID, UserID, ParentID as predefined fields and user defined field as Doctor Name , Speciality, Mobile number
        print("PrimaryDetailsDocViewController: Creating New Custom Object for Doctor Primary Details")
        let object = QBCOCustomObject()
        object.className = "DocPrimaryDetails"
        object.fields["fullname"] =  GlobalDocData.gdocname
        object.fields["email"] =  GlobalDocData.gdocemail
        object.fields["mcinumber"] = primaryDetailsTVC?.MCInumtxt.text
        object.fields["gender"] = primaryDetailsTVC?.gendertxt.text
        object.fields["experience"] = primaryDetailsTVC?.experiencetxt.text
        object.fields["specialisation"] = GlobalDocData.uploadspeciality
        object.fields["degree"] = primaryDetailsTVC?.textViewDegree.text
        object.fields["profileverified"] = 0
        GlobalDocData.gthefinalcalldoccode = (QBSession.current.currentUser?.customData) ?? ""
        // we are always sotring selected languages as comma seperated
        if ipad && maxLength == 1024 {
            let langstring = GlobalDocData.languagearrayipad.joined(separator: ",")  // we are making languages comma seperated here
            object.fields["languages"] = langstring
            object.fields["languagecounteripad"] = GlobalDocData.languagecounteripad
        } else {
            let langstring = GlobalDocData.languagearray.joined(separator: ",")   // we are making languages comma seperated here
            object.fields["languages"] = langstring
            object.fields["languagecounter"] = GlobalDocData.languagecounter
        }
        object.fields["address"] = primaryDetailsTVC?.textviewPracticeAddress.text
        object.fields["city"] = primaryDetailsTVC?.citytxt.text
        object.fields["state"] = primaryDetailsTVC?.statetxt.text
        object.fields["pincode"] = primaryDetailsTVC?.pincodetxt.text
        object.fields["profileUID"] =  GlobalDocData.gprofileURL
        object.fields["mciUID"] =  GlobalDocData.gMCIURL
        object.fields["signatureUID"] =  GlobalDocData.gsamplesignatureURL
        object.fields["calldoccode"] = GlobalDocData.gthefinalcalldoccode
        
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            // we are creating the record ID for doctors Primary details table , this record = customobjIDPrimarydetails will be used later to update record
            UserDefaults.standard.setValue((contributors?.id)!, forKey: "customobjIDPrimarydetails")
            UserDefaults.standard.synchronize()
            print("PrimaryDetailsDocViewController:CustomObject: Successfully created Primary Details !")
            // Now after Creating Primary details of the user at backend table, we need to check here if we have come to this new registration using calldoc code and also if that calldoc code is of Doctor or Patient
            if self.regCallDocCode.count == 6 { // here is valid calldoccode
                if self.CallDocTblName == "DocPrimaryDetails" {
                    self.handleInvitedByDoctorCallDocCode()
                } else if self.CallDocTblName == "PatPrimaryDetailsTable" {
                    self.handleInvitedByPatientCallDocCode()
                }
            } else {
                let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
                let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorDetailsBaseViewController") as! DoctorDetailsBaseViewController
                docDeatilsTVC.docspeciality = (self.primaryDetailsTVC?.specialisation.text)!
                self.present(docDeatilsTVC, animated: true, completion: nil)
            }
            
        }) { (response) in
            //Handle Error
            print("PrimaryDetailsDocViewController:CustomObject: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    // This function updates table of Backend DocPrimaryDetails table with the actuall data entered by user
    func  UpDateCustomObjectforDocPrimaryDetails() {
        let object = QBCOCustomObject()
        object.className = "DocPrimaryDetails"
        object.fields["fullname"] =  GlobalDocData.gdocname
        object.fields["email"] =  GlobalDocData.gdocemail
        object.fields["mcinumber"] = primaryDetailsTVC?.MCInumtxt.text
        object.fields["gender"] = primaryDetailsTVC?.gendertxt.text
        object.fields["experience"] = primaryDetailsTVC?.experiencetxt.text
        object.fields["specialisation"] = GlobalDocData.uploadspeciality
        object.fields["degree"] = primaryDetailsTVC?.textViewDegree.text
        GlobalDocData.gthefinalcalldoccode = (QBSession.current.currentUser?.customData) ?? ""
        // we are always sotring selected languages as comma seperated
        if ipad && maxLength == 1024 {
            let langstring = GlobalDocData.languagearrayipad.joined(separator: ",")  // we are making languages comma seperated here
            object.fields["languages"] = langstring
            object.fields["languagecounteripad"] = GlobalDocData.languagecounteripad
        } else {
            let langstring = GlobalDocData.languagearray.joined(separator: ",")   // we are making languages comma seperated here
            object.fields["languages"] = langstring
            object.fields["languagecounter"] = GlobalDocData.languagecounter
        }
        object.fields["address"] = primaryDetailsTVC?.textviewPracticeAddress.text
        object.fields["city"] = primaryDetailsTVC?.citytxt.text
        object.fields["state"] = primaryDetailsTVC?.statetxt.text
        object.fields["pincode"] = primaryDetailsTVC?.pincodetxt.text
        object.fields["profileUID"] =  GlobalDocData.gprofileURL
        object.fields["mciUID"] =  GlobalDocData.gMCIURL
        object.fields["signatureUID"] =  GlobalDocData.gsamplesignatureURL
        object.fields["calldoccode"] = GlobalDocData.gthefinalcalldoccode
        object.id = UserDefaults.standard.string(forKey: "customobjIDPrimarydetails") ?? ""      //record id of Doctor Primary details table
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("PrimaryDetailsDocViewController:CustomObject: Successfully Updated Primary Details !")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: true, completion: nil)
            
        }) { (response) in
            //Handle Error
            print("PrimaryDetailsDocViewController:CustomObject: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func ReadPrimaryDetailsFromServer() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("PrimaryDetailsDocViewController:ReadPrimaryDetailsFromServer(): Successfully  !")
            guard let contributors = contributors, contributors.count > 0 else {
                return
            }
            UserDefaults.standard.setValue(((contributors[0].id) ?? ""), forKey: "customobjIDPrimarydetails")
            UserDefaults.standard.synchronize()
            GlobalDocData.gdocname = (contributors[0].fields?.value(forKey: "fullname") as? String) ?? ""
            self.primaryDetailsTVC?.Emailaddtxt.text = contributors[0].fields?.value(forKey: "email") as? String
            GlobalDocData.gdocemail =  (contributors[0].fields?.value(forKey: "email") as? String) ?? ""
            self.primaryDetailsTVC?.MCInumtxt.text = contributors[0].fields?.value(forKey: "mcinumber") as? String
            GlobalDocData.gmcinum = (self.primaryDetailsTVC?.MCInumtxt.text) ?? ""
            self.primaryDetailsTVC?.gendertxt.text = contributors[0].fields?.value(forKey: "gender") as? String
            self.primaryDetailsTVC?.experiencetxt.text = contributors[0].fields?.value(forKey: "experience") as? String
            GlobalDocData.uploadspeciality = (contributors[0].fields?.value(forKey: "specialisation") as? String) ?? ""
            self.convertbacktodisplayspeciality(speciality: GlobalDocData.uploadspeciality)
            
            self.primaryDetailsTVC?.textViewDegree.text = contributors[0].fields?.value(forKey: "degree") as? String
            GlobalDocData.gdegree = (self.primaryDetailsTVC?.textViewDegree.text)!
            self.primaryDetailsTVC?.textviewPracticeAddress.text = contributors[0].fields?.value(forKey: "address") as? String
            self.primaryDetailsTVC?.citytxt.text = contributors[0].fields?.value(forKey: "city") as? String
            self.primaryDetailsTVC?.statetxt.text = contributors[0].fields?.value(forKey: "state") as? String
            let val =  contributors[0].fields?.value(forKey: "pincode") as? Int
            self.primaryDetailsTVC?.pincodetxt.text = String(val ?? 0)
            
            //profile image update
            self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = (self.primaryDetailsTVC?.topView?.imageViewProfileIcon.frame.size.width)! / 2;
            self.primaryDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true;
            self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 0.2
            self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.lightGray.cgColor
            
            GlobalDocData.gprofileURL = (contributors[0].fields?.value(forKey: "profileUID") as? String) ?? ""
            self.LoadProfileImage()
            
            GlobalDocData.gMCIURL = (contributors[0].fields?.value(forKey: "mciUID") as? String) ?? ""
            let defaults: UserDefaults? = UserDefaults.standard
            defaults?.set(URL(string: GlobalDocData.gMCIURL), forKey: "keyforMCIcertificateurl")
            defaults?.synchronize()
            self.primaryDetailsTVC?.MCIUploadimg.sd_setImage(with: URL(string: GlobalDocData.gMCIURL), placeholderImage: UIImage(named: "certificateimg"), options: .cacheMemoryOnly)
            
            GlobalDocData.gsamplesignatureURL = (contributors[0].fields?.value(forKey: "signatureUID") as? String) ?? ""
            defaults?.set(URL(string:  GlobalDocData.gsamplesignatureURL), forKey: "keyforsamplesignatureurl")
            defaults?.synchronize()
            
            GlobalDocData.gthefinalcalldoccode = (contributors[0].fields?.value(forKey: "calldoccode") as? String) ?? ""
            
            let languagestring = contributors[0].fields?.value(forKey: "languages") as? String
            let languagearray = languagestring?.components(separatedBy: ",")
            
            if ipad && maxLength == 1024 {
                GlobalDocData.languagecounteripad = (contributors[0].fields?.value(forKey: "languagecounteripad") as? Int8) ?? (0 as Int8)
                GlobalDocData.languagearrayipad = languagearray ?? []
            } else {
                GlobalDocData.languagearray = languagearray ?? []
                GlobalDocData.languagecounter = (contributors[0].fields?.value(forKey: "languagecounter") as? Int8) ?? (0 as Int8)
            }
            
            print("PrimaryDetailsDocViewController:CustomObject: Successfully Read Primary Details from Server !")
            
            self.UpdateLanguageGrid()
            
            if self.primaryDetailsTVC?.pincodetxt.text == "0" {
                self.primaryDetailsTVC?.cityView.isHidden = true
            } else {
                self.primaryDetailsTVC?.cityView.isHidden = false
            }
            
            if self.primaryDetailsTVC?.pincodetxt.text == "0" {
                self.primaryDetailsTVC?.stateView.isHidden = true
            } else {
                self.primaryDetailsTVC?.stateView.isHidden = false
            }
        }) { (errorresponse) in
            print("PrimaryDetailsDocViewController:CustomObject: ReadPrimaryDetailsFromServer() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func convertbacktodisplayspeciality(speciality: String) {
        var iitem = speciality
        GlobalDocData.gdocspeciality = iitem // this is used in rest of app logic as value same as what is stored in backend
        if iitem == "ent"{
            iitem = "E.N.T"
        } else if iitem == "general" {
            iitem = "G.P."
        } else if iitem == "altmedcine" {
            iitem = "Alt Medicine"
        } else if iitem == "cardiologist" {
            iitem = "Cardiologistc"
        } else if iitem == "dermatologist" {
            iitem = "Dermatologist"
        } else if iitem == "dietician" {
            iitem = "Dietician"
        } else if iitem == "endocrinologist" {
            iitem = "Endocrinologist"
        } else if iitem == "gastroentrologist" {
            iitem = "Gastroentrologist"
        } else if iitem == "gynaecologist" {
            iitem = "Gynaecologist"
        } else if iitem == "nephrologist" {
            iitem = "Nephrologist"
        } else if iitem == "neurologist" {
            iitem = "Neurologist"
        } else if iitem == "oncologist" {
            iitem = "Oncologist"
        } else if iitem == "orthopaedic" {
            iitem = "Orthopaedic"
        } else if iitem == "paediatrician" {
            iitem = "Paediatrician"
        } else if iitem == "psychiatrist" {
            iitem = "Psychiatrist"
        } else if iitem == "pulmonoligist" {
            iitem = "Pulmonoligist"
        } else if iitem == "sexologist" {
            iitem = "Sexologist"
        }

        GlobalDocData.displayspeciality = iitem

        self.primaryDetailsTVC?.specialisation.text = GlobalDocData.displayspeciality

    }
    
    func CreateNewEntrytoDocCountTable() {
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        object.fields["dermatologist"] = 0
        object.fields["dietician"] = 0
        object.fields["cardiologist"] = 0
        object.fields["ent"] = 0
        object.fields["general"] = 0
        object.fields["endocrinologist"] = 0
        object.fields["gynaecologist"] = 0
        object.fields["nephrologist"] = 0
        object.fields["neurologist"] = 0
        object.fields["oncologist"] = 0
        object.fields["gastroentrologist"] = 0
        object.fields["paediatrician"] = 0
        object.fields["psychiatrist"] = 0
        object.fields["orthopaedic"] = 0
        object.fields["sexologist"] = 0
        object.fields["altmedicine"] = 0
        object.fields["pulmonoligist"] = 0
        object.fields["myTeam"] = 0
        object.fields["teamStatus"] = "false"
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            UserDefaults.standard.setValue((contributors?.id)!, forKey: "RecordIDDocCount")
            UserDefaults.standard.synchronize()
        }) { (response) in
            print("PrimaryDetailsDocViewController: CreateNewEntrytoDocCountTable(): Response error: \(String(describing: response.error?.description))")
        }
    }

    @IBAction func ProfileImgButtonClicked(_ sender: UIButton) {

        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        profileimgbool = true
        let actionSheet = UIAlertController(title: "Photo Source", message: "choose a Source", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)}))

        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action:UIAlertAction) in imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)}))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        //for ipad related crash issue fix
        if ipad && maxLength == 1024 {
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        self.present(actionSheet, animated: true, completion: nil)
    }

    @IBAction func MCIButtonClicked(_ sender: UIButton) {
        print("MCI Button Pressed")
        if (primaryDetailsTVC?.MCInumtxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Valid MCI Number!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }

        if (primaryDetailsTVC?.MCInumtxt.text?.count)! >= 5 {
            let alert = UIAlertController(title: "Alert", message: "You Agree to Upload your MCI Certificate. Your MCI Certificate can be viewed by Patients from your Profile Page!!", preferredStyle: .alert)
            let NO = UIAlertAction(title: "NO", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here

            })
            let YES = UIAlertAction(title: "YES", style: .default, handler: {(_ action: UIAlertAction) -> Void in

                self.mcicertibool = true
                self.PresenttheImagepickerforMCI()
            })
            alert.addAction(YES)
            alert.addAction(NO)
            present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Alert", message: "Please Enter MCI Number greater then 5 digits!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)

        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    func PresenttheImagepickerforMCI() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        let actionSheet = UIAlertController(title: "Photo Source", message: "choose a Source", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)}))

        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action:UIAlertAction) in imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)}))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        //for ipad related crash issue fix
        if ipad && maxLength == 1024 {
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }

        self.present(actionSheet, animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let selectedimage = info[UIImagePickerControllerOriginalImage] as? UIImage
        if profileimgbool {
            self.profileimgbool = false
            let rzimage = resizeImage(image: selectedimage!, newWidth:150)
            let imageData: NSData = UIImagePNGRepresentation(rzimage)! as NSData
            QBRequest.tUploadFile(imageData as Data, fileName: "ProfileImageofDoc", contentType: "image/png", isPublic: true, successBlock:
                {(response: QBResponse!, uploadedBlob: QBCBlob!) in
                    let userParams = QBUpdateUserParameters()
                    userParams.blobID = uploadedBlob.id
                    userParams.tags = ["Doc0", "IOS"]
                    QBRequest.updateCurrentUser(userParams, successBlock:
                        {(_ response: QBResponse, _ user: QBUUser?) -> Void in
                            print("profile image Updated parameters and status is: \(String(describing: response))")
                    }, errorBlock: {(_ response: QBResponse) -> Void in
                            print("profile imageUpdated parameters Error Response is: \(String(describing: response))")
                    })
                    let url: String = uploadedBlob.publicUrl()!
                    GlobalDocData.gprofileURL  = url
                    SDImageCache.shared().store(selectedimage, forKey: url, completion: {
                    })
                    self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = (self.primaryDetailsTVC?.topView?.imageViewProfileIcon.frame.size.width)! / 2
                    self.primaryDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true;
                    self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 0.2
                    self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.lightGray.cgColor

                    self.primaryDetailsTVC?.topView?.imageViewProfileIcon.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "profile"))
                    // make profile image url persistance
                    let defaults: UserDefaults? = UserDefaults.standard
                    defaults?.set(URL(string: url), forKey: "keyforProfileurl")
                    defaults?.synchronize()
            }, statusBlock: {(request: QBRequest?, status: QBRequestStatus?) in
                                    DispatchQueue.main.async {
                                            print(" ProfileImageButton() percent = \(String(describing: status?.percentOfCompletion))")
                                            if (status!.percentOfCompletion >= 0.5) {
                                                self.primaryDetailsTVC?.topView?.activityIndicatordocimg.isHidden = true
                                                self.primaryDetailsTVC?.topView?.activityIndicatordocimg.stopAnimating()
                                            }
                                    }
            }, errorBlock: {(response: QBResponse!) in
                    self.primaryDetailsTVC?.topView?.activityIndicatordocimg.isHidden = true
                    self.primaryDetailsTVC?.topView?.activityIndicatordocimg.stopAnimating()
            })

        }
        if mcicertibool {
            userHasChosenMCIcerti(image: selectedimage!)
        }
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func userHasChosenMCIcerti(image: UIImage) {
        if mcicertibool {
            mcicertibool = false
            let rzimage = resizeImage(image: image, newWidth:480)
            let imageData: NSData = UIImagePNGRepresentation(rzimage)! as NSData
            let filename = "MCI_" + GlobalDocData.gdocname
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.MCInumtxt.text, forKey: "mcinumber")
            UserDefaults.standard.synchronize()
            self.primaryDetailsTVC?.activityIndicationMCIimg.isHidden = false
            self.primaryDetailsTVC?.activityIndicationMCIimg.startAnimating()
            QBRequest.tUploadFile(imageData as Data, fileName: filename, contentType: "image/png", isPublic: true, successBlock: {(response: QBResponse!, uploadedBlob: QBCBlob!) in
                let userParams = QBUpdateUserParameters()
                userParams.blobID = uploadedBlob.id
                userParams.tags = ["Doc0", "IOS"]
                QBRequest.updateCurrentUser(userParams, successBlock: {(_ response: QBResponse, _ user: QBUUser?) -> Void in
                    print("MCI Certificate image Updated parameters and status is: \(String(describing: response))")
                    self.primaryDetailsTVC?.MCIUploadimg.isHidden = false
                    self.primaryDetailsTVC?.MCIUploadimg.alpha = 1.0
                    self.primaryDetailsTVC?.activityIndicationMCIimg.isHidden = true
                    self.primaryDetailsTVC?.activityIndicationMCIimg.stopAnimating()
                }, errorBlock: {(_ response: QBResponse) -> Void in
                    print("MCI Certificate image Updated parameters Error Response is: \(String(describing: response))")
                })
                let url: String = uploadedBlob.publicUrl()!
                GlobalDocData.gMCIURL  = url
                SDImageCache.shared().store(image, forKey: url, completion: {
                })
                self.primaryDetailsTVC?.MCIUploadimg.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "certificateimg@2x.png"))
                // make MCI image url persistance
                let defaults: UserDefaults? = UserDefaults.standard
                defaults?.set(URL(string: url), forKey: "keyforMCIcertificateurl")
                defaults?.synchronize()
                // Status
            }, statusBlock: {(request: QBRequest?, status: QBRequestStatus?) in
                DispatchQueue.main.async {
                        print("userHasChosenMCIcerti() percent = \(String(describing: status!.percentOfCompletion))")
                        if (status!.percentOfCompletion >= 0.5) {
                            print("userHasChosenMCIcerti() percent become 1.0")
                            self.primaryDetailsTVC?.MCIUploadimg.alpha = 1.0
                            self.primaryDetailsTVC?.MCIUploadimg.isHidden = false
                            self.primaryDetailsTVC?.activityIndicationMCIimg.isHidden = true
                            self.primaryDetailsTVC?.activityIndicationMCIimg.stopAnimating()
                        }
                }
            }, errorBlock: {(response: QBResponse!) in
                // NSLog("error: %@", response.error)
                print("MCI Certificate  QBRequest tUploadFile response Error \(String(describing: response))")
            })
        }
    }

    @IBAction func SingnatureButtonClicked(_ sender: UIButton) {
        signaturebool = true
        self.primaryDetailsTVC?.SignatureImageView.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.signaturebackgroundview.isHidden = false
            self.opacityview.isHidden = false
        }
        signherelb.isHidden = false
        Okbuttonview.isHidden = true
    }

    @IBAction func SignatureClearButtonClicked(_ sender: UIButton) {
        print("DoctorPrimaryDetailsViewController:SignatureCleared()")
        SignatureView.clear()
        signherelb.isHidden = true
    }

    @IBAction func SignatureCancelButtonClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            self.signaturebackgroundview.isHidden = true
            self.opacityview.isHidden = true
        }
    }


    @IBAction func SignatureDoneButtonClicked(_ sender: UIButton) {
        signherelb.isHidden = false
        Okbuttonview.isHidden = false

        if SignatureView.signature != nil {
            self.primaryDetailsTVC?.SignatureViewDone.backgroundColor = UIColor(patternImage: resizeImage(image:self.SignatureView.signature!, newWidth:125))
            UIView.animate(withDuration: 0.3) {
                self.signaturebackgroundview.isHidden = true
                self.opacityview.isHidden = true
            }
            // this is to upload image to backend Server
            signaturebool = true
            userHasChosensignature(image: resizeImage(image:self.SignatureView.signature!, newWidth:125))
        } else {
            self.primaryDetailsTVC?.SignatureViewDone.backgroundColor = UIColor(patternImage: UIImage(named: "Signature")!)
            UIView.animate(withDuration: 0.3) {
                self.signaturebackgroundview.isHidden = true
                self.opacityview.isHidden = true
            }
        }
    }

    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {

        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale

        if ipad && maxLength == 1024 {
            UIGraphicsBeginImageContext(CGSize(width: newWidth, height: 60)) //for ipad
            image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: 60))
        } else {
            UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
            image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        }

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }

    // Uploading Signature to Server
    func userHasChosensignature(image: UIImage) {
        if signaturebool {
            self.signaturebool = false
            self.primaryDetailsTVC?.activityIndicationsignatureimg.isHidden = false
            self.primaryDetailsTVC?.activityIndicationsignatureimg.startAnimating()
            let sigfilename = "Signature_" + GlobalDocData.gdocname
            let imageData: NSData = UIImagePNGRepresentation(image)! as NSData
            QBRequest.tUploadFile(imageData as Data, fileName: sigfilename, contentType: "image/png", isPublic: true, successBlock: {(response: QBResponse!, uploadedBlob: QBCBlob!) in
                let userParams = QBUpdateUserParameters()
                userParams.blobID = uploadedBlob.id
                userParams.tags = ["Doc0", "IOS"]
                QBRequest.updateCurrentUser(userParams, successBlock: {(_ response: QBResponse, _ user: QBUUser?) -> Void in
                    print("DocSampleSignature image Updated parameters and status is: \(String(describing: response))")
                    self.primaryDetailsTVC?.activityIndicationsignatureimg.isHidden = true
                    self.primaryDetailsTVC?.activityIndicationsignatureimg.stopAnimating()
                }, errorBlock: {(_ response: QBResponse) -> Void in
                    print("DocSampleSignature imageUpdated parameters Error Response is: \(String(describing: response))")
                })
                let url: String = uploadedBlob.publicUrl()!
                
                self.primaryDetailsTVC?.SignatureImageView.isHidden = false
                self.primaryDetailsTVC?.SignatureImageView.sd_setImage(with: URL(string: url), placeholderImage: nil, options: .cacheMemoryOnly)

                
                GlobalDocData.gsamplesignatureURL = url
                SDImageCache.shared().store(image, forKey: url, completion: {
                })
                // make profile image url persistance
                let defaults: UserDefaults? = UserDefaults.standard
                defaults?.set(URL(string: url), forKey: "keyforsamplesignatureurl")
                defaults?.synchronize()
                // Status
            }, statusBlock: {(request: QBRequest?, status: QBRequestStatus?) in
                DispatchQueue.main.async {
                        print("DocSampleSignature upload percent = \(String(describing: status?.percentOfCompletion))")
                        if (status!.percentOfCompletion > 0.5) {
                            print("DocSampleSignature image Uploaded 100 %")
                            self.primaryDetailsTVC?.activityIndicationsignatureimg.isHidden = true
                            self.primaryDetailsTVC?.activityIndicationsignatureimg.stopAnimating()
                        }
                }
            }, errorBlock: {(response: QBResponse!) in
                // NSLog("error: %@", response.error)
                print("DocSampleSignature image QBRequest tUploadFile response Error \(String(describing: response))")
            })
        }
    }

    enum languages: String {
        case english = "English"
        case hindi = "Hindi"
        case urdu = "Urdu"
        case tamil = "Tamil"
        case kannada = "Kannada"
        case telugu = "Telugu"
        case malayalam = "Malayalam"
        case panjabi = "Punjabi"
        case gujarati = "Gujarati"
        case bengali = "Bengali"
        case marathi = "Marathi"
        case assamese = "Assamese"
        case oriya = "Oriya"
    }

    @IBAction func LanguageButtonTapped(_ sender:UIButton) {

        guard let title = sender.currentTitle, let language = languages(rawValue: title) else {
            return
        }

        switch language {
        case .english:

            self.EngbuttonSelected = !self.EngbuttonSelected

            if EngbuttonSelected {
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }

                setSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[0] = "English"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "EngbuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[0] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "EngbuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .hindi:

            self.HindibuttonSelected = !self.HindibuttonSelected

            if HindibuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }

                GlobalDocData.languagearray[1] = "Hindi"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "HindibuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[1] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "HindibuttonSelected")
                UserDefaults.standard.synchronize()
            }

            break

        case .urdu:
            self.UrdubuttonSelected = !self.UrdubuttonSelected

            if UrdubuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[2] = "Urdu"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "UrdubuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[2] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "UrdubuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .tamil:
            self.TamilbuttonSelected = !self.TamilbuttonSelected

            if TamilbuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[3] = "Tamil"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "TamilbuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[3] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "TamilbuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .kannada:
            self.KannadabuttonSelected = !self.KannadabuttonSelected

            if KannadabuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[4] = "Kannada"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "KannadabuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[4] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "KannadabuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .telugu:
            self.TelugubuttonSelected = !self.TelugubuttonSelected

            if TelugubuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[5] = "Telugu"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "TelugubuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[5] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "TelugubuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .malayalam:
            self.MalaybuttonSelected = !self.MalaybuttonSelected

            if MalaybuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[6] = "Malayalam"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "MalaybuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[6] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "MalaybuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .panjabi:
            self.PanjbuttonSelected = !self.PanjbuttonSelected

            if PanjbuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[7] = "Panjabi"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "PanjbuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[7] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "PanjbuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .gujarati:
            self.GujbuttonSelected = !self.GujbuttonSelected

            if GujbuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[8] = "Gujarati"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "GujbuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[8] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "GujbuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .bengali:
            self.BengalibuttonSelected = !self.BengalibuttonSelected

            if BengalibuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[9] = "Bangali"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "BengalibuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[9] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "BengalibuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .assamese:
            self.AssambuttonSelected = !self.AssambuttonSelected

            if AssambuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[10] = "Assamese"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "AssambuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[10] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "AssambuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .oriya:
            self.OriyabuttonSelected = !self.OriyabuttonSelected

            if OriyabuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[11] = "Oriya"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "OriyabuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[11] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "OriyabuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break

        case .marathi:
            self.MarathibuttonSelected = !self.MarathibuttonSelected

            if MarathibuttonSelected {
                setSelectedLanguage(btn: sender)
                if GlobalDocData.languagearray.isEmpty {
                    GlobalDocData.languagearray = [ "", "", "", "","","","","","","","","",""]
                }
                GlobalDocData.languagearray[12] = "Marathi"
                GlobalDocData.languagecounter += 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(true, forKey: "MarathibuttonSelected")
                UserDefaults.standard.synchronize()
            } else {
                setUnSelectedLanguage(btn: sender)
                GlobalDocData.languagearray[12] = ""
                GlobalDocData.languagecounter -= 1
                UserDefaults.standard.set(GlobalDocData.languagearray, forKey: "languageArray")
                UserDefaults.standard.set(GlobalDocData.languagecounter, forKey: "languagecounter")
                UserDefaults.standard.set(false, forKey: "MarathibuttonSelected")
                UserDefaults.standard.synchronize()
            }
            break
        }
    }
    
    @IBAction func iButtonTapped(_ sender:UIButton) {
        if isiBtn {
            primaryDetailsTVC?.signinfoimgview.isHidden = false
            isiBtn = false
        } else {
            primaryDetailsTVC?.signinfoimgview.isHidden = true
            isiBtn = true
        }
        
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PrimaryDetailsDocTableViewControllerSegue" {
            self.primaryDetailsTVC = segue.destination as? PrimaryDetailsDocTableViewController
        }
    }


    func firsttimeuserdataUpdate() {

        UserDefaults.standard.setValue("", forKey: "mcinumber")
        UserDefaults.standard.setValue("", forKey: "gender")
        UserDefaults.standard.setValue("", forKey: "age")
        UserDefaults.standard.setValue("", forKey: "specialisation")
        UserDefaults.standard.setValue("", forKey: "degree")
        UserDefaults.standard.setValue("", forKey: "languages")
        UserDefaults.standard.setValue("", forKey: "address")
        UserDefaults.standard.setValue("", forKey: "address2")
        UserDefaults.standard.setValue("", forKey: "city")
        UserDefaults.standard.setValue("", forKey: "state")
        UserDefaults.standard.setValue("", forKey: "pincode")
        UserDefaults.standard.set(0, forKey: "charge")   // setting here int 0 to consider full charge for fees
        UserDefaults.standard.set(false, forKey: "language")

        UserDefaults.standard.set(false, forKey: "teamswitch")  //setting my team switch off at the begning
        UserDefaults.standard.set(false, forKey: Constants.KEY_SCHEDULE_ON_OFF) // this is to control Scheduler on off

        UserDefaults.standard.set(false, forKey: "EngbuttonSelected")
        UserDefaults.standard.set(false, forKey: "HindibuttonSelected")
        UserDefaults.standard.set(false, forKey: "TamilbuttonSelected")
        UserDefaults.standard.set(false, forKey: "TelugubuttonSelected")
        UserDefaults.standard.set(false, forKey: "UrdubuttonSelected")
        UserDefaults.standard.set(false, forKey: "KannadabuttonSelected")
        UserDefaults.standard.set(false, forKey: "MalaybuttonSelected")
        UserDefaults.standard.set(false, forKey: "AssambuttonSelected")
        UserDefaults.standard.set(false, forKey: "OriyabuttonSelected")
        UserDefaults.standard.set(false, forKey: "GujbuttonSelected")
        UserDefaults.standard.set(false, forKey: "BengalibuttonSelected")
        UserDefaults.standard.set(false, forKey: "PanjbuttonSelected")
        UserDefaults.standard.set(false, forKey: "MarathibuttonSelected")

        UserDefaults.standard.set(0, forKey: "languagecounter")
        UserDefaults.standard.set(0, forKey: "languagecounteripad")

        let langarray = [ "", "", "", "","","","","","","","","",""]
        UserDefaults.standard.set(langarray, forKey: "languageArray")
        UserDefaults.standard.set(langarray, forKey: "languageArrayipad")

        // Speciality related
        UserDefaults.standard.set(false, forKey: "cardiologyselected")
        UserDefaults.standard.set(false, forKey: "dermatologyselected")
        UserDefaults.standard.set(false, forKey: "generalmedselected")
        UserDefaults.standard.set(false, forKey: "pediatricianselected")
        UserDefaults.standard.set(false, forKey: "dieteticsselected")
        UserDefaults.standard.set(false, forKey: "gynecologyoselected")
        UserDefaults.standard.set(false, forKey: "psychiatryselected")
        UserDefaults.standard.set(false, forKey: "neurologyselected")
        UserDefaults.standard.set(false, forKey: "gestrologyselected")
        UserDefaults.standard.set(false, forKey: "orthopedicsselected")
        UserDefaults.standard.set(false, forKey: "urologyselected")
        UserDefaults.standard.set(false, forKey: "pulmulonogyselected")
        UserDefaults.standard.set(false, forKey: "endocrinologyselected")
        UserDefaults.standard.set(false, forKey: "alternatemedselected")
        UserDefaults.standard.set(false, forKey: "entmedselected")
        UserDefaults.standard.set(false, forKey: "oncologmedselected")
        UserDefaults.standard.set(false, forKey: "sexologymedselected")
        UserDefaults.standard.synchronize()

        // for ther first time we are going to hide the city and state fields
        self.primaryDetailsTVC?.cityView.isHidden = true
        self.primaryDetailsTVC?.stateView.isHidden = true

        primaryDetailsTVC?.topView?.profileimgButtonview.isEnabled = true
    }

    func existinguserdataUpdate(objectarray: [Any]) {
        print("PrimaryDetailsDocViewController: existinguserdataUpdate()")
    }

    func firsttimeuserdataUpdateforExistingUser() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                //
                UserDefaults.standard.setValue((contributors![0].id)!, forKey: "customobjIDPrimarydetails")
                self.primaryDetailsTVC?.Emailaddtxt.text =  contributors![0].fields?.value(forKey: "email") as? String
                self.primaryDetailsTVC?.MCInumtxt.text = contributors![0].fields?.value(forKey: "mcinumber") as? String
                self.primaryDetailsTVC?.gendertxt.text = contributors![0].fields?.value(forKey: "gender") as? String
                self.primaryDetailsTVC?.experiencetxt.text = contributors![0].fields?.value(forKey: "experience") as? String

                GlobalDocData.uploadspeciality = (contributors![0].fields?.value(forKey: "specialisation") as? String)!
                self.convertbacktodisplayspeciality(speciality: GlobalDocData.uploadspeciality)

                self.primaryDetailsTVC?.textViewDegree.text = contributors![0].fields?.value(forKey: "degree") as? String
                self.primaryDetailsTVC?.textviewPracticeAddress.text = contributors![0].fields?.value(forKey: "address") as? String
                self.primaryDetailsTVC?.citytxt.text = contributors![0].fields?.value(forKey: "city") as? String
                self.primaryDetailsTVC?.statetxt.text = contributors![0].fields?.value(forKey: "state") as? String
                let val =  contributors![0].fields?.value(forKey: "pincode") as? Int
                self.primaryDetailsTVC?.pincodetxt.text = String(val ?? 0)

                //profile image update
                self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = (self.primaryDetailsTVC?.topView?.imageViewProfileIcon.frame.size.width)! / 2;
                self.primaryDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true;
                self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 0.2
                self.primaryDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.lightGray.cgColor

                GlobalDocData.gprofileURL = (contributors![0].fields?.value(forKey: "profileUID") as? String)!
                self.LoadProfileImage()

                GlobalDocData.gMCIURL = (contributors![0].fields?.value(forKey: "mciUID") as? String)!
                self.primaryDetailsTVC?.MCIUploadimg.sd_setImage(with: URL(string: GlobalDocData.gMCIURL), placeholderImage: UIImage(named: "certificateimg"))

                GlobalDocData.gsamplesignatureURL = (contributors![0].fields?.value(forKey: "signatureUID") as? String)!
                self.primaryDetailsTVC?.SignatureImageView.sd_setImage(with: URL(string: GlobalDocData.gsamplesignatureURL), placeholderImage: UIImage(named: "Signature"))

                let languagestring = contributors![0].fields?.value(forKey: "languages") as? String
                let languagearray = languagestring?.components(separatedBy: ",")

                if ipad && maxLength == 1024 {
                    GlobalDocData.languagecounteripad = (contributors![0].fields?.value(forKey: "languagecounteripad") as? Int8) ?? 0
                    GlobalDocData.languagearrayipad = languagearray ?? []
                } else {
                    GlobalDocData.languagearray = languagearray ?? []
                    GlobalDocData.languagecounter = (contributors![0].fields?.value(forKey: "languagecounter") as? Int8) ?? 0
                }

                self.UpdateLanguageGrid()

                if self.primaryDetailsTVC?.pincodetxt.text?.isEmpty ?? true {
                    self.primaryDetailsTVC?.cityView.isHidden = true
                    self.primaryDetailsTVC?.stateView.isHidden = true
                } else {
                    self.primaryDetailsTVC?.cityView.isHidden = false
                    self.primaryDetailsTVC?.stateView.isHidden = false
                }
                print("PrimaryDetailsDocViewController:firsttimeuserdataUpdateforExistingUser():Successfully Read Primary Details from Server !")
                GlobalDocData.gdocname =  (QBSession.current.currentUser?.fullName)!
                self.primaryDetailsTVC?.topView?.labelUserName.text = "Dr. " + GlobalDocData.gdocname
                self.primaryDetailsTVC?.topView?.labelUserRpleType.text = GlobalDocData.gdocspeciality
            }
        }) { (errorresponse) in
            print("DocTrustedNetworkViewController:firsttimeuserdataUpdateforExistingUser() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func setSelectedLanguage(btn: UIButton) {
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
    }

    func setUnSelectedLanguage(btn: UIButton) {
        btn.setTitleColor(UIColor.darkGray, for: .normal)
        btn.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
    }

    
    func fetchpincodebaseddata(pincode: String) {
        print("PrimaryDetailsViewDocController:fetchpincodebaseddata()pincode:\(pincode)")
        primaryDetailsTVC?.citystateactivityIndicator.isHidden = false
        primaryDetailsTVC?.citystateactivityIndicator.startAnimating()
        let location: String = pincode
        let geocoder: CLGeocoder = CLGeocoder()
        geocoder.geocodeAddressString(location, completionHandler: {(placemarks: [CLPlacemark]?, error: Error?) -> Void in
            if (placemarks == nil) {
                print("PrimaryDetailsViewDocController:fetchpincodebaseddata()= returned NIL : Not a valid pincode")
                let alert = UIAlertController(title: "Alert", message: "Pincode entered not valid !", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    self.primaryDetailsTVC?.pincodetxt.text = ""
                    self.primaryDetailsTVC?.citytxt.text = ""
                    self.primaryDetailsTVC?.statetxt.text  = ""
                    self.primaryDetailsTVC?.citystateactivityIndicator.isHidden = true
                    self.primaryDetailsTVC?.citystateactivityIndicator.stopAnimating()
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
                return
            }
            if ((placemarks?.count)! > 0) {
                let placemark: CLPlacemark = (placemarks?[0])!
                let country : String = placemark.country!
                let state: String = placemark.administrativeArea!
                let city = placemark.locality!
                let description = placemark.description
                print(state)
                print(city)
                print(country)
                print(description)
                self.primaryDetailsTVC?.citystateactivityIndicator.isHidden = true
                self.primaryDetailsTVC?.citystateactivityIndicator.stopAnimating()
                self.primaryDetailsTVC?.citytxt.text = city
                self.primaryDetailsTVC?.statetxt.text = state
                self.primaryDetailsTVC?.citytxt.isHidden = false
                self.primaryDetailsTVC?.statetxt.isHidden = false
                
                self.primaryDetailsTVC?.cityView.isHidden = false
                self.primaryDetailsTVC?.stateView.isHidden = false
            } else {
                self.primaryDetailsTVC?.citystateactivityIndicator.isHidden = true
                self.primaryDetailsTVC?.citystateactivityIndicator.stopAnimating()
                self.primaryDetailsTVC?.pincodetxt.text = ""
                self.primaryDetailsTVC?.citytxt.text = ""
                self.primaryDetailsTVC?.statetxt.text  = ""
            }
        } )
    }
    
    func updateUserEmail() {
        let updateParameters = QBUpdateUserParameters()
        updateParameters.fullName = GlobalDocData.gdocname
        updateParameters.phone = GlobalDocData.gdocmobile
        updateParameters.email = GlobalDocData.gdocemail
        
        QBRequest.updateCurrentUser(updateParameters, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
            // User updated successfully
        }, errorBlock: {(_ response: QBResponse) -> Void in
            // Handle error
        })
    }
    
    func handleInvitedByPatientCallDocCode() {
        // first lets read PatPrimaryDetailsTable using using calldoccode
        let getRequest = NSMutableDictionary()
        getRequest["calldoccode"] = self.regCallDocCode
        QBRequest.objects(withClassName: "PatPrimaryDetailsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            let patientuserid = contributors![0].userID
            let doctoruserid = QBSession.current.currentUser?.id
            let doctorfullname = QBSession.current.currentUser?.fullName
            let docspeciality =  GlobalDocData.uploadspeciality
            let docprofileimageURL = GlobalDocData.gprofileURL
            self.makeEntrytoRelDocPatTable(patientuserid: patientuserid, doctoruserid: doctoruserid!, doctorfullname: doctorfullname!,  docspeciality: docspeciality, docprofileimageURL: docprofileimageURL  )
            
        }) { (errorresponse) in
            print("PrimaryDetailsDocViewController:CustomObject: handleInvitedByPatientCallDocCode() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func makeEntrytoRelDocPatTable(patientuserid: UInt, doctoruserid: UInt, doctorfullname: String, docspeciality: String, docprofileimageURL: String) {
        // add new entry to RelDOcPat Table
        let object = QBCOCustomObject()
        object.className = "RelDocPat"
        object.fields["patientuserid"] = patientuserid
        object.fields["doctoruserid"] = doctoruserid
        object.fields["doctorfullname"] = doctorfullname
        object.fields["doctorspeciality"] = docspeciality
        object.fields["doctorprofileimageURL"] = docprofileimageURL
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("PrimaryDetailsDocViewController: makeEntrytoRelDocPatTable(): Successfully created")
            let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
            let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorDetailsBaseViewController") as! DoctorDetailsBaseViewController
            docDeatilsTVC.docspeciality = (self.primaryDetailsTVC?.specialisation.text)!
            self.present(docDeatilsTVC, animated: true, completion: nil)
        }) { (response) in
            //Handle Error
            print("PrimaryDetailsDocViewController: makeEntrytoRelDocPatTable(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func handleInvitedByDoctorCallDocCode() {
        // Here now we know what we came because of at the time of Registration user has putted in callcode of another doctor
        // lets use that calldoc code to get who is that doctor who has invited this doctor
        let getRequest = NSMutableDictionary()
        getRequest["calldoccode"] = self.regCallDocCode
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("PrimaryDetailsDocViewController:handleInvitedByDoctorCallDocCode():DocPrimaryDetails Successfully  !")
            let invitorspecilisation = (contributors![0].fields?.value(forKey: "specialisation") as? String)!
            let invitoruserid = contributors![0].userID
            let invitorprofileURL = (contributors![0].fields?.value(forKey: "profileUID") as? String)!
            let invitorfullname = (contributors![0].fields?.value(forKey: "fullname") as? String)!
            if (invitorspecilisation ==  GlobalDocData.uploadspeciality) {
                // same Speciality hence this registering doctor should show up in Team view of the invitor doctor
                // we need to do few things here, first check
                // 1. DocCountTable check using invitor userid what is the MyTeam count value, it should be less than 6 otherwise we can not
                // add doctor to invitor doc team
                let getRequest = NSMutableDictionary()
                getRequest["user_id"] = invitoruserid
                QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
                    //Handle Success
                    print("PrimaryDetailsDocViewController:handleInvitedByDoctorCallDocCode(): reading DocCountTable Successfully  !")
                    let teamcount = (contributors![0].fields?.value(forKey: "myTeam") as? Int)!
                    if teamcount >= 6 {
                        // registering user can not be added to Invitor Team
                        print("PrimaryDetailsDocViewController:handleInvitedByDoctorCallDocCode(): registering user can not be added to Invitor TEAM because there are already 6 team members ")
                        let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
                        let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorDetailsBaseViewController") as! DoctorDetailsBaseViewController
                        docDeatilsTVC.docspeciality = (self.primaryDetailsTVC?.specialisation.text)!
                        self.present(docDeatilsTVC, animated: true, completion: nil)
                    } else {
                        // as myTeam count is less than 6 hence we can add registering user to Invitor team
                        // 1. make new entry to RelDocTeam
                        // 2. increment DocCountTable myTeam using Invitor userid
                        // 3. update Invite Table entry : first read invite table using invitee as d_mobilenumber and callcode , this may return a record if Invitor had made entry in invite table while doing invite as team invite, if there is no entry in invite table for this read operation then also we have actually added this doctor to RelDocTeam anyways
                        self.MakeNewEntrytoRelDocTeam(invitordocuserid: invitoruserid, inviteedocuserid: (QBSession.current.currentUser?.id)!, inviteedocfullname: (QBSession.current.currentUser?.fullName)!, inviteeprofileURL: GlobalDocData.gprofileURL, status: false)
                    }
                    
                }) { (errorresponse) in
                    print("PrimaryDetailsDocViewController:CustomObject: handleInvitedByPatientCallDocCode()-DocCountTable Response error: \(String(describing: errorresponse.error?.description))")
                }
            } else {
                // Now we no that this has to be COT related work , we need to do few things first here
                // 1. DocCountTable check using invitor userid what is the Registering user speciality count value, it should be less than 6 otherwise we can not add this registering user to Invitor Doc COT of that speciality
                let getRequest = NSMutableDictionary()
                getRequest["user_id"] = invitoruserid
                QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
                    //Handle Success
                    print("PrimaryDetailsDocViewController:handleInvitedByDoctorCallDocCode(): reading DocCountTable Successfully  !")
                    let specialitycount = (contributors![0].fields?.value(forKey: GlobalDocData.uploadspeciality) as? Int)!
                    if specialitycount >= 6 {
                        // registering user can not be added to Invitor COT of that speciality
                        print("PrimaryDetailsDocViewController:handleInvitedByDoctorCallDocCode(): registering user can not be added to Invitor COT ")
                        let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
                        let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorDetailsBaseViewController") as! DoctorDetailsBaseViewController
                        docDeatilsTVC.docspeciality = (self.primaryDetailsTVC?.specialisation.text)!
                        self.present(docDeatilsTVC, animated: true, completion: nil)
                    } else {
                        // as COT count is less than 6 hence we can add registering user to Invitor COT
                        // 1. make new entry to RelSpeciality
                        // 2. increment DocCountTable myTeam using Invitor userid
                        // 3. update Invite Table entry : first read invite table using d_mobilenumber and callcode , this may return a record if Invitor had made entry in invite table while doing invite as team invite, if there is no entry in invite table for this read operation then also we have actually added this doctor to RelDocTeam anyways
                        self.MakeNewEntrytoRelSpeciality(invitoruserid: invitoruserid, inviteeuserid: (QBSession.current.currentUser?.id)!, inviteefullname: (QBSession.current.currentUser?.fullName)!, inviteeprofileURL: GlobalDocData.gprofileURL, inviteespeciality: GlobalDocData.gdocspeciality, invitorprofileURL: invitorprofileURL, invitorfullname: invitorfullname, status: false)
                    }
                    
                }) { (errorresponse) in
                    print("PrimaryDetailsDocViewController:CustomObject: handleInvitedByPatientCallDocCode()-DocCountTable Response error: \(String(describing: errorresponse.error?.description))")
                }
            }
        }) { (errorresponse) in
            print("PrimaryDetailsDocViewController:CustomObject: handleInvitedByPatientCallDocCode() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func MakeNewEntrytoRelDocTeam(invitordocuserid: UInt, inviteedocuserid: UInt, inviteedocfullname: String, inviteeprofileURL: String, status: Bool) {
        // add new entry to RelDocTeam Table
        let object = QBCOCustomObject()
        object.className = "RelDocTeam"
        object.fields["invitordocuserid"] = invitordocuserid
        object.fields["inviteedocuserid"] = inviteedocuserid
        object.fields["inviteedocfullname"] = inviteedocfullname
        object.fields["inviteeprofileURL"] = inviteeprofileURL
        object.fields["status"] = status
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            print("PrimaryDetailsDocViewController: MakeNewEntrytoRelDocTeam(): Successfully created")
            self.ReadInviteTableforTeam()
        }) { (response) in
            print("PrimaryDetailsDocViewController: makeEntrytoRelDocPatTable(): Response error: \(String(describing: response.error?.description))")
        }
    }

    func IncrementDocCountTableformyTeam(invitordocuserid: UInt) {
        //first we are reading the DocCounttable to get the record id of Invitor
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = invitordocuserid
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("PrimaryDetailsDocViewController:IncrementDocCountTableformyTeam(): Reading record ID of Invitor !")
            let invitorRecordID = (contributors![0].id)!
            let myTeamval =  contributors![0].fields?.value(forKey: "myTeam") as? Int
            self.finallyupdateDocCountTableforteam(invitorRecordID: invitorRecordID, myTeamval: myTeamval!)

        }) { (errorresponse) in
            print("PrimaryDetailsDocViewController:IncrementDocCountTableformyTeam() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    // here we are updating
    func finallyupdateDocCountTableforteam(invitorRecordID: String, myTeamval: Int) {
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        var val = myTeamval
        val = val+1
        object.fields["myTeam"] =  val
        object.id = invitorRecordID      //record id of Invitor
        QBRequest.update(object, successBlock: { (response, contributors) in
            self.ReadInviteTableforTeam()
        }) { (response) in
            print("PrimaryDetailsDocViewController:finallyupdateDocCountTableforteam() Response error: \(String(describing: response.error?.description))")
        }
    }

    func ReadInviteTableforTeam() {
        // we are going to read Invite table first here using inviteeuserid = d_invitormobilenumber and Invitor calldoc code
        let getRequest = NSMutableDictionary()
        getRequest["calldoccode"] = self.regCallDocCode  // this is the calldoc code user has entered at the time of Registration
        let stringWithoutpluswith_d: String? = QBSession.current.currentUser?.phone?.replacingOccurrences(of: "+", with: "d_")
        getRequest["inviteemobilenum"] = stringWithoutpluswith_d
        QBRequest.objects(withClassName: "InviteTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if (contributors?.count)! > 0 {
                let InvitetableRecordID = (contributors![0].id)!
                self.updateInviteTableforteam(InvitetableRecordID: InvitetableRecordID)
            } else {
                // no entry found in Invite table for this callcode code and invitee userid as d_mobilenumber , this mean Registering user must have got this calldoc code from word of mouth
                print("PrimaryDetailsDocViewController:ReadInviteTableforTeam(): There is no record entry found")
                let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
                let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorDetailsBaseViewController") as! DoctorDetailsBaseViewController
                docDeatilsTVC.docspeciality = (self.primaryDetailsTVC?.specialisation.text)!
                self.present(docDeatilsTVC, animated: true, completion: nil)
            }
            
        }) { (errorresponse) in
            print("PrimaryDetailsDocViewController:ReadInviteTableforTeam() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func updateInviteTableforteam(InvitetableRecordID: String) {
        let object = QBCOCustomObject()
        object.className = "InviteTable"
        object.fields["inviteeuserid"] =  (QBSession.current.currentUser?.id)!
        object.fields["inviteemobilenum"] = ""
        object.fields["typeofinvite"] = "TEAM"
        object.id = InvitetableRecordID      //record id of invitetable entry
        QBRequest.update(object, successBlock: { (response, contributors) in
            let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
            let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorDetailsBaseViewController") as! DoctorDetailsBaseViewController
            docDeatilsTVC.docspeciality = (self.primaryDetailsTVC?.specialisation.text)!
            self.present(docDeatilsTVC, animated: true, completion: nil)
        }) { (response) in
            //Handle Error
            print("PrimaryDetailsDocViewController:updateInviteTableforteam() Response error: \(String(describing: response.error?.description))")
        }
    }
    
    /////////////////////////////////////// COT related Logic from here ///////////////////////////////////////////
    
    func MakeNewEntrytoRelSpeciality(invitoruserid: UInt, inviteeuserid: UInt, inviteefullname: String, inviteeprofileURL: String, inviteespeciality: String, invitorprofileURL: String, invitorfullname: String, status: Bool) {
        // add new entry to RelSpeciality Table
        let object = QBCOCustomObject()
        object.className = "RelDocTeam"
        object.fields["invitoruserid"] = invitoruserid
        object.fields["inviteeuserid"] = inviteeuserid
        object.fields["inviteefullname"] = inviteefullname
        object.fields["inviteeprofileimageURL"] = inviteeprofileURL
        object.fields["inviteespeciality"] = inviteespeciality
        object.fields["invitorprofileimageURL"] = invitorprofileURL
        object.fields["invitorfullname"] = invitorfullname
        object.fields["status"] = status
        
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            self.ReadInviteTableforCOT()
        }) { (response) in
            print("PrimaryDetailsDocViewController: MakeNewEntrytoRelSpeciality(): Response error: \(String(describing: response.error?.description))")
        }
        
    }
    
    func IncrementDocCountTableforCOT(invitordocuserid: UInt) {
        //first we are reading the DocCounttable to get the record id of Invitor
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = invitordocuserid
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            let invitorRecordID = (contributors![0].id)!
            let specialityval =  contributors![0].fields?.value(forKey: GlobalDocData.gdocspeciality ) as? Int
            self.finallyupdateDocCountTableforCOT(invitorRecordID: invitorRecordID, specialityval: specialityval!)
        }) { (errorresponse) in
            print("PrimaryDetailsDocViewController:IncrementDocCountTableforCOT() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    // here we are updating for COT
    func finallyupdateDocCountTableforCOT(invitorRecordID: String, specialityval: Int) {
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        var val = specialityval
        val = val+1
        object.fields[GlobalDocData.gdocspeciality] =  val
        object.id = invitorRecordID      //record id of Invitor
        QBRequest.update(object, successBlock: { (response, contributors) in
            self.ReadInviteTableforCOT()
        }) { (response) in
            print("PrimaryDetailsDocViewController:finallyupdateDocCountTableforCOT() Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func ReadInviteTableforCOT() {
        // we are going to read Invite table first here using inviteeuserid = d_invitormobilenumber and Invitor calldoc code
        let getRequest = NSMutableDictionary()
        getRequest["calldoccode"] = self.regCallDocCode  // this is the calldoc code user has entered at the time of Registration
        let stringWithoutpluswith_d: String? = QBSession.current.currentUser?.phone?.replacingOccurrences(of: "+", with: "d_")
        getRequest["inviteemobilenum"] = stringWithoutpluswith_d
        QBRequest.objects(withClassName: "InviteTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if (contributors?.count)! > 0 {
                let InvitetableRecordID = (contributors![0].id)!
                self.updateInviteTableforCOT(InvitetableRecordID: InvitetableRecordID)
            } else {
                // no entry found in Invite table for this callcode code and invitee userid as d_mobilenumber , this mean Registering user must have got this calldoc code from word of mouth
                let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
                let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorDetailsBaseViewController") as! DoctorDetailsBaseViewController
                docDeatilsTVC.docspeciality = (self.primaryDetailsTVC?.specialisation.text)!
                self.present(docDeatilsTVC, animated: true, completion: nil)
            }
            
        }) { (errorresponse) in
            print("PrimaryDetailsDocViewController:ReadInviteTableforCOT() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func updateInviteTableforCOT(InvitetableRecordID: String) {
        let object = QBCOCustomObject()
        object.className = "InviteTable"
        object.fields["inviteeuserid"] =  (QBSession.current.currentUser?.id)!
        object.fields["inviteemobilenum"] = ""
        object.fields["typeofinvite"] = "COT"
        object.id = InvitetableRecordID      //record id of invitetable entry
        QBRequest.update(object, successBlock: { (response, contributors) in
            let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
            let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorDetailsBaseViewController") as! DoctorDetailsBaseViewController
            docDeatilsTVC.docspeciality = (self.primaryDetailsTVC?.specialisation.text)!
            self.present(docDeatilsTVC, animated: true, completion: nil)
        }) { (response) in
            //Handle Error
            print("PrimaryDetailsDocViewController:updateInviteTableforCOT() Response error: \(String(describing: response.error?.description))")
        }
        
    }
    
    func UpdateLanguageGrid() {

        self.EngbuttonSelected = UserDefaults.standard.bool(forKey: "EngbuttonSelected")
        self.HindibuttonSelected = UserDefaults.standard.bool(forKey: "HindibuttonSelected")
        self.TamilbuttonSelected = UserDefaults.standard.bool(forKey: "TamilbuttonSelected")
        self.TelugubuttonSelected = UserDefaults.standard.bool(forKey: "TelugubuttonSelected")
        self.UrdubuttonSelected = UserDefaults.standard.bool(forKey: "UrdubuttonSelected")
        self.KannadabuttonSelected = UserDefaults.standard.bool(forKey: "KannadabuttonSelected")
        self.MalaybuttonSelected = UserDefaults.standard.bool(forKey: "MalaybuttonSelected")
        self.AssambuttonSelected = UserDefaults.standard.bool(forKey: "AssambuttonSelected")
        self.OriyabuttonSelected = UserDefaults.standard.bool(forKey: "OriyabuttonSelected")
        self.GujbuttonSelected = UserDefaults.standard.bool(forKey: "GujbuttonSelected")
        self.BengalibuttonSelected = UserDefaults.standard.bool(forKey: "BengalibuttonSelected")
        self.PanjbuttonSelected = UserDefaults.standard.bool(forKey: "PanjbuttonSelected")
        self.MarathibuttonSelected = UserDefaults.standard.bool(forKey: "MarathibuttonSelected")
        
        let templangarray = GlobalDocData.languagearray
        print("DoctorPrimaryDetailsViewController:UpdateLanguageGrid()templangarray= \(templangarray)")
        
        if templangarray.count > 0 {

            if templangarray[0] == "English" {
                primaryDetailsTVC?.Englishbuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Englishbuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Englishbuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Englishbuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
            
            
            if templangarray[1] == "Hindi" {
                primaryDetailsTVC?.Hindibuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Hindibuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Hindibuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Hindibuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
            
            
            if templangarray[2] == "Urdu" {
                primaryDetailsTVC?.Urdubuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Urdubuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Urdubuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Urdubuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
            
            if templangarray[3] == "Tamil" {
                primaryDetailsTVC?.Tamilbuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Tamilbuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Tamilbuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Tamilbuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
            
            
            if templangarray[4] == "Kannada" {
                primaryDetailsTVC?.Kannadabuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Kannadabuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Kannadabuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Kannadabuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
            
            if templangarray[5] == "Telugu" {
                primaryDetailsTVC?.Telugubuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Telugubuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Telugubuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Telugubuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }

            if templangarray[6] == "Malayalam" {
                primaryDetailsTVC?.Malayalambuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Malayalambuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Malayalambuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Malayalambuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }

            if templangarray[7] == "Panjabi" {
                primaryDetailsTVC?.Panjabibuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Panjabibuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Panjabibuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Panjabibuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
            
            if templangarray[8] == "Gujarati" {
                primaryDetailsTVC?.Gujaratibuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Gujaratibuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Gujaratibuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Gujaratibuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
            
            
            if templangarray[9] == "Bengali" {
                primaryDetailsTVC?.Bengalibuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Bengalibuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Bengalibuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Bengalibuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
            

            if templangarray[10] == "Assamese" {
                primaryDetailsTVC?.Assamesebuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Assamesebuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Assamesebuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Assamesebuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
            
            if templangarray[11] == "Oriya" {
                primaryDetailsTVC?.Oriyabuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Oriyabuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Oriyabuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Oriyabuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }

            if templangarray[12] == "Marathi" {
                primaryDetailsTVC?.Marathibuttonview.setTitleColor(UIColor.white, for: .normal)
                primaryDetailsTVC?.Marathibuttonview.backgroundColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            } else {
                primaryDetailsTVC?.Marathibuttonview.setTitleColor(UIColor.darkGray, for: .normal)
                primaryDetailsTVC?.Marathibuttonview.backgroundColor = UIColor(red: 241/255, green: 246/255, blue: 250/255, alpha: 1)
            }
        }
    }
}



// Put this piece of code anywhere you like
extension UIViewController {

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

// random unique code generation
struct ShortCodeGenerator {
    
    private static let base62chars = [Character]("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".characters)
    private static let maxBase : UInt32 = 62 //16  //62
    
    static func getCode(withBase base: UInt32 = maxBase, length: Int) -> String {
        var code = ""
        for _ in 0..<length {
            let random = Int(arc4random_uniform(min(base, maxBase)))
            code.append(base62chars[random])
        }
        return code
    }
}

