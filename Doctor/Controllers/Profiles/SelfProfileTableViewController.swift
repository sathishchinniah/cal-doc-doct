import UIKit

class SelfProfileTableViewController: UITableViewController {

    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var medicalDegreesLabel: UILabel!
    @IBOutlet weak var fluentInLabel: UILabel!
    @IBOutlet weak var consultationChargesLabel: UILabel!
    @IBOutlet weak var mciRegNoLabel: UILabel!
    @IBOutlet weak var recommendedLabel: UILabel!
    @IBOutlet weak var profileVerifiedLabel: UILabel!
    @IBOutlet weak var connectedLabel: UILabel!
    
    @IBOutlet weak var mciView: UIView! {
        didSet {
            mciView.layer.cornerRadius = 5;
            mciView.layer.masksToBounds = false
            mciView.layer.shadowColor = UIColor.lightGray.cgColor
            mciView.layer.shadowOpacity = 0.5
            mciView.layer.shadowRadius = 3.0
            mciView.layer.shadowOffset = .zero
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- Custom methods

    func configureView() {
        experienceLabel.text = GlobalDocData.gdocage
        medicalDegreesLabel.text = GlobalDocData.gdegree
        mciRegNoLabel.text = GlobalDocData.gmcinum
        consultationChargesLabel.text = "Rs. " + String(GlobalVariables.gDoctorFee)
        updateLanguage()
        
        let recommendedString = "Recommended\nby 8 Doctors"
        let rAttributedString = "8 Doctors"
        recommendedLabel.attributedText = attributeString(string: recommendedString, rangeStr: rAttributedString)

        let profileVerifiedString = "Profile Verified\nby CallDoc"
        let pAttributedString = "CallDoc"
        profileVerifiedLabel.attributedText = attributeString(string: profileVerifiedString, rangeStr: pAttributedString)

        let connectedString = "Connected to\n200+ Patients"
        let cAttributedString = "200+"
        connectedLabel.attributedText = attributeString(string: connectedString, rangeStr: cAttributedString)
    }

    func attributeString(string: String, rangeStr:String) -> NSMutableAttributedString {
        let range = (string as NSString).range(of: rangeStr)
        let attribute = NSMutableAttributedString.init(string: string)
        let font = UIFont(name: "Rubik-Medium", size: 12.0)
        attribute.addAttribute(NSAttributedStringKey.font, value: font! , range: range)
        return attribute
    }
    
    func updateLanguage() {
        let templangarray = GlobalDocData.languagearray
        print("SelfProfileTableViewController:updateLanguage()= \(templangarray)")
        
        if templangarray.count > 0 {
            
            if templangarray[0] == "English" {
                fluentInLabel.text = "English"
            }

            if templangarray[1] == "Hindi" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Hindi"
            }

            if templangarray[2] == "Urdu" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Urdu"
            }

            if templangarray[3] == "Tamil" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Tamil"
            }

            if templangarray[4] == "Kannada" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Kannada"
            }

            if templangarray[5] == "Telugu" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Telgu"
            }

            if templangarray[6] == "Malayalam" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Malayalm"
            }

            if templangarray[7] == "Panjabi" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Panjabi"
            }

            if templangarray[8] == "Gujarati" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Gujarati"
            }

            if templangarray[9] == "Bengali" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Bengali"
            }

            if templangarray[10] == "Assamese" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Assamese"
            }

            if templangarray[11] == "Oriya" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Oriya"
            }

            if templangarray[12] == "Marathi" {
                fluentInLabel.text = fluentInLabel.text! + ", " + "Marathi"
            }
        }
    }

}
