import UIKit

class COTProfileViewController: UIViewController {

    @IBOutlet weak var docImageView: UIImageView!
    @IBOutlet weak var specialzationLabel: UILabel! {
        didSet {
            specialzationLabel.layer.cornerRadius = 15;
            specialzationLabel.layer.masksToBounds = true
            specialzationLabel.layer.shadowColor = UIColor.lightGray.cgColor
            specialzationLabel.layer.shadowOpacity = 0.5
            specialzationLabel.layer.shadowRadius = 3.0
            specialzationLabel.layer.shadowOffset = .zero
        }
    }
    
    @IBOutlet weak var addDocToCOTSwitch: UISwitch! {
        didSet {
            addDocToCOTSwitch.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }
    }

    @IBOutlet weak var referPatientsToDocSwitch: UISwitch! {
        didSet {
            referPatientsToDocSwitch.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }
    }
    
    var docfullname = ""
    var imageURLpassed = ""
    var docuserid: Int = 0
    var docspeciality = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title =  "Dr. " + docfullname
        docImageView.sd_setImage(with: URL(string: imageURLpassed), placeholderImage: UIImage(named: "profile"))
        specialzationLabel.text = "    " + docspeciality + "    "
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- Action Methods

    @IBAction func addDocToCOTSwitchValueChanged(_ sender: UISwitch) {
    }

    @IBAction func referPatientsToDocSwitchValueChanged(_ sender: UISwitch) {
    }
    
    //MARK:- Storyboard Instance

    static func storyboardInstance() -> COTProfileViewController {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "COTProfileViewController") as! COTProfileViewController
    }

    //MARK:- Segue delegate
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "COTProfilebasetoTbl" {
            let modalVC:COTProfileTableViewController = segue.destination as! COTProfileTableViewController
            modalVC.userid = docuserid
        }
    }

}
