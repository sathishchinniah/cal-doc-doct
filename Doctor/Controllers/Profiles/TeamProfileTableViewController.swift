import UIKit

class TeamProfileTableViewController: UITableViewController {

    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var medicalAddressLabel: UILabel!
    @IBOutlet weak var fluentInLabel: UILabel!
    @IBOutlet weak var consultationChargesLabel: UILabel!
    @IBOutlet weak var mciRegNoLabel: UILabel!
    @IBOutlet weak var recommendedLabel: UILabel!
    @IBOutlet weak var ProfileVerifiedLabel: UILabel!
    @IBOutlet weak var connectedLabel: UILabel!
    @IBOutlet weak var mciView: UIView! {
        didSet {
            mciView.layer.cornerRadius = 5;
            mciView.layer.masksToBounds = false
            mciView.layer.shadowColor = UIColor.lightGray.cgColor
            mciView.layer.shadowOpacity = 0.5
            mciView.layer.shadowRadius = 3.0
            mciView.layer.shadowOffset = .zero
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- Custom methods

    func configureView() {
        let recommendedString = "Recommended\nby 8 Doctors"
        let rAttributedString = "8 Doctors"
        recommendedLabel.attributedText = attributeString(string: recommendedString, rangeStr: rAttributedString)

        let profileVerifiedString = "Profile Verified\nby CallDoc"
        let pAttributedString = "CallDoc"
        ProfileVerifiedLabel.attributedText = attributeString(string: profileVerifiedString, rangeStr: pAttributedString)

        let connectedString = "Connected to\n200+ Patients"
        let cAttributedString = "200+"
        connectedLabel.attributedText = attributeString(string: connectedString, rangeStr: cAttributedString)
    }

    func attributeString(string: String, rangeStr:String) -> NSMutableAttributedString {
        let range = (string as NSString).range(of: rangeStr)
        let attribute = NSMutableAttributedString.init(string: string)
        let font = UIFont(name: "Rubik-Medium", size: 12.0)
        attribute.addAttribute(NSAttributedStringKey.font, value: font! , range: range)
        return attribute
    }

}
