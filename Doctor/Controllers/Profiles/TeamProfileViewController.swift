import UIKit

class TeamProfileViewController: UIViewController {

    @IBOutlet weak var docImageView: UIImageView!
    @IBOutlet weak var specialzationLabel: UILabel! {
        didSet {
            specialzationLabel.layer.cornerRadius = 15;
            specialzationLabel.layer.masksToBounds = false
            specialzationLabel.layer.shadowColor = UIColor.lightGray.cgColor
            specialzationLabel.layer.shadowOpacity = 0.5
            specialzationLabel.layer.shadowRadius = 3.0
            specialzationLabel.layer.shadowOffset = .zero
        }
    }

    @IBOutlet weak var referPatientsToDocSwitch: UISwitch! {
        didSet {
            referPatientsToDocSwitch.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- IBAction
    @IBAction func referPatientsToDocValueChanged(_ sender: UISwitch) {
    }

    //MARK:- Storyboard Instance

    static func storyboardInstance() -> TeamProfileViewController {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "TeamProfileViewController") as! TeamProfileViewController
    }

}
