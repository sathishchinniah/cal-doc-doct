import UIKit

class SelfProfileViewController: UIViewController {

    @IBOutlet weak var docImageView: UIImageView!
    @IBOutlet weak var docimageViewYConstraint: NSLayoutConstraint!
    @IBOutlet weak var docgenderlb: UILabel!
    @IBOutlet weak var specialzationLabel: UILabel! {
        didSet {
            specialzationLabel.layer.cornerRadius = 15;
            specialzationLabel.layer.masksToBounds = true
            specialzationLabel.layer.shadowColor = UIColor.lightGray.cgColor
            specialzationLabel.layer.shadowOpacity = 0.5
            specialzationLabel.layer.shadowRadius = 3.0
            specialzationLabel.layer.shadowOffset = .zero
        }
    }
    
    var imageURLpassed:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title =  "Dr. " + GlobalDocData.gdocname
        docImageView.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profile"))
        
        specialzationLabel.text = "    " + GlobalDocData.displayspeciality + "    "
        specialzationLabel.sizeToFit()
        docgenderlb.text = GlobalDocData.gdocgender
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        if phone && maxLength == 812 {
            
            docimageViewYConstraint.constant = 83.0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- IBAction Methods

    @IBAction func editButtonClicked(_ sender: UIButton) {
        GlobalDocData.firsttimeview = false
        let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
        let docprimaryViewController = storyboard.instantiateViewController(withIdentifier: "PrimaryDetailsDocBaseViewController") as! PrimaryDetailsDocBaseViewController
        present(docprimaryViewController, animated: true, completion: nil)
    }
    
    //MARK:- Storyboard Instance

    static func storyboardInstance() -> SelfProfileViewController {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SelfProfileViewController") as! SelfProfileViewController
    }

}
