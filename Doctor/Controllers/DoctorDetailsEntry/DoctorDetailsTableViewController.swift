import UIKit

let rowHeight : CGFloat = 74.0
let paddingForHeader : CGFloat  = 15.0
let navBarHeight : CGFloat  = 64.0
let footerHeight : CGFloat  = 55.0

class DoctorDetailsTableViewController: UITableViewController, UITextFieldDelegate {

    var topView : HeaderViewWithImage?

    @IBOutlet var accholderfirstname: UITextField!
    @IBOutlet var accountnumtxt: UITextField!
    @IBOutlet var accountnumcnftxt: UITextField!
    @IBOutlet var accounttypetxt: UITextField!
    @IBOutlet var banknametxt: UITextField!
    @IBOutlet var pannumbertxt: UITextField!
    @IBOutlet var ifsccodetxt: UITextField!
    @IBOutlet var gstnnumtxt: UITextField!
    @IBOutlet weak var dropDownAccounttypeview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let screenHeight  = UIScreen.main.bounds.height
        let totalHeaderHeight = 0.29 * screenHeight
        self.topView = HeaderViewWithImage.instantiateFromNib()
        self.topView?.activityIndicatordocimg.isHidden = true
        self.tableView.setParallaxHeader(self.topView!, mode: .topFill, height: totalHeaderHeight)
    }
    
    // MARK: Scroll Delegates
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.tableView.shouldPositionParallaxHeader()
    }

}
