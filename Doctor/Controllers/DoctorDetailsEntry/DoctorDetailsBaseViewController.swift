import UIKit
import DropDown

class DoctorDetailsBaseViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var footerView: UIView!
    @IBOutlet var footerInnerView: UIView!
    @IBOutlet var onbuttonview: UIButton!
    
    var topView : HeaderViewWithImage?
    var  doctorDetailsTVC : DoctorDetailsTableViewController?
    let AccounttypeDropDown = DropDown()
    var nav: UINavigationController?
    var docspeciality = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
    }
    
    func initialize() {
        self.doctorDetailsTVC?.accholderfirstname.delegate = self
        self.doctorDetailsTVC?.accountnumtxt.delegate = self
        self.doctorDetailsTVC?.accountnumcnftxt.delegate = self
        self.doctorDetailsTVC?.accounttypetxt.delegate = self
        self.doctorDetailsTVC?.banknametxt.delegate = self
        self.doctorDetailsTVC?.pannumbertxt.delegate = self
        self.doctorDetailsTVC?.ifsccodetxt.delegate = self
        self.doctorDetailsTVC?.gstnnumtxt.delegate = self
        
        let layer1 = doctorDetailsTVC?.topView?.line1
        layer1?.alpha = 0.40
        layer1?.layer.borderWidth = 1
        layer1?.layer.borderColor = UIColor.white.cgColor
        
        let layer2 = doctorDetailsTVC?.topView?.line2
        layer2?.alpha = 0.40
        layer2?.layer.borderWidth = 1
        layer2?.layer.borderColor = UIColor.black.cgColor
        
        doctorDetailsTVC?.topView?.circle2.setImage(#imageLiteral(resourceName: "whitecircle"), for: .normal)
        
        footerInnerView.backgroundColor = UIColor(patternImage: UIImage(named: "schbottombarbg")!)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(gesture:)))
        view.addGestureRecognizer(tapGesture)
        
        
        
        if phone && maxLength == 568 {
            //iphone 5
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = 32.0
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 2.2
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.green.cgColor
            doctorDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true
            self.doctorDetailsTVC?.topView?.profileimageYConstraint.constant = 40.0

        }
        
        if phone && maxLength == 667 {
            //iphone 6
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = 37.0
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 2.2
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.green.cgColor
            doctorDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true
            self.doctorDetailsTVC?.topView?.profileimageYConstraint.constant = 40.0
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = 40.0
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 2.2
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.green.cgColor
            doctorDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true
        }
        
        if phone && maxLength == 812 {
            //iphone x
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = 38.0
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 2.2
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.green.cgColor
            doctorDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true
        }
        
        if ipad && maxLength == 1024 {
            //iPad
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = 77.0
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 2.2
            doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.green.cgColor
            doctorDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true
        }
        
        LoadProfileImage()
        
        if !GlobalDocData.gdocspeciality.isEmpty {
            topView?.labelUserRpleType.text = GlobalDocData.displayspeciality
        }
        
        let isonline = UserDefaults.standard.bool(forKey: "doctoronline")
        
        if(isonline) {
            topView?.imageViewProfileIcon.layer.borderColor = UIColor.green.cgColor
        } else {
            topView?.imageViewProfileIcon.layer.borderColor = UIColor.orange.cgColor
        }
        
        if GlobalDocData.firsttimeviewbankdetails {
            print( "AccountDetailsDocViewController: this is for first time user" )
            // do this for existing user
            if isKeyPresentInUserDefaults(key: "existinguser") {
                // This is first time user from Existing User flow

                let isexistinguser = UserDefaults.standard.object(forKey: "existinguser") as! Bool
                if(isexistinguser) {
                    print( "AccountDetailsDocViewController: This is first time user from Existing User flow" )
                    if let existingusername = UserDefaults.standard.string(forKey: "existinguserfullname")  {
                        GlobalDocData.gdocname = existingusername
                    }
                    if let existingusercustomdata = UserDefaults.standard.string(forKey: "usercustomdata")  {
                        GlobalDocData.gusercustomdata = existingusercustomdata
                    }
                    self.firsttimeuserdataUpdateExistingUser()
                } else {
                    // This is first time user from Registration flow
                    print( "AccountDetailsDocViewController:  This is first time user from Registration flow" )
                    if let tmpgdocname = UserDefaults.standard.string(forKey: "fullname")  {
                        GlobalDocData.gdocname = tmpgdocname
                    }
                    self.RegistrationfirsttimeuserdataUpdate()
                }
            } else {
                // Something not right
                print( "AccountDetailsDocViewController: Something is not right existinguser key is not found")
            }
        } else {
            // this is for not first time user
            print( "AccountDetailsDocViewController: this is for not first time user ")

            if isKeyPresentInUserDefaults(key: "accholderfirstname") {
                doctorDetailsTVC?.accholderfirstname.text  = UserDefaults.standard.string(forKey: "accholderfirstname")!
                doctorDetailsTVC?.accholderfirstname.font =  UIFont(name: "Rubik-Regular", size: 15)
            }

            if isKeyPresentInUserDefaults(key: "accountnumber") {
                doctorDetailsTVC?.accountnumtxt.text  = UserDefaults.standard.string(forKey: "accountnumber")!
                doctorDetailsTVC?.accountnumtxt.font =  UIFont(name: "Rubik-Regular", size: 15)
            }

            if isKeyPresentInUserDefaults(key: "confirmaccountnumber") {
                doctorDetailsTVC?.accountnumcnftxt.text  = UserDefaults.standard.string(forKey: "confirmaccountnumber")!
                doctorDetailsTVC?.accountnumcnftxt.font =  UIFont(name: "Rubik-Regular", size: 15)
            }

            if isKeyPresentInUserDefaults(key: "ifsccode") {
                doctorDetailsTVC?.ifsccodetxt.text  = UserDefaults.standard.string(forKey: "ifsccode")!
                doctorDetailsTVC?.ifsccodetxt.font =  UIFont(name: "Rubik-Regular", size: 15)
            }

            if isKeyPresentInUserDefaults(key: "gstnnumber") {
                doctorDetailsTVC?.gstnnumtxt.text  = UserDefaults.standard.string(forKey: "gstnnumber")!
                doctorDetailsTVC?.gstnnumtxt.font =  UIFont(name: "Rubik-Regular", size: 15)
            }

            if isKeyPresentInUserDefaults(key: "pannumber") {
                doctorDetailsTVC?.pannumbertxt.text  = UserDefaults.standard.string(forKey: "pannumber")!
                doctorDetailsTVC?.pannumbertxt.font =  UIFont(name: "Rubik-Regular", size: 15)
            }
            ReadAccountDetailsFromServer()
        }
        doctorDetailsTVC?.topView?.labelUserName.text = "Dr. " + GlobalDocData.gdocname
        doctorDetailsTVC?.topView?.labelUserRpleType.text = GlobalDocData.displayspeciality

    } //initilize ends here
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == doctorDetailsTVC?.accounttypetxt) {
            hideKeyboardWhenTappedAround()
            textField.resignFirstResponder()
            textField.endEditing(true)
            
            AccounttypeDropDown.anchorView = doctorDetailsTVC?.dropDownAccounttypeview
            AccounttypeDropDown.dataSource = ["Current", "Savings"]
            AccounttypeDropDown.show()
            AccounttypeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.doctorDetailsTVC?.accounttypetxt.text = item
                UserDefaults.standard.setValue(item, forKey: "accounttype")
                GlobalDocData.gaccounttype = item
            }
        }
    }
    
    func RegistrationfirsttimeuserdataUpdate() {
        print( "AccountDetailsDocViewController: firsttimeuserdataUpdate()")
        UserDefaults.standard.setValue("", forKey: "accholderfirstname")
        UserDefaults.standard.setValue("", forKey: "accountnumber")
        UserDefaults.standard.setValue("", forKey: "confirmaccountnumber")
        UserDefaults.standard.setValue("", forKey: "accouttype")
        UserDefaults.standard.setValue("", forKey: "nameofthebank")
        UserDefaults.standard.setValue("", forKey: "ifsccode")
        UserDefaults.standard.setValue("", forKey: "gstnnumber")
        UserDefaults.standard.setValue("", forKey: "pannumber")
    }
    
    func firsttimeuserdataUpdateExistingUser() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocAccountDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                UserDefaults.standard.setValue((contributors![0].id)!, forKey: "customobjIDPrimarydetails")
                UserDefaults.standard.synchronize()
                self.doctorDetailsTVC?.accholderfirstname.text = contributors![0].fields?.value(forKey: "firstname") as? String
                self.doctorDetailsTVC?.accountnumtxt.text = contributors![0].fields?.value(forKey: "accountnumber") as? String
                self.doctorDetailsTVC?.accountnumcnftxt.text = contributors![0].fields?.value(forKey: "confirmaccountnumber") as? String
                self.doctorDetailsTVC?.accounttypetxt.text = contributors![0].fields?.value(forKey: "accounttype") as? String
                self.doctorDetailsTVC?.banknametxt.text = contributors![0].fields?.value(forKey: "bankname") as? String
                self.doctorDetailsTVC?.ifsccodetxt.text = contributors![0].fields?.value(forKey: "ifsccode") as? String
                self.doctorDetailsTVC?.pannumbertxt.text = contributors![0].fields?.value(forKey: "pannumber") as? String
                self.doctorDetailsTVC?.gstnnumtxt.text = contributors![0].fields?.value(forKey: "gstnumber") as? String
                GlobalDocData.gdocname =  (QBSession.current.currentUser?.fullName)!
                self.doctorDetailsTVC?.topView?.labelUserName.text = "Dr. " + GlobalDocData.gdocname
                self.doctorDetailsTVC?.topView?.labelUserRpleType.text = GlobalDocData.displayspeciality
            }
        }) { (errorresponse) in
            print("DocTrustedNetworkViewController:readDocTrustedNetworkRecords() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    @objc func didTapView(gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.doctorDetailsTVC?.accholderfirstname {
            let maxLength = 35
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        
        if textField == self.doctorDetailsTVC?.accounttypetxt {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField == self.doctorDetailsTVC?.accountnumcnftxt {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField == self.doctorDetailsTVC?.accountnumtxt {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField == self.doctorDetailsTVC?.banknametxt {
            let maxLength = 30
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField == self.doctorDetailsTVC?.ifsccodetxt {
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField == self.doctorDetailsTVC?.gstnnumtxt {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        
        
        return true
    }
    
    func LoadProfileImage() {   //profile image update
        self.doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.cornerRadius = (self.doctorDetailsTVC?.topView?.imageViewProfileIcon.frame.size.width)! / 2
        self.doctorDetailsTVC?.topView?.imageViewProfileIcon.clipsToBounds = true;
        self.doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderWidth = 0.2
        self.doctorDetailsTVC?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.lightGray.cgColor
        self.doctorDetailsTVC?.topView?.imageViewProfileIcon.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profile"))
    }
    
    // MARK: Button Actions
    
    @IBAction func OkButton(_ sender: UIButton) {
        
        if (doctorDetailsTVC?.accholderfirstname.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter your first name, last name at Bank", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.doctorDetailsTVC?.accholderfirstname.text, forKey: "accholderfirstname")
            UserDefaults.standard.synchronize()
        }
        
        if (doctorDetailsTVC?.accountnumtxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Bank Account Number", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.doctorDetailsTVC?.accountnumtxt.text, forKey: "accountnumber")
            UserDefaults.standard.synchronize()
        }
        
        if (doctorDetailsTVC?.accountnumcnftxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please confirm Bank Account Number", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.doctorDetailsTVC?.accountnumcnftxt.text, forKey: "accountnumber")
            UserDefaults.standard.synchronize()
        }
        
        let accountstring1 = doctorDetailsTVC?.accountnumtxt.text
        let accountstring2 = doctorDetailsTVC?.accountnumcnftxt.text
        
        if accountstring1 == accountstring2 {
            
        } else {
            let alert = UIAlertController(title: "Alert", message: "Please check the account number they did not Match!!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
//                self.doctorDetailsTVC?.accountnumtxt.text = ""
//                self.doctorDetailsTVC?.accountnumcnftxt.text = ""
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        
        if (doctorDetailsTVC?.accounttypetxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please select Account type", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.doctorDetailsTVC?.accounttypetxt.text, forKey: "accounttype")
            UserDefaults.standard.synchronize()
        }
        
        if (doctorDetailsTVC?.ifsccodetxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter your first name, last name at Bank", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.doctorDetailsTVC?.ifsccodetxt.text, forKey: "ifsccode")
            UserDefaults.standard.synchronize()
        }
        
        //Validating PAN
        let panRegex = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
        let panRange = NSRange(location: 0, length: (doctorDetailsTVC?.pannumbertxt.text?.count)!)
        let panTest = NSPredicate(format:"SELF MATCHES %@", panRegex)
        
        
        if (doctorDetailsTVC?.pannumbertxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter your PAN Number", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else if (panTest.evaluate(with: (doctorDetailsTVC?.pannumbertxt.text)!) == false)/*panRegex?.firstMatch(in: (doctorDetailsTVC?.pannumbertxt.text)!, options: [], range: panRange) == nil*/ {
            let alert = UIAlertController(title: "Alert", message: "Please Enter valid PAN Number.(In Capital Letters)", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.doctorDetailsTVC?.pannumbertxt.text, forKey: "pannumber")
            UserDefaults.standard.synchronize()
        }
        
        //Validating GST
        let gstRegex = "[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}[A-Z]{1}[0-9]{1}"
        let gstRange = NSRange(location: 0, length: (doctorDetailsTVC?.gstnnumtxt.text?.count)!)
        let gstTest = NSPredicate(format:"SELF MATCHES %@", gstRegex)
        
        if (doctorDetailsTVC?.gstnnumtxt.text?.count)! < 1 {
            self.doctorDetailsTVC?.gstnnumtxt.text = ""
        } else if (gstTest.evaluate(with: (doctorDetailsTVC?.gstnnumtxt.text)!) == false)/*gstRegex?.firstMatch(in: (doctorDetailsTVC?.gstnnumtxt.text)!, options: [], range: gstRange) == nil*/ {
            let alert = UIAlertController(title: "Alert", message: "Please Enter valid GST Number", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.doctorDetailsTVC?.gstnnumtxt.text, forKey: "gstnumber")
            UserDefaults.standard.synchronize()
        }
        
        if GlobalDocData.firsttimeviewbankdetails {
            CreateCustomObjectforDocBankDetails()
        } else {
            UpDateCustomObjectforDocBankDetails()
        }
    }
    
    func GetPatientDatausingregcalldoccode() {
        let getRequest = NSMutableDictionary()
        getRequest["calldoccode"] = GlobalDocData.regcalldoccode
        QBRequest.objects(withClassName: "PatPrimaryDetailsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                let patientuserid = contributors![0].userID
                self.createEntryinRelDocPatTablePatientInvite(invitoruid: patientuserid)
            } else {
                // Ok so now we do not have this calldoc code in PatPrimaryDetailsTable, that means this referal code must have
                // come from some other way means may be from doctor we need to check DocPrimarydetails table for it
                self.CheckcalldoccodeinDocPrimaryDetailsTable()
            }
            
        }) { (errorresponse) in
            print("AccountDetailsDocViewController:GetPatientDatausingregcalldoccode() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func createEntryinRelDocPatTablePatientInvite(invitoruid: UInt) {
        print("AccountDetailsDocViewController: createEntryinRelDocPatTablePatientInvite(): Creating new entry on RelDocPatable")
        let object = QBCOCustomObject()
        object.className = "RelDocPat"
        let doctoruserid = QBSession.current.currentUser?.id
        object.fields["doctoruserid"] = doctoruserid
        object.fields["patientuserid"] = invitoruid
        object.fields["doctorfullname"] = QBSession.current.currentUser?.fullName
        object.fields["lock"] = "true"
        object.fields["doctorspeciality"] = docspeciality   // this coming from primarydetails page
        object.fields["doctorprofileimageURL"] = GlobalDocData.gprofileURL
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //now we can send push notification to this patient that doctor has accepted his SMS Invite and now on CallDoc
            self.sendPushToPatients(patientuid: invitoruid)
        }) { (response) in
            //Handle Error
            LoadingIndicatorView.hide()
            print("AccountDetailsDocViewController:createEntryinRelDocPatTablePatientInvite(): Response error: \(String(describing: response.error?.description))")
        }
    }

    func sendPushToPatients(patientuid: UInt) {
        let currentUserLogin = QBSession.current.currentUser!.fullName
        let userid = "\(patientuid)"
        
        QBRequest.sendPush(withText: "\("Dr " + currentUserLogin! ) has accepted your SMS invite, Doctor is on CallDoc Now!", toUsers: userid, successBlock: { (response: QBResponse, event:[QBMEvent]?) in
            print("AccountDetailsDocViewController:sendPushToPatient: Push sent!")
            
        }) { (error:QBError?) in
            print("AccountDetailsDocViewController:sendPushToPatient:Can not send push: \(error!))")
        }
    }
    
    func CheckcalldoccodeinDocPrimaryDetailsTable() {
        let getRequest = NSMutableDictionary()
        getRequest["calldoccode"] = GlobalDocData.regcalldoccode
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                // ok we got userid of doctor who has invited this doctor using calldoc code
                // now we need to add this doctor either to COT of this current doctor or team of current doctor
                // if both doctor speciality is same that means we should add this doctor to current doctor team, else we need to add
                // this doctor to COT of current doctor
                let invitordoctoruserid = contributors![0].userID
                let invitordoctorspeciality = contributors![0].fields?.value(forKey: "specialisation") as? String
                if (self.docspeciality == invitordoctorspeciality) {
                    // as both speciality is same hence doctor should be added to team of current doctor
                } else {
                    // add doctor to COT of current doctor
                }
            } else {
                // Ok so now this calldoc code has not be founf in both the tables namely DocPrimaryDetails, PatPrimaryDetailsTable
                let alert = UIAlertController(title: "Alert", message: "CallDoc Code you have entered not found on Our System", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    return
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
                return
            }
            
        }) { (errorresponse) in
            print("AccountDetailsDocViewController:CheckcalldoccodeinDocPrimaryDetailsTable() Response error: \(String(describing: errorresponse.error?.description))")
        }
        
    }
    
    func  CreateCustomObjectforDocBankDetails() {
        print("AccountDetailsDocViewController: Creating New Custom Object for Doctor Primary Details")
        let object = QBCOCustomObject()
        object.className = "DocAccountDetails"
        object.fields["firstname"] = doctorDetailsTVC?.accholderfirstname.text
        object.fields["accountnumber"] = doctorDetailsTVC?.accountnumtxt.text
        object.fields["confirmaccountnumber"] = doctorDetailsTVC?.accountnumcnftxt.text
        object.fields["ifsccode"] = doctorDetailsTVC?.ifsccodetxt.text
        object.fields["accounttype"] = doctorDetailsTVC?.accounttypetxt.text
        object.fields["bankname"] = doctorDetailsTVC?.banknametxt.text
        object.fields["pannumber"] = doctorDetailsTVC?.pannumbertxt.text
        object.fields["gstnumber"] = doctorDetailsTVC?.gstnnumtxt.text
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            // we are creating the record ID for doctors Account details table , this record = customobjIDAccountdetails will be used later to update record
            UserDefaults.standard.setValue((contributors?.id)!, forKey: "customobjIDAccountdetails")
            UserDefaults.standard.synchronize()
            GlobalDocData.firsttimeviewbankdetails = false
            print("AccountDetailsDocViewController:CreateCustomObjectforDocBankDetails: Successfully created Account Details !")
            let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
            let docchargesTVC = storyboard.instantiateViewController(withIdentifier: "DoctorConsultChargeBaseViewController")
            self.present(docchargesTVC, animated: true, completion: nil)
        }) { (response) in
            //Handle Error
            print("AccountDetailsDocViewController:CreateCustomObjectforDocBankDetails: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func  UpDateCustomObjectforDocBankDetails() {
        print("AccountDetailsDocViewController:Update Custom Object for Doctor Account Details")
        let object = QBCOCustomObject()
        object.className = "DocAccountDetails"
        object.fields["firstname"] = doctorDetailsTVC?.accholderfirstname.text
        object.fields["accountnumber"] = doctorDetailsTVC?.accountnumtxt.text
        object.fields["confirmaccountnumber"] = doctorDetailsTVC?.accountnumcnftxt.text
        object.fields["ifsccode"] = doctorDetailsTVC?.ifsccodetxt.text
        object.fields["accounttype"] = doctorDetailsTVC?.accounttypetxt.text
        object.fields["bankname"] = doctorDetailsTVC?.banknametxt.text
        object.fields["pannumber"] = doctorDetailsTVC?.pannumbertxt.text
        object.fields["gstnumber"] = doctorDetailsTVC?.gstnnumtxt.text
        object.id = UserDefaults.standard.string(forKey: "customobjIDAccountdetails") ?? ""
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("AccountDetailsDocViewController:UpDateCustomObjectforDocBankDetails(): Successfully Updated Account Details !")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: true, completion: nil)
        }) { (response) in
            print("AccountDetailsDocViewController:UpDateCustomObjectforDocBankDetails: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func ReadAccountDetailsFromServer() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "DocAccountDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            
            if let contributors = contributors, contributors.count > 0 {
                UserDefaults.standard.setValue((contributors[0].id)!, forKey: "customobjIDAccountdetails")
                UserDefaults.standard.synchronize()
                self.doctorDetailsTVC?.accholderfirstname.text = contributors[0].fields?.value(forKey: "firstname") as? String
                self.doctorDetailsTVC?.accountnumtxt.text = contributors[0].fields?.value(forKey: "accountnumber") as? String
                self.doctorDetailsTVC?.accountnumcnftxt.text = contributors[0].fields?.value(forKey: "confirmaccountnumber") as? String
                self.doctorDetailsTVC?.ifsccodetxt.text = contributors[0].fields?.value(forKey: "ifsccode") as? String
                self.doctorDetailsTVC?.accounttypetxt.text = contributors[0].fields?.value(forKey: "accounttype") as? String
                self.doctorDetailsTVC?.banknametxt.text = contributors[0].fields?.value(forKey: "bankname") as? String
                self.doctorDetailsTVC?.pannumbertxt.text = contributors[0].fields?.value(forKey: "pannumber") as? String
                self.doctorDetailsTVC?.gstnnumtxt.text = contributors[0].fields?.value(forKey: "gstnumber") as? String
            }
            
            print("AccountDetailsDocViewController:ReadAccountDetailsFromServer: Successfully Read Account Details from Server !")
        }) { (response) in
            print("AccountDetailsDocViewController:ReadAccountDetailsFromServer: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    // MARK: - Segue delegates
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DoctorDetailsTableViewControllerSegue" {
            self.doctorDetailsTVC = segue.destination as? DoctorDetailsTableViewController
        }
    }
}
