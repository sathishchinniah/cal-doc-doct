import UIKit
import KMPlaceholderTextView

class MedicalAdviceTableViewController: UITableViewController {

    let medicalAdviceHeight: [CGFloat] = [68, 60, 74, 74, 70, 70, 40, 57, 135, 60, 74, 72, 0, 188]
    let skippedMedicalAdviceHeight: [CGFloat] = [68, 60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 72, 188]

    @IBOutlet var patientnametxt: UITextField!
    @IBOutlet var patientgendertxt: UITextField!
    @IBOutlet var patientagetxt: UITextField!
    @IBOutlet var symptomstxt: KMPlaceholderTextView!
    @IBOutlet var diagnosistxt: KMPlaceholderTextView!
    @IBOutlet var advicetxt: KMPlaceholderTextView!
    @IBOutlet var followupxt: KMPlaceholderTextView!
    @IBOutlet var skipReason: KMPlaceholderTextView!
    @IBOutlet var fullchargeButtonview: UIButton!
    @IBOutlet var halfchargeButtonview: UIButton!
    @IBOutlet var waiveoffchargeButtonview: UIButton!
    @IBOutlet var fullchargelb: UILabel!
    @IBOutlet var halfchargelb: UILabel!
    @IBOutlet var buttonAddMedicine: UIButton!
    @IBOutlet var buttonAddInvestigation: UIButton!
    @IBOutlet weak var skipMedicalAdviceCheckboxImageView: UIImageView!
    @IBOutlet weak var skipMedicalAdviceContentView: UIView! {
        didSet {
            skipMedicalAdviceContentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(skipMedicalAdviceCellClicked)))
        }
    }

    @IBOutlet weak var rxMedicineView: UIView!
    @IBOutlet weak var rxMedicineViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var dxMedicineView: UIView!
    @IBOutlet weak var dxMedicineViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var consultChargesView: UIView!
    
    var rxdelegate: RXMedicineViewDelegate?
    var dxdelegate: DXMedicineViewDelegate?
    var isSkipMedicalAdvice = false
    var rxMedicines = [RxMedicalAdviceData]()
    var dxMedicines = [DxMedicalAdviceData]()
    var consultType = ""
    // fix for rx, dx as string to be stroed to backend
    var rxMedicinesString = ""
    var dxMedicinesString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.symptomstxt.sizeToFit()
        self.diagnosistxt.sizeToFit()
        self.advicetxt.sizeToFit()
        self.followupxt.sizeToFit()
        self.skipReason.sizeToFit()
        configureRxMedicineView()
        configureDxMedicineView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if consultType == "NEW" {
            self.consultChargesView.isHidden = false
        } else if consultType == "EXISTING" {
            self.consultChargesView.isHidden = true
        } else {
            self.consultChargesView.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- TableView delegate and datasource methods

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isSkipMedicalAdvice {
            return skippedMedicalAdviceHeight[indexPath.row]
        }

        if indexPath.row == 5 {
            return CGFloat(Int(medicalAdviceHeight[indexPath.row]) * rxMedicines.count)
        }

        if indexPath.row == 8 {
            return CGFloat(Int(medicalAdviceHeight[indexPath.row]) * dxMedicines.count)
        }

        return medicalAdviceHeight[indexPath.row]
    }

    @objc func skipMedicalAdviceCellClicked() {
        if skipMedicalAdviceCheckboxImageView.image == #imageLiteral(resourceName: "uncheck_icon") {
            skipMedicalAdviceCheckboxImageView.image = #imageLiteral(resourceName: "check_icon")
            isSkipMedicalAdvice = true
            // set green radio button to no charge button
            fullchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
            halfchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
            waiveoffchargeButtonview.setImage(#imageLiteral(resourceName: "greenradio"), for: UIControlState.normal)
            GlobalVariables.gchargevalueint = 2

        } else {
            isSkipMedicalAdvice = false
            skipMedicalAdviceCheckboxImageView.image = #imageLiteral(resourceName: "uncheck_icon")
            // set green radio button to full charge
            fullchargeButtonview.setImage(#imageLiteral(resourceName: "greenradio"), for: UIControlState.normal)
            halfchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
            waiveoffchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
            GlobalVariables.gchargevalueint = 0
        }
        self.tableView.reloadData()
    }

    func configureRxMedicineView() {
        rxMedicineView.subviews.forEach { $0.removeFromSuperview() }
        rxMedicineViewHeightConstraint.constant = CGFloat(70 * rxMedicines.count)

        for (i, medicine) in rxMedicines.enumerated() {

            let rxView = RXMedicineView()
            rxView.rxdelegate = rxdelegate
            rxMedicineView.addSubview(rxView)

            rxView.snp.makeConstraints { (make) in
                make.height.equalTo(70)
                make.leading.equalToSuperview()
                make.trailing.equalToSuperview()
                make.top.equalTo(i * 70)
            }

            rxView.configureView(rxData: medicine, rxNo: "\(i + 1)")
        }
    }

    func configureDxMedicineView() -> [DXMedicineView] {
        dxMedicineView.subviews.forEach { $0.removeFromSuperview() }
        dxMedicineViewHeightConstraint.constant = CGFloat(135 * dxMedicines.count)

        var dxViews = [DXMedicineView]()

        for (i, medicine) in dxMedicines.enumerated() {

            let dxView = DXMedicineView()
            dxView.dxdelegate = dxdelegate
            dxMedicineView.addSubview(dxView)

            dxView.snp.makeConstraints { (make) in
                make.height.equalTo(135)
                make.leading.equalToSuperview()
                make.trailing.equalToSuperview()
                make.top.equalTo(i * 135)
            }

            dxView.configureView(dxData: medicine, index: i)
            dxViews.append(dxView)
        }
        return dxViews
    }


}
