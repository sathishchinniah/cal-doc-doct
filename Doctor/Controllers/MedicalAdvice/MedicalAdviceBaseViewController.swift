import UIKit

struct GlobalMedicalAdvicedata {
    static var gpatientname = ""
    static var gpatientgender = ""
    static var gpatientage = ""
    static var gsymptoms = ""
    static var gdiagnosis = ""
    static var grx1 = ""
    static var grx2 = ""
    static var gadvice = ""
    static var gfollowup = ""
    static var afterorbeforefood = ""
    static var booldoRx1:Bool = false
    static var booldoRx2:Bool = false
    static var boolupdateRx1:Bool = false
    static var boolupdateRx2:Bool = false
    static var editit:Bool = false
    static var totalrx1data = ""
    static var totalrx2data = ""

    // these values comes from CallViewIncomingCallController
    static  var ConsultTablerecordID = ""
    static  var patientname = ""
    static var patientgender = ""
    static  var patientage = ""
    static  var patientcalldoccode = ""
    static  var patientpatuserid:UInt = 0
}

struct RxMedicalAdviceData {
    var medicineName = ""
    var dosage = ""
    var form = ""
    var genericName = ""
    var strength = ""
    var frequency = ""
    var duration = ""
    var durationType = ""
    var afterFood = true
    var specialRemarks: String?
}

struct DxMedicalAdviceData {
    var testName = ""
    var instructions = ""
}

struct ConfirmMedicalAdviceData {
    var patientName = ""
    var symptoms: String?
    var provisionalDiagnosis: String?
    var rxMedicines = [RxMedicalAdviceData]()
    var dxMedicines = [DxMedicalAdviceData]()
    var rxMedicinesString: String?
    var dxMedicinesString: String?
    var advice: String?
    var followUp: String?
    var consultationCharges = ""
    var skipMedicalAdvice = false
    var charge = 500
    var reasonForSkip: String?
}

protocol RXMedicineDelegate: class {
    func didReceiveMedicineData(data: RxMedicalAdviceData)
}

protocol RXMedicineViewDelegate: class {
    func rxCancelBtnClicked(index: Int)
}

protocol DXMedicineViewDelegate: class {
    func dxCancelBtnClicked(index: Int)
}

class MedicalAdviceBaseViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, RXMedicineDelegate, RXMedicineViewDelegate, DXMedicineViewDelegate {

    @IBOutlet weak var bottombarview: UIView!

    var  medicalAdviceTVC : MedicalAdviceTableViewController?
    var signatureimgbool: Bool = false
    var nav: UINavigationController?
    var rxArray = [RxMedicalAdviceData]()
    var dxArray = [DxMedicalAdviceData]()
    var dxViews = [DXMedicineView]()

    var ConsultTablerecordIDpassed = ""
    var patientnamepassed = ""
    var patientgenderpassed = ""
    var patientagepassed = ""
    var patientcalldoccodepassed = ""
    var patientpatuseridpassed:UInt = 0
    var consultType = ""
    var existingprescriptionIDpassed = ""
    var isFirstConsult = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Medical Advice"

        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.tintColor =  UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        bottombarview.backgroundColor = UIColor(patternImage: UIImage(named: "docprobbarbk")!)
        self.hideKeyboardWhenTappedAround()

        medicalAdviceTVC?.patientnametxt.delegate = self
        medicalAdviceTVC?.patientgendertxt.delegate = self
        medicalAdviceTVC?.patientagetxt.delegate = self
        medicalAdviceTVC?.symptomstxt.delegate = self
        medicalAdviceTVC?.diagnosistxt.delegate = self
        medicalAdviceTVC?.advicetxt.delegate = self
        medicalAdviceTVC?.followupxt.delegate = self
        medicalAdviceTVC?.skipReason.delegate = self

        medicalAdviceTVC?.symptomstxt.textContainer.maximumNumberOfLines = 3
        medicalAdviceTVC?.diagnosistxt.textContainer.maximumNumberOfLines = 3
        medicalAdviceTVC?.advicetxt.textContainer.maximumNumberOfLines = 3
        medicalAdviceTVC?.followupxt.textContainer.maximumNumberOfLines = 3
        medicalAdviceTVC?.skipReason.textContainer.maximumNumberOfLines = 3

        medicalAdviceTVC?.symptomstxt.textContainer.lineBreakMode = .byClipping
        medicalAdviceTVC?.diagnosistxt.textContainer.lineBreakMode = .byClipping
        medicalAdviceTVC?.advicetxt.textContainer.lineBreakMode = .byClipping
        medicalAdviceTVC?.followupxt.textContainer.lineBreakMode = .byClipping
        medicalAdviceTVC?.skipReason.textContainer.lineBreakMode = .byClipping

        medicalAdviceTVC?.patientnametxt.text = patientnamepassed
        medicalAdviceTVC?.patientgendertxt.text = patientgenderpassed
        medicalAdviceTVC?.patientagetxt.text = patientagepassed

        GlobalMedicalAdvicedata.ConsultTablerecordID = ConsultTablerecordIDpassed
        GlobalMedicalAdvicedata.patientname = patientnamepassed
        GlobalMedicalAdvicedata.patientgender = patientgenderpassed
        GlobalMedicalAdvicedata.patientage = patientagepassed
        GlobalMedicalAdvicedata.patientpatuserid = patientpatuseridpassed
        GlobalMedicalAdvicedata.patientcalldoccode = patientcalldoccodepassed

        if isKeyPresentInUserDefaults(key: "charge") {
            print("MedicalAdviceViewController:got the charge value from persistance")
            GlobalVariables.gchargevalueint = UserDefaults.standard.integer(forKey: "charge")
        } else {
            print("MedicalAdviceViewController:Did not get charge value from persistance hence setting it now as default value 0")
            UserDefaults.standard.set(0, forKey: "charge")
            UserDefaults.standard.synchronize()
            GlobalVariables.gchargevalueint = 0   // setting default value full charge
        }

        if (GlobalVariables.gDoctorFee > 0) {
            medicalAdviceTVC?.fullchargelb.text = "Rs " + String(GlobalVariables.gDoctorFee)
            let fullchargeStr : String? = String(GlobalVariables.gDoctorFee)
            var fullchargeint = (fullchargeStr != nil) ? Int(fullchargeStr!) : 0
            fullchargeint = (fullchargeint != nil) ? fullchargeint! : 0

            GlobalDocData.gfullchargeval = fullchargeint!
            let halfchargeint:Int = fullchargeint!/2
            GlobalDocData.ghalfchargeval = halfchargeint
            medicalAdviceTVC?.halfchargelb.text = "Rs " + String(halfchargeint)
        } else {
            medicalAdviceTVC?.fullchargelb.text = "Rs " + "0"
            medicalAdviceTVC?.halfchargelb.text = "Rs " + "0"
        }

        if GlobalVariables.gchargevalueint == 0 {
            // set green radio button to full charge
            medicalAdviceTVC?.fullchargeButtonview.setImage(#imageLiteral(resourceName: "greenradio"), for: UIControlState.normal)
            medicalAdviceTVC?.halfchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
            medicalAdviceTVC?.waiveoffchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
        }

        if GlobalVariables.gchargevalueint == 1 {
            // set green radio button to half charge button
            medicalAdviceTVC?.fullchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
            medicalAdviceTVC?.halfchargeButtonview.setImage(#imageLiteral(resourceName: "greenradio"), for: UIControlState.normal)
            medicalAdviceTVC?.waiveoffchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
        }

        if GlobalVariables.gchargevalueint == 2 {
            // set green radio button to no charge button
            medicalAdviceTVC?.fullchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
            medicalAdviceTVC?.halfchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
            medicalAdviceTVC?.waiveoffchargeButtonview.setImage(#imageLiteral(resourceName: "greenradio"), for: UIControlState.normal)
        }

        if(GlobalMedicalAdvicedata.editit) {
            GlobalMedicalAdvicedata.editit = false
        }

        medicalAdviceTVC?.buttonAddMedicine.addTarget(self, action: #selector(self.AddMedicineButton(_:)), for: UIControlEvents.touchUpInside)
        medicalAdviceTVC?.buttonAddInvestigation.addTarget(self, action: #selector(self.AddInvestigationButton(_:)), for: UIControlEvents.touchUpInside)
        medicalAdviceTVC?.fullchargeButtonview.addTarget(self, action: #selector(self.fullChargeButton(_:)), for: UIControlEvents.touchUpInside)
        medicalAdviceTVC?.halfchargeButtonview.addTarget(self, action: #selector(self.halfChargeButton(_:)), for: UIControlEvents.touchUpInside)
        medicalAdviceTVC?.waiveoffchargeButtonview.addTarget(self, action: #selector(self.waiveofffeesButton(_:)), for: UIControlEvents.touchUpInside)

    }

    func textViewDidChange(_ textView: UITextView) {
        UIView.performWithoutAnimation {
            self.medicalAdviceTVC?.tableView.beginUpdates()
            self.medicalAdviceTVC?.tableView.endUpdates()
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
    }

    // to check if key is present for persistance key value storage
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction  func SkipMedAdvButton(_ sender: UIButton) {

    }

    @IBAction func AddMedicineButton(_ sender: UIButton) {
        let popOverVC = AddMedicinePopUpTableViewController.storyBoardInstance()
        popOverVC.delegate = self
        self.present(popOverVC, animated: true, completion: nil)
    }


    @IBAction func AddInvestigationButton(_ sender: UIButton) {
        let dxData = DxMedicalAdviceData()
        dxArray.append(dxData)
        self.medicalAdviceTVC?.dxMedicines = dxArray
        let existingDxViews = dxViews
        dxViews = self.medicalAdviceTVC!.configureDxMedicineView()
        for (i,existingDXView) in existingDxViews.enumerated() {
            dxViews[i].instructionsTextField.text = existingDXView.instructionsTextField.text
            dxViews[i].testNameTextField.text = existingDXView.testNameTextField.text
        }
        self.medicalAdviceTVC?.tableView.reloadData()
    }

    @IBAction func fullChargeButton(_ sender: UIButton) {
        UserDefaults.standard.set(0, forKey: "charge")  // set to full charge
        UserDefaults.standard.synchronize()
        GlobalVariables.gchargevalueint = 0
        medicalAdviceTVC?.fullchargeButtonview.setImage(#imageLiteral(resourceName: "greenradio"), for: UIControlState.normal)
        medicalAdviceTVC?.halfchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
        medicalAdviceTVC?.waiveoffchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
    }

    @IBAction func halfChargeButton(_ sender: UIButton) {
        UserDefaults.standard.set(1, forKey: "charge")  //set to half charge
        UserDefaults.standard.synchronize()
        GlobalVariables.gchargevalueint = 1
        medicalAdviceTVC?.fullchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
        medicalAdviceTVC?.halfchargeButtonview.setImage(#imageLiteral(resourceName: "greenradio"), for: UIControlState.normal)
        medicalAdviceTVC?.waiveoffchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
    }

    @IBAction func waiveofffeesButton(_ sender: UIButton) {
        UserDefaults.standard.set(2, forKey: "charge")   // set to no charge
        UserDefaults.standard.synchronize()
        GlobalVariables.gchargevalueint = 2
        medicalAdviceTVC?.fullchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
        medicalAdviceTVC?.halfchargeButtonview.setImage(#imageLiteral(resourceName: "whiteradio"), for: UIControlState.normal)
        medicalAdviceTVC?.waiveoffchargeButtonview.setImage(#imageLiteral(resourceName: "greenradio"), for: UIControlState.normal)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func showAlert(str: String) {
        let alert = UIAlertController(title: "Alert", message: str, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    @IBAction func NextButton(_ sender: UIButton) {

        let isSkipMedicalAdvice = medicalAdviceTVC?.isSkipMedicalAdvice ?? false
        var dxDataArray = [DxMedicalAdviceData]()

        if !isSkipMedicalAdvice {
            guard let symptoms = medicalAdviceTVC?.symptomstxt.text, symptoms.count > 0 else {
                showAlert(str: "Please Enter Symptoms details!")
                return
            }

            guard let provisionalDiagnosis = medicalAdviceTVC?.diagnosistxt.text, provisionalDiagnosis.count > 0 else {
                showAlert(str: "Please Enter Provisional Diagnosis details!")
                return
            }

            guard let advice = medicalAdviceTVC?.advicetxt.text else {
                showAlert(str: "Please Enter Advice details!")
                return
            }

            guard let followUp = medicalAdviceTVC?.followupxt.text else {
                showAlert(str: "Please Enter Follow Up details!")
                return
            }


            for dxView in dxViews {
                var dxData = DxMedicalAdviceData()

                guard let testName = dxView.testNameTextField.text, let instruction = dxView.instructionsTextField.text, testName.count > 0 , instruction.count > 0 else {
                    showAlert(str: "Please Enter the DX Details")
                    return
                }

                dxData.instructions = instruction
                dxData.testName = testName
                dxDataArray.append(dxData)
            }
        }

        var medicalAdviceData = ConfirmMedicalAdviceData()
        medicalAdviceData.symptoms = medicalAdviceTVC?.symptomstxt.text
        medicalAdviceData.provisionalDiagnosis = medicalAdviceTVC?.diagnosistxt.text
        medicalAdviceData.advice = medicalAdviceTVC?.advicetxt.text
        medicalAdviceData.followUp = medicalAdviceTVC?.followupxt.text

        medicalAdviceData.rxMedicines = rxArray
        medicalAdviceData.dxMedicines = dxDataArray

        let rxstring =  encodeRxData(rxArray: rxArray)
        medicalAdviceData.rxMedicinesString = rxstring

        let dxstring =  encodeDxData(dxArray: dxDataArray)
        medicalAdviceData.dxMedicinesString = dxstring

        medicalAdviceData.skipMedicalAdvice = medicalAdviceTVC?.isSkipMedicalAdvice ?? false
        if (medicalAdviceData.skipMedicalAdvice)
        {
            guard let reason = medicalAdviceTVC?.skipReason.text, reason.count > 0 else {
                showAlert(str: "Please Enter Skip Reason!")
                return
            }
            medicalAdviceData.reasonForSkip = medicalAdviceTVC?.skipReason.text

        }
        medicalAdviceData.charge = getConsultationCharges()

        let VC1 = ConfirmMedicalAdviceBaseViewController.storyBoardInstance()
        VC1.medicalData = medicalAdviceData
        VC1.consultType = consultType
        VC1.existingprescriptionIDpassed = existingprescriptionIDpassed
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    func getConsultationCharges() -> Int {
        if GlobalVariables.gchargevalueint == 0 {
            return  GlobalVariables.gDoctorFee //500
        } else if GlobalVariables.gchargevalueint == 1 {
            return  GlobalVariables.gDoctorFee/2 //250
        } else {
            return 0
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MedicalAdviceTableViewControllerSegue" {
            self.medicalAdviceTVC = segue.destination as? MedicalAdviceTableViewController
            self.medicalAdviceTVC?.rxdelegate = self
            self.medicalAdviceTVC?.dxdelegate = self
            let isFirst = isFirstConsult.lowercased()
            if isFirst == "true" {
                 (segue.destination as? MedicalAdviceTableViewController)?.consultType = "NEW"
            } else {
                 (segue.destination as? MedicalAdviceTableViewController)?.consultType = "EXISTING"
            }
           
        }
    }

    func didReceiveMedicineData(data: RxMedicalAdviceData) {
        rxArray.append(data)
        self.medicalAdviceTVC?.rxMedicines = rxArray
        self.medicalAdviceTVC?.configureRxMedicineView()
        self.medicalAdviceTVC?.tableView.reloadData()
    }

    func rxCancelBtnClicked(index: Int) {
        rxArray.remove(at: index)
        self.medicalAdviceTVC?.rxMedicines = rxArray
        self.medicalAdviceTVC?.configureRxMedicineView()
        self.medicalAdviceTVC?.tableView.reloadData()
    }

    func dxCancelBtnClicked(index: Int) {
        dxArray.remove(at: index)
        self.medicalAdviceTVC?.dxMedicines = dxArray

        dxViews.remove(at: index)
        let existingDxViews = dxViews
        dxViews = self.medicalAdviceTVC!.configureDxMedicineView()
        for (i,existingDXView) in existingDxViews.enumerated() {
            dxViews[i].instructionsTextField.text = existingDXView.instructionsTextField.text
            dxViews[i].testNameTextField.text = existingDXView.testNameTextField.text
        }
        self.medicalAdviceTVC?.tableView.reloadData()
    }
    
    func decodeRxData(rxText: String) -> [RxMedicalAdviceData] {
        let rxStrings = rxText.components(separatedBy: "<>")
        var rxDatas = [RxMedicalAdviceData]()
        
        for rx in rxStrings {
            if rx.count > 3 {
                let rxContents = rx.components(separatedBy: "|")
                if rxContents.count == 10 {
                    var rxData = RxMedicalAdviceData()
                    rxData.medicineName = rxContents[0]
                    rxData.genericName = rxContents[1]
                    rxData.dosage = rxContents[2]
                    rxData.form = rxContents[3]
                    rxData.strength = rxContents[4]
                    rxData.afterFood = "\(rxContents[5])" == "After Food" ? true : false
                    rxData.frequency = rxContents[6]
                    rxData.specialRemarks = rxContents[7]
                    rxData.duration = rxContents[8]
                    rxData.durationType = rxContents[9]
                    rxDatas.append(rxData)
                }
            }
        }
        
        return rxDatas
    }
    
    func decodeDXData(dxText: String) -> [DxMedicalAdviceData] {
        let dxStrings = dxText.components(separatedBy: "<>")
        var dxDatas = [DxMedicalAdviceData]()
        
        for dx in dxStrings {
            if dx.count > 3 {
                let dxContents = dx.components(separatedBy: "|")
                if dxContents.count == 2 {
                    var dxData = DxMedicalAdviceData()
                    dxData.testName = dxContents[0]
                    dxData.instructions = dxContents[1]
                    dxDatas.append(dxData)
                }
            }
        }
        
        return dxDatas
    }
    
    func encodeRxData(rxArray: [RxMedicalAdviceData]) -> String {
        var rxStr = ""
        for rxData in rxArray {
            rxStr.append("\(rxData.medicineName)|\(rxData.genericName)|\(rxData.dosage)|\(rxData.form)|\(rxData.strength)|\(rxData.afterFood == true ? "After Food" : "Before Food")|\(rxData.frequency)|\(rxData.specialRemarks ?? "Nil")|\(rxData.duration)|\(rxData.durationType)<>")
        }
        return rxStr
    }
    
    func encodeDxData(dxArray: [DxMedicalAdviceData]) -> String {
        var dxStr = ""
        for dxData in dxArray {
            dxStr.append("\(dxData.testName)|\(dxData.instructions)<>")
        }
        return dxStr
    }

}
