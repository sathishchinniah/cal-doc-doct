import UIKit

class ConfirmMedicalAdviceTableViewController: UITableViewController {

    @IBOutlet var signatureimg: UIImageView!
    @IBOutlet var patientnamelb: UILabel!
    @IBOutlet var patientgenderlb: UILabel!
    @IBOutlet var patientagelb: UILabel!
    @IBOutlet var labelofSymptiom: UILabel!  // this is used to set text as Reason for skip prescription
    @IBOutlet var symptomdlb: UILabel!
    @IBOutlet var labelofProvisionalDiagnosis: UILabel!
    @IBOutlet var provisionaldiagnosislb: UILabel!
    @IBOutlet var advicelb: UILabel!
    @IBOutlet var follopuplb: UILabel!
    @IBOutlet var doctornameib: UILabel!
    @IBOutlet var doctordegreelb: UILabel!
    @IBOutlet var mcinumlb: UILabel!
    @IBOutlet weak var consultationChargesLabel: UILabel!
    @IBOutlet weak var callDocChargesLabel: UILabel!
    @IBOutlet var datelb: UILabel!
    @IBOutlet var btnSignature: UIButton!
    @IBOutlet weak var rxItemsView: UIView!
    @IBOutlet weak var rxItemsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dxItemsView: UIView!
    @IBOutlet weak var dxItemsViewHeightConstraint: NSLayoutConstraint!

    var medicalData: ConfirmMedicalAdviceData?
    var confirmMedicalAdviceHeight: [CGFloat] = [69, 71, 60, 60, 50, 180, 60, 145, 60, 61, 140, 122]
    var skipRxConfirmMedicalAdviceHeight: [CGFloat] = [69, 71, 60, 60, 0, 0, 0, 0, 0, 0, 140, 122]

    override func viewDidLoad() {
        super.viewDidLoad()
        configureRxView()
        configureDxView()
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let medicalData = medicalData, medicalData.skipMedicalAdvice {
            return skipRxConfirmMedicalAdviceHeight[indexPath.row]
        }

        if indexPath.row == 4 {
            if let medicalData = medicalData, medicalData.rxMedicines.count == 0 {
                return 0
            }
        }

        if indexPath.row == 5 {
            return CGFloat(180 * (medicalData?.rxMedicines.count ?? 0))
        }

        if indexPath.row == 6 {
            if let medicalData = medicalData, medicalData.dxMedicines.count == 0 {
                return 0
            }
        }

        if indexPath.row == 7 {
            return CGFloat(145 * (medicalData?.dxMedicines.count ?? 0))
        }

        return confirmMedicalAdviceHeight[indexPath.row]
    }

    func configureRxView() {
        if let rxMedicines = medicalData?.rxMedicines {
            rxItemsView.subviews.forEach { $0.removeFromSuperview() }
            rxItemsViewHeightConstraint.constant = CGFloat(180 * rxMedicines.count)

            for (i, medicine) in rxMedicines.enumerated() {

                let rxView = RXConfirmMedicineView()
                rxItemsView.addSubview(rxView)

                rxView.snp.makeConstraints { (make) in
                    make.height.equalTo(160)
                    make.leading.equalToSuperview()
                    make.trailing.equalToSuperview()
                    make.top.equalTo(i * 160)
                }

                rxView.configureView(rxData: medicine)
            }
        }
    }

    func configureDxView() {
        if let dxMedicines = medicalData?.dxMedicines {
            dxItemsView.subviews.forEach { $0.removeFromSuperview() }
            dxItemsViewHeightConstraint.constant = CGFloat(145 * dxMedicines.count)

            for (i, medicine) in dxMedicines.enumerated() {

                let dxView = DXConfirmMedicineView()
                dxItemsView.addSubview(dxView)

                dxView.snp.makeConstraints { (make) in
                    make.height.equalTo(145)
                    make.leading.equalToSuperview()
                    make.trailing.equalToSuperview()
                    make.top.equalTo(i * 145)
                }

                dxView.configureView(dxData: medicine)
            }
        }
    }

}
