import UIKit
import DropDown

class AddMedicinePopUpTableViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet var medicinenametxt: UITextField!
    @IBOutlet var formtxt: UITextField!
    @IBOutlet var dosagetxt: UITextField! {
        didSet {
            setupTextFieldKeyboard(textField: dosagetxt)
        }
    }
    @IBOutlet var specialRemarktxt: UITextField!
    @IBOutlet var genericnametxt: UITextField!
    @IBOutlet var strengthtxt: UITextField!
    @IBOutlet var frequencytxt: UITextField!
    @IBOutlet var durationnumtxt: UITextField!{
        didSet {
            setupTextFieldKeyboard(textField: durationnumtxt)
        }
    }
    @IBOutlet var durationdaystxt: UITextField!
    @IBOutlet var beforefoodradioimg: UIImageView!
    @IBOutlet var afterfoodradioimg: UIImageView!
    @IBOutlet var formPopView: UIView!
    @IBOutlet var frequencyPopView: UIView!
    @IBOutlet var daysPopView: UIView!

    var textToAppendRX1 = ""
    var textToAppendRX2 = ""
    var boolbeforeorafterfood:Bool = false
    var afterFood = true
    var nav: UINavigationController?
    var activeField : UITextField!
    weak var delegate: RXMedicineDelegate?

    let dropDownForm = DropDown()
    let dropDownDays = DropDown()
    let dropDownFrequency = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        self.hideKeyboardWhenTappedAround()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

        activeField = textField

        if (textField == self.formtxt) {
            hideKeyboardWhenTappedAround()
            textField.resignFirstResponder()
            textField.endEditing(true)
            self.view.endEditing(true)

            dropDownForm.anchorView = formPopView
            dropDownForm.dataSource = ["Syrup Teaspoon", "Syrup Tablespoon", "Drops", "Tablets", "Capsules", "Injections", "Patches", "Others"]
            dropDownForm.show()
            dropDownForm.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.formtxt.text = item
            }
        }

        if (textField == self.durationdaystxt) {
            hideKeyboardWhenTappedAround()
            textField.resignFirstResponder()
            textField.endEditing(true)
            self.view.endEditing(true)

            dropDownDays.anchorView = daysPopView
            dropDownDays.dataSource = ["Days(s)","Week(s)","Month(s)"]
            dropDownDays.show()
            dropDownDays.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.durationdaystxt.text = item
            }
        }

        if (textField == self.frequencytxt) {
            hideKeyboardWhenTappedAround()
            textField.resignFirstResponder()
            //textField.endEditing(true)
            //self.view.endEditing(true)

            dropDownFrequency.anchorView = frequencyPopView
            dropDownFrequency.dataSource = ["1-0-0", "0-1-0","1-0-1","1-1-1","1-1-1-1",]
            dropDownFrequency.show()
            dropDownFrequency.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.frequencytxt.text = item
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }


    @IBAction func closeButton(_ sender: UIButton) {
        navigationController?.isNavigationBarHidden = false
        self.removeAnimate()
    }

    func showAlert(msg: String) {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    @IBAction func saveButton(_ sender: UIButton) {
        navigationController?.isNavigationBarHidden = false

        guard let medicineTxt = medicinenametxt.text, medicineTxt.count > 0 else {
            showAlert(msg: "Please Enter Medicine details!")
            return
        }

        guard let frequencyTxt = frequencytxt.text, frequencyTxt.count > 0 else {
            showAlert(msg: "Please Enter Medicine frequency!")
            return
        }

        guard let dosageTxt = dosagetxt.text, dosageTxt.count > 0 else {
            showAlert(msg: "Please Enter Medicine dosage!")
            return
        }

        guard let durationNumTxt = durationnumtxt.text, durationNumTxt.count > 0 else {
            showAlert(msg: "Please Enter Medicine duration!")
            return
        }

        guard let durDaytxt = durationdaystxt.text, durDaytxt.count > 0 else {
            showAlert(msg: "Please Enter Medicine days/weeks/months!")
            return
        }

        guard let ftxt = formtxt.text, ftxt.count > 0 else {
            showAlert(msg: "Please Enter Medicine type!")
            return
        }

        guard let strengthText = strengthtxt.text, strengthText.count > 0 else {
            showAlert(msg: "Please Enter Medicine generic Name!")
            return
        }

        var rxData = RxMedicalAdviceData()
        rxData.medicineName = medicineTxt
        rxData.dosage = dosageTxt
        rxData.form = ftxt
        rxData.genericName = genericnametxt.text ?? ""
        rxData.strength = strengthText
        rxData.frequency = frequencyTxt
        rxData.duration = durationNumTxt
        rxData.durationType = durDaytxt
        rxData.specialRemarks = specialRemarktxt.text ?? ""
        rxData.afterFood = afterFood

        delegate?.didReceiveMedicineData(data: rxData)

        self.removeAnimate()

    }

    func showAnimate() {
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.0, animations: {
            self.view.alpha = 1.0
        });
    }

    func removeAnimate() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func BeforefoodButton(_ sender: UIButton) {
        beforefoodradioimg.image = #imageLiteral(resourceName: "radiogreen")
        afterfoodradioimg.image = #imageLiteral(resourceName: "radiowhite")
        afterFood = false
    }

    @IBAction func AfterfoodButton(_ sender: UIButton) {
        afterfoodradioimg.image = #imageLiteral(resourceName: "radiogreen")
        beforefoodradioimg.image = #imageLiteral(resourceName: "radiowhite")
        afterFood = true
    }

    static func storyBoardInstance() ->  AddMedicinePopUpTableViewController {
        let storyboard = UIStoryboard.init(name: "MedicalStoryboard", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AddMedicinePopUpTableViewController") as! AddMedicinePopUpTableViewController
    }

    func setupTextFieldKeyboard(textField: UITextField) {
        let textFieldToolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        textFieldToolBar.barStyle = UIBarStyle.default
        textFieldToolBar.items = [UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action:nil), UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done, target: nil, action: #selector(AddMedicinePopUpTableViewController.doneClicked))]
        textFieldToolBar.sizeToFit()
        textField.inputAccessoryView = textFieldToolBar
    }

    @objc func doneClicked() {
        dosagetxt.resignFirstResponder()
        durationnumtxt.resignFirstResponder()
    }

}

