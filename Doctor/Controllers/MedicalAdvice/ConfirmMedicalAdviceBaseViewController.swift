import UIKit
import WebKit

class ConfirmMedicalAdviceBaseViewController: UIViewController ,  UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var viewforPdf: UIScrollView!
    @IBOutlet weak var bottombarview: UIView!

    var nav: UINavigationController?
    var confirmAdviceTVController : ConfirmMedicalAdviceTableViewController?
    var medicalData: ConfirmMedicalAdviceData?
    var ConsultTablerecordIDpassed = ""
    var existingprescriptionIDpassed = ""
    var patientnamepassed = ""
    var patientcalldoccodepassed = ""
    var patientgenderpassed = ""
    var patientagepassed = ""
    var lconsultchagre = 0
    var lcalldoccharge = 0
    var consultType = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Confirm Prescription"
        let navBarColor = self.navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.tintColor =  UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        bottombarview.backgroundColor = UIColor(patternImage: UIImage(named: "docprobbarbk")!)
        navigationItem.setHidesBackButton(true, animated: true)  // hide left Back button
        initialize()
    }

    func initialize() {
        self.confirmAdviceTVController?.datelb.text = Date().string(format: "dd/MM/yyyy")
        self.confirmAdviceTVController?.patientnamelb.text = GlobalMedicalAdvicedata.patientname
        self.confirmAdviceTVController?.patientgenderlb.text = GlobalMedicalAdvicedata.patientgender
        self.confirmAdviceTVController?.patientagelb.text = GlobalMedicalAdvicedata.patientage

        if (medicalData?.skipMedicalAdvice)! {
            self.confirmAdviceTVController?.labelofSymptiom.text = "Prescription not provided, Reason:"
            self.confirmAdviceTVController?.symptomdlb.lineBreakMode = .byWordWrapping
            self.confirmAdviceTVController?.symptomdlb.numberOfLines = 0
            self.confirmAdviceTVController?.symptomdlb.text = medicalData?.reasonForSkip
            self.confirmAdviceTVController?.labelofProvisionalDiagnosis.isHidden = true
            self.confirmAdviceTVController?.provisionaldiagnosislb.isHidden = true
        } else {
            self.confirmAdviceTVController?.symptomdlb.lineBreakMode = .byWordWrapping
            self.confirmAdviceTVController?.symptomdlb.numberOfLines = 0
            self.confirmAdviceTVController?.symptomdlb.text = medicalData?.symptoms

            self.confirmAdviceTVController?.provisionaldiagnosislb.lineBreakMode = .byWordWrapping
            self.confirmAdviceTVController?.provisionaldiagnosislb.numberOfLines = 0
            self.confirmAdviceTVController?.provisionaldiagnosislb.text = medicalData?.provisionalDiagnosis

            self.confirmAdviceTVController?.advicelb.lineBreakMode = .byWordWrapping
            self.confirmAdviceTVController?.advicelb.numberOfLines = 0
            self.confirmAdviceTVController?.advicelb.text = medicalData?.advice

            self.confirmAdviceTVController?.follopuplb.lineBreakMode = .byWordWrapping
            self.confirmAdviceTVController?.follopuplb.numberOfLines = 0
            self.confirmAdviceTVController?.follopuplb.text = medicalData?.followUp
        }
        self.confirmAdviceTVController?.doctornameib.text = "Dr. " + GlobalDocData.gdocname
        self.confirmAdviceTVController?.doctordegreelb.text = GlobalDocData.gdegree
        self.confirmAdviceTVController?.mcinumlb.text = "MCI Reg No.  " + GlobalDocData.gmcinum

        self.confirmAdviceTVController?.callDocChargesLabel.text = getCharges().callDoc
        self.confirmAdviceTVController?.consultationChargesLabel.text = getCharges().consultation

        LoadSignatureImage()

        confirmAdviceTVController?.btnSignature.addTarget(self, action: #selector(self.SignatureButton(_:)), for: UIControlEvents.touchUpInside)
    }

    func getCharges() -> (consultation: String, callDoc: String) {
        if let medicalData = medicalData {
            let charges = medicalData.charge
            if charges == 0 {
                lconsultchagre = 0
                lcalldoccharge = 0
                return ("Rs. 0","Rs. 0")
            }
            let callDocCharges = Int(Double(charges) * 0.2)
            lconsultchagre = charges - callDocCharges
            lcalldoccharge = callDocCharges
            return ("Rs. \(charges - callDocCharges)", "Rs. \(callDocCharges)")
        }
        return ("Rs. 0","Rs. 0")
    }

    func LoadSignatureImage() {
        print("ConfirmMedicalAdviceBaseViewController: LoadSignatureImage:  \(GlobalDocData.gsamplesignatureURL)")
        self.confirmAdviceTVController?.signatureimg.sd_setImage(with: URL(string: GlobalDocData.gsamplesignatureURL), placeholderImage: UIImage(named: "signaturesample@2x.png"), options: .cacheMemoryOnly)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
    }

    // to check if key is present for persistance key value storage
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }

    @IBAction func ConfirmButton(_ sender: UIButton) {

        LoadingIndicatorView.show("Generating and sending Prescription....")

        // This is big place now we have prescription Data and also ConsultTablerecordIDpassed , what we have do here is
        // as doctors has pressed the confirm button we need to first store prescription to Prescription Table

        if consultType == "NEW" {
            self.CreateNewPrescription()
        } else if consultType == "EXISTING" {
            self.CreatePrescriptionAndUpdateConsultTable()
        }

    }

    @IBAction func EditButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func SignatureButton(_ sender: UIButton) {

    }
    
  /*  func getCheckSum(customerID:String, amount:String, user_email:String, user_mobile:String) {
        MyLoader.showLoadingView()
        let params:[String: Any] = ["CUST_ID":custID, "TXN_AMOUNT":"540.00", "MID":"FlotaS90100524961231"/*,"MOBILE_NO":"9003999889", "EMAIL":"@gopi@gmail.com"*/, "ORDER_ID":orderID, "INDUSTRY_TYPE_ID":"Retail", "CHANNEL_ID":"WAP","WEBSITE":"APPSTAGING", "CALLBACK_URL":"https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=\(orderID)"]
        //"https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp"]
        //"https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=\(orderID)"]
        //"https://pguat.paytm.com/paytmchecksum/paytmCheckSumVerify.jsp"]
        
        //["CUST_ID":customerID, "TXN_AMOUNT":amount, "email":user_email, "phone":user_mobile]
        //  printLog(log: params)
        postRequest(CheckSumGenerationURL, params: params as [String : AnyObject]?,oauth: true, result: {
            (response: JSON?, error: NSError?, statuscode: Int) in
            MyLoader.hideLoadingView()
            guard error == nil else {
                return
            }
            if response!["status"].stringValue == "fail" {
                printLog(log: response!["reason"].stringValue)
            } else {
                printLog(log: response!)
                if statuscode == 200
                {
//                    self.showTransectionController(txn_amount: "540.00"/*response!["TXN_AMOUNT"].stringValue*/, orderId: response!["ORDER_ID"].stringValue, checksumhash: response!["CHECKSUMHASH"].stringValue, cust_id: custID/*response!["CUST_ID"].stringValue*/, mobile_no: user_mobile, email: user_email)
                    self.initiateRefund(txn_amount: amount, orderId: response!["ORDER_ID"].stringValue, checksumhash: response!["CHECKSUMHASH"].stringValue, cust_id: custID, mobile_no: user_mobile, email: user_email)
                }
            }
        })
    } */
    
    func initiateRefund(txn_amount: String, orderId: String, checksumhash: String, cust_id: String, mobile_no: String, email: String) {
        var transactionURL = "https://securegw-stage.paytm.in/refund/HANDLER_INTERNAL/getRefundStatus"
        var merchantMid = "FlotaS90100524961231"//"rxazcv89315285244163"
        var orderId = orderId//"order1"
        var merchantKey = "gKpu7IKaLSbkchFS"
        var transactionType = "REFUND"
        var refundAmount = 50
        var transactionId = "20180914111212800110168018200018021"
        var refId = "reforder1"
        var comment = "doctor initiated refund"
        
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = "80951"
        QBRequest.objects(withClassName: "Payments", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            orderId = (contributors![0].fields?.value(forKey: "transactionId") as? String)!
            refundAmount = (contributors![0].fields?.value(forKey: "totalAmount") as? Int)!
            transactionId = (contributors![0].fields?.value(forKey: "transactionId") as? String)!
            refId = (contributors![0].fields?.value(forKey: "consultId") as? String)!
            
            
            var params:[String : Any] = [String : Any]()
            
            var checksum = checksumhash//""
            
            params["MID"] = merchantMid
            params["REFID"] = NSUUID().uuidString.lowercased()//refId
            params["TXNID"] = transactionId
            params["ORDERID"] = orderId
            params["REFUNDAMOUNT"] = refundAmount
            params["TXNTYPE"] = transactionType
            params["COMMENTS"] = comment
            params["CHECKSUM"] = checksumhash//comment
            
            print(params)
            
            let url = URL(string: transactionURL)!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let postString = "id=13&name=Jack"
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else { return }
            request.httpBody = httpBody
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                print("responseString = \(responseString)")
            }
            task.resume()
            
            
            
            
        }) { (errorresponse) in
            print("HomeViewController: DoctorsDetails() Response error: \(String(describing: errorresponse.error?.description))")
        }
        
        
        
        switch GlobalVariables.gchargevalueint {
        case 1:
            print("Half refund")
        case 2:
            print("Full refund")
        default:
            print("fullcarge")
        }
        
    }
    
    func generateRandomCharacters(with prefix:String) -> String {
        
        let letters : NSString = "0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< 9 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return prefix + randomString
    }
    
    func CreateNewPrescription() {
        
        if GlobalVariables.gchargevalueint != 0
        {
           // getCheckSum(customerID: generateRandomCharacters(with: "CUST"), amount: String(GlobalVariables.gchargevalueint), user_email: "", user_mobile: "")
        }
        
        let object = QBCOCustomObject()
        object.className = "PrescriptionTable"
        object.fields["docfullname"] =  GlobalDocData.gdocname
        object.fields["docdegrees"] =  GlobalDocData.gdegree
        object.fields["docmci"] =  GlobalDocData.gmcinum
        object.fields["docsignatureUID"] =  GlobalDocData.gsamplesignatureURL
        object.fields["patuserid"] =  GlobalMedicalAdvicedata.patientpatuserid
        object.fields["patfullname"] =  GlobalMedicalAdvicedata.patientname
        object.fields["patgender"] =  GlobalMedicalAdvicedata.patientgender
        object.fields["patage"] =  GlobalMedicalAdvicedata.patientage
        object.fields["skipadvice"] = medicalData?.skipMedicalAdvice
        object.fields["reason"] =  medicalData?.reasonForSkip
        object.fields["symptoms"] =  medicalData?.symptoms
        object.fields["prodiag"] =  medicalData?.provisionalDiagnosis
        object.fields["rxdata"] =  medicalData?.rxMedicinesString
        object.fields["dxdata"] =  medicalData?.dxMedicinesString
        object.fields["advice"] =  medicalData?.advice
        object.fields["followup"] =  medicalData?.followUp
        object.fields["waiver"] =  false
        object.fields["doctoramount"] =  lconsultchagre
        object.fields["cdcharges"] =   lcalldoccharge
        let totalcharge = lconsultchagre + lcalldoccharge
        object.fields["totalamount"] = totalcharge

        QBRequest.createObject(object, successBlock: { (response, contributors) in
            print("ConfirmMedicalAdviceBaseViewController:CreateNewPrescription(): success")
            let recordIDprescription = (contributors?.id)!
            self.UpdateConsultTableforPrescriptionrID(recordIDprescription: recordIDprescription)
        }) { (response) in
            LoadingIndicatorView.hide()
            print("ConfirmMedicalAdviceBaseViewController:CreateNewPrescription(): Response error: \(String(describing: response.error?.description))")
        }
    }

    func UpdateConsultTableforPrescriptionrID(recordIDprescription: String) {
        let object = QBCOCustomObject()
        object.className = "ConsultsTable"
        object.fields["prescriptionid"] =  recordIDprescription
        object.fields["status"] = "open"
        object.id = GlobalMedicalAdvicedata.ConsultTablerecordID
        QBRequest.update(object, successBlock: { (response, contributors) in
            // now here we can generate PDF and show it to doctor for prescription view before going to home page
            self.sendPushToOpponentsAboutPrescription()
        }) { (response) in
            LoadingIndicatorView.hide()
            print("ConfirmMedicalAdviceBaseViewController:UpdateConsultTableforPrescriptionrID: Response error: \(String(describing: response.error?.description))")
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ConfirmMedicalAdviceTableViewControllerSegue" {
            self.confirmAdviceTVController = segue.destination as? ConfirmMedicalAdviceTableViewController
            self.confirmAdviceTVController?.medicalData = medicalData
        }
    }

    //MARK:- StoryboardInstance()

    static func storyBoardInstance() -> ConfirmMedicalAdviceBaseViewController {
        let storyboard = UIStoryboard.init(name: "MedicalStoryboard", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ConfirmMedicalAdviceBaseViewController") as! ConfirmMedicalAdviceBaseViewController
    }
    
    func CreatePrescriptionAndUpdateConsultTable() {
        if GlobalVariables.gchargevalueint != 0
        {
          //  getCheckSum(customerID: generateRandomCharacters(with: "CUST"), amount: String(GlobalVariables.gchargevalueint), user_email: "", user_mobile: "")
        }
        let object = QBCOCustomObject()
        object.className = "PrescriptionTable"
        object.fields["docfullname"] =  GlobalDocData.gdocname
        object.fields["docdegrees"] =  GlobalDocData.gdegree
        object.fields["docmci"] =  GlobalDocData.gmcinum
        object.fields["docsignatureUID"] =  GlobalDocData.gsamplesignatureURL
        object.fields["patuserid"] =  GlobalMedicalAdvicedata.patientpatuserid
        object.fields["patfullname"] =  GlobalMedicalAdvicedata.patientname
        object.fields["patgender"] =  GlobalMedicalAdvicedata.patientgender
        object.fields["patage"] =  GlobalMedicalAdvicedata.patientage
        object.fields["skipadvice"] = medicalData?.skipMedicalAdvice
        object.fields["reason"] =  medicalData?.reasonForSkip
        object.fields["symptoms"] =  medicalData?.symptoms
        object.fields["prodiag"] =  medicalData?.provisionalDiagnosis
        object.fields["rxdata"] =  medicalData?.rxMedicinesString
        object.fields["dxdata"] =  medicalData?.dxMedicinesString
        object.fields["advice"] =  medicalData?.advice
        object.fields["followup"] =  medicalData?.followUp
        object.fields["waiver"] =  false
        object.fields["doctoramount"] =  lconsultchagre
        object.fields["cdcharges"] =   lcalldoccharge
        let totalcharge = lconsultchagre + lcalldoccharge
        object.fields["totalamount"] = totalcharge

        QBRequest.createObject(object, successBlock: { (response, contributors) in
            let recordIDprescription = (contributors?.id)!
            self.UpdateConsultTableforExitingPrescriptionrID(recordIDprescription: recordIDprescription)
        }) { (response) in
            LoadingIndicatorView.hide()
            print("ConfirmMedicalAdviceBaseViewController:CreatePrescriptionAndUpdateConsultTable(): Response error: \(String(describing: response.error?.description))")
        }

    }
    
    func UpdateConsultTableforExitingPrescriptionrID(recordIDprescription: String) {
        let object = QBCOCustomObject()
        object.className = "ConsultsTable"
        if existingprescriptionIDpassed == nil || existingprescriptionIDpassed == "" || existingprescriptionIDpassed == " " {
            object.fields["prescriptionid"] = recordIDprescription
        } else {
            object.fields["prescriptionid"] = existingprescriptionIDpassed + "," + recordIDprescription
        }
        
        object.fields["status"] = "open"
        object.id = GlobalMedicalAdvicedata.ConsultTablerecordID
        QBRequest.update(object, successBlock: { (response, contributors) in
            // now here we can generate PDF and show it to doctor for prescription view before going to home page
            self.sendPushToOpponentsAboutPrescription()
        }) { (response) in
            LoadingIndicatorView.hide()
            print("ConfirmMedicalAdviceBaseViewController:UpdateConsultTableforPrescriptionrID: Response error: \(String(describing: response.error?.description))")
        }
    }

    func sendPushToOpponentsAboutPrescription() {
        let userid = GlobalMedicalAdvicedata.patientpatuserid
        let currentUserId = "\(userid)"
        let currentUserLogin = QBSession.current.currentUser!.fullName
        
        QBRequest.sendPush(withText: "\("Dr " + currentUserLogin! ) has sent you Prescription", toUsers: currentUserId, successBlock: { (response: QBResponse, event:[QBMEvent]?) in
            LoadingIndicatorView.hide()

            let homeViewController = HomeViewController.storyBoardInstance()
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: false, completion: nil)

        }) { (error:QBError?) in
            
            LoadingIndicatorView.hide()
            let homeViewController = HomeViewController.storyBoardInstance()
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: false, completion: nil)
        }
    }
    
}

extension Date {

    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
}

