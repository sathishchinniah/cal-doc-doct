import UIKit

class DoctorConsultChargeTableViewController: UITableViewController {
    
    var topView : HeaderViewWithImage?
    
    @IBOutlet weak var feestxt: UITextField!
    
    func doneButtonTappedForfeestxt() {
        feestxt.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenHeight  = UIScreen.main.bounds.height
        let totalHeaderHeight = 0.29 * screenHeight
        self.topView = HeaderViewWithImage.instantiateFromNib()
        self.topView?.activityIndicatordocimg.isHidden = true
        self.tableView.setParallaxHeader(self.topView!, mode: .topFill, height: totalHeaderHeight)
    }
    
    // MARK: Scroll Delegates
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.tableView.shouldPositionParallaxHeader()
    }
    
}


