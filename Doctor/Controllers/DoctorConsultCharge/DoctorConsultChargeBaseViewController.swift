import UIKit

class DoctorConsultChargeBaseViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var footerView: UIView!
    @IBOutlet var footerInnerView: UIView!
    @IBOutlet var okButtonview: UIButton!

    var topView : HeaderViewWithImage?
    var docConsultingController : DoctorConsultChargeTableViewController?
    let button = UIButton(type: UIButtonType.custom)
    var nav: UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layer1 = docConsultingController?.topView?.line1
        layer1?.alpha = 0.40
        layer1?.layer.borderWidth = 1
        layer1?.layer.borderColor = UIColor.white.cgColor
        
        let layer2 = docConsultingController?.topView?.line2
        layer2?.alpha = 0.40
        layer2?.layer.borderWidth = 1
        layer2?.layer.borderColor = UIColor.white.cgColor
        
        docConsultingController?.topView?.circle2.setImage(#imageLiteral(resourceName: "whitecircle"), for: .normal)
        docConsultingController?.topView?.circle3.setImage(#imageLiteral(resourceName: "whitecircle"), for: .normal)
        footerInnerView.backgroundColor = UIColor(patternImage: UIImage(named: "schbottombarbg")!)
        self.initialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initialize() -> Void {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(gesture:)))
        view.addGestureRecognizer(tapGesture)
        
        self.docConsultingController?.feestxt.delegate = self
        
        if !GlobalDocData.gdocspeciality.isEmpty {
            topView?.labelUserRpleType.text = GlobalDocData.displayspeciality
        }

        if phone && maxLength == 568 {
            //iphone 5
            self.docConsultingController?.topView?.profileimageYConstraint.constant = 40.0
            
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            self.docConsultingController?.topView?.profileimageYConstraint.constant = 40.0
            
        }
        
        
        let isonline = UserDefaults.standard.bool(forKey: "doctoronline")
        
        if(isonline) {
            topView?.imageViewProfileIcon.layer.borderColor = UIColor.green.cgColor
        } else {
            topView?.imageViewProfileIcon.layer.borderColor = UIColor.orange.cgColor
        }

        LoadProfileImage()
        
        if GlobalDocData.firsttimeviewcharges {
            print( "DoctorConsultChargeBaseViewController: this is for first time user" )
            // do this for existing user
            if isKeyPresentInUserDefaults(key: "existinguser") {
                // This is first time user from Existing User flow

                let isexistinguser = UserDefaults.standard.object(forKey: "existinguser") as! Bool
                if(isexistinguser) {
                    print( "AccountDetailsDocViewController: This is first time user from Existing User flow" )
                    if let existingusername = UserDefaults.standard.string(forKey: "existinguserfullname")  {
                        GlobalDocData.gdocname = existingusername
                    }
                    if let existingusercustomdata = UserDefaults.standard.string(forKey: "usercustomdata")  {
                        GlobalDocData.gusercustomdata = existingusercustomdata
                    }
                    self.firsttimeReadingExistingUserdata()

                } else {

                    // This is first time user from Registration flow
                    self.upadteUserdata()
                    
                    print( "DoctorConsultChargeBaseViewController:  This is first time user from Registration flow" )
                    if let tmpgdocname = UserDefaults.standard.string(forKey: "fullname")  {
                        GlobalDocData.gdocname = tmpgdocname
                    }
                    UserDefaults.standard.setValue("", forKey: "fees")
                    UserDefaults.standard.synchronize()
                    self.docConsultingController?.feestxt.text = ""
                }
            } else {
                // Something not right
                print( "DoctorConsultChargeBaseViewController: Something is not right existinguser key is not found")
            }
        } else {
            // this is for not first time user
            print( "DoctorConsultChargeBaseViewController: this is for not first time user ")
            
            if isKeyPresentInUserDefaults(key: "feeamount") {
                let val =  Int8(UserDefaults.standard.integer(forKey: "feeamount"))
                self.docConsultingController?.feestxt.text = String(val )
                docConsultingController?.feestxt.font =  UIFont(name: "Rubik-Regular", size: 15)
            }
            ReadCustomObjectforDocConsultCharges()
        }
        
        topView?.labelUserName.text = "Dr. " + GlobalDocData.gdocname
        
        docConsultingController?.topView?.labelUserName.text = "Dr. " + GlobalDocData.gdocname
        docConsultingController?.topView?.labelUserRpleType.text = GlobalDocData.displayspeciality
    }
    
    func LoadProfileImage() {
        self.docConsultingController?.topView?.imageViewProfileIcon.layer.cornerRadius = (self.docConsultingController?.topView?.imageViewProfileIcon.frame.size.width)! / 2;
        self.docConsultingController?.topView?.imageViewProfileIcon.clipsToBounds = true;
        self.docConsultingController?.topView?.imageViewProfileIcon.layer.borderWidth = 0.2
        self.docConsultingController?.topView?.imageViewProfileIcon.layer.borderColor = UIColor.lightGray.cgColor
        self.docConsultingController?.topView?.imageViewProfileIcon.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profile"))
    }
    
    @objc func didTapView(gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.addObserver(self, selector: #selector(DoctorConsultChargeBaseViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    @objc func keyboardWillShow(_ note : Notification) -> Void{
        DispatchQueue.main.async { () -> Void in
            self.button.isHidden = false
            let keyBoardWindow = UIApplication.shared.windows.last
            self.button.frame = CGRect(x: 0, y: (keyBoardWindow?.frame.size.height)!-53, width: 106, height: 53)
            keyBoardWindow?.addSubview(self.button)
            keyBoardWindow?.bringSubview(toFront: self.button)
            
            UIView.animate(withDuration: (((note.userInfo! as NSDictionary).object(forKey: UIKeyboardAnimationCurveUserInfoKey) as AnyObject).doubleValue)!, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                self.view.frame = self.view.frame.offsetBy(dx: 0, dy: 0)
            }, completion: { (complete) -> Void in
                // print("Complete")
            })
        }
    }

    // MARK: TextField Delegates
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.docConsultingController?.feestxt {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if (textField.text?.count)! > 5 {
                textField.resignFirstResponder()
            }
            return newString.length <= maxLength
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // to check if key is present for persistance key value storage
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    // MARK: Button Actions
    @IBAction func OkButton(_ sender: UIButton) {
        
        if (self.docConsultingController?.feestxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Entre Your fees amount Here!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.docConsultingController?.feestxt.text, forKey: "fees")
            UserDefaults.standard.synchronize()
        }
        
        if GlobalDocData.firsttimeviewcharges {
            forfirsttimeUPDateDocPrimaryDetailsforDocFee()
        } else {
            UpDateCustomObjectforDocConsultCharges()
        }
    }
    
    @IBAction func PayTMButton(_ sender: UIButton) {
        
    }
    
    func showAlert(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Segue delegate

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DoctorConsultChargeTableViewControllerSegue" {
            self.docConsultingController = segue.destination as? DoctorConsultChargeTableViewController
        }
    }

    func CreateCustomObjectforDocConsultCharges() {

        let object = QBCOCustomObject()
        object.className = "DocConsultationCharge"
        
        object.fields["feeamount"] =  docConsultingController?.feestxt.text
        
        QBRequest.createObject(object, successBlock: { (response, contributors) in

            // we are creating the record ID for doctors Consultation Charges table , this record = customobjIDPrimarydetails will be used later to update record
            UserDefaults.standard.setValue((contributors?.id)!, forKey: "customobjIDConsultationCharges")
            UserDefaults.standard.synchronize()
            print("DoctorConsultChargeBaseViewController:CustomObject: Successfully created Consultation Charges !")
            GlobalDocData.firsttimeviewcharges = false
            
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: true, completion: nil)
            
        }) { (response) in
            print("DoctorConsultChargeBaseViewController:CustomObject: Response error: \(String(describing: response.error?.description))")
        }
    }
    

    func  UpDateCustomObjectforDocConsultCharges() {
        let object = QBCOCustomObject()
        object.className = "DocConsultationCharge"
        object.fields["feeamount"] =  self.docConsultingController?.feestxt.text
        object.id = UserDefaults.standard.string(forKey: "customobjIDConsultationCharges") ?? ""  //record id of consult  charges table
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("DoctorConsultChargeBaseViewController:UpDateCustomObjectforDocConsultCharges: Successfully Updated Consultation Charges !")
            self.UpDateDocPrimaryDetailsforFee()
            
        }) { (response) in
            //Handle Error
            print("DoctorConsultChargeBaseViewController:UpDateCustomObjectforDocConsultCharges: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func UpDateDocPrimaryDetailsforFee() {
        print("DoctorConsultChargeBaseViewController:UpDateDocPrimaryDetailsforFee")
        let object = QBCOCustomObject()
        object.className = "DocPrimaryDetails"
        object.fields["fee"] =  self.docConsultingController?.feestxt.text
        object.id = UserDefaults.standard.string(forKey: "customobjIDPrimarydetails")!
        QBRequest.update(object, successBlock: { (response, contributors) in
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: true, completion: nil)
        }) { (response) in
            print("DoctorConsultChargeBaseViewController:UpDateDocPrimaryDetailsforFee: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func ReadCustomObjectforDocConsultCharges() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocConsultationCharge", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if let contributors = contributors, contributors.count > 0 {
                UserDefaults.standard.setValue((contributors[0].id)!, forKey: "customobjIDConsultationCharges")
                UserDefaults.standard.synchronize()
                let val =  contributors[0].fields?.value(forKey: "feeamount") as? Int
                self.docConsultingController?.feestxt.text = String(val ?? 0)
            }            
        }) { (response) in
            print("DoctorConsultChargeBaseViewController:ReadCustomObjectforDocConsultCharges: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func firsttimeReadingExistingUserdata() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocConsultationCharge", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                UserDefaults.standard.setValue((contributors![0].id)!, forKey: "customobjIDConsultationCharges")
                let val =  contributors![0].fields?.value(forKey: "feeamount") as? Int
                self.docConsultingController?.feestxt.text = String(val ?? 0)
                
                GlobalDocData.gdocname =  (QBSession.current.currentUser?.fullName)!
                self.docConsultingController?.topView?.labelUserName.text = "Dr. " + GlobalDocData.gdocname
                self.docConsultingController?.topView?.labelUserRpleType.text = GlobalDocData.displayspeciality
            }
        }) { (errorresponse) in
            print("DoctorCOnsultChargebaeViewController:firsttimeReadingExistingUserdata() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func  forfirsttimeUPDateDocPrimaryDetailsforDocFee() {
        print("DoctorCOnsultChargebaeViewController:UPDateDocPrimaryDetailsforDocFeeUpdate Custom Object for Doctor Primary Details")
        let object = QBCOCustomObject()
        object.className = "DocPrimaryDetails"
        object.fields["fee"] =  docConsultingController?.feestxt.text
        object.id = UserDefaults.standard.string(forKey: "customobjIDPrimarydetails")!
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("DoctorConsultChargeBaseViewController:UPDateDocPrimaryDetailsforDocFee: Successfully Updated Consultation Charges !")
            self.CreateCustomObjectforDocConsultCharges()

        }) { (response) in
            print("DoctorConsultChargeBaseViewController:UPDateDocPrimaryDetailsforDocFee: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    // This function is noit useed now : 19/5/18
    func ReadexistinguserdataUpdate(objectarray: [Any]) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "DocAccountDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            UserDefaults.standard.setValue((contributors![0].id)!, forKey: "customobjIDConsultationCharges")
            UserDefaults.standard.synchronize()
            let val =  contributors![0].fields?.value(forKey: "feeamount") as? Int
            self.docConsultingController?.feestxt.text = String(val ?? 0)
        }) { (response) in
            print("DoctorConsultChargeBaseViewController:ReadexistinguserdataUpdate: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func upadteUserdata() {
        let ver = versionBuild()
        let ver1 = ver.components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
        let version = "Ver" + ver1
        print("DoctorConsultChargeBaseViewController: UpdateUserData()- Updating  User data for first time")
        let updateParameters = QBUpdateUserParameters()
        updateParameters.tags =  ["Doc1","IOS", version]
        updateParameters.fullName = GlobalDocData.gdocname
        QBRequest.updateCurrentUser(updateParameters, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
            print("DoctorConsultChargeBaseViewController: User data Updated Success!!")
        }, errorBlock: {(_ response: QBResponse) -> Void in
            // Handle error
            print("DoctorConsultChargeBaseViewController: Error in User Update data")
        })
    }
    //MARK: Helpers
    
    func appVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    func build() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    func versionBuild() -> String {
        let version = self.appVersion()
        let build = self.build()
        var versionBuild = String(format:"v%@",version)
        if version != build {
            versionBuild = String(format:"%@(%@)", versionBuild, build )
        }
        return versionBuild
    }
    
}

extension UITextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(cancelButtonTapped))
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: onCancel.target, action: onCancel.action),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc func doneButtonTapped() { self.resignFirstResponder() }
    @objc func cancelButtonTapped() { self.resignFirstResponder() }
}
