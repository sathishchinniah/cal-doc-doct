import UIKit
import MIBadgeButton_Swift

class myClinicTableViewCell: UITableViewCell {
    
    @IBOutlet var patientImage: UIImageView!
    @IBOutlet var patientNamelb: UILabel!
    @IBOutlet var dateslb: UILabel!
    @IBOutlet var myclinicChatBtn: MIBadgeButton!
    @IBOutlet var myclinicCallBtn: UIButton!
    @IBOutlet var statuslabel: UILabel!
    @IBOutlet var cellrightarrowbtn: UIButton!
    @IBOutlet var timelabel: UILabel!
    @IBOutlet var cellBtn: UIButton!
    @IBOutlet weak var cellBadgeLabel: UILabel!
    @IBOutlet var statuslabelwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var billedAmtValueLabel: UILabel!
    
    @IBOutlet weak var extendButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        patientImage.layer.cornerRadius = patientImage.frame.size.width/2
        patientImage.clipsToBounds = true
        patientImage.layer.borderColor = UIColor.black.cgColor
        patientImage.layer.borderWidth = 0.2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
