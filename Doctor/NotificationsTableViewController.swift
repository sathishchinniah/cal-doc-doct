import UIKit

struct cellData {
    var opened = Bool()
    var title = String()
    var sectionData = [String]()
}

class NotificationsTableViewController: UITableViewController {

    var nav: UINavigationController?
    var tableViewData = [cellData]()
    let expandedCellHeight: CGFloat = 150
    let collapsedCellHeight: CGFloat = 80
    var selectedIndex = Int.max
    var invitorRecordID = ""
    var typeofinvite = ""
    var Switch1array: [Bool] = []
    var Switch2array: [Bool] = []
    var TeamSwitcharray: [Bool] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewData = [cellData(opened: false, title: "Title", sectionData: ["cell1"])]

        let navBarColor = navigationController!.navigationBar
        navBarColor.topItem?.title = "NOTIFICATIONS"
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white

    }

    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  NotificationArray.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == selectedIndex {
            return expandedCellHeight
        }
        return collapsedCellHeight
    }

    // The NotificationArray has been populated from InviteTable using inviteeuserid
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.selectionStyle = .none

        cell.docImageView.sd_setImage(with: URL(string: (NotificationArray[indexPath.row].fields?.value(forKey: "profileUID") as? String) ?? ""), placeholderImage: UIImage(named: "profilenonedit"), options: .cacheMemoryOnly)
        cell.docNameLabel.text = "Dr. " + ((NotificationArray[indexPath.row].fields?.value(forKey: "invitorfullname") as? String) ?? "")

        //date and time
        let epochtime =  NotificationArray[indexPath.row].createdAt
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"    //"MMM dd YYYY hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: epochtime!)

        let speciality = (NotificationArray[indexPath.row].fields?.value(forKey: "invitorspeciality") as? String) ?? ""
        cell.docDetailsLabel.text = speciality + "    | " +  dateString

        self.typeofinvite = (NotificationArray[indexPath.row].fields?.value(forKey: "typeofinvite") as? String) ?? ""

        if (self.typeofinvite == "COT") {
            if /*!self.Switch1array.isEmpty*/Switch1array.count > indexPath.row {
                let switch1 =  self.Switch1array[indexPath.row]
                if switch1 {
                    cell.addDocSwitch.isOn = true
                } else {
                    cell.addDocSwitch.isOn = false
                }
            }

            if /*!self.Switch2array.isEmpty*/self.Switch2array.count > indexPath.row {
                let switch2 =  self.Switch2array[indexPath.row]
                if switch2 {
                    cell.referPatientsSwitch.isOn = true
                } else {
                    cell.referPatientsSwitch.isOn = false
                }
            }
            cell.addDocSwitch.addTarget(self, action: #selector(AddDocswitchTriggered), for: .valueChanged)
            cell.addDocSwitch.tag = indexPath.row

            cell.referPatientsSwitch.addTarget(self, action: #selector(referPatientswitchTriggered), for: .valueChanged)
            cell.referPatientsSwitch.tag = indexPath.row
        }

        if (self.typeofinvite == "TEAM") {
            cell.referPatientsSwitch.isHidden = true
            cell.referPatientsLabel.isHidden = true
            if !self.TeamSwitcharray.isEmpty {
               
                let switch2 =  self.TeamSwitcharray.count > indexPath.row ? self.TeamSwitcharray[indexPath.row] : self.TeamSwitcharray.first
                if switch2! {
                    cell.addDocSwitch.isOn = true
                    cell.referPatientsSwitch.isHidden = true
                } else {
                    cell.addDocSwitch.isOn = false
                    cell.referPatientsSwitch.isHidden = true
                }
            }

            cell.addDocSwitch.addTarget(self, action: #selector(AddTeamDocswitchTriggered), for: .valueChanged)
            cell.addDocSwitch.tag = indexPath.row

        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row

        let cellFrame = tableView.rectForRow(at: IndexPath(item: selectedIndex, section: 0))
        if cellFrame.size.height == expandedCellHeight {
            selectedIndex = Int.max
        } else {
            self.typeofinvite = (NotificationArray[indexPath.row].fields?.value(forKey: "typeofinvite") as? String)!
            if (self.typeofinvite == "COT") {
                self.checkfirstoptionSwitchstatus(index: selectedIndex) // this is called when we expand the row
                self.checkSecondoptionSwitchstatus(index: selectedIndex)
            }
            if (self.typeofinvite == "TEAM") {
                self.checkteamSwitchstatus(index: selectedIndex)
            }
        }

        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        self.tableView.scrollToRow(at: indexPath, at: .none, animated: true)
    }
    
    func checkfirstoptionSwitchstatus(index: Int) {
        //first check is there is entry in RelSpeciality Table for invitor and invitee combination ?
        let getRequest = NSMutableDictionary()
        getRequest["inviteedocuserid"] =  ((NotificationArray[index].fields?.value(forKey: "inviteeuserid") as? Int)!)
        getRequest["invitordocuserid"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                let status = contributors![0].fields?.value(forKey: "status") as? Bool
                if status! {
                    if !self.Switch1array.isEmpty {
                        self.Switch1array.insert(true, at: index)
                    } else {
                        self.Switch1array.insert(true, at: 0)
                    }
                } else {
                    if !self.Switch1array.isEmpty {
                        self.Switch1array.insert(false, at: index)
                    } else {
                        self.Switch1array.insert(false, at: 0)
                    }
                }
            } else {
                //there is not entry found that means switch is default off
                if !self.Switch1array.isEmpty {
                    self.Switch1array.insert(false, at: index)
                } else {
                    self.Switch1array.insert(false, at: 0)
                }
                
                
            }
            self.tableView.reloadData()
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("NotificationTableViewController::checkInviteTableforCOTInvite(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    @objc func AddDocswitchTriggered(sender: UISwitch) {
        // Lets very first read the DocCountTable for the Invitor entry for the speciality count value, as we need to cheeck first if count is not greater than 6 , if it is then we can not add doctor to that speciality on Invitor COT
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((NotificationArray[sender.tag].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("NotificationTableViewController:UpdateEntrytoDocCountTable(): Reading record ID of Invitor !")
            self.invitorRecordID = (contributors![0].id)!
            let specialityval =  (contributors![0].fields?.value(forKey: GlobalVariables.gdocspeciality) as? Int) ?? 0
            
            if( specialityval < 6) {
                // here we are doing off to ON of the switch
                if sender.isOn {
                    let msg = "Allow Dr. \(((NotificationArray[sender.tag].fields?.value(forKey: "invitorfullname") as? String)!)) to add you on his/her Circle of Trust and refer their patients to you?"
                    let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "SKIP", style: .default, handler: { (action) in
                        //Perform SKIP actions
                        sender.isOn = false
                        sender.isEnabled = true
                    }))
                    alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
                        //Perform YES actions
                        sender.isOn = true
                        sender.isEnabled = true
                        if self.Switch1array.count > sender.tag {
                            self.Switch1array[sender.tag] = true
                        }
                        
                        self.InviteAcceptedforaddingdocswitch1Actions(index: sender.tag)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {  // here we are dealing with ON to OFF of the switch
                    sender.isOn = false
                    let msg = "Dr. \(((NotificationArray[sender.tag].fields?.value(forKey: "invitorfullname") as? String)!)) will not have you on his/her Circle of Trust ?"
                    let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "SKIP", style: .default, handler: { (action) in
                        //Perform SKIP actions
                        sender.isOn = true
                        sender.isEnabled = true
                    }))
                    alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
                        //Perform YES actions
                        sender.isOn = false
                        sender.isEnabled = true
                        self.InviteAcceptedforRemovaldocswitch1Actions(index: sender.tag)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            } else { // this is when the speciality count is either 6 or greater
                let msg = "At this time you can not be part of Dr. \(((NotificationArray[sender.tag].fields?.value(forKey: "invitorfullname") as? String)!)) Circle of Trust as Max count of doctors at his/her side exceeded!"
                let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    return
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
        }) { (errorresponse) in
            print("NotificationTableViewController:UpdateEntrytoDocCountTable() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    
    func InviteAcceptedforaddingdocswitch1Actions(index: Int) {
        self.updateInviteTableNotificationread(indexval: index, actionflag: true)
        self.MakeEntrytoRelSpecialityTable(index: index)
        self.UpdateEntrytoDocCountTable(index: index, actionflag: true )
    }
    
    func InviteAcceptedforRemovaldocswitch1Actions(index: Int) {
        self.updateInviteTableNotificationread(indexval: index, actionflag: true)
        self.updateRelSpecialityforStatusFalse(index: index)
        self.decerementCountonDocCountTable(index: index)
    }
    
    func updateInviteTableNotificationread(indexval: Int, actionflag: Bool) {
        let object = QBCOCustomObject()
        object.className = "InviteTable"

        object.fields["notificationread"] = true
        object.id = NotificationArray[indexval].id

        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController:: updateInviteTableNotificationread() Successfully Updated!")
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:: updateInviteTableNotificationread(): Response error: \(String(describing: response.error?.description))")
        }
    }

    func MakeEntrytoRelSpecialityTable(index: Int) {
        // before making new entry we need to check if there is already record entry for invitor and invitee combination, hence first we read
        // RelSpeciality Table and see if there is any record

        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        getRequest["inviteedocuserid"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if contributors?.count == 0 {
                // No entry found hence create new entry
                self.CteateNewEntryinRelSpecialityTable(index: index)
                self.UpdateEntrytoDocCountTable(index: index, actionflag: true)
            } else {
                // entry found hence update entry just the status value to true
                let RecordID = (contributors![0].id)!
                self.UpdateRelSpecialityTableforstatus(recordID: RecordID, flag: true)
                self.UpdateEntrytoDocCountTable(index: index, actionflag: true)
            }
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:MakeEntrytoRelSpecialityTable(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func CteateNewEntryinRelSpecialityTable(index: Int) {
        print("NotificationTableViewController: MakeEntrytoRelSpecialityTable(): Creating new entry on RelSpeciality Table")
        let object = QBCOCustomObject()
        object.className = "RelSpeciality"
        let invitoruserid = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? UInt)!)
        object.fields["invitordocuserid"] = invitoruserid
        object.fields["inviteedocuserid"] = QBSession.current.currentUser?.id
        object.fields["inviteefullname"] = QBSession.current.currentUser?.fullName
        object.fields["inviteeprofileimageURL"] = GlobalDocData.gprofileURL
        object.fields["inviteespeciality"] = GlobalVariables.gdocspeciality
        object.fields["invitorprofileimageURL"] = (NotificationArray[index].fields?.value(forKey: "profileUID") as? String) ?? ""
        object.fields["invitorfullname"] = (NotificationArray[index].fields?.value(forKey: "invitorfullname") as? String) ?? ""
        object.fields["status"] = "true"
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController: CteateNewEntryinRelSpecialityTable(): Successfully created New entry in RelSpeciality Table")
            // we should send Push notification to invitor
            self.sendPushToInvitor(invitoruserid: invitoruserid)
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:CteateNewEntryinRelSpecialityTable(): Response error: \(String(describing: response.error?.description))")
        }
    }

    func UpdateRelSpecialityTableforstatus(recordID: String, flag: Bool) {
        let object = QBCOCustomObject()
        object.className = "RelSpeciality"
        if flag {
            object.fields["status"] =  true
        } else {
            object.fields["status"] =  false
        }
        object.id = recordID
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController:UpdateRelSpecialityTableforstatus() Success !")
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:UpdateRelSpecialityTableforstatus() Response error: \(String(describing: response.error?.description))")
        }
    }

    func UpdateEntrytoDocCountTable(index: Int, actionflag: Bool) {
        //first we are reading the DocCounttable to get the record id of Invitor
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int) ?? 0)
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("NotificationTableViewController:UpdateEntrytoDocCountTable(): Reading record ID of Invitor !")
            let invitorRecordID = (contributors![0].id)!
            let specialityval =  (contributors![0].fields?.value(forKey: GlobalVariables.gdocspeciality) as? Int) ?? 0
            self.finallyupdateDocCountTable(invitorRecordID: invitorRecordID, specialityval: specialityval, actionflag: actionflag)
        }) { (errorresponse) in
            print("NotificationTableViewController:UpdateEntrytoDocCountTable() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }

    // here we are updating
    func finallyupdateDocCountTable(invitorRecordID: String, specialityval: Int, actionflag: Bool) {
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        var val = specialityval

        if actionflag {
            val = val+1
        } else {
            val = val-1
        }

        object.fields[GlobalVariables.gdocspeciality] =  val
        object.id = invitorRecordID      //record id of Invitor
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController:finallyupdateDocCountTable() Success !")
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:finallyupdateDocCountTable() Response error: \(String(describing: response.error?.description))")
        }
    }

    //as we are making switch1 off hence make status false
    func updateRelSpecialityforStatusFalse(index: Int) {
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        getRequest["inviteedocuserid"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            let RecordID = (contributors![0].id) ?? ""
            self.UpdateRelSpecialityTableforstatus(recordID: RecordID, flag: false)
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:MakeEntrytoRelSpecialityTable(): Response error: \(String(describing: response.error?.description))")
        }
    }

    func decerementCountonDocCountTable(index: Int) {
        //first we are reading the DocCounttable to get the record id of Invitor
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("NotificationTableViewController:decerementCountonDocCountTable(): Reading record ID of Invitor !")
            let invitorRecordID = (contributors![0].id)!
            let specialityval =  (contributors![0].fields?.value(forKey: GlobalVariables.gdocspeciality) as? Int) ?? 0
            self.finallydecerementDocCountTable(invitorRecordID: invitorRecordID, specialityval: specialityval)
        }) { (errorresponse) in
            print("NotificationTableViewController:decerementCountonDocCountTable() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func finallydecerementDocCountTable(invitorRecordID: String, specialityval: Int) {
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        var val = specialityval
        val = val-1
        object.fields[GlobalVariables.gdocspeciality] =  val
        object.id = invitorRecordID      //record id of Invitor
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController:finallydecerementDocCountTable() Success !")
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:finallydecerementDocCountTable() Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func sendPushToInvitor(invitoruserid: UInt) {
        let currentUserLogin = QBSession.current.currentUser!.fullName
        let userid = "\(invitoruserid)"
        let message = "\("Dr. " + currentUserLogin! ) has accepted your Invitation, he will be part of your COT"
        var payload = [String: AnyObject]()
        var pushKeys = [String: AnyObject]()
        //pushKeys["type"] = "COT" as AnyObject?
        //pushKeys["title"] = "Added to COT"
        pushKeys[QBMPushMessageAlertKey] = message as AnyObject?
        pushKeys["type"] = "COT" as AnyObject? //(NotificationArray[indexPath.row].fields?.value(forKey: "typeofinvite") as? String) ?? ""
        payload[QBMPushMessageApsKey] = pushKeys as AnyObject?
        let pushMessage = QBMPushMessage.init()
        pushMessage.payloadDict = NSMutableDictionary(dictionary: payload)
        QBRequest.sendPush(pushMessage, toUsers: userid, successBlock: { (_, _) in
            print("NotificationTableViewController:sendPushToInvitor(): Push sent!")
        }) { (error:QBError?) in
            print("NotificationTableViewController:sendPushToInvitor():Can not send push: \(error!))")
        }
       /* QBRequest.sendPush(withText: "\("Dr. " + currentUserLogin! ) has accepted your Invitation, he will be part of your COT", toUsers: userid, successBlock: { (response: QBResponse, event:[QBMEvent]?) in
            
        }) { (error:QBError?) in
            print("NotificationTableViewController:sendPushToInvitor():Can not send push: \(error!))")
        } */
    }
    
    //////////////////////////////// The Second Switch Logic start from here  /////////////////////////////////////////////
    func checkSecondoptionSwitchstatus(index: Int) {
        //first check is there is entry in RelSpeciality Table for reverse of invitor and invitee combination ?
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] =  ((NotificationArray[index].fields?.value(forKey: "inviteeuserid") as? Int)!)
        getRequest["inviteedocuserid"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(NotificationTableViewController::checkSecondoptionSwitchstatus(): Successfully  !")
            if ((contributors?.count)! > 0) {
                print("(NotificationTableViewController::checkSecondoptionSwitchstatus():Entry Found for index: \(index)  !")
                let status = contributors![0].fields?.value(forKey: "status") as? Bool
                /*if status! {
                    if self.Switch1array.count > index {
                        self.Switch1array.insert(true, at: index)
                    } else {
                        self.Switch1array.insert(true, at: 0)
                    }
                } else {
                    if self.Switch1array.count > index {
                        self.Switch1array.insert(false, at: index)
                    } else {
                        self.Switch1array.insert(false, at: 0)
                    }
                } */
                if status! {
                    if self.Switch2array.count > index {
                        self.Switch2array.insert(true, at: index)
                    } else {
                        self.Switch2array.insert(true, at: 0)
                    }
                } else {
                    if self.Switch2array.count > index {
                        self.Switch2array.insert(false, at: index)
                    } else {
                        self.Switch2array.insert(false, at: 0)
                    }
                }
            } else {
                print("(NotificationTableViewController::checkSecondoptionSwitchstatus():NO Entry Found in RelSpeciality for index: \(index) !")
                //there is not entry found that means switch is default off
                if !self.Switch2array.isEmpty {
                    self.Switch2array.insert(false, at: index)
                } else {
                    self.Switch2array.insert(false, at: 0)
                }
                
            }
            self.tableView.reloadData()

        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("NotificationTableViewController::checkInviteTableforCOTInvite(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    
    @objc func referPatientswitchTriggered(sender: UISwitch) {
        // lets first Read the DocCount Table the record of Invitee for the speciality of Invitor

        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((NotificationArray[sender.tag].fields?.value(forKey: "inviteeuserid") as? Int)!)
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("NotificationTableViewController:UpdateEntrytoDocCountTable(): Reading record ID of Invitor !")
            self.invitorRecordID = (contributors![0].id)!
            let specialityval =  (contributors![0].fields?.value(forKey: ((NotificationArray[sender.tag].fields?.value(forKey: "invitorspeciality") as? String) ?? "")) as? Int) ?? 0

            if( specialityval < 6) {
                // here we are doing off to ON of the switch
                if sender.isOn {
                    let msg = "Allow Dr. \(((NotificationArray[sender.tag].fields?.value(forKey: "invitorfullname") as? String)!)) to be part of your Circle of Trust."
                    let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "SKIP", style: .default, handler: { (action) in
                        //Perform SKIP actions
                        sender.isOn = false
                        sender.isEnabled = true
                    }))
                    alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
                        //Perform YES actions
                        sender.isOn = true
                        sender.isEnabled = true
                        if self.Switch2array.count > sender.tag {
                            self.Switch2array[sender.tag] = true
                        }
                        self.InviteAcceptedforreferPatientswitch2Actions(index: sender.tag)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {  // here we are dealing with ON to OFF of the switch
                    sender.isOn = false
                    let msg = "Dr. \(((NotificationArray[sender.tag].fields?.value(forKey: "invitorfullname") as? String)!)) will not be part of yourr Circle of Trust ?"
                    let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "SKIP", style: .default, handler: { (action) in
                        //Perform SKIP actions
                        sender.isOn = true
                        sender.isEnabled = true
                    }))
                    alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
                        //Perform YES actions
                        sender.isOn = false
                        sender.isEnabled = true
                        self.InviteAcceptedforRemovalreferPatientswitch2Actions(index: sender.tag)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            } else { // this is when the speciality count is either 6 or greater
                let msg = "At this time you can not be part of Dr. \(((NotificationArray[sender.tag].fields?.value(forKey: "invitorfullname") as? String)!)) Circle of Trust as Max count of doctors at his/her side exceeded!"
                let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    return
                }))
                self.present(alert, animated: true, completion: nil)
            }

        }) { (errorresponse) in
            print("NotificationTableViewController:UpdateEntrytoDocCountTable() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func InviteAcceptedforreferPatientswitch2Actions(index: Int) {
        self.updateInviteTableNotificationread(indexval: index, actionflag: true)
        self.MakeEntrytoRelSpecialityTableforreferPatientswitch2(index: index)
        self.UpdateEntrytoDocCountTableforreferPatientswitch2(index: index, actionflag: true )
    }

    func MakeEntrytoRelSpecialityTableforreferPatientswitch2(index: Int) {
        // before making new entry we need to check if there is already record entry for invitor and invitee combination, hence first we read
        // RelSpeciality Table and see if there is any record

        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteedocuserid"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if contributors?.count == 0 {
                // No entry found hence create new entry
                self.CteateNewEntryinRelSpecialityTableforreferPatientswitch2(index: index)
            } else {
                // entry found hence update entry just the status value to true
                let RecordID = (contributors![0].id)!
                self.UpdateRelSpecialityTableforstatusforreferPatientswitch2(recordID: RecordID, flag: true)
            }
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:MakeEntrytoRelSpecialityTable(): Response error: \(String(describing: response.error?.description))")
        }
        
    }

    func CteateNewEntryinRelSpecialityTableforreferPatientswitch2(index: Int) {
        let object = QBCOCustomObject()
        object.className = "RelSpeciality"
        let invitoruserid = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? UInt)!)
        object.fields["invitordocuserid"] = QBSession.current.currentUser?.id
        object.fields["inviteedocuserid"] = invitoruserid
        object.fields["inviteefullname"] = ((NotificationArray[index].fields?.value(forKey: "invitorfullname") as? String) ?? "")
        object.fields["inviteeprofileimageURL"] = ((NotificationArray[index].fields?.value(forKey: "profileUID") as? String) ?? "")
        object.fields["inviteespeciality"] = ((NotificationArray[index].fields?.value(forKey: "invitorspeciality") as? String) ?? "")
        object.fields["invitorprofileimageURL"] = GlobalDocData.gprofileURL
        object.fields["invitorfullname"] = QBSession.current.currentUser?.fullName
        object.fields["status"] = "true"
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            // we should send Push notification to invitor
            self.sendPushToInvitor(invitoruserid: invitoruserid)
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:CteateNewEntryinRelSpecialityTableforreferPatientswitch2(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func UpdateRelSpecialityTableforstatusforreferPatientswitch2(recordID: String, flag: Bool) {
        let object = QBCOCustomObject()
        object.className = "RelSpeciality"
        if flag {
            object.fields["status"] =  true
        } else {
            object.fields["status"] =  false
        }
        object.id = recordID
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("NotificationTableViewController:UpdateRelSpecialityTableforstatusforreferPatientswitch2() Success !")
        }) { (response) in
            print("NotificationTableViewController:UpdateRelSpecialityTableforstatusforreferPatientswitch2() Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func UpdateEntrytoDocCountTableforreferPatientswitch2(index: Int, actionflag: Bool ) {
        //first we are reading the DocCounttable to get the record id of Invitee
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((NotificationArray[index].fields?.value(forKey: "inviteeuserid") as? Int)!)
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            let inviteeRecordID = (contributors![0].id)!
            let specialityval =  (contributors![0].fields?.value(forKey: ((NotificationArray[index].fields?.value(forKey: "invitorspeciality") as? String) ?? "")) as? Int) ?? 0
            self.finallyupdateDocCountTableforreferPatientswitch2(index: index, inviteeRecordID: inviteeRecordID, specialityval: specialityval, actionflag: actionflag)
        }) { (errorresponse) in
            print("NotificationTableViewController:UpdateEntrytoDocCountTableforreferPatientswitch2() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func finallyupdateDocCountTableforreferPatientswitch2(index: Int, inviteeRecordID: String, specialityval: Int, actionflag: Bool) {
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        var val = specialityval
        if actionflag {
            val = val+1
        } else {
            val = val-1
        }
        object.fields[((NotificationArray[index].fields?.value(forKey: "invitorspeciality") as? String) ?? "")] =  val
        object.id = inviteeRecordID      //record id of Invitor
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("NotificationTableViewController:finallyupdateDocCountTableforreferPatientswitch2() Success !")
        }) { (response) in
            print("NotificationTableViewController:finallyupdateDocCountTableforreferPatientswitch2() Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func InviteAcceptedforRemovalreferPatientswitch2Actions(index: Int) {
        self.updateInviteTableNotificationread(indexval: index, actionflag: true)
        self.updateRelSpecialityforStatusFalseSwitch2(index: index)
        self.decerementCountonDocCountTableSwitch2(index: index)
    }

    //we are making switch 2 off hence status to false on RelSpeciality table
    func updateRelSpecialityforStatusFalseSwitch2(index: Int) {
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteedocuserid"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            let RecordID = (contributors![0].id)!
            self.UpdateRelSpecialityTableforstatus(recordID: RecordID, flag: false)
        }) { (response) in
            print("NotificationTableViewController:updateRelSpecialityforStatusFalseSwitch2(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func decerementCountonDocCountTableSwitch2(index: Int) {
        //first we are reading the DocCounttable to get the record id of Invitee
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((NotificationArray[index].fields?.value(forKey: "inviteeuserid") as? Int)!)
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            let invitorRecordID = (contributors![0].id)!
            let specialityval =  (contributors![0].fields?.value(forKey: ((NotificationArray[index].fields?.value(forKey: "invitorspeciality") as? String) ?? "")) as? Int) ?? 0
            self.finallydecerementDocCountTableforSwitch2(index: index, invitorRecordID: invitorRecordID, specialityval: specialityval)
        }) { (errorresponse) in
            print("NotificationTableViewController:decerementCountonDocCountTableSwitch2() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func finallydecerementDocCountTableforSwitch2(index: Int, invitorRecordID: String, specialityval: Int) {
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        var val = specialityval
        val = val-1
        object.fields[((NotificationArray[index].fields?.value(forKey: "invitorspeciality") as? String) ?? "")] =  val
        object.id = invitorRecordID      //record id of Invitor
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("NotificationTableViewController:finallydecerementDocCountTableforSwitch2() Success !")
        }) { (response) in
            print("NotificationTableViewController:finallydecerementDocCountTableforSwitch2() Response error: \(String(describing: response.error?.description))")
        }
    }
    
    /////////////////////////////////// TEAM invite related logic here on /////////////////////////////////////

    func checkteamSwitchstatus(index: Int) {
        //first check is there is entry in RelDOcTeam Table for invitor and invitee combination ?
        let getRequest = NSMutableDictionary()
        getRequest["inviteedocuserid"] =  ((NotificationArray[index].fields?.value(forKey: "inviteeuserid") as? Int)!)
        getRequest["invitordocuserid"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "RelDocTeam", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                print("(NotificationTableViewController::checkteamSwitchstatus():Entry Found for index: \(index)  !")
                let status = contributors![0].fields?.value(forKey: "status") as? Bool
                if self.TeamSwitcharray.count > index {
                    if status! {
                        self.TeamSwitcharray.insert(true, at: index)
                    } else {
                        self.TeamSwitcharray.insert(false, at: index)
                    }
                } else {
                    if status! {
                        self.TeamSwitcharray.insert(true, at: 0)
                    } else {
                        self.TeamSwitcharray.insert(false, at: 0)
                    }
                }
                
            } else {
                //self.TeamSwitcharray.insert(false, at: index)
            }
            self.tableView.reloadData()
            
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("NotificationTableViewController::checkteamSwitchstatus(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    
    @objc func AddTeamDocswitchTriggered(sender: UISwitch) {
        // Lets very first read the DocCountTable for the Invitor entry for the myTeam count value, as we need to cheeck first if count is not greater than 6 , if it is then we can not add doctor to that speciality on Invitor COT
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((NotificationArray[sender.tag].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("NotificationTableViewController:AddTeamDocswitchTriggered(): Reading record ID of Invitor !")
            self.invitorRecordID = (contributors![0].id)!
            let myTeamval =  contributors![0].fields?.value(forKey: "myTeam") as? Int
            
            if( myTeamval! < 6) {
                // here we are doing off to ON of the switch
                if sender.isOn {
                    let msg = "Allow Dr. \(((NotificationArray[sender.tag].fields?.value(forKey: "invitorfullname") as? String)!)) to add you on his/her Team?"
                    let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "SKIP", style: .default, handler: { (action) in
                        //Perform SKIP actions
                        sender.isOn = false
                        sender.isEnabled = true
                    }))
                    alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
                        //Perform YES actions
                        sender.isOn = true
                        sender.isEnabled = true
                        if self.TeamSwitcharray.count > sender.tag {
                            self.TeamSwitcharray[sender.tag] = true
                        } else {
                            self.TeamSwitcharray.insert(true, at: 0)
                        }
                        
                        self.InviteAcceptedforaddingTeamdocswitchActions(index: sender.tag)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {  // here we are dealing with ON to OFF of the switch
                    sender.isOn = false
                    let msg = "Dr. \(((NotificationArray[sender.tag].fields?.value(forKey: "invitorfullname") as? String)!)) will not have you on his/her Team ?"
                    let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "SKIP", style: .default, handler: { (action) in
                        //Perform SKIP actions
                        sender.isOn = true
                        sender.isEnabled = true
                    }))
                    alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
                        //Perform YES actions
                        sender.isOn = false
                        sender.isEnabled = true
                        self.InviteAcceptedforRemovalTeamdocswitchActions(index: sender.tag)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            } else { // this is when the speciality count is either 6 or greater
                let msg = "At this time you can not be part of Dr. \(((NotificationArray[sender.tag].fields?.value(forKey: "invitorfullname") as? String)!)) Team as Max count of doctors at his/her side exceeded!"
                let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    return
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
        }) { (errorresponse) in
            print("NotificationTableViewController:AddTeamDocswitchTriggered() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func InviteAcceptedforaddingTeamdocswitchActions(index: Int) {
        self.updateInviteTableNotificationread(indexval: index, actionflag: true)
        self.MakeEntrytoRelDocTeamTable(index: index)
        self.UpdateEntrytoDocCountTableforteam(index: index, actionflag: true )
    }
    
    func MakeEntrytoRelDocTeamTable(index: Int) {
        // before making new entry we need to check if there is already record entry for invitor and invitee combination, hence first we read  RelDocTeam Table and see if there is any record
        
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        getRequest["inviteedocuserid"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "RelDocTeam", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if contributors?.count == 0 {
                // No entry found hence create new entry
                self.CteateNewEntryinRelDocTeamTable(index: index)
            } else {
                // entry found hence update entry just the status value to true
                let RecordID = (contributors![0].id)!
                self.UpdateRelDocTeamTableforstatus(recordID: RecordID, flag: true)
            }
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:MakeEntrytoRelSpecialityTable(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func CteateNewEntryinRelDocTeamTable(index: Int) {
        let object = QBCOCustomObject()
        object.className = "RelDocTeam"
        let invitoruserid = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? UInt)!)
        object.fields["invitordocuserid"] = invitoruserid
        object.fields["inviteedocuserid"] = QBSession.current.currentUser?.id
        object.fields["inviteedocfullname"] = QBSession.current.currentUser?.fullName
        object.fields["inviteeprofileURL"] = GlobalDocData.gprofileURL
        object.fields["status"] = "true"
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            print("NotificationTableViewController: CteateNewEntryinRelDocTeamTable(): Successfully created New entry in RelSpeciality Table")
            // we should send Push notification to invitor
            self.sendPushToTeamInvitor(invitoruserid: invitoruserid)
        }) { (response) in
            print("NotificationTableViewController:CteateNewEntryinRelDocTeamTable(): Response error: \(String(describing: response.error?.description))")
        }
    }

    func UpdateRelDocTeamTableforstatus(recordID: String, flag: Bool) {
        let object = QBCOCustomObject()
        object.className = "RelDocTeam"
        if flag {
            object.fields["status"] =  true
        } else {
            object.fields["status"] =  false
        }
        object.id = recordID      //record id
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController:UpdateRelDocTeamTableforstatus() Success !")
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:UpdateRelDocTeamTableforstatus() Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func sendPushToTeamInvitor(invitoruserid: UInt) {
        let currentUserLogin = QBSession.current.currentUser!.fullName
        let userid = "\(invitoruserid)"
        QBRequest.sendPush(withText: "\("Dr. " + currentUserLogin! ) has accepted your Invitation, he will be part of your Team", toUsers: userid, successBlock: { (response: QBResponse, event:[QBMEvent]?) in
            print("NotificationTableViewController:sendPushToTeamInvitor(): Push sent!")
        }) { (error:QBError?) in
            print("NotificationTableViewController:sendPushToTeamInvitor():Can not send push: \(error!))")
        }
    }
    
    func UpdateEntrytoDocCountTableforteam(index: Int, actionflag: Bool) {
        //first we are reading the DocCounttable to get the record id of Invitor
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("NotificationTableViewController:UpdateEntrytoDocCountTableforteam(): Reading record ID of Invitor !")
            let invitorRecordID = (contributors![0].id)!
            let myteamval =  contributors![0].fields?.value(forKey: "myTeam") as? Int
            self.finallyupdateDocCountTableforteam(invitorRecordID: invitorRecordID, myteamval: myteamval!, actionflag: actionflag)
        }) { (errorresponse) in
            print("NotificationTableViewController:UpdateEntrytoDocCountTableforteam() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    // here we are updating
    func finallyupdateDocCountTableforteam(invitorRecordID: String, myteamval: Int, actionflag: Bool) {
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        var val = myteamval
        
        if actionflag {
            val = val+1
        } else {
            val = val-1
        }
        
        object.fields["myTeam"] =  val
        object.id = invitorRecordID      //record id of Invitor
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController:finallyupdateDocCountTableforteam() Success !")
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:finallyupdateDocCountTableforteam() Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func InviteAcceptedforRemovalTeamdocswitchActions(index: Int) {
        self.updateInviteTableNotificationread(indexval: index, actionflag: true)
        self.updateRelDocTeamforStatusFalse(index: index)
        self.decerementCountonDocCountTableforTeam(index: index)
    }
    
    //As we are making Team switch off hence make status false
    func updateRelDocTeamforStatusFalse(index: Int) {
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        getRequest["inviteedocuserid"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "RelDocTeam", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            let RecordID = (contributors![0].id)!
            self.UpdateRelDocTeamTableforstatus(recordID: RecordID, flag: false)
        }) { (response) in
            print("NotificationTableViewController:MakeEntrytoRelSpecialityTable(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func decerementCountonDocCountTableforTeam(index: Int) {
        //first we are reading the DocCounttable to get the record id of Invitor
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((NotificationArray[index].fields?.value(forKey: "invitoruserid") as? Int)!)
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            print("NotificationTableViewController:decerementCountonDocCountTableforTeam(): Reading record ID of Invitor !")
            let invitorRecordID = (contributors![0].id)!
            let myteamval =  contributors![0].fields?.value(forKey: "myTeam") as? Int
            self.finallydecerementDocCountTableforteam(invitorRecordID: invitorRecordID, myteamval: myteamval!)
        }) { (errorresponse) in
            print("NotificationTableViewController:decerementCountonDocCountTableforTeam() Response error: \(String(describing: errorresponse.error?.description))")
        }
        
    }
    
    func finallydecerementDocCountTableforteam(invitorRecordID: String, myteamval: Int) {
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        var val = myteamval
        val = val-1
        object.fields["myTeam"] =  val
        object.id = invitorRecordID      //record id of Invitor
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("NotificationTableViewController:finallydecerementDocCountTableforteam() Success !")
        }) { (response) in
            print("NotificationTableViewController:finallydecerementDocCountTableforteam() Response error: \(String(describing: response.error?.description))")
        }
    }
    
}
