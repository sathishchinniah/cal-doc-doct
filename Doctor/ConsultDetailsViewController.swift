import UIKit

class ConsultDetailsViewController: UIViewController {
    
  @IBOutlet weak var patientimage: UIImageView!
  @IBOutlet weak var patientnamelb:  UILabel!
  @IBOutlet weak var timedatelb:  UILabel!
  @IBOutlet weak var chatContainerView: UIView!
  @IBOutlet weak var prescriptionContainerView: UIView!
  @IBOutlet weak var invoicesContainerView: UIView!
  @IBOutlet weak var chatBtn: UIButton!
  @IBOutlet weak var prescriptionBtn: UIButton!
  @IBOutlet weak var invoicesBtn: UIButton!
  @IBOutlet weak var shareButton: UIButton!
  @IBOutlet weak var callButton: UIButton!
  @IBOutlet weak var chatButton: UIButton!
  @IBOutlet weak var invoiceSeparatorView: UIView!
  @IBOutlet weak var prescriptionTrail: NSLayoutConstraint!
    
    var patientindex = 0
  var patientuseridpassed: UInt = 0
  var patientnamepassed = ""
  var datepassed = ""
  var timepassed = ""
  var imageurlpassed = ""
  var tblindexpassed = 0
  var consultONOFF: Bool = false
  var ConsultRecordID = ""
    
  var prescriptionTVC = PrescriptionTableViewController()
  
  var userid : UInt = 0
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let mainVC = (self.parent as! UINavigationController).viewControllers.first
    if mainVC is SharedConsultsViewController {
        invoicesBtn.isHidden = true
        invoicesBtn.frame = CGRect(x: self.view.frame.width - 20, y: self.view.frame.height - 1, width: 0, height: 0)
        invoiceSeparatorView.isHidden = true
        
        
    }
    shareButton.isHidden = true// false
    chatButton.isHidden = true
    chatBtn.alpha = 0.5
    prescriptionContainerView.isHidden = true
    invoicesContainerView.isHidden = true
    self.title = "Consult Details"
    UINavigationBar.appearance().barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    UIApplication.shared.statusBarStyle = .default
    
    navigationController?.navigationBar.tintColor = UIColor.white
    self.navigationItem.backBarButtonItem?.title = ""
    // this removes navigation bar horizental line
    navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    navigationController?.navigationBar.shadowImage = UIImage()
    
    patientnamelb.text = patientnamepassed
    timedatelb.text = timepassed + " | " + datepassed
    
    patientimage.sd_setImage(with: URL(string: imageurlpassed), placeholderImage: UIImage(named: "profile"))
    patientimage.layer.cornerRadius = patientimage.frame.size.width / 2;
    patientimage.clipsToBounds = true;
    patientimage.layer.borderWidth = 0.5
    patientimage.layer.borderColor = UIColor.clear.cgColor
    
    userid = /*PatientConsultsdetailsArray*/MyAllConsultsArray[tblindexpassed].userID
    self.performSegue(withIdentifier: "ChatViewSegue", sender: self)
    //configureChatWindow()
  }
  
    override func viewWillAppear(_ animated: Bool) {
        //code
    }
    
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  //MARK:- IBAction methods
  
  @IBAction func chatBtnClicked(_ sender: UIButton) {
    shareButton.isHidden = true
    chatBtn.alpha = 0.5
    prescriptionBtn.alpha = 1
    invoicesBtn.alpha = 1
    prescriptionContainerView.isHidden = true
    invoicesContainerView.isHidden = true
    chatContainerView.isHidden = false
    self.performSegue(withIdentifier: "ChatViewSegue", sender: self)
  }
  
  @IBAction func prescriptionBtnClicked(_ sender: UIButton) {
    chatBtn.alpha = 1
    prescriptionBtn.alpha = 0.5
    invoicesBtn.alpha = 1
    chatContainerView.isHidden = true
    invoicesContainerView.isHidden = true
    shareButton.isHidden = true
    prescriptionContainerView.isHidden = false
    self.performSegue(withIdentifier: "prescriptionViewSegue", sender: self)
  }
  
  @IBAction func invoicedBtnClicked(_ sender: UIButton) {
    shareButton.isHidden = true
    chatBtn.alpha = 1
    prescriptionBtn.alpha = 1
    invoicesBtn.alpha = 0.5
    chatContainerView.isHidden = true
    prescriptionContainerView.isHidden = true
    invoicesContainerView.isHidden = false
    self.performSegue(withIdentifier: "invoicesViewSegue", sender: self)
  }
    
    @IBAction func extendButtonTapped(_ sender: Any) {
        
    }
    
  //MARK:- Storyboard Instance
  
  static func storyboardInstance() -> ConsultDetailsViewController {
    let storyboard = UIStoryboard(name: "Consult", bundle: nil)
    return storyboard.instantiateViewController(withIdentifier: "ConsultDetailsViewController") as! ConsultDetailsViewController
  }
    
    //MARK:- Custom methods
    
    func leftButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func callButtonPressed(_ sender: UIButton) {
        if (!ConsultRecordID.isEmpty) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let mycallviewController = storyboard.instantiateViewController(withIdentifier: "makeCall") as! CallViewControllerswft
        mycallviewController.ConsultRecordID = ConsultRecordID
        mycallviewController.callingnumber = patientuseridpassed
        mycallviewController.patientname = patientnamepassed
        self.navigationController?.pushViewController(mycallviewController, animated: true)
        }
    }
    
    @IBAction func chatButtonPressed(_ sender: UIButton) {
        
//        if (!ConsultRecordID.isEmpty) {
//            let user = QBUUser()
//            user.id =  patientuseridpassed
//            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
//            chatDialog.occupantIDs = [user.id] as [NSNumber]
//
//            QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
//                let storyboard = UIStoryboard.init(name: "Consult", bundle: nil)
//                let chatvc = storyboard.instantiateViewController(withIdentifier: "ChatLogsViewController") as! ChatLogsViewController
//                chatvc.ConsultRecordID = self.ConsultRecordID
//                chatvc.dialog = createdDialog
//                chatvc.profileimageurl = (MyAllConsultsUniqueArray[self.patientindex].fields?.value(forKey: "patprofileUID") as! String)
//                //self.navigationController?.pushViewController(chatvc, animated: true)
////                self.addChildViewController(chatvc)
////                chatvc.view.frame = CGRect(x: 0, y: 0, width: self.chatContainerView.frame.size.width, height: self.chatContainerView.frame.size.height)//(0, 0, self.chatContainerView.frame.size.width, self.chatContainerView.frame.size.height)
////
////                self.chatContainerView.addSubview(chatvc.view)
////                chatvc.didMove(toParentViewController: self)
//                self.prescriptionContainerView.isHidden = true
//                self.invoicesContainerView.isHidden = true
//                self.chatContainerView.isHidden = false
//
//            }, errorBlock: {(response: QBResponse!) in
//                print("Error response + \(String(describing: response))")
//            })
//        }
    }
    
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        
        //let a = prescriptionTVC.tableView.bounds
        let tableView = prescriptionTVC.tableView
        let priorBounds = tableView?.bounds
        let fittedSize = tableView?.sizeThatFits(CGSize(width:(priorBounds?.size.width)!, height:(tableView?.contentSize.height)!))
        tableView?.bounds = CGRect(x:0, y:0, width:(fittedSize?.width)!, height:(fittedSize?.height)!)
        let pdfPageBounds = CGRect(x:0, y:0, width:(tableView?.frame.width)!, height:self.view.frame.height)
         let pdfData = NSMutableData()
         UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
         var pageOriginY: CGFloat = 0
        while pageOriginY < (fittedSize?.height)! {
         UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
         UIGraphicsGetCurrentContext()!.saveGState()
         UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            tableView?.layer.render(in: UIGraphicsGetCurrentContext()!)
         UIGraphicsGetCurrentContext()!.restoreGState()
         pageOriginY += pdfPageBounds.size.height
         }
         UIGraphicsEndPDFContext()
        tableView?.bounds = priorBounds!
         var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
         docURL = docURL.appendingPathComponent("myDocument.pdf")
         //pdfData.write(to: docURL as URL, atomically: true)
        
        let pdfFile = pdfData
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ChatViewSegue" {
            if (!ConsultRecordID.isEmpty) {
                let user = QBUUser()
                user.id =  patientuseridpassed
                let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
                chatDialog.occupantIDs = [user.id] as [NSNumber]
                
                QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                    //let storyboard = UIStoryboard.init(name: "Consult", bundle: nil)
                    let chatvc = segue.destination as? ChatLogsViewController//storyboard.instantiateViewController(withIdentifier: "ChatLogsViewController") as! ChatLogsViewController
                    chatvc?.ConsultRecordID = self.ConsultRecordID
                    chatvc?.dialog = createdDialog
                    chatvc?.profileimageurl = (MyAllConsultsArray[self.patientindex].fields?.value(forKey: "patprofileUID") as? String) ?? ""
                    //self.navigationController?.pushViewController(chatvc, animated: true)
                    //                self.addChildViewController(chatvc)
                    //                chatvc.view.frame = CGRect(x: 0, y: 0, width: self.chatContainerView.frame.size.width, height: self.chatContainerView.frame.size.height)//(0, 0, self.chatContainerView.frame.size.width, self.chatContainerView.frame.size.height)
                    //
                    //                self.chatContainerView.addSubview(chatvc.view)
                    //                chatvc.didMove(toParentViewController: self)
                    chatvc?.reloadInputViews()
                    self.prescriptionContainerView.isHidden = true
                    self.invoicesContainerView.isHidden = true
                    self.chatContainerView.isHidden = false
                    
                }, errorBlock: {(response: QBResponse!) in
                    print("Error response + \(String(describing: response))")
                })
            }
        } else if segue.identifier == "prescriptionViewSegue" {
            prescriptionTVC = (segue.destination as? PrescriptionTableViewController) ?? PrescriptionTableViewController()
        }
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        switch identifier {
        case "ChatViewSegue":
            return
        case "prescriptionViewSegue":
            return
        case "invoicesViewSegue":
            return
        default:
            return
        }
    }
    
    func configureChatWindow() {
        if (!ConsultRecordID.isEmpty) {
            let user = QBUUser()
            user.id =  patientuseridpassed
            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
            chatDialog.occupantIDs = [user.id] as [NSNumber]
            
            QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                let storyboard = UIStoryboard.init(name: "Consult", bundle: nil)
                let chatvc = storyboard.instantiateViewController(withIdentifier: "ChatLogsViewController") as! ChatLogsViewController
                chatvc.ConsultRecordID = self.ConsultRecordID
                chatvc.dialog = createdDialog
                chatvc.profileimageurl = (MyAllConsultsUniqueArray[self.patientindex].fields?.value(forKey: "patprofileUID") as! String)
                chatvc.opponentID = self.patientuseridpassed
                //self.navigationController?.pushViewController(chatvc, animated: true)
                //                self.addChildViewController(chatvc)
                //                chatvc.view.frame = CGRect(x: 0, y: 0, width: self.chatContainerView.frame.size.width, height: self.chatContainerView.frame.size.height)//(0, 0, self.chatContainerView.frame.size.width, self.chatContainerView.frame.size.height)
                //
                //                self.chatContainerView.addSubview(chatvc.view)
                //                chatvc.didMove(toParentViewController: self)
                self.prescriptionContainerView.isHidden = true
                self.invoicesContainerView.isHidden = true
                self.chatContainerView.isHidden = false
                // Retrieving messages
                if (chatvc.storedMessages()?.count ?? 0 > 0 && chatvc.chatDataSource.messagesCount() == 0) {
                    
                    chatvc.chatDataSource.add(chatvc.storedMessages()!)
                }
                
                chatvc.loadMessages()
                chatvc.enableTextCheckingTypes = NSTextCheckingAllTypes
                self.performSegue(withIdentifier: "ChatViewSegue", sender: self)
                
            }, errorBlock: {(response: QBResponse!) in
                print("Error response + \(String(describing: response))")
            })
        }
    }
}
