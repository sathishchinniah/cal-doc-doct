import UIKit
import WebKit

class BotWebViewController: UIViewController {

    @IBOutlet weak var WebView: WKWebView!
    
    var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        // this removes navigation bar horizental line
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.title = "Bot Help"
        
        
        //left side Back Button
        let btnImage = UIImage(named: "backBtn")
        let leftbtn = UIButton(type: .custom)
        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        leftbtn.setImage(btnImage, for: .normal)
        let backButton = UIBarButtonItem(customView: leftbtn)
        navigationItem.leftBarButtonItem = backButton
        
        
        // web request for bot
        let urlstring = "https://snatchbot.me/webchat?botID=22708&appID=LbpY6zKrPiXUek6OzpXG"
        let url = URL(string: urlstring)!
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "GET"
        WebView.load(request as URLRequest)
    }

    @objc func backBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let homeViewController = storyboard.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }

}
