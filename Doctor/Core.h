//
//  Core.h
//  Doctor
//
//  Created by Manish Verma on 18/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, QBNetworkStatus)  {
    
    QBNetworkStatusNotReachable = 0,
    QBNetworkStatusReachableViaWiFi,
    QBNetworkStatusReachableViaWWAN
};

typedef void(^QBNetworkStatusBlock)(QBNetworkStatus status);

@class QBCore;

@protocol QBCoreDelegate <NSObject>

@optional

@end

#define Core [QBCore instance]

@interface QBCore : NSObject


@property (copy, nonatomic, nullable)  QBNetworkStatusBlock networkStatusBlock;

+ (instancetype _Nullable )instance;
- (instancetype _Nullable )init NS_UNAVAILABLE;
+ (instancetype _Nullable )new NS_UNAVAILABLE;

#pragma mark - Multicast Delegate

- (void)addDelegate:(id <QBCoreDelegate>)delegate;

- (QBNetworkStatus)networkStatus;

@end


