//
//  RegisterViewController.m
//  Doctor
//
//  Created by CallDoc on 13/03/18.
//  Copyright © 2018 calldoc. All rights reserved.
//

#import "GData.h"
#import "VerifyViewController.h"
#import <sys/utsname.h>
@import FirebaseAuth;
#import "RegisterViewController.h"

#define USERPHONE @"userphonenumber"

@interface RegisterViewController ()
{
    bool isLoggedin;
    // UIActivityIndicatorView *indicator;
    NSString *VerficationID;
    NSString *totalmobilenum; // country code + mobile number = 919845......
    NSString *fullname;
     double devtype;
    long docuserid;
    NSString *doccalldoccode;
    bool calldoccodevalid;
}

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UITableView *countrycodetableView;
@property (weak, nonatomic) IBOutlet UITextField *Firstnametxt;
@property (weak, nonatomic) IBOutlet UITextField *Lastnametxt;
@property (weak, nonatomic) IBOutlet UITextField *Emailtxt;
@property (weak, nonatomic) IBOutlet UITextField *referralcodetxt;
@property (weak, nonatomic) IBOutlet UITextField *mobilenumbertxt;
@property (weak, nonatomic) IBOutlet UITextField *countrycodetxt;
@property (weak, nonatomic) IBOutlet UIScrollView *thescrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;
@property (weak, nonatomic) IBOutlet UILabel *referralcodelb;


@end

UITextField *activeField;

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     calldoccodevalid = false;
    
    self.dataPointer = [GData sharedData];
    
    self.progressIndicator.hidden = true;
    //    self.navigationItem.title = @"SignIn";
    //    self.navigationItem.leftBarButtonItem.accessibilityElementsHidden = true;
    //    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    CGRect newFrame = CGRectMake( self.topView.frame.origin.x, self.topView.frame.origin.y, 200, 200);
    
    self.view.frame = newFrame;
    
    self.countrycodeData = [[NSArray alloc]initWithObjects:@"India +91", @"United Kingdom +44", @"Singapore +64", @"United States +1", @"Australia +61" ,nil];
    
    _countrycodetxt.text = @"+91"; // the default value
    
    self.bottomView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"schbottombarbg"]];
    
    
    double devtype = [self checkdevicetrype];
    
    if (devtype == 736) // iphone 6 plus
    {
        _Firstnametxt.font = [UIFont fontWithName:@"Rubik" size:17.0];
        _Lastnametxt.font = [UIFont fontWithName:@"Rubik" size:17.0];
        _mobilenumbertxt.font = [UIFont fontWithName:@"Rubik" size:17.0];
        _Emailtxt.font = [UIFont fontWithName:@"Rubik" size:17.0];
        _referralcodetxt.font = [UIFont fontWithName:@"Rubik" size:17.0];
        _countrycodetxt.font = [UIFont fontWithName:@"Rubik" size:17.0];
        _referralcodelb.font = [UIFont fontWithName:@"Rubik" size:15.0];
        
    }
    
    if (devtype == 812) // iphone X
    {
        // self.sendotpbuttonview.translatesAutoresizingMaskIntoConstraints = true;
        // CGRect frame = self.sendotpbuttonview.frame;
        // frame.origin.x = frame.origin.x+55; // new x coordinate
        //frame.origin.y = 130; // new y coordinate
        // self.sendotpbuttonview.frame = frame;
        _Firstnametxt.font = [UIFont fontWithName:@"Rubik-Regular" size:17.0];
        _Lastnametxt.font = [UIFont fontWithName:@"Rubik-Regular" size:17.0];
        _mobilenumbertxt.font = [UIFont fontWithName:@"Rubik-Regular" size:17.0];
        _Emailtxt.font = [UIFont fontWithName:@"Rubik" size:17.0];
        _referralcodetxt.font = [UIFont fontWithName:@"Rubik-Regular" size:17.0];
        _countrycodetxt.font = [UIFont fontWithName:@"Rubik-Regular" size:17.0];
        _referralcodelb.font = [UIFont fontWithName:@"Rubik-Regular" size:15.0];
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    self.Firstnametxt.delegate=self;
    self.Lastnametxt.delegate=self;
    self.Emailtxt.delegate=self;
    self.mobilenumbertxt.delegate= self;
    self.referralcodetxt.delegate=self;
    self.countrycodetxt.delegate=self;
    
    self.countrycodetableView.delegate = self;
    self.countrycodetableView.dataSource = self;
    self.countrycodetableView.hidden = true;
    
    self.Firstnametxt.tintColor = [UIColor whiteColor];
    self.Lastnametxt.tintColor = [UIColor whiteColor];  // Last name
    self.Emailtxt.tintColor = [UIColor whiteColor];
    self.mobilenumbertxt.tintColor = [UIColor whiteColor];
    self.referralcodetxt.tintColor = [UIColor whiteColor];
    self.countrycodetxt.tintColor = [UIColor whiteColor];
    
    //to hide KB in scrollview
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [self.thescrollView addGestureRecognizer:tapGesture];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(20, 20, 44.0f, 30.0f)];
    [backButton setImage:[UIImage imageNamed:@"backBtn"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

- (void) BackButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)hideKeyboard
{
    [self.Firstnametxt resignFirstResponder];
    [self.Lastnametxt resignFirstResponder];
    [self.Emailtxt resignFirstResponder];
    [self.mobilenumbertxt resignFirstResponder];
    [self.referralcodetxt resignFirstResponder];
    [self.countrycodetxt resignFirstResponder];
}




-(double)checkdevicetrype
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812)
        {
            NSLog(@"iPhone X");
            return screenSize.height;
        }
        
        if (screenSize.height == 736)
        {
            NSLog(@"iPhone 6 plus");
            return screenSize.height;
        }
        
        if (screenSize.height == 667)
        {
            NSLog(@"iPhone 6");
            return screenSize.height;
        }
        if (screenSize.height == 568)
        {
            NSLog(@"iPhone 5");
            return screenSize.height;
        }
        
    }
    return 0;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWasShown:(NSNotification *)aNotification
{
    //    double devtype = [self checkdevicetrype];
    //    if (devtype == 568) //iphone 5
    //    {
    //        CGRect newFrame = CGRectMake( self.topView.frame.origin.x, self.topView.frame.origin.y, 320, 130);
    //        self.topView.frame = newFrame;
    //
    //        [self.scrollview setContentOffset:CGPointMake(0, 55) animated:YES];
    //       // self.topView.hidden = true;
    //    }
    //
    //    if (devtype == 667) //iphone 6
    //    {
    //        [self.scrollview setContentOffset:CGPointMake(0, 10) animated:YES];
    //    }
    //
    //    if (devtype == 736) //iphone 6 plus
    //    {
    //        [self.scrollview setContentOffset:CGPointMake(0, 0) animated:YES];
    //    }
    //
    //    if (devtype == 812) //iphone X
    //    {
    //        [self.scrollview setContentOffset:CGPointMake(0, 0) animated:YES];
    //    }
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.thescrollView.contentInset = contentInsets;
    self.thescrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.thescrollView scrollRectToVisible:activeField.frame animated:YES];
    }
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //    double devtype = [self checkdevicetrype];
    //    if (devtype == 568) //iphone 5
    //    {
    //        CGRect newFrame = CGRectMake( self.topView.frame.origin.x, self.topView.frame.origin.y, 320, 136.5);
    //        self.topView.frame = newFrame;
    //    }
    //
    //
    //    [self.scrollview setContentOffset:CGPointMake(0, 0) animated:YES];
    //    self.topView.hidden = false;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.thescrollView.contentInset = contentInsets;
    self.thescrollView.scrollIndicatorInsets = contentInsets;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationItem setHidesBackButton:YES];
}

//Dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"RegisterViewConteroller:touchesBegan");
    [self.view endEditing:YES];
    [self.thescrollView endEditing:YES];
    [super touchesBegan:touches withEvent:event];
    
}

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField {
    //sender.delegate = self;
    activeField = textField;
    
    if (textField == _countrycodetxt)
    {
        [_countrycodetxt resignFirstResponder]; // this should hide KB
        [_Firstnametxt resignFirstResponder];
        [_Lastnametxt resignFirstResponder];
        [_Emailtxt resignFirstResponder];
        [_mobilenumbertxt resignFirstResponder];
        [_referralcodetxt resignFirstResponder];
        self.countrycodetableView.hidden = false;
        
    }
    else
    {
        _countrycodetableView.hidden = true;
    }
}

- (IBAction)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _Firstnametxt)
    {
        [_Firstnametxt becomeFirstResponder];
        _countrycodetableView.hidden = true;
    }
    else if (textField == _Lastnametxt)
    {
        [_Emailtxt becomeFirstResponder];
        _countrycodetableView.hidden = true;
        
    }
    
    else if (textField == _Emailtxt)
    {
        [_mobilenumbertxt becomeFirstResponder];
        _countrycodetableView.hidden = true;
        
    }
    
    else if (textField == _mobilenumbertxt)
    {
        [_referralcodetxt becomeFirstResponder];
        _countrycodetableView.hidden = true;
        
    }
    else if (textField == _referralcodetxt)
    {
        [_referralcodetxt resignFirstResponder];
        _countrycodetableView.hidden = true;
        
    }
    else
    {
        [_referralcodetxt resignFirstResponder];
    }
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    devtype = [self checkdevicetrype];
    
    if(textField == self.Firstnametxt)
    {
        if (devtype == 568)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 10;
        }
        else
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 16;
        }
    }
    
    if(textField == self.Lastnametxt)
    {
        if (devtype == 568)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 10;
        }
        else
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 16;
        }
    }
    
    if(textField == self.Emailtxt)
    {
        if (devtype == 568)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 25;
        }
        else
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <= 28;
        }
    }
    
    if(textField == self.referralcodetxt)
    {
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatreferralcode:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatreferralcode:totalString deleteLastChar:NO ];
        }
        return false;
    }
    
    // if it's the phone number textfield format it.
    if(textField.tag==10 ) {
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    
    return YES;
}

-(NSString*) formatreferralcode:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    
    if (simpleNumber.length == 6 && deleteLastChar == NO) { [_mobilenumbertxt resignFirstResponder]; }
    
    // check if the number is to long
    if(simpleNumber.length>6) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:6];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    return simpleNumber;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    
    if (simpleNumber.length == 10 && deleteLastChar == NO) { [_mobilenumbertxt resignFirstResponder]; }
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    return simpleNumber;
}


- (IBAction)mobilenumbertextdidend:(id)sender{
    
    if([self.mobilenumbertxt.text length] < 10)
    {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Your have not entered 10 digit Mobile Number!"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       
                                                   }];
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (IBAction)BacktoVCButton:(UIButton *)sender {
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"ViewController"];
//    [self presentViewController:VC animated:NO completion:nil];
}



- (IBAction)SendOTPButton:(UIButton *)sender {
    
    self.progressIndicator.hidden = false;
    [self.progressIndicator startAnimating];
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"] invertedSet];
    if([self.mobilenumbertxt.text length] < 10)
    {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Your have not entered 10 digit Mobile Number!"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       return;
                                                   }];
        
        [alert addAction:ok];
        [self.progressIndicator stopAnimating];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else if(([self.Firstnametxt.text length] < 1) || ([self.Lastnametxt.text length] < 1))
    {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Your have not entered Full Name, ie. first name, last name !"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       self.progressIndicator.hidden = true;
                                                        [self.progressIndicator stopAnimating];
                                                       return;
                                                   }];
        [alert addAction:ok];
       
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
    
    else if ([self.Firstnametxt.text rangeOfCharacterFromSet:set].location != NSNotFound) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Please enter valid first name !"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       self.progressIndicator.hidden = true;
                                                       [self.progressIndicator stopAnimating];
                                                       return;
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else if ([self.Lastnametxt.text rangeOfCharacterFromSet:set].location != NSNotFound) {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Please enter valid last name !"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       self.progressIndicator.hidden = true;
                                                       [self.progressIndicator stopAnimating];
                                                       return;
                                                   }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else if([self.Emailtxt.text length] < 1)
    {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Your have not entered Email!"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       self.progressIndicator.hidden = true;
                                                       [self.progressIndicator stopAnimating];
                                                       return;
                                                   }];
        [alert addAction:ok];
       
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    BOOL checkemailvalidity = [self validateEmail:self.Emailtxt.text];
    
    if(checkemailvalidity)
    {
        NSLog(@"RegisterViewConteroller:Valid Email Entered , hence storing it");
        
        [[NSUserDefaults standardUserDefaults] setValue:self.Emailtxt.text forKey:@"useremail"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Please Enter Valid Email Address !!"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       self.Emailtxt.text = @"";
                                                       self.progressIndicator.hidden = true;
                                                       [self.progressIndicator stopAnimating];
                                                       return;
                                                   }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
    //totalmibilenum is = +91 plus entered mobile number
    totalmobilenum = [NSString stringWithFormat:@"%@%@",self.countrycodetxt.text, self.mobilenumbertxt.text];
    
    // use UIAlertController to Check Mobile number
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:totalmobilenum
                               message:@"Correct Mobile Number ?"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   
                                                   NSLog(@"RegisterViewConteroller Wrong  Mobile Number!");
                                                   self.progressIndicator.hidden = true;
                                                   [self.progressIndicator stopAnimating];
                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                   return;
                                               }];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                                   [ self checkemailwithuser];
                                               }];
    [alert addAction:no];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}



-(void)CheckifUserAlreadyExist
{
    NSString *stringWithoutpluswith_d = [totalmobilenum
                                   stringByReplacingOccurrencesOfString:@"+" withString:@"d_"];
    [QBRequest usersWithLogins:@[stringWithoutpluswith_d] page:[QBGeneralResponsePage responsePageWithCurrentPage:1 perPage:10]
                  successBlock:^(QBResponse *response, QBGeneralResponsePage *page, NSArray *users) {
                      // Successful response with page information and users array
                      if (users.count > 0)
                      {
                          NSLog(@"RegisterViewConteroller:CheckifUserAlreadyExist - yes user exists");
                          QBUUser *selectedUser = [users objectAtIndex:0] ;
                          [[NSUserDefaults standardUserDefaults]
                           setValue:selectedUser.fullName forKey:@"existinguserfullname"];
                          [[NSUserDefaults standardUserDefaults]
                           setValue:selectedUser.email forKey:@"existinguseremail"];
                          [[NSUserDefaults standardUserDefaults]
                           setValue:selectedUser.phone forKey:@"existinguserphone"];
                          UIAlertController *alert= [UIAlertController
                                                     alertControllerWithTitle:@"User already exists !!!!"
                                                     message:@"Please Sigin as existing User !"
                                                     preferredStyle:UIAlertControllerStyleAlert];
                          
                          UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * action){
                                                                         
                                                                         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                         UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"ViewController"];
                                                                         [self presentViewController:VC animated:NO completion:nil];
                                                                     }];
                          [alert addAction:ok];
                          [self presentViewController:alert animated:YES completion:nil];
                          
                      }
                      else
                      {    // user does not already exists in our DB hence go for fresh registration
                          if (([self.referralcodetxt.text length] > 0) && ([self.referralcodetxt.text length] == 6))
                          {
                              // this means user has entered the calldoc code we need to check if this code is there in our table
                              [self checkforuserwithcalldoccode:self.referralcodetxt.text];
                          }
                          else
                          {
                              [ self CallFirebaseAuthentication];
                          }
                      }
                      
                  } errorBlock:^(QBResponse *response) {
                      // Handle error
                      NSLog(@"RegisterViewConteroller:CheckifUserAlreadyExist - Response error");
                  }];
    
    
}

- (void)checkemailwithuser
{
    // use UIAlertController to Check email
    UIAlertController *alert= [UIAlertController
                                alertControllerWithTitle:self.Emailtxt.text
                                message:@"Correct Email Address ?"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action) {
                                                    
                                                    NSLog(@"RegisterViewController Wrong  Email Address!");
                                                    self.progressIndicator.hidden = true;
                                                    [self.progressIndicator stopAnimating];
                                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                                    return;
                                                }];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action){
                                                    [self CheckifUserAlreadyExist];
                                                }];
    [alert addAction:no];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (BOOL)validateEmail:(NSString *)inputText {
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot = aRange.location;
        //NSLog(@"aRange.location:%d - %d",aRange.location, indexOfDot);
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            //NSLog(@"topleveldomains:%@",topLevelDomain);
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com",@".co", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
            /*else {
             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
             }*/
        }
    }
    return FALSE;
}


-(void)CallFirebaseAuthentication
{
    //Uncomment this when actual testing with Firebase
//        [[FIRPhoneAuthProvider provider] verifyPhoneNumber:totalmobilenum
//                                                completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:totalmobilenum
                                            UIDelegate:nil
                                            completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
                                                    
                                                    if (error) {
                                                        NSLog(@"RegisterViewController: Firebase Auth Error %@",[error localizedDescription]);
                                                        return;
                                                    }
    
                                                    VerficationID = verificationID;
                                                    [_progressIndicator stopAnimating];
                                                    [[NSUserDefaults standardUserDefaults]
                                                     setValue:VerficationID forKey:@"authVID"];
                                                    [[NSUserDefaults standardUserDefaults] synchronize];
    
                                                    [[NSUserDefaults standardUserDefaults]
                                                     setValue:totalmobilenum forKey:USERPHONE];
                                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                                    
                                                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"doctoronline"];
                                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                                    
                                                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"existinguser"];
                                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                                
                                                //remove any whitespace
                                                self.Firstnametxt.text = [self.Firstnametxt.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
                                                self.Lastnametxt.text = [self.Lastnametxt.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
                                                
                                                     fullname = [NSString stringWithFormat:@"%@%@%@", self.Firstnametxt.text, @" ", self.Lastnametxt.text];
                                                    [[NSUserDefaults standardUserDefaults]
                                                     setValue:fullname forKey:@"fullname"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];

                                                    if (calldoccodevalid)
                                                    {
                                                        [[NSUserDefaults standardUserDefaults]
                                                         setValue:self.referralcodetxt.text forKey:@"regcalldoccode"];
                                                    }
                                                    else
                                                    {
                                                        [[NSUserDefaults standardUserDefaults]
                                                         setValue:@"NA" forKey:@"regcalldoccode"];
                                                    }
                                                
                                                    [[NSUserDefaults standardUserDefaults]
                                                     setValue:self.Emailtxt.text forKey:@"email"];
                                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                                    
                                                    
                                                    
                                                    [self performSegueWithIdentifier:@"Verifyreg" sender:nil];
    
                                                }];
    
    // remove these lines when testing with Actual firebase
//    [[NSUserDefaults standardUserDefaults]
//     setValue:totalmobilenum forKey:USERPHONE];
//
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"existinguser"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [self performSegueWithIdentifier:@"Verifyreg" sender:nil];
}


- (void) checkforuserwithcalldoccode:(NSString *)calldoccode
{
    NSLog(@"RegisterViewConteroller:checkforuserwithcalldoccode()");
    NSMutableDictionary *getRequest = [NSMutableDictionary dictionary];
    [getRequest setObject:calldoccode forKey:@"calldoccode"];
    // first we are checking if the calldoc code is there on PatPrimaryDetailsTable , that means patient has invited this doctor
    // hence we first check if this calldoc code is there in PatPrimaryDetailsTable
    [QBRequest objectsWithClassName:@"PatPrimaryDetailsTable" extendedRequest:getRequest successBlock:^(QBResponse *response, NSArray *objects, QBResponsePage *page) {
        // response processing
        if ( [objects count] > 0 )
        {
            // found entry for calldoccode
            calldoccodevalid = true;
            QBCOCustomObject *obj = objects.firstObject;
            docuserid = obj.userID;
          //  doccalldoccode = [[obj.fields objectForKey:@"calldoccode"] stringValue];
            NSLog(@"RegisterViewConteroller:checkforuserwithcalldoccode(): Entry found in PatPrimaryDetailsTable");
            [[NSUserDefaults standardUserDefaults]
             setValue:@"PatPrimaryDetailsTable" forKey:@"calldoccodetblname"];
            [ self CallFirebaseAuthentication];
        }
        else
        {
            NSMutableDictionary *getRequest = [NSMutableDictionary dictionary];
            [getRequest setObject:calldoccode forKey:@"calldoccode"];
            // Now we are checking if the calldoc code is there on DocPrimaryDetails , that means Doctor has invited this doctor
            // hence we first check if this calldoc code is there in DocPrimaryDetails
            [QBRequest objectsWithClassName:@"DocPrimaryDetails" extendedRequest:getRequest successBlock:^(QBResponse *response, NSArray *objects, QBResponsePage *page) {
                // response processing
                if ( [objects count] > 0 )
                {
                    // found entry for calldoccode
                    calldoccodevalid = true;
                    QBCOCustomObject *obj = objects.firstObject;
                    docuserid = obj.userID;
                    NSLog(@"RegisterViewConteroller:checkforuserwithcalldoccode(): Entry found in DocPrimaryDetails");
                    [[NSUserDefaults standardUserDefaults]
                     setValue:@"DocPrimaryDetails" forKey:@"calldoccodetblname"];
                    [ self CallFirebaseAuthentication];
                }
                else
                {
                    NSLog(@"RegisterViewConteroller:checkforuserwithcalldoccode(): NO Entry found in PatPrimaryDetailsTable and DocPrimaryDetails");
                    // no record in DocPrimaryDetails table for this calldoccode, tell user there is no entry with this code and return
                    UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:@"CallDoc code is not Valid !!"
                                       message:@"Please check your CallDoc code, its not available on our System!"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           calldoccodevalid = false;
                                                           self.referralcodetxt.text = @"";
                                                           self.progressIndicator.hidden = true;
                                                           [self.progressIndicator stopAnimating];
                                                           return;
                                                       }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            } errorBlock:^(QBResponse *response) {
                // error handling
                NSLog(@"Response error: %@", [response.error description]);
            }];
            
        }
        
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"Response error: %@", [response.error description]);
    }];
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifer = @"SimpletableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifer];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifer];
    }
    
    cell.textLabel.text = [self.countrycodeData objectAtIndex:indexPath.row];
    
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.countrycodeData count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.countrycodetableView cellForRowAtIndexPath:indexPath];
    NSString *str = cell.textLabel.text;
    NSArray *item = [str componentsSeparatedByString:@"+"];
    NSString *str2=[item objectAtIndex:1];
    NSString *myString = [NSString stringWithFormat:@"%@%@", @"+" , str2];
    _countrycodetxt.text = myString;
    _countrycodetableView.hidden = true;
    [self.mobilenumbertxt becomeFirstResponder];
}

@end
