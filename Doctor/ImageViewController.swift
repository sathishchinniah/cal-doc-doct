import UIKit

class ImageViewController: UIViewController {

    @IBOutlet var imageview: UIImageView!
    
    var theimage: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageview.image = theimage

        let rtbtnImage = UIImage(named: "share")
        let rightbtn = UIButton(type: .custom)
        rightbtn.bounds = CGRect(x: 0, y: 0, width: 40, height: 48)
        rightbtn.addTarget(self, action: #selector(self.shareBtnPressed), for: .touchUpInside)
        rightbtn.setImage(rtbtnImage, for: .normal)
        let shareButton = UIBarButtonItem(customView: rightbtn)
        navigationItem.rightBarButtonItem = shareButton

        let btnImage = UIImage(named: "backBtn")
        let leftbtn = UIButton(type: .custom)
        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        leftbtn.setImage(btnImage, for: .normal)
        let backButton = UIBarButtonItem(customView: leftbtn)
        navigationItem.leftBarButtonItem = backButton

    }
    
    @objc func shareBtnPressed(_ sender: Any) {
        let activityviewcontroller = UIActivityViewController(activityItems: [imageview.image!], applicationActivities: nil)
        activityviewcontroller.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(activityviewcontroller, animated: true, completion: nil)
    }

    @objc func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
