import UIKit
import KDCircularProgress
import SwiftCharts

class Slide: UIView {

    @IBOutlet var fullView: UIView!
    @IBOutlet var label: UILabel!
    @IBOutlet var line1: UIView!
    @IBOutlet var mycallslb: UILabel!
    @IBOutlet var callnums: UILabel!
    @IBOutlet var callcval: UILabel!
    @IBOutlet var billedval: UILabel!
    @IBOutlet var callslb: UILabel!
    @IBOutlet var billedlb: UILabel!
    @IBOutlet var hourslb: UILabel!
    @IBOutlet var adlabel1: UILabel!
    @IBOutlet var adlabel2: UILabel!
    @IBOutlet var adbuttonclick: UIButton!
    @IBOutlet var circleProgressBar: KDCircularProgress!
    @IBOutlet var barchartView: UIView!
    
    @IBOutlet weak var circleProgressBarConstXdistance: NSLayoutConstraint!
    @IBOutlet weak var circleProgressBarConstYdistance: NSLayoutConstraint!
    @IBOutlet weak var circleProgressBarWidth: NSLayoutConstraint!
    @IBOutlet weak var circleProgressBarHight: NSLayoutConstraint!
    @IBOutlet weak var mycallslbtop: NSLayoutConstraint!
    @IBOutlet weak var billinglbleadingXspaceconstraint: NSLayoutConstraint!
    @IBOutlet weak var mycallsaverageverticalconstraint: NSLayoutConstraint!
    @IBOutlet weak var mycallsaverageLeadingconstraint: NSLayoutConstraint!
    @IBOutlet weak var mybillingavrageLeadingconstraint: NSLayoutConstraint!

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        if UI_USER_INTERFACE_IDIOM() == .phone {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            
            if iOSDeviceScreenSize.height == 812 {  //iphone X
                fullView.translatesAutoresizingMaskIntoConstraints = true
                fullView.frame = CGRect(x: fullView.frame.origin.x+0, y: fullView.frame.origin.y+05, width: iOSDeviceScreenSize.width, height: iOSDeviceScreenSize.height)
            }
            
            if iOSDeviceScreenSize.height == 736 {  //iphone 6 Plus
                fullView.translatesAutoresizingMaskIntoConstraints = true
                fullView.frame = CGRect(x: fullView.frame.origin.x+0, y: fullView.frame.origin.y-20, width: iOSDeviceScreenSize.width, height: iOSDeviceScreenSize.height-505)
            }
            
            if iOSDeviceScreenSize.height == 667 {  //iphone 6
                fullView.translatesAutoresizingMaskIntoConstraints = true
                fullView.frame = CGRect(x: fullView.frame.origin.x+0, y: fullView.frame.origin.y+5, width: iOSDeviceScreenSize.width, height: iOSDeviceScreenSize.height)
            }
            
            if iOSDeviceScreenSize.height == 568 {  //iphone 5
                fullView.translatesAutoresizingMaskIntoConstraints = true
                fullView.frame = CGRect(x: fullView.frame.origin.x+0, y: fullView.frame.origin.y+13, width: iOSDeviceScreenSize.width, height: iOSDeviceScreenSize.height)
            }
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {   //for ipad 9.7 inch
                
                fullView.translatesAutoresizingMaskIntoConstraints = true
                fullView.frame = CGRect(x: fullView.frame.origin.x+0, y: fullView.frame.origin.y+10, width: iOSDeviceScreenSize.width, height: iOSDeviceScreenSize.height)
                mycallslbtop.constant = (-35.0)
                billinglbleadingXspaceconstraint.constant = (-45.0)
                mycallsaverageverticalconstraint.constant = 85
                mycallsaverageLeadingconstraint.constant = 75
                mybillingavrageLeadingconstraint.constant = (-45.0)
                
                circleProgressBar.frame = CGRect(x: 150, y: 0, width: 400, height: 400)
                circleProgressBarConstXdistance.constant = 100
                circleProgressBarConstYdistance.constant = 10
                circleProgressBarWidth.constant = 220
                circleProgressBarHight.constant = 220
            } 
        }
    }

}
