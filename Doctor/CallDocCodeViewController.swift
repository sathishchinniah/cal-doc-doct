import UIKit

class CallDocCodeViewController: UIViewController {
    
    @IBOutlet weak var calldoccode: UILabel!
    @IBOutlet weak var appversion: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        calldoccode.layer.borderWidth = 1.0
        calldoccode.layer.borderColor = UIColor.lightGray.cgColor
        appversion.text = versionBuild()
        
        let thecalldoccode = QBSession.current.currentUser?.customData
        calldoccode.text = "  " + (thecalldoccode ?? "")
        
        let btnImage = UIImage(named: "backBtn")
        let leftbtn = UIButton(type: .custom)
        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        leftbtn.setImage(btnImage, for: .normal)
        let backButton = UIBarButtonItem(customView: leftbtn)
        navigationItem.leftBarButtonItem = backButton
    }

    @objc func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Helpers
    
    func appVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    func build() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    func versionBuild() -> String {
        let version = self.appVersion()
        let build = self.build()
        var versionBuild = String(format:"v%@",version)
        if version != build {
            versionBuild = String(format:"%@(%@)", versionBuild, build )
        }
        return versionBuild
    }

}
