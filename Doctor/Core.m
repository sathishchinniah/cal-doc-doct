//
//  Core.m
//  Doctor
//
//  Created by Manish Verma on 18/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import "Core.h"

#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>
#import <sys/socket.h>
#import <netinet/in.h>


@interface QBCore() <QBChatDelegate>

@property (strong, nonatomic) QBMulticastDelegate <QBCoreDelegate> *multicastDelegate;


@property (nonatomic, assign) SCNetworkReachabilityRef reachabilityRef;
@property (nonatomic, strong) dispatch_queue_t reachabilitySerialQueue;


@end

@implementation QBCore

+ (instancetype)instance {
    
    static QBCore *_core = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _core = [QBCore alloc];
        [_core commonInit];
    });
    
    return _core;
}

- (void)commonInit {
    
    _multicastDelegate = (id<QBCoreDelegate>)[[QBMulticastDelegate alloc] init];
    
    
    [self startReachabliyty];
}

- (void)addDelegate:(id <QBCoreDelegate>)delegate {
    
    [self.multicastDelegate addDelegate:delegate];
}

#pragma mark - Handle errors

//- (void)handleError:(NSError *)error domain:(ErrorDomain)domain {
//    
//    if ([self.multicastDelegate respondsToSelector:@selector(core:error:domain:)]) {
//        [self.multicastDelegate core:self error:error domain:domain];
//    }
//}

#pragma mark - Reachability

- (void)startReachabliyty {
    
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    _reachabilityRef = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr *)&zeroAddress);
    
    SCNetworkReachabilityContext context = { 0, NULL, NULL, NULL, NULL };
    context.info = (__bridge void *)self;
    
    if (SCNetworkReachabilitySetCallback(self.reachabilityRef, CoreReachabilityCallback, &context)) {
        
        self.reachabilitySerialQueue = dispatch_queue_create("com.quickblox.samplecore.reachability", NULL);
        // Set it as our reachability queue, which will retain the queue
        if (SCNetworkReachabilitySetDispatchQueue(self.reachabilityRef, self.reachabilitySerialQueue)) {
            
        }
        else {
            
            NSLog(@"SCNetworkReachabilitySetDispatchQueue() failed: %s", SCErrorString(SCError()));
            SCNetworkReachabilitySetCallback(self.reachabilityRef, NULL, NULL);
        }
    }
}

- (QBNetworkStatus)networkStatus {
    
    if (_reachabilityRef != NULL) {
        //NetworkStatus retVal = NotReachable;
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(_reachabilityRef, &flags)) {
            
            return [self networkStatusForFlags:flags];
        }
    }
    
    return NO;
}

- (QBNetworkStatus)networkStatusForFlags:(SCNetworkReachabilityFlags)flags {
    
    //    PrintReachabilityFlags(flags, "networkStatusForFlags");
    if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
    {
        // The target host is not reachable.
        return QBNetworkStatusNotReachable;
    }
    
    QBNetworkStatus returnValue = QBNetworkStatusNotReachable;
    
    if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
    {
        /*
         If the target host is reachable and no connection is required then we'll assume (for now) that you're on Wi-Fi...
         */
        returnValue = QBNetworkStatusReachableViaWiFi;
    }
    
    if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
         (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
    {
        /*
         ... and the connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs...
         */
        
        if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
        {
            /*
             ... and no [user] intervention is needed...
             */
            returnValue = QBNetworkStatusReachableViaWiFi;
        }
    }
    
    if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
    {
        /*
         ... but WWAN connections are OK if the calling application is using the CFNetwork APIs.
         */
        returnValue = QBNetworkStatusReachableViaWWAN;
    }
    
    return returnValue;
}

// Start listening for reachability notifications on the current run loop
static void CoreReachabilityCallback(SCNetworkReachabilityRef target, SCNetworkReachabilityFlags flags, void* info) {
    
    QBCore *core = ((__bridge QBCore*)info);
    
    @autoreleasepool {
        
        [core reachabilityChanged:flags];
    }
}

- (void)reachabilityChanged:(SCNetworkReachabilityFlags)flags {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.networkStatusBlock) {
            self.networkStatusBlock([self networkStatusForFlags:flags]);
        }
    });
}

@end

