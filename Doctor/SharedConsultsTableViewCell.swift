import UIKit

class SharedConsultsTableViewCell: UITableViewCell {

    @IBOutlet var doctornamelb: UILabel!
    @IBOutlet var doctorspecialitylb: UILabel!
    @IBOutlet var lockbuttonview: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor.white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
