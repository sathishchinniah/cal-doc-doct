import Foundation

class HomeViewHelper {

    static let sharedInstance = HomeViewHelper()

//    static let defaultArray = [false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
     static let defaultArray = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false]

    var mondayArrayOfBools = defaultArray
    var tuesdayArrayOfBools = defaultArray
    var wednesdayArrayOfBools = defaultArray
    var thursdayArrayOfBools = defaultArray
    var fridayArrayOfBools = defaultArray
    var saturdayArrayOfBools = defaultArray
    var sundayArrayOfBools = defaultArray
    var nextScheduleDay = ""
    var nextOnlineDay = ""
    var timerOn = false
    var currentOnline = false

    func getSchedularDatafromBackend(completion: @escaping (Bool) -> ()) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id

        QBRequest.objects(withClassName: "DocSchedularDayView", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in

            if ((contributors?.count)! > 0) {
                self.mondayArrayOfBools = contributors![0].fields?.value(forKey: "Monday") as! [Bool]
                self.tuesdayArrayOfBools = contributors![0].fields?.value(forKey: "Tuesday") as! [Bool]
                self.wednesdayArrayOfBools = contributors![0].fields?.value(forKey: "Wednesday") as! [Bool]
                self.thursdayArrayOfBools = contributors![0].fields?.value(forKey: "Thursday") as! [Bool]
                self.fridayArrayOfBools = contributors![0].fields?.value(forKey: "Friday") as! [Bool]
                self.saturdayArrayOfBools = contributors![0].fields?.value(forKey: "Saturday") as! [Bool]
                self.sundayArrayOfBools = contributors![0].fields?.value(forKey: "Sunday") as! [Bool]

                UserDefaults.standard.set(self.mondayArrayOfBools, forKey: Constants.mondaySchedularData)
                UserDefaults.standard.set(self.tuesdayArrayOfBools, forKey: Constants.tuesdaySchedularData)
                UserDefaults.standard.set(self.wednesdayArrayOfBools, forKey: Constants.wednesdaySchedularData)
                UserDefaults.standard.set(self.thursdayArrayOfBools, forKey: Constants.thursdaySchedularData)
                UserDefaults.standard.set(self.fridayArrayOfBools, forKey: Constants.fridaySchedularData)
                UserDefaults.standard.set(self.saturdayArrayOfBools, forKey: Constants.saturdaySchedularData)
                UserDefaults.standard.set(self.sundayArrayOfBools, forKey: Constants.sundaySchedularData)
                
            }
            completion(true)
            
        }) { (errorresponse) in
            print("HomeViewController: getSchedularDatafromBackend() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }

    func getDoctorStatus(completion: @escaping (String, String) -> ()) {

        getSchedularDatafromBackend { (success) in
            if success {
                let dayOfTheWeek = self.getDayOfWeek()
                var nextScheduleArray = [Bool]()
                if dayOfTheWeek == 1 {
                    completion(self.getCurrentSchedule(array: self.sundayArrayOfBools), self.getNextSchedule(todayArray: self.sundayArrayOfBools, tomoArray: self.getNextScheduleArray(currentDay: dayOfTheWeek)/*self.mondayArrayOfBools*/))
                }

                if dayOfTheWeek == 2 {
                    completion(self.getCurrentSchedule(array: self.mondayArrayOfBools), self.getNextSchedule(todayArray: self.mondayArrayOfBools, tomoArray: self.getNextScheduleArray(currentDay: dayOfTheWeek)/*self.tuesdayArrayOfBools*/))
                }

                if dayOfTheWeek == 3 {
                    completion(self.getCurrentSchedule(array: self.tuesdayArrayOfBools), self.getNextSchedule(todayArray: self.tuesdayArrayOfBools, tomoArray: self.getNextScheduleArray(currentDay: dayOfTheWeek)/*self.wednesdayArrayOfBools*/))
                }

                if dayOfTheWeek == 4 {
                    completion(self.getCurrentSchedule(array: self.wednesdayArrayOfBools), self.getNextSchedule(todayArray: self.wednesdayArrayOfBools, tomoArray: self.getNextScheduleArray(currentDay: dayOfTheWeek)/*self.thursdayArrayOfBools*/))
                }

                if dayOfTheWeek == 5 {
                    completion(self.getCurrentSchedule(array: self.thursdayArrayOfBools), self.getNextSchedule(todayArray: self.thursdayArrayOfBools, tomoArray: self.getNextScheduleArray(currentDay: dayOfTheWeek)/*self.fridayArrayOfBools*/))
                }

                if dayOfTheWeek == 6 {
                    completion(self.getCurrentSchedule(array: self.fridayArrayOfBools), self.getNextSchedule(todayArray: self.fridayArrayOfBools, tomoArray: self.getNextScheduleArray(currentDay: dayOfTheWeek)/*self.saturdayArrayOfBools*/))
                }

                if dayOfTheWeek == 7 {
                    completion(self.getCurrentSchedule(array: self.saturdayArrayOfBools), self.getNextSchedule(todayArray: self.saturdayArrayOfBools, tomoArray: self.getNextScheduleArray(currentDay: dayOfTheWeek)/*self.sundayArrayOfBools*/))
                }
            }
        }
    }
    
    func getNextScheduleArray(currentDay: Int) -> [Bool] {
        
        var day: Int
        if currentDay == 7 {
            day = 0
        } else {
            day = currentDay
        }
        //var day = currentDay
        var nextScheduleArray = [Bool]()
        
        let arrayOfSchedules = [self.sundayArrayOfBools, self.mondayArrayOfBools, self.tuesdayArrayOfBools, self.wednesdayArrayOfBools, self.thursdayArrayOfBools, self.fridayArrayOfBools, self.saturdayArrayOfBools]
       let daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        
        while nextScheduleArray.isEmpty {
            let tempArr = arrayOfSchedules[day]
            for item in tempArr {
                if item {
                    nextScheduleArray = tempArr
                    //let index = arrayOfSchedules.index(of: tempArr)
                    if /*index*/day == Calendar.current.component(.weekday, from: Date()) {
                        //next day
                        nextScheduleDay = "Tomorrow"
                    } else {
                        //get the day
                        nextScheduleDay = daysOfWeek[day/*index!*/]
                    }
                    print("nextScheduleDay\(nextScheduleDay)")
                    return nextScheduleArray//break
                }
            }
            day = day + 1
            if day > 6 {
                day = 0
            }
            //continue
        }
        
        return nextScheduleArray
    }

    func getDayOfWeek() -> Int {
        return Calendar.current.component(.weekday, from: Date())
    }
    
    func getNextSchedule(todayArray: [Bool], tomoArray: [Bool]) -> String {

        let todayDate = Date()
        let hour = Calendar.current.component(.hour, from: todayDate)
        let minute = Calendar.current.component(.minute, from: todayDate) > 30 ? 0.5 : 0.0
        let currentTime = Double(hour) + minute

        var todaySelectedSchedules = [Double]()
        var filteredTodaySchedules = [Double]()
        var tomoSelectedSchedules = [Double]()

        //Fetch all the selected Schedules for Today, future
        for (index, item) in todayArray.enumerated() {
            let time: Double = (Double(index) / 2.0)
            if item {
                //Filtering out the past date
                if time >= currentTime {
                    todaySelectedSchedules.append(time)
                }
            }
        }

        //Fetch all the selected Schedules for tomorrow.
        for (index, item) in tomoArray.enumerated() {
            let time: Double = (Double(index) / 2.0)
            if item {
                tomoSelectedSchedules.append(time)
            }
        }


        for (index, item) in todaySelectedSchedules.enumerated() {
            if item != currentTime + (Double(index) * 0.5) {
                filteredTodaySchedules.append(item)
            }
        }

        var startSchedule = 999.0
        var endSchedule = 999.0
        var isNextDayScheduleContinued = false

        if filteredTodaySchedules.count > 0 {
            for (index, item) in filteredTodaySchedules.enumerated() {
                if index == 0 {
                    startSchedule = item
                    endSchedule = item + 0.5
                } else {
                    if (item + 0.5 > endSchedule) && (endSchedule/* + 0.5*/ == item) {
                        endSchedule = item + 0.5
                    }
                }
            }
        }

        if startSchedule == 999.0 || endSchedule == 23.5 {
            for (index, item) in tomoSelectedSchedules.enumerated() {
                if index == 0 {
                    if startSchedule == 999.0 {
                        isNextDayScheduleContinued = true
                        startSchedule = item
                        endSchedule = item + 0.5
                    } else if item == 0.0 {
                        endSchedule = 0.5//0.0
                    }
                } else {
                    if (item + 0.5 > endSchedule) && (endSchedule/* + 0.5*/ == item) {
                        endSchedule = item + 0.5
                    }
                }
            }
        }

        print("NEXT SCHEDULE DAY: \(nextScheduleDay)")
        if startSchedule != 999.0 || endSchedule != 999.0 {
            if isNextDayScheduleContinued {
                return "\(nextScheduleDay): \(getTimeFormatted(time: startSchedule)) to \(getTimeFormatted(time: endSchedule/*+0.5*/))"
            } else {
                return "Today: \(getTimeFormatted(time: startSchedule)) to \(getTimeFormatted(time: endSchedule))"
            }
        }

        return ""
    }

    func getCurrentSchedule(array: [Bool]) -> String {

        let todayDate = Date()
        let hour = Calendar.current.component(.hour, from: todayDate)
        let minute = Calendar.current.component(.minute, from: todayDate) > 30 ? 0.5 : 0.0
        let currentTime = Double(hour) + minute

        var selectedSchedules = [Double]()
        var unSelectedSchedules = [Double]()

        for (index, item) in array.enumerated() {
            let time: Double = (Double(index) / 2.0)
            if item {
                selectedSchedules.append(time)
            } else {
                unSelectedSchedules.append(time)
            }
        }

        for (index, selectedTime) in selectedSchedules.enumerated() {
            if selectedTime == currentTime{
                let nextScheduleIndex = index + 1
                updateOnlineStatus(status: true)
                if (nextScheduleIndex + 1) <= selectedSchedules.count {
                    currentOnline = true
                    let time = getScheduleTime(index: nextScheduleIndex, selectedSchedules: selectedSchedules)
//                    runTimerToUpdateOnlineOfflineStatus(time: time)
                    return "Online till \(time)"
                } else {
                    return "Online"
                }
            }
        }

        for (index, unSelectedTime) in unSelectedSchedules.enumerated() {
            if unSelectedTime == currentTime {
                let nextScheduleIndex = index + 1
                updateOnlineStatus(status: false)
                if (nextScheduleIndex + 1) <= unSelectedSchedules.count {
                    currentOnline = false
                    let time = getScheduleTime(index: nextScheduleIndex, selectedSchedules: unSelectedSchedules)
//                    runTimerToUpdateOnlineOfflineStatus(time: time)
                    return "Next Online at \(nextOnlineDay) \(time)"
                } else {
                    return "Offline"
                }
            }
        }

        return ""
    }

    func getScheduleTime(index: Int, selectedSchedules: [Double]) -> String {

        let nextSchedules = currentOnline ? Array(selectedSchedules[(index - 1)..<selectedSchedules.count]) : Array(selectedSchedules[(index - 1)..<selectedSchedules.count])//Array(selectedSchedules[index..<selectedSchedules.count])

        var nextScheduleTime = 0.0

        for (nIndex, nextSchedule) in nextSchedules.enumerated() {
            if nIndex == 0 {
                nextScheduleTime = nextSchedule
            } else {
                if nextSchedule == (nextScheduleTime + 0.5) {
                    nextScheduleTime = nextSchedule
                }
            }
            
            
        }

        if nextScheduleTime + 0.5 == 24.0 {
            //return "12.00 AM"
            let nextSchedule = getNextScheduleArray(currentDay: self.getDayOfWeek())
            if nextSchedule.first == true || !currentOnline {
                let time = getNextScheduleTime(nextSchedule: nextSchedule)
                nextOnlineDay = nextScheduleDay
                return time
            } else {
                return "12.00 AM"
            }
            
            //return getCurrentSchedule(array: nextSchedule)
            
        }

        if nextScheduleTime + 0.5 == 24.5 {
            return "12.00 AM"
        }
        nextOnlineDay = "Today"
        return  getTimeFormatted(time: (nextScheduleTime + 0.5))

    }
    
    func getNextScheduleTime(nextSchedule: [Bool]) -> String {
        let todayDate = Date()
        let hour = Calendar.current.component(.hour, from: todayDate)
        let minute = Calendar.current.component(.minute, from: todayDate) > 30 ? 0.5 : 0.0
        let currentTime = Double(hour) + minute
        
        var selectedSchedules = [Double]()
        var unSelectedSchedules = [Double]()
        
        for (index, item) in nextSchedule.enumerated() {
            let time: Double = (Double(index) / 2.0)
            if item {
                selectedSchedules.append(time)
            } else {
                unSelectedSchedules.append(time)
            }
        }
        if selectedSchedules.first == 00.00 {
            
        } else { }
        for (index, selectedTime) in selectedSchedules.enumerated() {
           // if selectedTime/* == currentTime */{
                let nextScheduleIndex = index/* + 1*/
                //updateOnlineStatus(status: true)
                if (nextScheduleIndex + 1) <= selectedSchedules.count {
                    //var time = getScheduleTime(index: (nextScheduleIndex), selectedSchedules: selectedSchedules)
                    //let nextSchedules = Array(selectedSchedules[nextScheduleIndex..<selectedSchedules.count])
                    
                    var nextScheduleTime = selectedSchedules.first ?? 0.0
                    
                    /*for (nIndex, nextSchedule) in nextSchedules.enumerated() {
                        if nIndex == 0 {
                            nextScheduleTime = nextSchedule
                        } else {
                            if nextSchedule == (nextScheduleTime + 0.5) {
                                nextScheduleTime = nextSchedule
                            }
                        }
                    } */
                    return  getTimeFormatted(time: (nextScheduleTime))
                    //                    runTimerToUpdateOnlineOfflineStatus(time: time)
                    
                    //return time
                } else {
                    return ""
                }
            //}
        }
        return ""
    }

    func getTimeFormatted(time: Double) -> String {

        var formattedTime: Double = time

        var isAM = true

        if time >= 12 {
            if time > 12.5 {
                formattedTime = time - 12
            }
            isAM = false
        }

        let valueArray = "\(formattedTime)".components(separatedBy: ".")
        if let intValue = valueArray.first, let decimalValue = valueArray.last, let decimalDouble = Double(decimalValue) {
            return "\(intValue):\(decimalDouble == 5.0 ? "30" : "00") \(isAM ? "AM" : "PM")"
        }

        return ""
    }

    func setUserDefaultsForSchedular() {
        UserDefaults.standard.set(HomeViewHelper.defaultArray, forKey: Constants.mondaySchedularData)
        UserDefaults.standard.set(HomeViewHelper.defaultArray, forKey: Constants.tuesdaySchedularData)
        UserDefaults.standard.set(HomeViewHelper.defaultArray, forKey: Constants.wednesdaySchedularData)
        UserDefaults.standard.set(HomeViewHelper.defaultArray, forKey: Constants.thursdaySchedularData)
        UserDefaults.standard.set(HomeViewHelper.defaultArray, forKey: Constants.fridaySchedularData)
        UserDefaults.standard.set(HomeViewHelper.defaultArray, forKey: Constants.saturdaySchedularData)
        UserDefaults.standard.set(HomeViewHelper.defaultArray, forKey: Constants.sundaySchedularData)
    }

    func updateOnlineStatus(status: Bool) {
        UserDefaults.standard.set(status, forKey: Constants.KEY_DOCTOR_ONLINE)
        UserDefaults.standard.set(!status, forKey: Constants.KEY_DOCTOR_OFFLINE)
        if !status && UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_PAUSED) {
            UserDefaults.standard.set(false, forKey: Constants.KEY_DOCTOR_PAUSED)
        }
    }

    func runTimerToUpdateOnlineOfflineStatus(time: String) {

        if !UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) && !timerOn {
            timerOn = true
            let hourAndMinutesArray = time.components(separatedBy: " ")

            if hourAndMinutesArray.count == 2, let hour = "\(hourAndMinutesArray[0])".components(separatedBy: ":").first , let minute = "\(hourAndMinutesArray[0])".components(separatedBy: ":").last, let amStatus = hourAndMinutesArray.last {

                let isAM = amStatus == "AM" ? true : false

                var hourDouble = Double(hour) ?? 0.0
                let minuteDouble = minute == "30" ? 0.5 : 0.0

                // converting to 24 hr format
                if !isAM {
                    hourDouble = hourDouble + 12
                }

                if hourDouble == 24.0 {
                    hourDouble = 0.0
                } else if hourDouble == 24.5 {
                    hourDouble = 0.5
                }

                var dateComponents = DateComponents()
                dateComponents.day = Calendar.current.component(.day, from: Date())
                dateComponents.month = Calendar.current.component(.month, from: Date())
                dateComponents.year = Calendar.current.component(.year, from: Date())
                dateComponents.minute = Int(minuteDouble * 60)
                dateComponents.hour = Int(hourDouble)

                let date = Calendar.current.date(from: dateComponents)

                let timer = Timer(fireAt: date!, interval: 0, target: self, selector: #selector(notify), userInfo: nil, repeats: false)
                RunLoop.main.add(timer, forMode: RunLoopMode.commonModes)
            }

        }

    }

    @objc func notify() {
        timerOn = false
       // NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: Constants.REFRESH_HOME_SCREEN)))
    }

}
