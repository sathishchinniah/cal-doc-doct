import Foundation

class DoctorScheduleHelper {

    static func updateScheduleUserDefaultsValue(object: QBCOCustomObject?) {
        if let object = object {
            if let mondayArrayOfBools = object.fields["Monday"] as? [Bool] {
                UserDefaults.standard.set(mondayArrayOfBools, forKey: Constants.mondaySchedularData)
            }

            if let tuesdayArrayOfBools = object.fields["Tuesday"] as? [Bool] {
                UserDefaults.standard.set(tuesdayArrayOfBools, forKey: Constants.tuesdaySchedularData)
            }

            if let wednesdayArrayOfBools = object.fields["Wednesday"] as? [Bool] {
                UserDefaults.standard.set(wednesdayArrayOfBools, forKey: Constants.wednesdaySchedularData)
            }

            if let thursdayArrayOfBools = object.fields["Thursday"] as? [Bool] {
                UserDefaults.standard.set(thursdayArrayOfBools, forKey: Constants.thursdaySchedularData)
            }

            if let fridayArrayOfBools = object.fields["Friday"] as? [Bool] {
                UserDefaults.standard.set(fridayArrayOfBools, forKey: Constants.fridaySchedularData)
            }

            if let saturdayArrayOfBools = object.fields["Saturday"] as? [Bool] {
                UserDefaults.standard.set(saturdayArrayOfBools, forKey: Constants.saturdaySchedularData)
            }

            if let sundayArrayOfBools = object.fields["Sunday"] as? [Bool] {
                UserDefaults.standard.set(sundayArrayOfBools, forKey: Constants.sundaySchedularData)
            }
        }
    }
}
