//
//  RegisterViewController.h
//  Doctor
//
//  Created by CallDoc on 13/03/18.
//  Copyright © 2018 calldoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GData;

@interface RegisterViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,retain) GData *dataPointer;
@property (nonatomic,retain) NSArray *countrycodeData;

@end
