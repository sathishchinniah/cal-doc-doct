//
//  CallViewController.m
//  Doctor
//
//  Created by Manish Verma on 18/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import "GData.h"
#import "CallViewController.h"



static const NSTimeInterval kRefreshTimeInterval = 1.f;

@interface CallViewController () <QBRTCClientDelegate, QBRTCAudioSessionDelegate>

@property (strong, nonatomic) IBOutlet QBRTCRemoteVideoView *mylocalopponentVideoView;
@property (strong, nonatomic) IBOutlet UIView *localVideoView;
@property (strong, nonatomic) IBOutlet UIButton *videocallMutebutton;
@property (strong, nonatomic) IBOutlet UIButton *videoonoffbutton;
@property (strong, nonatomic) IBOutlet UIButton *videosoundonoffbutton;
@property (strong, nonatomic) IBOutlet UIButton *videocallcancelbutton;
@property (strong, nonatomic) IBOutlet UIButton *SwitchCameraButton;
@property (strong, nonatomic) IBOutlet UIButton *chatButton;
@property (strong, nonatomic) IBOutlet UILabel *callTiminglb;
@property (strong, nonatomic) IBOutlet UILabel *callstatuslb;
@property (strong, nonatomic) UINavigationController *nav;
@property (assign, nonatomic) NSTimeInterval timeDuration;
@property (strong, nonatomic) NSTimer *callTimer;
@property (assign, nonatomic) NSTimer *beepTimer;
@property (strong, nonatomic) QBRTCCameraCapture *cameraCapture;
@property (strong, nonatomic) QBRTCVideoFormat *videoFormat;
@property (assign, nonatomic) AVCaptureDevicePosition preferredCameraPostion;

@end

@implementation CallViewController

UInt8 callingnumber = 0;

- (void)awakeFromNib {
    [super awakeFromNib];
    NSLog(@"CallViewController:awakeFromNib()");
    self.dataPointer = [GData sharedData];
    self.session = self.dataPointer.theSession;
    [self viewDidLoad];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataPointer = [GData sharedData];
    
    [QBRTCClient.instance addDelegate:self];
    self.mylocalopponentVideoView.hidden=YES;
    
    self.navigationItem.leftBarButtonItem.accessibilityElementsHidden = true;
    [self.navigationItem setHidesBackButton:YES];

    [[QBRTCAudioSession instance] initializeWithConfigurationBlock:^(QBRTCAudioSessionConfiguration *configuration) {
        // adding blutetooth support
        configuration.categoryOptions |= AVAudioSessionCategoryOptionAllowBluetooth;
        configuration.categoryOptions |= AVAudioSessionCategoryOptionAllowBluetoothA2DP;
        
        // adding airplay support
        configuration.categoryOptions |= AVAudioSessionCategoryOptionAllowAirPlay;
        
        if (_session.conferenceType == QBRTCConferenceTypeVideo) {
            // setting mode to video chat to enable airplay audio and speaker only
            configuration.mode = AVAudioSessionModeVideoChat;
        }
    }];

    if (self.dataPointer.thecurrentuserID > 0) {
        [self startCall];
    } else if (self.session.initiatorID.unsignedIntegerValue > 0) {
        [self acceptCall];
    }

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationItem setHidesBackButton:YES];
}

- (void)startCall {
    //Begin play calling sound
    self.beepTimer = [NSTimer scheduledTimerWithTimeInterval:[QBRTCConfig dialingTimeInterval]
                                                      target:self
                                                    selector:@selector(playCallingSound:)
                                                    userInfo:nil
                                                     repeats:YES];
    [self playCallingSound:nil];
    
    if ((self.session.conferenceType == QBRTCConferenceTypeVideo) || (self.session.conferenceType == QBRTCConferenceTypeAudio)){
        NSLog(@"CallViewController:OutgoingCall with Session=%@",self.session);
        
        NSString *calltype = self.session.conferenceType == QBRTCConferenceTypeVideo ? @"Video call to:" : @"Audio call to:";
        self.callstatuslb.text = [NSString stringWithFormat:@"%@ %@",calltype,self.dataPointer.calledUserName];
        
        self.callstatuslb.hidden = NO;
        self.callstatuslb.backgroundColor = [UIColor clearColor];
        [self.mylocalopponentVideoView addSubview:self.callstatuslb];
        
        self.callTiminglb.hidden = NO;
        self.callTiminglb.backgroundColor = [UIColor clearColor];
        [self.mylocalopponentVideoView addSubview:self.callTiminglb];
        
        [self.mylocalopponentVideoView addSubview:self.videocallcancelbutton];
        [self.mylocalopponentVideoView addSubview:self.videosoundonoffbutton];
        [self.mylocalopponentVideoView addSubview:self.videoonoffbutton];
        [self.mylocalopponentVideoView addSubview:self.videocallMutebutton];
        [self.mylocalopponentVideoView addSubview:self.chatButton];
        
        self.mylocalopponentVideoView.hidden = NO;
        self.mylocalopponentVideoView.backgroundColor = [UIColor clearColor];
        self.mylocalopponentVideoView.contentMode = UIViewContentModeScaleAspectFill;
        self.localVideoView.hidden = NO;
        self.localVideoView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.mylocalopponentVideoView];
        [self.mylocalopponentVideoView addSubview:self.localVideoView];

        //This part is for local video
        QBRTCVideoFormat *videoFormat = [[QBRTCVideoFormat alloc] init];
        videoFormat.frameRate = 30;
        videoFormat.pixelFormat =  QBRTCPixelFormat420f; //QBRTCVideoCodecH264;
        videoFormat.width = 640;
        videoFormat.height = 480;
        self.cameraCapture = [[QBRTCCameraCapture alloc] initWithVideoFormat:videoFormat position:AVCaptureDevicePositionFront];
        self.cameraCapture.previewLayer.frame = self.localVideoView.bounds;
        [self.cameraCapture startSession:nil];
        [self.localVideoView.layer insertSublayer:self.cameraCapture.previewLayer atIndex:0];
        self.session.localMediaStream.videoTrack.videoCapture = self.cameraCapture;
        self.localVideoView.contentMode = UIViewContentModeScaleAspectFill;
    }
    //Start call
    NSDictionary *userInfo = @{@"name" : @"CallDoc",
                               @"url" : @"http.calldoc.com",
                               @"param" : @"\"1,2,3,4\""};
    
    [self.session startCall:userInfo];
}


- (void)acceptCall {
    
     if ((self.session.conferenceType == QBRTCConferenceTypeVideo) || (self.session.conferenceType == QBRTCConferenceTypeAudio)) {
        NSLog(@"CallViewController:AcceptedIncomingCall with Session=%@",self.session);
        
        self.callTiminglb.hidden = NO;
        self.callTiminglb.backgroundColor = [UIColor clearColor];
        [self.mylocalopponentVideoView addSubview:self.callTiminglb];
        
        [self.mylocalopponentVideoView addSubview:self.videocallcancelbutton];
        [self.mylocalopponentVideoView addSubview:self.videosoundonoffbutton];
        [self.mylocalopponentVideoView addSubview:self.videoonoffbutton];
        [self.mylocalopponentVideoView addSubview:self.videocallMutebutton];
        [self.mylocalopponentVideoView addSubview:self.chatButton];
        
        self.mylocalopponentVideoView.hidden = NO;
        self.mylocalopponentVideoView.backgroundColor = [UIColor clearColor];
        self.mylocalopponentVideoView.contentMode = UIViewContentModeScaleAspectFill;
        self.localVideoView.hidden = NO;
        self.localVideoView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.mylocalopponentVideoView];
        [self.mylocalopponentVideoView addSubview:self.localVideoView];
        
        //This part is for local video
        QBRTCVideoFormat *videoFormat = [[QBRTCVideoFormat alloc] init];
        videoFormat.frameRate = 30;
        videoFormat.pixelFormat = QBRTCVideoCodecH264; //QBRTCPixelFormat420f;
        videoFormat.width = 640;
        videoFormat.height = 480;
        self.cameraCapture = [[QBRTCCameraCapture alloc] initWithVideoFormat:videoFormat position:AVCaptureDevicePositionFront];
        self.cameraCapture.previewLayer.frame = self.localVideoView.bounds;
        [self.cameraCapture startSession:nil];
        [self.localVideoView.layer insertSublayer:self.cameraCapture.previewLayer atIndex:0];
        self.session.localMediaStream.videoTrack.videoCapture = self.cameraCapture;
        self.localVideoView.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    [[QMSoundManager instance] stopAllSounds];
    //Accept call
    NSDictionary *userInfo = @{@"acceptCall" : @"userInfo"};
    [self.session acceptCall:userInfo];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)VideoCallCancelButtonClicked:(id)sender {
    
    __weak __typeof(self)weakSelf = self;
    
    [weakSelf.callTimer invalidate];
    weakSelf.callTimer = nil;
    [weakSelf.session hangUp:@{@"hangup" : @"hang up"}];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"HomeNavController"];
    [self.navigationController presentViewController:VC animated:YES completion:nil];
}

- (IBAction)videoSpeakerButtonClicked:(id)sender {
    
    self.videosoundonoffbutton.selected = !self.videosoundonoffbutton.selected;
    QBRTCAudioDevice device = [QBRTCAudioSession instance].currentAudioDevice;
    
    [QBRTCAudioSession instance].currentAudioDevice =
    device == QBRTCAudioDeviceSpeaker ? QBRTCAudioDeviceReceiver : QBRTCAudioDeviceSpeaker;
}

- (IBAction)VideoOnOffButtonClicked:(id)sender {
    self.videoonoffbutton.selected = !self.videoonoffbutton.selected;
    self.session.localMediaStream.videoTrack.enabled ^=1;
    self.localVideoView.hidden = !self.session.localMediaStream.videoTrack.enabled;
}

- (IBAction)SoundMuteButtonClicked:(id)sender {
    self.session.localMediaStream.audioTrack.enabled ^=1;
    self.videocallMutebutton.selected = !self.videocallMutebutton.selected;
}

- (IBAction)SwitchCameraButtonClicked:(UIButton *)sender {
    
    AVCaptureDevicePosition position = self.cameraCapture.position;
    AVCaptureDevicePosition newPosition = position == AVCaptureDevicePositionBack ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack;
    
    if ([self.cameraCapture hasCameraForPosition:newPosition]) {
        
        CATransition *animation = [CATransition animation];
        animation.duration = .75f;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.type = @"oglFlip";
        
        if (position == AVCaptureDevicePositionFront) {
            animation.subtype = kCATransitionFromRight;
        } else if(position == AVCaptureDevicePositionBack) {
            animation.subtype = kCATransitionFromLeft;
        }
        
        [self.localVideoView.viewForFirstBaselineLayout.layer  addAnimation:animation forKey:nil];
        self.cameraCapture.position = newPosition;
    }
    
}

- (void)session:(QBRTCSession *)session initializedLocalMediaStream:(QBRTCMediaStream *)mediaStream {
    
    if (self.session ) {
        session.localMediaStream.videoTrack.videoCapture = self.cameraCapture;
    }
}



#pragma mark - QBRTCClientDelegate

- (void)session:(QBRTCSession *)session updatedStatsReport:(QBRTCStatsReport *)report forUserID:(NSNumber *)userID {
    NSString *result = [report statsString];
    NSLog(@"%@", result);
}

/**
 * Called in case when you are calling to user, but he hasn't answered
 */
- (void)session:(QBRTCSession *)session userDoesNotRespond:(NSNumber *)userID {

}

- (void)session:(QBRTCSession *)session acceptedByUser:(NSNumber *)userID userInfo:(NSDictionary *)userInfo {

}

/**
 * Called in case when opponent has rejected you call
 */
- (void)session:(QBRTCSession *)session rejectedByUser:(NSNumber *)userID userInfo:(NSDictionary *)userInfo {

}

/**
 *  Called in case when opponent hung up
 */
- (void)session:(QBRTCSession *)session hungUpByUser:(NSNumber *)userID userInfo:(NSDictionary *)userInfo {

}

/**
 *  Called in case when receive remote video track from opponent
 */

- (void)session:(QBRTCSession *)session receivedRemoteVideoTrack:(QBRTCVideoTrack *)videoTrack fromUser:(NSNumber *)userID {
    
    if (session == self.session) {
        
        
        NSLog(@"CallViewController:User %@ did receivedRemoteVideoTrack", userID);
        if (session == self.session)
        {
            self.callstatuslb.hidden = YES;
            QBRTCVideoTrack *remoteVideoTrak = [session remoteVideoTrackWithUserID:userID];
            [self.mylocalopponentVideoView setVideoTrack:remoteVideoTrak];
            self.mylocalopponentVideoView.contentMode = UIViewContentModeScaleAspectFill; //UIViewContentModeScaleToFill;
        }
    }
}

/**
 *  Called in case when connection is established with opponent
 */
- (void)session:(QBRTCSession *)session connectedToUser:(NSNumber *)userID {
    
    if (session == self.session) {
        if (self.beepTimer) {
            
            [self.beepTimer invalidate];
            self.beepTimer = nil;
            [[QMSoundManager instance] stopAllSounds];
        }
        
        if (!self.callTimer) {
            
            self.callTimer = [NSTimer scheduledTimerWithTimeInterval:kRefreshTimeInterval
                                                              target:self
                                                            selector:@selector(refreshCallTime:)
                                                            userInfo:nil
                                                             repeats:YES];
        }
    }
}

/**
 *  Called in case when connection state changed
 */
- (void)session:(QBRTCSession *)session connectionClosedForUser:(NSNumber *)userID {
    
    if (session == self.session) {
        
        NSLog(@"CallViewController:User %@ connectionClosedForUser", userID);
        [self.mylocalopponentVideoView setVideoTrack:Nil];
        self.mylocalopponentVideoView.hidden=YES;
        [self.mylocalopponentVideoView removeFromSuperview];
        self.localVideoView.hidden = YES;
        self.session=nil;
        self.dataPointer.theSession = nil;
        if (self.beepTimer) {
            [self.beepTimer invalidate];
            self.beepTimer = nil;
        }

        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

/**
 *  Called in case when disconnected from opponent
 */
- (void)session:(QBRTCSession *)session disconnectedFromUser:(NSNumber *)userID {

}

/**
 *  Called in case when disconnected by timeout
 */
- (void)session:(QBRTCSession *)session disconnectedByTimeoutFromUser:(NSNumber *)userID {

}

/**
 *  Called in case when connection failed with user
 */
- (void)session:(QBRTCSession *)session connectionFailedWithUser:(NSNumber *)userID {

}

/**
 *  Called in case when session will close
 */
- (void)sessionDidClose:(QBRTCSession *)session {
    
    if (session == self.session) {
        
        [self.cameraCapture stopSession:nil];
        
        [[QBRTCAudioSession instance] deinitialize];
        
        if (self.beepTimer) {
            
            [self.beepTimer invalidate];
            self.beepTimer = nil;
            [[QMSoundManager instance] stopAllSounds];
        }
        
        [self.callTimer invalidate];
        self.callTimer = nil;
    }
}

#pragma mark - QBRTCAudioSessionDelegate

- (void)audioSession:(QBRTCAudioSession *)audioSession didChangeCurrentAudioDevice:(QBRTCAudioDevice)updatedAudioDevice {
    BOOL isSpeaker = updatedAudioDevice == QBRTCAudioDeviceSpeaker;
}

#pragma mark - Timers actions

- (void)playCallingSound:(id)sender {
    
    [QMSoundManager playCallingSound];
}

- (void)refreshCallTime:(NSTimer *)sender {
    self.timeDuration += kRefreshTimeInterval;
    NSString *extraTitle = @" ";
    self.callTiminglb.text = [NSString stringWithFormat:@"%@ %@", extraTitle, [self stringWithTimeDuration:self.timeDuration]];
}

- (NSString *)stringWithTimeDuration:(NSTimeInterval )timeDuration {
    NSInteger minutes = timeDuration / 60;
    NSInteger seconds = (NSInteger)timeDuration % 60;
    NSString *timeStr = [NSString stringWithFormat:@"%ld:%02ld", (long)minutes, (long)seconds];
    return timeStr;
}

- (IBAction)chatButtonClicked:(UIButton *)sender {

}

@end
