import UIKit
import Fabric
import Crashlytics
import UserNotifications
var gsession: QBRTCSession?
var dialignTimer: Timer?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, NotificationServiceDelegate, QBRTCClientDelegate, UNUserNotificationCenterDelegate, QBChatDelegate {
    
    var window: UIWindow?
    var isLoggedin = false
    
    static let sharedInstance = AppDelegate()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        application.applicationIconBadgeNumber = 0
        window?.backgroundColor = UIColor.white;
        // Set QuickBlox credentials
        QBSettings.applicationID = 5
        QBSettings.authKey = "kQhRpZfJMwseN24"
        QBSettings.authSecret = "4MNHHZg9N3NRXKb"
        QBSettings.accountKey = "VAA3hEMsuMmBtg34kWg4"
        QBSettings.carbonsEnabled = true
        QBSettings.logLevel = .debug
        
        // Enables detailed XMPP logging in console output.
        QBSettings.enableXMPPLogging()
        
        FirebaseApp.configure()
        
        QBRTCClient.initializeRTC()
        QBRTCClient.instance().add(self)
        
        QBSettings.apiEndpoint = "https://api.calldoc.com"
        QBSettings.chatEndpoint = "chat.calldoc.com"
        
        // Intialized fabric
        Fabric.with([Crashlytics.self()])
        
        registerForRemoteNotification()
        // set the delegate in didFinishLaunchingWithOptions
        UNUserNotificationCenter.current().delegate = self
        
        
        // app was launched from push notification, handling it
        let remoteNotification: NSDictionary! = launchOptions?[.remoteNotification] as? NSDictionary
        if (remoteNotification != nil) {
            ServicesManager.instance().notificationService.pushDialogID = remoteNotification["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        isLoggedin = Bool(UserDefaults.standard.bool(forKey: "isLoggedIn") )
        if isLoggedin {
            print("AppDelegate:Already Loggedin successfully before")
            let VC = storyboard.instantiateViewController(withIdentifier: "HomeNavController") as? UINavigationController
            window?.rootViewController = VC
            return true
        }
        
        var ovc: UIViewController
        
        if (UserDefaults.standard.value(forKey: "OnBoard")as? String) == nil {
            // show the onboarding screen
            ovc = (storyboard.instantiateViewController(withIdentifier: "OnBoardVC") as? UIViewController)!
        } else {
            // show main screen
            ovc = storyboard.instantiateInitialViewController()!
        }
        
        window?.rootViewController = ovc
        
        let user = QBUUser()
        user.id = 0 as UInt
        user.password = ""
        QBChat.instance.connect(withUserID: user.id, password: user.password!) { (error: Error?) in
            //Code
        }
        
        ServicesManager.instance().chatService.connect(completionBlock: nil)
        if isLoggedin {
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: Constants.REFRESH_HOME_SCREEN)))
        }
        
        return true
    }
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: ([.sound, .alert, .badge]), completionHandler: {(_ granted: Bool, _ error: Error?) -> Void in
                DispatchQueue.main.async(execute: {() -> Void in
                    UIApplication.shared.registerForRemoteNotifications()
                })
            })
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        print("AppDelegate:didReceiveNewSession")
        if gsession == nil {
            gsession = session
            handleIncomingCall(userInfo: userInfo!)
        }
    }
    
    func handleIncomingCall(userInfo:Dictionary <String, String>) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "InComingCallViewController") as! InComingCallViewController
        VC.session = gsession
        VC.userInfo = userInfo
        
        self.window?.rootViewController =  VC
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
        let subscription = QBMSubscription()
        
        subscription.notificationChannel = .APNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("DEVICE TOKEN: \(token)")
        QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
          print(response)
            print(objects)
        }) { (response: QBResponse!) -> Void in
            print(response)
        }
        
//        let user = QBUUser()
//        user.id = 0 as UInt
//        user.password = ""
//        QBChat.instance.connect(withUserID: user.id, password: user.password!) { (error: Error?) in
//            //Code
//        }
//
//        ServicesManager.instance().chatService.connect(completionBlock: nil)
//        if isLoggedin {
//            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: Constants.REFRESH_HOME_SCREEN)))
//        }
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Push failed to register with error: %@", error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("AppDelegate:didReceiveRemoteNotification my push is: %@", userInfo)
        guard application.applicationState == UIApplicationState.active else {
            return
        }
        
        guard let dialogID = userInfo["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String else {
            return
        }
        
        guard !dialogID.isEmpty else {
            return
        }
        
        
        let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
        if dialogWithIDWasEntered == dialogID {
            return
        }
        
        ServicesManager.instance().notificationService.pushDialogID = dialogID
        
        // calling dispatch async for push notification handling to have priority in main queue
        DispatchQueue.main.async {
            ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(delegate: self)
        }
    }
    
    func application(application: UIApplication,  didReceiveRemoteNotification userInfo: [NSObject : AnyObject],  fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        
        print("Recived: \(userInfo)")
        
        completionHandler(.newData)
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Logging in to chat.
        // Setting up chat
        let user = QBUUser()
        user.id = 0 as UInt
        user.password = ""
        QBChat.instance.connect(withUserID: user.id, password: user.password!) { (error: Error?) in
            //Code
        }
        
        ServicesManager.instance().chatService.connect(completionBlock: nil)
        if isLoggedin {
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: Constants.REFRESH_HOME_SCREEN)))
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    // MARK: NotificationServiceDelegate protocol
    
    func notificationServiceDidStartLoadingDialogFromServer() {
        
    }
    
    func notificationServiceDidFinishLoadingDialogFromServer() {
        
    }
    
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!) {
        let navigatonController: UINavigationController! = /*self.window?.rootViewController*/UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        
        let chatController: ChatViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatController.dialog = chatDialog
        
        let dialogWithIDWasEntered = ServicesManager.instance().currentDialogID
        if !dialogWithIDWasEntered.isEmpty {
            // some chat already opened, return to dialogs view controller first
            navigatonController.popViewController(animated: false);
        }
        
        //navigatonController!.pushViewController(chatController, animated: true)
    }
    
    func notificationServiceDidFailFetchingDialog() {
    }
    
    func CreateNewEntrytoConsultTable(userID: NSNumber, userInfo:Dictionary <String, String>) {
        print("AppDelegate: CreateNewEntrytoConsultTable() Creating New new entry in  Consult Table")
        
        let calldoccode = userInfo["calldoccode"]
        let relation = userInfo["relation"]
        let patfullname = userInfo["patientname"]
        let patprofileid = userInfo["patprofileid"]
        
        let object = QBCOCustomObject()
        object.className = "ConsultsTable"
        object.fields["patientuserid"] =  userID
        object.fields["docfullname"] = GlobalDocData.gdocname
        object.fields["patfullname"] = patfullname
        object.fields["patrelation"] = relation
        object.fields["patcalldoccode"] = calldoccode
        object.fields["patprofileUID"] = patprofileid

        QBRequest.createObject(object, successBlock: { (response, contributors) in
            GlobalVariables.recordIDConsulttable = (contributors?.id)!  // this record id will be used later to update the record for
            let storyboard = UIStoryboard(name: "MedicalStoryboard", bundle: nil)
            let VC = storyboard.instantiateViewController(withIdentifier: "MedicalAdviceBaseViewController") as? UINavigationController
            self.window?.rootViewController = VC
        }) { (response) in
            //Handle Error
            print("AppDelegate:CreateNewEntrytoConsultTable: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print(notification.request.content.userInfo)
        
        let vc = self.window?.rootViewController//((self.window?.rootViewController as? UINavigationController)?.visibleViewController)
        //Accepted video                                                        //Rejected Video
        if (notification.request.content.userInfo["code"] as? Int) == 101 || (notification.request.content.userInfo["code"] as? Int) == 102 {
            if vc is InComingCallViewController {
                if (vc as! InComingCallViewController).nav?.visibleViewController is CallViewIncomingCallController {
                    ((vc as! InComingCallViewController).nav?.visibleViewController as! CallViewIncomingCallController).handleVideoRequestCallback(withcode: (notification.request.content.userInfo["code"] as? Int)!)
                }
                
            } else {
                let vc = CallViewIncomingCallController()
                vc.handleVideoRequestCallback(withcode: (notification.request.content.userInfo["code"] as? Int)!)
            }
        }
        
        if (notification.request.content.userInfo["code"] as? Int) == 201 {
            
            
//            UNNotificationPresentationOptions.alert = (notification.request.content.userInfo["code"]
            
            //sendUserNotification()
             //= ((notification.request.content.userInfo["aps"] as? NSDictionary)?.value(forKey: "alert") as? String)
            //((notification.request.content.userInfo["aps"] as? NSDictionary)?.value(forKey: "alert") as? String)
            completionHandler(UNNotificationPresentationOptions.alert)
            //completionHandler([.badge, .alert, .sound])
//            let patName = String(describing: notification.request.content.userInfo["patientName"]!)
//            let alert = UIAlertController(title: "", message: "\(patName ?? "") has requested chat session.", preferredStyle: .alert)
//            let ok = UIAlertAction(title: "Accept", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//                //Do Some action here
//                let user = QBUUser()
//                //user.id =  notification.request.content.userInfo["user_id"]!  //(notification.request.content.userInfo["user_id"]! as? UInt) ?? 0
//                let id = notification.request.content.userInfo["user_id"]!
//                let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
//
//                user.id = UInt(String(describing: id)) ?? 0//id as! UInt
//                chatDialog.occupantIDs = [user.id] as! [NSNumber]
//                //patientName
//
//                let patProflePic = String(describing: notification.request.content.userInfo["patProfilePic"]!)
//
//                QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
//                    print("sucess + \(String(describing: response))")
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let chatvc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//                    chatvc.dialog = createdDialog
//                    chatvc.profileimageurl = patProflePic//(MyAllConsultsUniqueArray[self.patientindex].fields?.value(forKey: "patprofileUID") as! String)
//                    let navController = self.window?.rootViewController as? UINavigationController
//                    navController?.pushViewController(chatvc, animated: true)
//                }, errorBlock: {(response: QBResponse!) in
//                    print("Error response + \(response)")
//                })
//
//            })
//            let no = UIAlertAction(title: "Reject", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//
//            })
//            alert.addAction(ok)
//            alert.addAction(no)
//            (vc as? UINavigationController)?.visibleViewController?.present(alert, animated: true, completion: nil)
//
        }
        
//        if (notification.request.content.userInfo["code"] as? Int) == 100 {
//            if ((self.window?.rootViewController as? UINavigationController)?.visibleViewController) is CallViewControllerswft {
//                let vc = ((self.window?.rootViewController as? UINavigationController)?.visibleViewController) as? CallViewControllerswft
//                vc?.videoTurnOnNotificationReceived()
//            }
//        }
        //notification.request.content.userInfo["code"]!
        //(self.window?.rootViewController as! UINavigationController).visibleViewController
        //((self.window?.rootViewController as! UINavigationController).visibleViewController) is CallViewControllerswft
    }
    
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        //code
//        print(response.notification.request.content.userInfo)
//        completionHandler()
//    }
   
    func sendUserNotification() {
        let centre = UNUserNotificationCenter.current()
        centre.getNotificationSettings { (settings) in
            if settings.authorizationStatus != UNAuthorizationStatus.authorized {
                print("Not Authorised")
            } else {
                print("Authorised")
                let content = UNMutableNotificationContent()
                content.title = NSString.localizedUserNotificationString(forKey: "This is the title", arguments: nil)
                content.body = NSString.localizedUserNotificationString(forKey: "The message body goes here.", arguments: nil)
                content.categoryIdentifier = "Category"
                // Deliver the notification in five seconds.
                content.sound = UNNotificationSound.default()
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                
                // Schedule the notification.
                let request = UNNotificationRequest(identifier: "FiveSecond", content: content, trigger: trigger)
                let center = UNUserNotificationCenter.current()
                
                let completeAction = UNNotificationAction.init(identifier: "Complete", title: "Complete", options: UNNotificationActionOptions())
                let editAction = UNNotificationAction.init(identifier: "Edit", title: "Edit", options: UNNotificationActionOptions.foreground)
                let deleteAction = UNNotificationAction.init(identifier: "Delete", title: "Delete", options: UNNotificationActionOptions.destructive)
                let categories = UNNotificationCategory.init(identifier: "Category", actions: [completeAction, editAction, deleteAction], intentIdentifiers: [], options: [])
                
                centre.setNotificationCategories([categories])
                
                center.add(request, withCompletionHandler: nil)
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.actionIdentifier == UNNotificationDismissActionIdentifier {
            print ("Message Closed")
        }
        else if response.actionIdentifier == UNNotificationDefaultActionIdentifier {
            print ("App is Open")
        }
        
        if (response.notification.request.content.userInfo["code"] as? Int) == 201 {
            
            let user = QBUUser()
            //user.id =  notification.request.content.userInfo["user_id"]!  //(notification.request.content.userInfo["user_id"]! as? UInt) ?? 0
            let id = response.notification.request.content.userInfo[/*"user_id"*/"patientId"]!
            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
            
            user.id = UInt(String(describing: id)) ?? 0//id as! UInt
            chatDialog.occupantIDs = [user.id] as! [NSNumber]
            //patientName
            
            let patProflePic = String(describing: response.notification.request.content.userInfo["patProfilePic"]!)
            
            QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                print("sucess + \(String(describing: response))")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let chatvc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatvc.dialog = createdDialog
                chatvc.profileimageurl = patProflePic//(MyAllConsultsUniqueArray[self.patientindex].fields?.value(forKey: "patprofileUID") as! String)
                let navController = self.window?.rootViewController as? UINavigationController
                if navController == nil {
                    let viewController = self.window?.rootViewController as? UIViewController
                    let navC = UINavigationController(rootViewController: viewController!)
                    navC.pushViewController(chatvc, animated: true)
                } else {
                    navController?.pushViewController(chatvc, animated: true)
                }
                
            }, errorBlock: {(response: QBResponse!) in
                print("Error response + \(response)")
            })

        }
        // Else handle any custom actions. . .
        completionHandler()
    }
    
   
//    func chatDidReceiveSystemMessage(message: QBChatMessage!) {
//
//    }
    
}
