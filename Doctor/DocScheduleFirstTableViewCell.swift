import UIKit

class DocScheduleFirstTableViewCell: UITableViewCell {

    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var sepatatorView: UIView!
    @IBOutlet weak var grayView: UIView!
    @IBOutlet weak var hrLabel: UILabel!
    @IBOutlet weak var amPMLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
