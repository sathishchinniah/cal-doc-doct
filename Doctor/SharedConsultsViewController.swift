//
//  SharedConsultsViewController.swift
//  Doctor
//
//  Created by Kirthika Raukutam on 06/12/18.
//  Copyright © 2018 calldoc. All rights reserved.
//

import UIKit

class SharedConsultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    
    var consultID = ""
    var sharedConsultIDsList: [String] = []
    var consults = [QBCOCustomObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //left side Back Button
        let btnImage = UIImage(named: "backBtn")
        let leftbtn = UIButton(type: .custom)
        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        leftbtn.setImage(btnImage, for: .normal)
        let backButton = UIBarButtonItem(customView: leftbtn)
        navigationItem.leftBarButtonItem = backButton
        sharedConsultIDsList = []
        self.getConsultDetails()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getConsultDetails() {
        let getRequest = NSMutableDictionary()
        getRequest["id"] = consultID//QBSession.current.currentUser?.id
        
        QBRequest.object(withClassName: "ConsultsTable", id: /*"5c0e0d91dbda237dfe15b6d7"*/consultID, successBlock: { (response, object) in
            //code
            if let consult = object {
                let trimmedString = ((consult.fields.value(forKey: "sharedconsults")) as? String)?.replacingOccurrences(of: " ", with: "")
                let sharedConsults = trimmedString?.split(separator: ",")//((consult.fields.value(forKey: "sharedconsults")) as? String)?.split(separator: ",")
                if let consults = sharedConsults {
                    for string in sharedConsults! {
                        self.sharedConsultIDsList.append(String(string))
                    }
                    //self.getConsultDetailsFromID()
                } else {
                   self.sharedConsultIDsList = []
                }
                print("sharedConsultIDsList\(self.sharedConsultIDsList)")
                self.getConsultDetailsFromID()
                 //self.tableView.reloadData()
                //self.sharedConsultIDsList = sharedConsults//([(consult.fields.value(forKey: "sharedconsults"))] as? [String]) ?? []
            }
        }) { (response) in
            //Error handling
            print("getConsultDetails: Response error: \(String(describing: response.error?.description))")
        }
        

        
//        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
//
//            if ((contributors?.count)! > 0) {
////                for consult in contributors! {
////                    self.sharedConsultIDsList.append((consult.fields.value(forKey: "sharedconsults") as? String) ?? "")
////                }
//                self.sharedConsultIDsList = [(contributors?.first?.fields.value(forKey: "sharedconsults") as? String) ?? ""]
//            } else {
//                self.sharedConsultIDsList = []
//            }
//            self.tableView.reloadData()
//
//        }) { (response) in
//            //Error handling
//            print("getConsultDetails: Response error: \(String(describing: response.error?.description))")
//        }
    }
    
    func getConsultDetailsFromID() {
        for id in sharedConsultIDsList {
//            let getRequest = NSMutableDictionary()
//            getRequest["id"] = id
            
            QBRequest.object(withClassName: "ConsultsTable", id: /*"5c0e0d91dbda237dfe15b6d7"*/id, successBlock: { (response, object) in
                //code
                var index = IndexPath(row: 0, section: 0)
                if let consult = object {
                    self.consults.append(consult)
                    index.row = self.consults.index(of: consult)!
                }
                self.tableView.reloadData()
                //self.tableView.reloadRows(at: [index], with: UITableViewRowAnimation.automatic)
            }) { (response) in
                //Error handling
                print("getConsultDetailsFromID: Response error: \(String(describing: response.error?.description))")
            }
        }
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return consults.count//sharedConsultIDsList.count//10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sharedConsultCell", for: indexPath)
        
        cell.selectionStyle = .none
        cell.textLabel?.text = (consults[indexPath.row].fields?.value(forKey: "docfullname") as? String) ?? ""//"Doctor"
        cell.detailTextLabel?.text = (consults[indexPath.row].fields?.value(forKey: "docspeciality") as? String) ?? ""
        // Configure the cell...
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //code
        let vc = ConsultDetailsViewController.storyboardInstance()
        vc.patientnamepassed = (consults[indexPath.row].fields?.value(forKey: "patfullname") as? String) ?? ""//thePatientName
        vc.imageurlpassed = (consults[indexPath.row].fields?.value(forKey: "patprofileUID") as? String) ?? ""
        
        let currenttime: Int = Int(NSDate().timeIntervalSince1970)
        let createdat  = /*PatientConsultsdetailsArray*/MyAllConsultsArray[myIndex/*sender.tag*/].createdAt! as NSDate
        let createdatepochtime = Int(createdat.timeIntervalSince1970)
        let diff = currenttime - createdatepochtime
        let hourslasped = diff/3600
        
        if (hourslasped > 72) {
            vc.consultONOFF = false
        } else {
            vc.consultONOFF = true
        }
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"    //"MMM dd YYYY hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
        let fulldate = dateString.components(separatedBy: "|")
        let lastdatetimelb = fulldate[0]
        let lastdatebtn = fulldate[1]
        vc.ConsultRecordID = /*PatientConsultsdetailsArray*/MyAllConsultsArray[myIndex/*sender.tag*/].id! // pasing consult record ID
        vc.patientuseridpassed = (consults[indexPath.row].fields?.value(forKey: "patientuserid") as? UInt) ?? 0//patientuseridpassed
        vc.patientindex = myIndex
        vc.datepassed = lastdatebtn
        vc.timepassed = lastdatetimelb
        vc.tblindexpassed = indexPath.row//sender.tag
        theindexval = Int(indexPath.row/*sender.tag*/)
        thedateval = lastdatebtn
        
        //vc.configureChatWindow()
        // this line will hide back button title on next viewcontroller navigation bar
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        return
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
