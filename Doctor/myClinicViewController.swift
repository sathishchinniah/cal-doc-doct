import UIKit
import SDWebImage

var patientlist = ["Anamika", "Priya Raj", "Shikha", "Anuradha", "Kavita", "Rashmi Dubey", "Pratibha Gupta", "Richa Sharma"]
var badgelist = ["3","1","2","4","2","3","1","4","3","5","0"]
var patientimageNames = ["patient1","patient2","patient3", "patient4","patient1","patient2","patient3", "patient4"]
var myIndex = 0
let openColor = UIColor(red:0.23, green:0.70, blue:0.55, alpha:1.0)
let closesColor = UIColor(red:0.99, green:0.64, blue:0.33, alpha:1.0)
let closedColor = UIColor(red:0.89, green:0.32, blue:0.27, alpha:1.0)
let kDialogsPageLimit:UInt = 100
var chatDialog: [QBChatDialog]?

class DialogTableViewCellModel: NSObject {
    
    var detailTextLabelText: String = ""
    var textLabelText: String = ""
    var unreadMessagesCounterLabelText : String?
    var unreadMessagesCounterHiden = true
    var dialogIcon : UIImage?
    var myindex = 0

    init(dialog: QBChatDialog) {
        super.init()
        
        switch (dialog.type){
        case .publicGroup:
            self.detailTextLabelText = "public group"
        case .group:
            self.detailTextLabelText = "group"
        case .private:
            self.detailTextLabelText = "private"
            
            if dialog.recipientID == -1 {
                return
            }
        }
        
        if self.textLabelText.isEmpty {
            if let dialogName = dialog.name {
                self.textLabelText = dialogName
            }
        }
        
        // Unread messages counter label
        
        if (dialog.unreadMessagesCount > 0) {
            
            var trimmedUnreadMessageCount : String
            
            if dialog.unreadMessagesCount > 99 {
                trimmedUnreadMessageCount = "99+"
            } else {
                trimmedUnreadMessageCount = String(format: "%d", dialog.unreadMessagesCount)
            }
            
            self.unreadMessagesCounterLabelText = trimmedUnreadMessageCount
            self.unreadMessagesCounterHiden = false
            
        } else {
            
            self.unreadMessagesCounterLabelText = nil
            self.unreadMessagesCounterHiden = true
        }
        
        // Dialog icon
        
        if dialog.type == .private {
            self.dialogIcon = UIImage(named: "user")
        } else {
            self.dialogIcon = UIImage(named: "group")
        }
    }
}

class myClinicViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIPopoverPresentationControllerDelegate {

    var nav: UINavigationController?
    private var didEnterBackgroundDate: NSDate?
    private var observer: NSObjectProtocol?
    var session : QBRTCSession!
    var dataPointer = GData.shared()
    var occupantsList = [NSNumber]()

    @IBOutlet var myClinictableview: UITableView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.observer = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main) { (notification) -> Void in

        }
        
        if (QBChat.instance.isConnected) {
            self.getDialogs()
        }
    }
    // MARK: - Notification handling
    
    func didEnterBackgroundNotification() {
        self.didEnterBackgroundDate = NSDate()
    }
    
    // MARK: - DataSource Action
    
    func getDialogs() {

    }
    
    // MARK: - Helpers
    func reloadTableViewIfNeeded() {
        self.myClinictableview.reloadData()
    }
    public func tableView(_ myClinictableview: UITableView, numberOfRowsInSection section: Int) -> Int {
        return /*MyAllConsultsUniqueArray*/MyAllConsultsArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        UITableViewCell.appearance().backgroundColor = UIColor.clear
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! myClinicTableViewCell
        
        cell.selectionStyle = .none
        if /*MyAllConsultsUniqueArray*/MyAllConsultsArray.count == 0 {
            return(cell)
        }
        
        
        cell.patientImage.sd_setImage(with: URL(string: (/*MyAllConsultsUniqueArray*/MyAllConsultsArray[indexPath.row].fields?.value(forKey: "patprofileUID") as? String) ?? ""), placeholderImage: UIImage(named: "profile"))
        
        cell.tag = indexPath.row
        
        cell.statuslabel.layer.masksToBounds = true
        cell.statuslabel.layer.cornerRadius = 5
        
        
        let currenttime: Int = Int(NSDate().timeIntervalSince1970)
        let createdat  = /*MyAllConsultsUniqueArray*/MyAllConsultsArray[indexPath.row].createdAt! as NSDate
        let createdatepochtime = Int(createdat.timeIntervalSince1970)
        let diff = currenttime - createdatepochtime
        let hourslasped = diff/3600
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"    //"MMM dd YYYY hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
        let fulldate = dateString.components(separatedBy: "|")
      //  let lastdatetimelb = fulldate[0]
      //  let lastdatebtn = fulldate[1]
        
        cell.dateslb.text = dateString
       
        
        let status = (MyAllConsultsArray[indexPath.row].fields?.value(forKey: "status") as? String)?.lowercased() ?? ""
        if status == "pending" {
            cell.statuslabel.text = "Pending"
            cell.statuslabel.backgroundColor = UIColor.lightGray
            cell.extendButton.isHidden = true
            cell.extendButton.isEnabled = false
        } else if status == "open" {
            cell.statuslabel.text = "Open"
            cell.statuslabel.backgroundColor = openColor
            cell.extendButton.isHidden = true
            cell.extendButton.isEnabled = false
        } else if status == "closing soon" {
            cell.statuslabel.text = "closes soon "
            cell.statuslabel.backgroundColor = closesColor
            cell.extendButton.isHidden = false
            cell.extendButton.isEnabled = true
        } else if status == "closed" {
            cell.statuslabel.text = "Closed"
            cell.statuslabel.backgroundColor = closedColor
            cell.extendButton.isHidden = true
            cell.extendButton.isEnabled = false
        } else {
            //uncomment for new
//            cell.statuslabel.text = ""
//            cell.statuslabel.backgroundColor = UIColor.clear
//            cell.extendButton.isHidden = true
//            cell.extendButton.isEnabled = false
            //for old consults
            cell.statuslabel.text = "Closed"
            cell.statuslabel.backgroundColor = closedColor
            cell.extendButton.isHidden = true
            cell.extendButton.isEnabled = false
        }
        // cell Button action
        cell.cellBtn.tag = indexPath.row
        //cell.cellBtn.addTarget(self,action:#selector(CellAction), for:.touchUpInside)
        cell.cellBtn.isEnabled = false
        cell.extendButton.tag = indexPath.row
        cell.extendButton.addTarget(self, action: #selector(extendButtonTouchUpInside), for: .touchUpInside)
        cell.cellBtn.isUserInteractionEnabled = true
        cell.patientNamelb.text = (/*MyAllConsultsUniqueArray*/MyAllConsultsArray[indexPath.row].fields?.value(forKey: "patfullname") as? String) ?? ""
        //cell.cellBadgeLabel.text = "1"
        cell.cellBadgeLabel.isHidden = true
        cell.cellrightarrowbtn.isHidden = false
        //cell.statuslabel.backgroundColor = openColor
        let amount = MyAllConsultsArray[indexPath.row].fields?.value(forKey: "amount") as? Int
        cell.billedAmtValueLabel.text = "Rs " + String(describing: (amount ?? 0))

        cell.separatorInset.left = 90
        
        return(cell)
    }
    
    @objc func extendButtonTouchUpInside(sender: UIButton) {
        
        guard let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "ExtendOptionsViewController") as? ExtendOptionsViewController else { return }
        popoverContent.selectedConsult = MyAllConsultsArray[sender.tag]
        let nav = UINavigationController(rootViewController: popoverContent)
        nav.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = nav.popoverPresentationController
        popoverContent.preferredContentSize = CGSize(width: 250, height: 175)//CGSizeMake(500,600)
        popoverContent.modalPresentationStyle = UIModalPresentationStyle.popover
        popover?.delegate = self
        popover?.sourceView = sender//self.view
        popover?.sourceRect = sender.bounds//CGRect(x: 100, y: 100, width: 0, height: 0)//CGRectMake(100,100,0,0)
        popoverContent.navigationController?.navigationBar.isHidden = true
        self.present(nav, animated: true, completion: nil)
    
        
        
    }
    
    // UIPopoverPresentationControllerDelegate method
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        // Force popover style
        return UIModalPresentationStyle.none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
    
    @objc func CellAction(sender: UIButton) {
        /*myIndex = sender.tag
        self.performSegue(withIdentifier: "GotoPatientView" , sender: /*MyAllConsultsUniqueArray*/MyAllConsultsArray[sender.tag])*/
        myIndex = sender.tag
        let vc = ConsultDetailsViewController.storyboardInstance()
        vc.patientnamepassed = (MyAllConsultsArray[myIndex].fields?.value(forKey: "patfullname") as? String) ?? ""//thePatientName
        vc.imageurlpassed = (MyAllConsultsArray[myIndex].fields?.value(forKey: "patprofileUID") as? String) ?? ""
        
        let currenttime: Int = Int(NSDate().timeIntervalSince1970)
        let createdat  = /*PatientConsultsdetailsArray*/MyAllConsultsArray[sender.tag].createdAt! as NSDate
        let createdatepochtime = Int(createdat.timeIntervalSince1970)
        let diff = currenttime - createdatepochtime
        let hourslasped = diff/3600
        
        if (hourslasped > 72) {
            vc.consultONOFF = false
        } else {
            vc.consultONOFF = true
        }
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"    //"MMM dd YYYY hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
        let fulldate = dateString.components(separatedBy: "|")
        let lastdatetimelb = fulldate[0]
        let lastdatebtn = fulldate[1]
        vc.ConsultRecordID = /*PatientConsultsdetailsArray*/MyAllConsultsArray[sender.tag].id! // pasing consult record ID
        vc.patientuseridpassed = (MyAllConsultsArray[myIndex].fields?.value(forKey: "patientuserid") as? UInt) ?? 0//patientuseridpassed
        vc.patientindex = myIndex
        vc.datepassed = lastdatebtn
        vc.timepassed = lastdatetimelb
        vc.tblindexpassed = sender.tag
        theindexval = Int(sender.tag)
        thedateval = lastdatebtn
        
        //vc.configureChatWindow()
        // this line will hide back button title on next viewcontroller navigation bar
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GotoPatientView" {
            if let vc = segue.destination as? PatientDetailsViewController {
                vc.thePatientName =  (/*MyAllConsultsUniqueArray*/MyAllConsultsArray[myIndex].fields?.value(forKey: "patfullname") as! String)
                vc.patientindex = myIndex
                vc.patientcalldoccodepassed = (/*MyAllConsultsUniqueArray*/MyAllConsultsArray[myIndex].fields?.value(forKey: "patcalldoccode") as! String)
                vc.patientuseridpassed = (/*MyAllConsultsUniqueArray*/MyAllConsultsArray[myIndex].fields?.value(forKey: "patientuserid") as! UInt)
            }
        }
    }
    
    func ChatAction(sender: UIButton) {

    }

    func AVCallAction(sender: UIButton) {

    }
    
    func tableView(_ myClinictableview: UITableView, didSelectRowAt indexPath: IndexPath) {
        myIndex = indexPath.row//sender.tag
        let status = (MyAllConsultsArray[indexPath.row].fields?.value(forKey: "status") as? String) ?? ""
        let consultRecordId = MyAllConsultsArray[myIndex/*sender.tag*/].id ?? ""
        if status == "pending" {
            /*let user = QBUUser()
             user.id =  patientuseridpassed
             let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
             chatDialog.occupantIDs = [user.id] as [NSNumber]
             
             QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
             print("sucess + \(String(describing: response))")
             let chatvc = self.storyboard!.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
             chatvc.dialog = createdDialog
             chatvc.profileimageurl = (MyAllConsultsUniqueArray[self.patientindex].fields?.value(forKey: "patprofileUID") as! String)
             self.navigationController?.pushViewController(chatvc, animated: true)
             }, errorBlock: {(response: QBResponse!) in
             print("Error response + \(response)")
             })*/
            if (!consultRecordId.isEmpty) {
                let user = QBUUser()
                user.id =  (MyAllConsultsArray[myIndex].fields?.value(forKey: "patientuserid") as? UInt) ?? 0
                let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
                chatDialog.occupantIDs = [user.id] as [NSNumber]
                
                QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    let chatvc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                    chatvc.ConsultRecordID = consultRecordId
                    chatvc.dialog = createdDialog
                    chatvc.profileimageurl = (MyAllConsultsArray[myIndex].fields?.value(forKey: "patprofileUID") as! String)
                    self.navigationController?.pushViewController(chatvc, animated: true)
                    
                    
                }, errorBlock: {(response: QBResponse!) in
                    print("Error response + \(String(describing: response))")
                })
            }
            return
        } else {
            let vc = ConsultDetailsViewController.storyboardInstance()
            vc.patientnamepassed = (MyAllConsultsArray[myIndex].fields?.value(forKey: "patfullname") as? String) ?? ""//thePatientName
            vc.imageurlpassed = (MyAllConsultsArray[myIndex].fields?.value(forKey: "patprofileUID") as? String) ?? ""
            
            let currenttime: Int = Int(NSDate().timeIntervalSince1970)
            let createdat  = /*PatientConsultsdetailsArray*/MyAllConsultsArray[myIndex/*sender.tag*/].createdAt! as NSDate
            let createdatepochtime = Int(createdat.timeIntervalSince1970)
            let diff = currenttime - createdatepochtime
            let hourslasped = diff/3600
            
            if (hourslasped > 72) {
                vc.consultONOFF = false
            } else {
                vc.consultONOFF = true
            }
            
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"    //"MMM dd YYYY hh:mm a"
            let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
            let fulldate = dateString.components(separatedBy: "|")
            let lastdatetimelb = fulldate[0]
            let lastdatebtn = fulldate[1]
            vc.ConsultRecordID = /*PatientConsultsdetailsArray*/MyAllConsultsArray[myIndex/*sender.tag*/].id! // pasing consult record ID
            vc.patientuseridpassed = (MyAllConsultsArray[myIndex].fields?.value(forKey: "patientuserid") as? UInt) ?? 0//patientuseridpassed
            vc.patientindex = myIndex
            vc.datepassed = lastdatebtn
            vc.timepassed = lastdatetimelb
            vc.tblindexpassed = myIndex//sender.tag
            theindexval = Int(myIndex/*sender.tag*/)
            thedateval = lastdatebtn
            
            //vc.configureChatWindow()
            // this line will hide back button title on next viewcontroller navigation bar
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateConsultsStatus()
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        myClinictableview.separatorInset = .zero
        myClinictableview.layoutMargins = .zero
        CreateSearchbar()
        let searchItem : UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = searchItem
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        //MyAllConsultsArray = MyAllConsultsArray.sorted(by: { $0.createdAt! > $1.createdAt! })
    }

    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    func CreateSearchbar() {

    }

    // pauseToPlay()
    @IBAction func pauseButton(sender: UIBarButtonItem){
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateConsultsStatus() {
        for consult in MyAllConsultsArray {
            
            let createdAt = consult.createdAt
            let extendedDuration = (consult.fields.value(forKey: "extendduration") as? Int) ?? 0
            let prescriptionId = (consult.fields.value(forKey: "prescriptionid") as? String) ?? ""
            //let currentTime = Date()
            //72hrs = 259200seconds
            let totalConsultPeriod = 259200 + extendedDuration
            let consultTimePassed = Int(Date().timeIntervalSince(createdAt!))
            let consultTimeRemaining = totalConsultPeriod - consultTimePassed
            let object = QBCOCustomObject()
            object.className = "ConsultsTable"
            if prescriptionId == nil || prescriptionId == "" {
                object.fields["status"] = "pending"
            } else if consultTimeRemaining <= 24 && consultTimeRemaining > 0 {
                //closing soon
                object.fields["status"] = "CLOSING SOON"
            } else if consultTimeRemaining <= 0 {
                //closed
                object.fields["status"] = "CLOSED"
            } else {
                //open
                object.fields["status"] = "OPEN"
            }
            //prescriptionid
            //object.fields["status"] = "open"
            object.id = consult.id//GlobalMedicalAdvicedata.ConsultTablerecordID
            QBRequest.update(object, successBlock: { (response, contributors) in
                print("SUCCESS")
                self.myClinictableview.reloadData()
                //                if consult == MyAllConsultsArray.last {
                //                    DispatchQueue.main.async {
                //                        let myclinicViewController = self.storyboard!.instantiateViewController(withIdentifier: "myClinic") as! myClinicViewController
                //                        self.nav = UINavigationController(rootViewController: myclinicViewController)
                //                        self.present(self.nav!, animated: true, completion: nil)
                //                    }
                //                }
            }) { (response) in
                
                print("ConfirmMedicalAdviceBaseViewController:UpdateConsultTableforPrescriptionrID: Response error: \(String(describing: response.error?.description))")
            }
            
            //            if consult == MyAllConsultsArray.last {
            //                DispatchQueue.main.async {
            //                    let myclinicViewController = self.storyboard!.instantiateViewController(withIdentifier: "myClinic") as! myClinicViewController
            //                    self.nav = UINavigationController(rootViewController: myclinicViewController)
            //                    self.present(self.nav!, animated: true, completion: nil)
            //                }
            //            }
            
        }
    }
    
    func FetchPatients() {
    }
    
    func downloadAndShowUserAvatar() {
        let blobIDArray: NSMutableArray = []
        var mutablePatientCPArray: [QBUUser]
        mutablePatientCPArray = GlobalVariables.PatientArray as! [QBUUser]

        for user: QBUUser in mutablePatientCPArray {
            let userIDString = "\(UInt(user.blobID))"
            blobIDArray.add(userIDString)
        }
        
        //var _: Int
        for i in 0..<blobIDArray.count {
            let myArrayElement = blobIDArray[i] as? String
            let imgurl: String = QBCBlob.publicUrl(forFileUID: (myArrayElement!))!
            let newurlString = imgurl.replacingOccurrences(of: ".json", with: "/download", options: .literal, range: nil)
            GlobalVariables.patientsimagearray.append(newurlString)
        }
        print("lets check the blob images")
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        if session == self.session {
            print("myClinicViewController: sessionDidClose()")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(UInt64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
                self.nav?.view.isUserInteractionEnabled = false
                self.nav?.dismiss(animated: false, completion: nil)
                self.session = nil
                self.nav = nil
            })
        }
    }

}

// this removes duplicates for Array
extension Array where Element: Equatable {
    mutating func removeDuplicates() {
        var result = [Element]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        self = result
    }
}
