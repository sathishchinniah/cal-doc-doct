//
//  ViewController.h
//  Doctor
//
//  Created by Manish Verma on 10/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GData;

@interface ViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,retain) GData *dataPointer;
@property (weak, nonatomic) IBOutlet UITableView *countrycodetableView;
@property (nonatomic,retain) NSArray *countrycodeData;

@end

