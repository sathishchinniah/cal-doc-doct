import UIKit
import Toast_Swift

class DocTrustedNetworkViewController: UIViewController {
    
    @IBOutlet var toplabel: UILabel!
    @IBOutlet var docimg: UIImageView!
    @IBOutlet var docimg1: UIImageView!
    @IBOutlet var docimg2: UIImageView!
    @IBOutlet var docimg3: UIImageView!
    @IBOutlet var docimg4: UIImageView!
    @IBOutlet var docimg5: UIImageView!
    @IBOutlet var docimg6: UIImageView!
    
    @IBOutlet var genericButtonview: UIButton!
    @IBOutlet var doc1Buttonview: UIButton!
    @IBOutlet var doc2Buttonview: UIButton!
    @IBOutlet var doc3Buttonview: UIButton!
    @IBOutlet var doc4Buttonview: UIButton!
    @IBOutlet var doc5Buttonview: UIButton!
    @IBOutlet var doc6Buttonview: UIButton!
    
    @IBOutlet weak var diagonalline1: UIView!
    @IBOutlet weak var diagonalline2: UIView!
    @IBOutlet weak var diagonalline3: UIView!
    @IBOutlet weak var diagonalline4: UIView!
    @IBOutlet weak var line5: UIView!
    @IBOutlet weak var line6: UIView!
    @IBOutlet weak var namelabel1: UILabel!
    @IBOutlet weak var namelabel2: UILabel!
    @IBOutlet weak var namelabel3: UILabel!
    @IBOutlet weak var namelabel4: UILabel!
    @IBOutlet weak var namelabel5: UILabel!
    @IBOutlet weak var namelabel6: UILabel!
    
    @IBOutlet weak var digonallin1ConstraintX: NSLayoutConstraint!
    @IBOutlet weak var digonallin1ConstraintY: NSLayoutConstraint!
    @IBOutlet weak var digonallin2ConstraintX: NSLayoutConstraint!
    @IBOutlet weak var digonallin2ConstraintY: NSLayoutConstraint!
    @IBOutlet weak var digonallin3ConstraintX: NSLayoutConstraint!
    @IBOutlet weak var digonallin3ConstraintY: NSLayoutConstraint!
    @IBOutlet weak var digonallin4ConstraintX: NSLayoutConstraint!
    @IBOutlet weak var digonallin4ConstraintY: NSLayoutConstraint!
    
    @IBOutlet weak var hrlinehightConstraint: NSLayoutConstraint!
    @IBOutlet weak var docimg1contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg1contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg2contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg2contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg3contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg3contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg4contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg4contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg5contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg5contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg6contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg6contraintsY: NSLayoutConstraint!
    
    enum specialities: String {
        case cardiologist = "Cardiologist"
        case dermatologist = "Dermatologist"
        case gastroentrologist = "Gastroentrologist"
        case ent = "E.N.T."
        case endocrinologist = "Endocrinologist"
        case pulmonoligist = "Pulmonoligist"
        case pediatrician = "Pediatrician"
        case nephrologist = "Nephrologist"
        case gp = "G.P."
        case gynaecologist = "Gynaecologist"
        case neurologist = "Neurologist"
        case dietician = "Dietician"
        case psychiatrist = "Psychiatrist"
        case oncologist = "Oncologist"
        case orthopedic = "Orthopedic"
        case sexologist = "Sexologist"
        case altMedicine = "Alt Medicine"
    }
    
    var nav: UINavigationController?
    var stringpassed = ""
    var receivedrecordcount = 0
    var finalArray:[QBCOCustomObject] = []
    var position = 0
    var firstname = ""
    var lastname = ""
    var doc1profileaction: Bool = false,  doc2profileaction = false, doc3profileaction = false, doc4profileaction = false, doc5profileaction = false, doc6profileaction = false
    var doc1fullname: String = "", doc2fullname = "", doc3fullname = "", doc4fullname = "", doc5fullname = "", doc6fullname = ""
    var doc1imgURL: String = "", doc2imgURL = "", doc3imgURL = "", doc4imgURL = "", doc5imgURL = "", doc6imgURL = ""
    var doc1userid: Int = 0, doc2userid = 0, doc3userid = 0, doc4userid = 0, doc5userid = 0, doc6userid = 0
    var doc1speciality: String = "", doc2speciality = "", doc3speciality = "", doc4speciality = "", doc5speciality = "", doc6speciality = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        toplabel.text = "My recommended "
        toplabel.text = toplabel.text! + stringpassed  // this will display speciality which got clicked from COT
        
        genericButtonview.setTitle(stringpassed , for: .normal)
        
        namelabel1.layer.cornerRadius = 8
        namelabel1.layer.masksToBounds = true
        namelabel2.layer.cornerRadius = 8
        namelabel2.layer.masksToBounds = true
        namelabel3.layer.cornerRadius = 8
        namelabel3.layer.masksToBounds = true
        namelabel4.layer.cornerRadius = 8
        namelabel4.layer.masksToBounds = true
        namelabel5.layer.cornerRadius = 8
        namelabel5.layer.masksToBounds = true
        namelabel6.layer.cornerRadius = 8
        namelabel6.layer.masksToBounds = true
        
        if phone && maxLength == 568 {
            //iphone 5
            docimg.layer.cornerRadius = 32.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            docimg.layer.cornerRadius = 37.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docimg6contraintsY.constant = -90
            docimg1contraintsY.constant = -90
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            docimg.layer.cornerRadius = 40.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docimg6contraintsY.constant = -95
            docimg1contraintsY.constant = -105
        }
        
        if phone && maxLength == 812 {
            //iphone x
            docimg.layer.cornerRadius = 38.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docimg5contraintsY.constant = 65
            docimg4contraintsY.constant = 65
            docimg2contraintsX.constant = -5
            docimg2contraintsY.constant = 62
            docimg6contraintsY.constant = -115
            docimg1contraintsY.constant = -120
        }
        
        if ipad && maxLength == 1024 {
            //iPad
            docimg.layer.cornerRadius = 77.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            hrlinehightConstraint.constant = 5
            docimg6contraintsY.constant = -125
            docimg1contraintsY.constant = -125
            addnameLabeltoImagesiphoneiPad()
        }
        
        LoadProfileImage()
        let isonline = UserDefaults.standard.object(forKey: "doctoronline") as! Bool
        
        if(isonline) {
            docimg.layer.borderColor = UIColor.green.cgColor
        } else {
            docimg.layer.borderColor = UIColor.orange.cgColor
        }
        
        let helpButton = UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self,  action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpButton;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.updateTheView()
    }
    
    
    func LoadProfileImage() {
        self.docimg.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
//        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
//        self.nav = UINavigationController(rootViewController: homeViewController)
//        self.present(self.nav!, animated: false, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func BacktoCOTButton(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func Adddoc1Button(_ sender: UIButton) {
        if self.doc1profileaction {
            print("Time to go to doc 1 profile")
            let vc = COTProfileViewController.storyboardInstance()
            vc.docfullname = doc1fullname
            vc.imageURLpassed = doc1imgURL
            vc.docspeciality = doc1speciality
            vc.docuserid = doc1userid
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            print("go and add doctor")
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = stringpassed
            GlobalVariables.docposition = 1
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Adddoc2Button(_ sender: UIButton) {
        if self.doc2profileaction {
            print("Time to go to doc 2 profile")
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel2.text!
            docprofileviewController.imageURLpassed = doc2imgURL
            docprofileviewController.docuserid = doc2userid
            docprofileviewController.docspeciality = doc2speciality
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = stringpassed
            GlobalVariables.docposition = 2
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Adddoc3Button(_ sender: UIButton) {
        if self.doc3profileaction {
            print("Time to go to doc 3 profile")
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel3.text!
            docprofileviewController.imageURLpassed = doc3imgURL
            docprofileviewController.docuserid = doc3userid
            docprofileviewController.docspeciality = doc3speciality
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = stringpassed
            GlobalVariables.docposition = 3
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Adddoc4Button(_ sender: UIButton) {
        if self.doc4profileaction {
            print("Time to go to doc 4 profile")
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel4.text!
            docprofileviewController.imageURLpassed = doc4imgURL
            docprofileviewController.docuserid = doc4userid
            docprofileviewController.docspeciality = doc4speciality
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = stringpassed
            GlobalVariables.docposition = 4
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Adddoc5Button(_ sender: UIButton) {
        if self.doc5profileaction {
            print("Time to go to doc 5 profile")
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel5.text!
            docprofileviewController.imageURLpassed = doc5imgURL
            docprofileviewController.docuserid = doc5userid
            docprofileviewController.docspeciality = doc5speciality
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = stringpassed
            GlobalVariables.docposition = 5
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Adddoc6Button(_ sender: UIButton) {
        if self.doc6profileaction {
            print("Time to go to doc 6 profile")
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel6.text!
            docprofileviewController.imageURLpassed = doc6imgURL
            docprofileviewController.docuserid = doc6userid
            docprofileviewController.docspeciality = doc6speciality
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = stringpassed
            GlobalVariables.docposition = 6
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    func updateTheView() {
        let speciality = specialities(rawValue: stringpassed)
        
        switch speciality {
        case .some(.cardiologist):
            self.handlingofCrdiologists()
            break
        case .some(.dermatologist):
            self.handlingofDermatologist()
        case .some(.gastroentrologist):
            self.handlingofGastroentrologist()
        case .some(.ent):
            self.handlingofEnt()
        case .some(.endocrinologist):
            self.handlingofEndocrinologist()
        case .some(.pulmonoligist):
            self.handlingofPulmonoligist()
        case .some(.pediatrician):
            self.handlingofPediatrician()
        case .some(.nephrologist):
            self.handlingofNephrologist()
        case .some(.gp):
            self.handlingofGP()
        case .some(.gynaecologist):
            self.handlingofGynaecologist()
        case .some(.neurologist):
            self.handlingofNeurologist()
        case .some(.dietician):
            self.handlingofDietician()
        case .some(.psychiatrist):
            self.handlingofPsychiatrist()
        case .some(.oncologist):
            self.handlingofOncologist()
        case .some(.orthopedic):
            self.handlingofOrthopedic()
        case .some(.sexologist):
            self.handlingofSexologist()
        case .some(.altMedicine):
            self.handlingofAltMedicine()
        case .none:
            break
        }
    }
    
    func handlingofCrdiologists() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "cardiologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofCrdiologists(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    
    func handlingofDermatologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "dermatologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofDermatologist(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
            
        }
        
    }
    
    func handlingofGastroentrologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "gastroentrologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofGastroentrologist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofEnt() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "ent"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            //Handle Error
            print("DocTrustedNetworkViewController:handlingofEnt(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofEndocrinologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "endocrinologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            //Handle Error
            print("DocTrustedNetworkViewController:handlingofEndocrinologist(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofPulmonoligist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "pulmonoligist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofPulmonoligist(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofPediatrician() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "paediatrician"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("DocTrustedNetworkViewController:handlingofPediatrician(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            }
            else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            //Handle Error
            print("DocTrustedNetworkViewController:handlingofPediatrician(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofNephrologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "nephrologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofNephrologist(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofGP() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "general"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofGP(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofGynaecologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "gynaecologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofGynaecologist(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofNeurologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "neurologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofNeurologist(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofDietician() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "dietician"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofDietician(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofPsychiatrist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "psychiatrist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            //Handle Error
            print("DocTrustedNetworkViewController:handlingofPsychiatrist(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofOncologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "oncologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofOncologist(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofOrthopedic() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "orthopaedic"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("DocTrustedNetworkViewController:handlingofOrthopedic(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofSexologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "sexologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            //Handle Error
            print("DocTrustedNetworkViewController:handlingofSexologist(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofAltMedicine() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["inviteespeciality"] = "altMedcine"//"altmedicine"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("DocTrustedNetworkViewController:handlingofAltMedicine(): Success ")
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            //Handle Error
            print("DocTrustedNetworkViewController:handlingofAltMedicine(): Response error: \(String(describing: response.error?.description))")
        }
        
        if recordcount == 0 {
            diagonalline1.isHidden = false
            docimg1.isHidden = false
            doc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func PopulateData(recordcount: Int, recordArray: [QBCOCustomObject]) {
        self.receivedrecordcount = recordArray.count
        for i in 0..<self.receivedrecordcount {
            
            if  let username = (recordArray[i].fields?.value(forKey: "inviteefullname") as? String)  {
                if i == 0 { // for first position
                    self.doc1profileaction = true
                    self.namelabel1.text = "Dr. " + username
                    self.docimg1.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg1.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg1.layer.borderWidth = 0.5
                    self.docimg1.layer.cornerRadius = self.docimg1.bounds.size.height / 2
                    self.docimg1.layer.masksToBounds = true
                    //pass data for next view
                    doc1fullname = username
                    doc1imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc1userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc1speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.diagonalline2.isHidden = false
                    self.namelabel1.isHidden = false
                    self.docimg2.isHidden = false
                    self.doc2Buttonview.isHidden = false
                    self.namelabel2.isHidden = true
                    self.diagonalline3.isHidden = true
                    self.docimg3.isHidden = true
                    self.doc3Buttonview.isHidden = true
                    self.namelabel3.isHidden = true
                    self.diagonalline4.isHidden = true
                    self.docimg4.isHidden = true
                    self.doc4Buttonview.isHidden = true
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }
                
                if i == 1 { // for second position
                    self.doc2profileaction = true
                    self.namelabel2.text = "Dr. " + username
                    self.docimg2.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String) ?? ""), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg2.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg2.layer.borderWidth = 0.5
                    self.docimg2.layer.cornerRadius = self.docimg2.bounds.size.height / 2
                    self.docimg2.layer.masksToBounds = true
                    //pass data for next view
                    doc2fullname = username
                    doc2imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc2userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc2speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel2.isHidden = false
                    self.diagonalline3.isHidden = false
                    self.docimg3.isHidden = false
                    self.doc3Buttonview.isHidden = false
                    self.namelabel3.isHidden = true
                    self.diagonalline4.isHidden = true
                    self.docimg4.isHidden = true
                    self.doc4Buttonview.isHidden = true
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }
                
                if i == 2 { // for third position
                    self.doc3profileaction = true
                    self.namelabel3.text = "Dr. " + username
                    self.docimg3.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String) ?? ""), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg3.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg3.layer.borderWidth = 0.5
                    self.docimg3.layer.cornerRadius = self.docimg3.bounds.size.height / 2
                    self.docimg3.layer.masksToBounds = true
                    //pass data for next view
                    doc3fullname = username
                    doc3imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc3userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc3speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel3.isHidden = false
                    self.diagonalline4.isHidden = false
                    self.docimg4.isHidden = false
                    self.doc4Buttonview.isHidden = false
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }
                
                if i == 3 { // for forth position
                    self.doc4profileaction = true
                    self.namelabel4.text = "Dr. " + username
                    self.docimg4.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String) ?? ""), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg4.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg4.layer.borderWidth = 0.5
                    self.docimg4.layer.cornerRadius = self.docimg4.bounds.size.height / 2
                    self.docimg4.layer.masksToBounds = true
                    //pass data for next view
                    doc4fullname = username
                    doc4imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc4userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc4speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel4.isHidden = false
                    self.line5.isHidden = false
                    self.docimg5.isHidden = false
                    self.doc5Buttonview.isHidden = false
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }
                
                if i == 4 { // for fifth position
                    self.doc5profileaction = true
                    self.namelabel5.text = "Dr. " + username
                    self.docimg5.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String) ?? ""), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg5.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg5.layer.borderWidth = 0.5
                    self.docimg5.layer.cornerRadius = self.docimg5.bounds.size.height / 2
                    self.docimg5.layer.masksToBounds = true
                    //pass data for next view
                    doc5fullname = username
                    doc5imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc5userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc5speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel5.isHidden = false
                    self.line6.isHidden = false
                    self.docimg6.isHidden = false
                    self.doc6Buttonview.isHidden = false
                    self.namelabel6.isHidden = true
                }
                
                if i == 5 { // for sixth position
                    self.doc6profileaction = true
                    self.namelabel6.text = "Dr. " + username
                    self.docimg6.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String) ?? ""), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg6.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg6.layer.borderWidth = 0.5
                    self.docimg6.layer.cornerRadius = self.docimg6.bounds.size.height / 2
                    self.docimg6.layer.masksToBounds = true
                    //pass data for next view
                    doc6fullname = username
                    doc6imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc6userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc6speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel6.isHidden = false
                }
                
            }
            
        }
    }
    
    
    func readDocTrustedNetworkRecords(specialitytbl:String) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: specialitytbl, extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            self.receivedrecordcount = (contributors?.count)!
            self.finalArray = contributors!
            
            for i in 0..<self.receivedrecordcount {
                self.position = (contributors![i].fields?.value(forKey: "position") as? Int)!
                self.firstname = (contributors![i].fields?.value(forKey: "firstname") as? String)!
                self.lastname = (contributors![i].fields?.value(forKey: "lastname") as? String)!
                
                if self.position == 1 {
                    self.doc1profileaction = true
                    self.namelabel1.text = "Dr. " + self.firstname + " " + self.lastname
                    self.doc1fullname =  self.firstname + " " + self.lastname
                    self.diagonalline2.isHidden = false
                    self.namelabel1.isHidden = false
                    self.docimg2.isHidden = false
                    self.doc2Buttonview.isHidden = false
                    self.namelabel2.isHidden = true
                    self.diagonalline3.isHidden = true
                    self.docimg3.isHidden = true
                    self.doc3Buttonview.isHidden = true
                    self.namelabel3.isHidden = true
                    self.diagonalline4.isHidden = true
                    self.docimg4.isHidden = true
                    self.doc4Buttonview.isHidden = true
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }
                
                if self.position == 2 {
                    self.doc2profileaction = true
                    self.namelabel2.text = "Dr. " + self.firstname + " " + self.lastname
                    self.doc2fullname =  self.firstname + " " + self.lastname
                    self.namelabel2.isHidden = false
                    self.diagonalline3.isHidden = false
                    self.docimg3.isHidden = false
                    self.doc3Buttonview.isHidden = false
                    self.namelabel3.isHidden = true
                    self.diagonalline4.isHidden = true
                    self.docimg4.isHidden = true
                    self.doc4Buttonview.isHidden = true
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }
                
                if self.position == 3 {
                    self.doc3profileaction = true
                    self.namelabel3.text = "Dr. " + self.firstname + " " + self.lastname
                    self.doc3fullname =  self.firstname + " " + self.lastname
                    self.namelabel3.isHidden = false
                    self.diagonalline4.isHidden = false
                    self.docimg4.isHidden = false
                    self.doc4Buttonview.isHidden = false
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                    
                }
                
                if self.position == 4 {
                    self.doc4profileaction = true
                    self.namelabel4.text = "Dr. " + self.firstname + " " + self.lastname
                    self.doc4fullname =  self.firstname + " " + self.lastname
                    self.namelabel4.isHidden = false
                    self.line5.isHidden = false
                    self.docimg5.isHidden = false
                    self.doc5Buttonview.isHidden = false
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }
                
                if self.position == 5 {
                    self.doc5profileaction = true
                    self.namelabel5.text = "Dr. " + self.firstname + " " + self.lastname
                    self.doc5fullname =  self.firstname + " " + self.lastname
                    self.namelabel5.isHidden = false
                    self.line6.isHidden = false
                    self.docimg6.isHidden = false
                    self.doc6Buttonview.isHidden = false
                    self.namelabel6.isHidden = true
                }
                
                if self.position == 6 {
                    self.doc6profileaction = true
                    self.namelabel6.text = "Dr. " + self.firstname + " " + self.lastname
                    self.doc6fullname =  self.firstname + " " + self.lastname
                    self.namelabel6.isHidden = false
                }
            }
            
            print("DocTrustedNetworkViewController:readDocTrustedNetworkRecords(): Successfully  !")
        }) { (errorresponse) in
            print("DocTrustedNetworkViewController:readDocTrustedNetworkRecords() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func ToastNotication() {
        var style = ToastStyle()
        style.messageColor = .lightGray
        ToastManager.shared.style = style
        ToastManager.shared.isTapToDismissEnabled = true
        self.view.makeToast("Hi Doc you have already added 6 doctors", duration: 1000.0, position: .bottom, title: "", image: UIImage(named: "toast.png"), style: style, completion: {(_ didTap: Bool) -> Void in
            if didTap {
                print("Toast completion from tap")
                istoastbar = false
            }
            else {
                print("Toast completion without tap")
                istoastbar = false
            }
        })
    }
    
    func doc1ProfileButton(_ sender: UIButton) {
        print("DocTrustedNetworkViewController:doc1ProfileButton pressed ")
        
    }
    
    func addnameLabeltoImagesiphoneiPad() {
        //docimg1
        let namelayer1 = UIView(frame: CGRect(x: 0, y: 130, width: 205, height: 30))
        namelayer1.alpha = 0.80
        namelayer1.layer.cornerRadius = 15
        namelayer1.backgroundColor = UIColor(red:0.15, green:0.31, blue:0.47, alpha:1)
        self.docimg1.addSubview(namelayer1)
        
        let textLayer1 = UILabel(frame: CGRect(x: 10, y: 6, width: 202, height: 25))
        textLayer1.lineBreakMode = .byWordWrapping
        textLayer1.numberOfLines = 0
        textLayer1.textColor = UIColor.white
        textLayer1.textAlignment = .left
        let textContent1 = "Dr Sridhar Dhulipala"
        let textString1 = NSMutableAttributedString(string: textContent1, attributes: [NSAttributedStringKey.font: UIFont(name: "Rubik-Regular", size: 15)!])
        let textRange1 = NSRange(location: 0, length: textString1.length)
        let paragraphStyle1 = NSMutableParagraphStyle()
        paragraphStyle1.lineSpacing = 1.18
        textString1.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle1, range: textRange1)
        textLayer1.attributedText = textString1
        textLayer1.sizeToFit()
        namelayer1.addSubview(textLayer1)
        
        //docimg2
        let namelayer2 = UIView(frame: CGRect(x: 0, y: 130, width: 205, height: 30))
        namelayer2.alpha = 0.80
        namelayer2.layer.cornerRadius = 15
        namelayer2.backgroundColor = UIColor(red:0.15, green:0.31, blue:0.47, alpha:1)
        self.docimg2.addSubview(namelayer2)
        
        let textLayer2 = UILabel(frame: CGRect(x: 10, y: 6, width: 202, height: 25))
        textLayer2.lineBreakMode = .byWordWrapping
        textLayer2.numberOfLines = 0
        textLayer2.textColor = UIColor.white
        textLayer2.textAlignment = .left
        let textContent2 = "Dr Sudha Menon"
        let textString2 = NSMutableAttributedString(string: textContent2, attributes: [NSAttributedStringKey.font: UIFont(name: "Rubik-Regular", size: 15)!])
        let textRange2 = NSRange(location: 0, length: textString2.length)
        let paragraphStyle2 = NSMutableParagraphStyle()
        paragraphStyle2.lineSpacing = 1.18
        textString2.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle2, range: textRange2)
        textLayer2.attributedText = textString2
        textLayer2.sizeToFit()
        namelayer2.addSubview(textLayer2)
        
        //docimg3
        let namelayer3 = UIView(frame: CGRect(x: 0, y: 130, width: 205, height: 30))
        namelayer3.alpha = 0.80
        namelayer3.layer.cornerRadius = 15
        namelayer3.backgroundColor = UIColor(red:0.15, green:0.31, blue:0.47, alpha:1)
        self.docimg3.addSubview(namelayer3)
        
        let textLayer3 = UILabel(frame: CGRect(x: 10, y: 6, width: 202, height: 25))
        textLayer3.lineBreakMode = .byWordWrapping
        textLayer3.numberOfLines = 0
        textLayer3.textColor = UIColor.white
        textLayer3.textAlignment = .left
        let textContent3 = "Dr S Moorthy"
        let textString3 = NSMutableAttributedString(string: textContent3, attributes: [NSAttributedStringKey.font: UIFont(name: "Rubik-Regular", size: 15)!])
        let textRange3 = NSRange(location: 0, length: textString3.length)
        let paragraphStyle3 = NSMutableParagraphStyle()
        paragraphStyle3.lineSpacing = 1.18
        textString3.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle3, range: textRange3)
        textLayer3.attributedText = textString3
        textLayer3.sizeToFit()
        namelayer3.addSubview(textLayer3)
        
        //docimg4
        let namelayer4 = UIView(frame: CGRect(x: 0, y: 130, width: 205, height: 30))
        namelayer4.alpha = 0.80
        namelayer4.layer.cornerRadius = 15
        namelayer4.backgroundColor = UIColor(red:0.15, green:0.31, blue:0.47, alpha:1)
        self.docimg4.addSubview(namelayer4)
        
        let textLayer4 = UILabel(frame: CGRect(x: 10, y: 6, width: 202, height: 25))
        textLayer4.lineBreakMode = .byWordWrapping
        textLayer4.numberOfLines = 0
        textLayer4.textColor = UIColor.white
        textLayer4.textAlignment = .left
        let textContent4 = "Dr Damodar Rao"
        let textString4 = NSMutableAttributedString(string: textContent4, attributes: [NSAttributedStringKey.font: UIFont(name: "Rubik-Regular", size: 15)!])
        let textRange4 = NSRange(location: 0, length: textString4.length)
        let paragraphStyle4 = NSMutableParagraphStyle()
        paragraphStyle4.lineSpacing = 1.18
        textString4.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle4, range: textRange4)
        textLayer4.attributedText = textString4
        textLayer4.sizeToFit()
        namelayer4.addSubview(textLayer4)
        
        //docig5
        let namelayer5 = UIView(frame: CGRect(x: 0, y: 130, width: 205, height: 30))
        namelayer5.alpha = 0.80
        namelayer5.layer.cornerRadius = 15
        namelayer5.backgroundColor = UIColor(red:0.15, green:0.31, blue:0.47, alpha:1)
        self.docimg5.addSubview(namelayer5)
        
        let textLayer5 = UILabel(frame: CGRect(x: 10, y: 6, width: 202, height: 25))
        textLayer5.lineBreakMode = .byWordWrapping
        textLayer5.numberOfLines = 0
        textLayer5.textColor = UIColor.white
        textLayer5.textAlignment = .left
        let textContent5 = "Dr Suresh Kumar"
        let textString5 = NSMutableAttributedString(string: textContent5, attributes: [NSAttributedStringKey.font: UIFont(name: "Rubik-Regular", size: 15)!])
        let textRange5 = NSRange(location: 0, length: textString5.length)
        let paragraphStyle5 = NSMutableParagraphStyle()
        paragraphStyle5.lineSpacing = 1.18
        textString5.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle5, range: textRange5)
        textLayer5.attributedText = textString5
        textLayer5.sizeToFit()
        namelayer5.addSubview(textLayer5)
        
        let namelayer6 = UIView(frame: CGRect(x: 0, y: 130, width: 205, height: 30))
        namelayer6.alpha = 0.80
        namelayer6.layer.cornerRadius = 15
        namelayer6.backgroundColor = UIColor(red:0.15, green:0.31, blue:0.47, alpha:1)
        self.docimg6.addSubview(namelayer6)
        
        let textLayer6 = UILabel(frame: CGRect(x: 10, y: 6, width: 202, height: 25))
        textLayer6.lineBreakMode = .byWordWrapping
        textLayer6.numberOfLines = 0
        textLayer6.textColor = UIColor.white
        textLayer6.textAlignment = .left
        let textContent6 = "Dr Aditya Sharma"
        let textString6 = NSMutableAttributedString(string: textContent6, attributes: [NSAttributedStringKey.font: UIFont(name: "Rubik-Regular", size: 15)!])
        let textRange6 = NSRange(location: 0, length: textString6.length)
        let paragraphStyle6 = NSMutableParagraphStyle()
        paragraphStyle6.lineSpacing = 1.18
        textString6.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle6, range: textRange6)
        textLayer6.attributedText = textString6
        textLayer6.sizeToFit()
        namelayer6.addSubview(textLayer6)
        
    }
}
