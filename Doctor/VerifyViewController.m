//
//  VerifyViewController.m
//  Doctor
//
//  Created by Manish Verma on 11/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import "VerifyViewController.h"
#import "FIRPhoneAuthProvider.h"
//#import "Doctor-Swift.h"
@import FirebaseAuth;


@interface VerifyViewController ()
{
    NSString *usermobilenumber;
    NSString *VerficationID;
    NSString *accesstoken;
    int sendbtncount;
    bool existinguser;
    NSString *newuserlogin;
    NSString *newuserpwd;
}

@property (weak, nonatomic) IBOutlet UIView *bottomBarView;
@property (weak, nonatomic) IBOutlet UIButton *ReSendButtonView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *MyprogressIndicator;
@property (strong, nonatomic) UINavigationController *nav;

@property (weak, nonatomic) IBOutlet UILabel *mobilenumberlb;
@property (weak, nonatomic) IBOutlet UITextField *otppin1;
@property (weak, nonatomic) IBOutlet UITextField *otppin2;
@property (weak, nonatomic) IBOutlet UITextField *otppin3;
@property (weak, nonatomic) IBOutlet UITextField *otppin4;
@property (weak, nonatomic) IBOutlet UITextField *otppin5;
@property (weak, nonatomic) IBOutlet UITextField *otppin6;

@end

@implementation VerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.navigationItem.leftBarButtonItem.accessibilityElementsHidden = true;
    [self.navigationItem setHidesBackButton:YES];
    
    self.MyprogressIndicator.hidden = true;
    self.ReSendButtonView.enabled = NO;
    sendbtncount = 0;
    
    [NSTimer scheduledTimerWithTimeInterval:30.0
                                     target:self
                                   selector:@selector(enableResendbuttonnow:)
                                   userInfo:nil
                                    repeats:NO];
    
    usermobilenumber = [[NSUserDefaults standardUserDefaults] stringForKey:@"userphonenumber"];
    
    // NSString *phoneString = @"+919845702415";
    NSString *displaymobilestring = [NSString stringWithFormat:@"%@ %@ %@", [usermobilenumber substringToIndex:3], [usermobilenumber substringWithRange:NSMakeRange(3, 5)],[usermobilenumber substringFromIndex:8]];
    
    self.mobilenumberlb.text = displaymobilestring;
    self.bottomBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"schbottombarbg"]];
    
    self.otppin1.delegate = self;
    self.otppin2.delegate = self;
    self.otppin3.delegate = self;
    self.otppin4.delegate = self;
    self.otppin5.delegate = self;
    self.otppin6.delegate = self;
    
    UIColor *color = [UIColor whiteColor];
    self.otppin1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
    self.otppin1.tintColor = [UIColor whiteColor];
    
    self.otppin2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
    self.otppin2.tintColor = [UIColor whiteColor];
    
    self.otppin3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
    self.otppin3.tintColor = [UIColor whiteColor];
    
    self.otppin4.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
    self.otppin4.tintColor = [UIColor whiteColor];
    
    self.otppin5.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
    self.otppin5.tintColor = [UIColor whiteColor];
    
    self.otppin6.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
    self.otppin6.tintColor = [UIColor whiteColor];
    
    NSLog(@"VerifyViewController:The Mobile number is %@", usermobilenumber);
    
    existinguser = [[[NSUserDefaults standardUserDefaults] valueForKey:@"existinguser"] boolValue];
    
}


- (void)enableResendbuttonnow:(NSTimer*)timer
{
    NSLog(@"ViewConteroller:enableResendbuttonnow() RESEND Button is enabled");
    self.ReSendButtonView.enabled = YES;
}

//Dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"ViewConteroller:touchesBegan");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
    
}


- (IBAction)SignInButton:(UIBarButtonItem *)sender {
    
}


- (IBAction)ConfirmButton:(UIButton *)sender {
    
    if(([self.otppin1.text length] < 1) || ([self.otppin2.text length] < 1) || ([self.otppin3.text length] < 1) || ([self.otppin4.text length] < 1) || ([self.otppin5.text length] < 1) || ([self.otppin6.text length] < 1))
    {
        NSLog(@"VerifyViewController:The OTP pin is not entered");
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Your have not entered 6 digit OTP!"
                                   preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                   }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return;
        
    }
    else
    {
        self.MyprogressIndicator.hidden = false;
        [self.MyprogressIndicator startAnimating];
        
        NSString *totalnumber=[NSString stringWithFormat:@"%@%@%@%@%@%@",self.otppin1.text,self.otppin2.text,self.otppin3.text,self.otppin4.text,self.otppin5.text,self.otppin6.text];
        
        FIRAuthCredential *credential = [[FIRPhoneAuthProvider provider]
                                         credentialWithVerificationID:[[NSUserDefaults standardUserDefaults] stringForKey:@"authVID"]
                                         verificationCode:totalnumber];
        
        
        [[FIRAuth auth] signInWithCredential:credential completion:^(FIRUser *user, NSError *error) {
            if (error)
            {
                NSLog(@"VerifyViewController:Error in Firebase signInWithCredential:: %@",[error localizedDescription]);
                                UIAlertController *alert= [UIAlertController
                                            alertControllerWithTitle:@"Alert"
                                            message:[error localizedDescription]
                                            preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action){
                                            //Do Some action here
                                            }];
                                            [alert addAction:ok];
                                            [self presentViewController:alert animated:YES completion:nil];
                                            return;
            }
                // User successfully signed in. Get user data from the FIRUser object
                NSLog(@"VerifyViewController:Firebase signInWithCredential SUCCESS");
                NSLog(@"VerifyViewController:Firebase Authenticated Phone Number : %@ ", user.phoneNumber);
                NSLog(@"VerifyViewController:Firebase Authenticated Provider ID : %@ ", user.providerID);
                NSLog(@"VerifyViewController:Firebase Authenticated user ID : %@ ", user.uid);
                //  NSLog(@"VerifyViewController:Firebase Authenticated user : %@ ", user);
            
                if(existinguser)
                {
                    NSString *stringWithoutpluswith_d = [user.phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@"d_"];
                    NSString *stringPWDWithoutplus = [user.phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
                    
                    [[NSUserDefaults standardUserDefaults]
                     setValue:stringWithoutpluswith_d forKey:@"userlogin"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [[NSUserDefaults standardUserDefaults]
                     setValue:stringPWDWithoutplus forKey:@"userpwd"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [QBRequest logInWithUserLogin:stringWithoutpluswith_d password:stringPWDWithoutplus
                            successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user)
                            {
                                // Login succeeded
                                 NSLog(@"VerifyViewController:QB Existing Login is  SUCCESS");
                              
                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                
                                [[NSUserDefaults standardUserDefaults]
                                 setValue:@"OnBoardDone" forKey:@"OnBoard"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                
                                
                                [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"userid"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                
                                [self performSegueWithIdentifier:@"gotoHomepg" sender:nil];
//                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DoctorDetailsStoryboard" bundle:nil];
//                                UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"PrimaryDetailsDocBaseViewController"];
//                                [self presentViewController:VC animated:NO completion:nil];
                                
                            } errorBlock:^(QBResponse * _Nonnull response) {
                                // Handle error
                                NSLog(@"VerifyViewController:QB Existing User Login !! ERROR !! : %@",response);
                    }];
                }
                else
                {
                    NSString *stringWithoutpluswith_d = [user.phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@"d_"];
                    NSString *stringPWDWithoutplus = [user.phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
                    
                    [[NSUserDefaults standardUserDefaults]
                     setValue:stringWithoutpluswith_d forKey:@"userlogin"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [[NSUserDefaults standardUserDefaults]
                     setValue:stringPWDWithoutplus forKey:@"userpwd"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSString *fullname = [[NSUserDefaults standardUserDefaults] valueForKey:@"fullname"];
                    
                    newuserlogin = stringWithoutpluswith_d;
                    newuserpwd = stringPWDWithoutplus;
                    
                    QBUUser *userqb = [QBUUser user];
                    userqb.password = stringPWDWithoutplus;
                    userqb.login = stringWithoutpluswith_d;
                    userqb.fullName = fullname;
                    NSString *stringWithoutplus = [user.phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
                    userqb.phone = stringWithoutplus; //usermobilenumber;
                    userqb.tags = [@[@"Doc0", @"IOS"] mutableCopy];
                    
                    // Registration/sign up of User
                    [QBRequest signUp:userqb successBlock:^(QBResponse *response, QBUUser *user) {
                    // Sign up was successful
                    NSLog(@"VerifyViewController:QB User SignUp for New Registration was SUCCESS");
                        NSLog(@"VerifyViewController:QB - New User");
                        [self loginafterRegistration];
                                          
                    } errorBlock:^(QBResponse *response) {
                        // Handle error here
                        NSLog(@"VerifyViewController:QB User SignUp for New Registration!! ERROR !! : %@",response);
                        
                        NSString *errorstring = @"Registration Error";
                        if(response.status == 422)
                        {
                            errorstring = @"Email has already been Taken";
                        }
                        
                        self.MyprogressIndicator.hidden = false;
                        [self.MyprogressIndicator startAnimating];
                        UIAlertController *alert= [UIAlertController
                                                   alertControllerWithTitle:@"Error !!"
                                                   message:errorstring
                                                   preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action){
                                                                       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                       UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"RegisterViewController"];
                                                                       [self presentViewController:VC animated:NO completion:nil];
                                                                       return;
                                                                   }];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                    }];
                }
        }];
        //  comment below code after simulator testing
        //        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //        UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"PrimaryDetails"];
        //        // [self.navigationController presentViewController:VC animated:YES completion:nil];
        //        self.nav = [[UINavigationController alloc] initWithRootViewController:VC];
        //        self.nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        //        [self presentViewController:self.nav animated:NO completion:nil];
    }
}

// We need to login after successful New User Registration
- (void)loginafterRegistration {
    
    NSLog(@"VerifyViewController:QB New User Login");
    // Authenticate user
    [QBRequest logInWithUserLogin:newuserlogin
                         password:newuserpwd
                     successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user)
     {
         // Login succeeded
         //new user hence go to Primary details page gotoPrimarydetails
          NSLog(@"VerifyViewController:QB New User Login SUCCESS");
         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         [[NSUserDefaults standardUserDefaults]
          setValue:@"OnBoardDone" forKey:@"OnBoard"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"userid"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DoctorDetailsStoryboard" bundle:nil];
         UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"PrimaryDetailsDocBaseViewController"];
         [self presentViewController:VC animated:NO completion:nil];
         
     } errorBlock:^(QBResponse * _Nonnull response) {
         // Handle error
          NSLog(@"VerifyViewController:QB New User Login ERROR !! : %@",response);
     }];
}


//-(void)SignIntoQB
//{
//    [QBRequest logInWithFirebaseProjectID:@"calldoc-62ef4" accessToken:accesstoken successBlock:^(QBResponse *response, QBUUser *user) {
//        NSLog(@"VerifyViewController:SignIntoQB() - Logged in with fireBase digits!");
//        if(existinguser)
//        {
//            NSLog(@"VerifyViewController:SignIntoQB() - Existing User");
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"userid"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//    
//            //[self performSegueWithIdentifier:@"gotoHomepg" sender:nil];
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DoctorDetailsStoryboard" bundle:nil];
//            UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"PrimaryDetailsDocBaseViewController"];
//            [self presentViewController:VC animated:NO completion:nil];
//        }
//        else
//        {
//            NSLog(@"VerifyViewController:SignIntoQB() - New User");
//            //new user hence go to Primary details page gotoPrimarydetails
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"userid"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DoctorDetailsStoryboard" bundle:nil];
//            UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"PrimaryDetailsDocBaseViewController"];
//            [self presentViewController:VC animated:NO completion:nil];
//        }
//        
//    } errorBlock:^(QBResponse *response) {
//        NSLog(@"VerifyViewController:SignIntoQB() - Response error: %@", response.error);
//    }];
//}



-(IBAction)ReSendOTPButton:(UIButton *)sender {
    
    self.MyprogressIndicator.hidden = false;
    sendbtncount++;
    if (sendbtncount > 3)
    {
        // use UIAlertController to Check Mobile number
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Network busy please try again after 10 minutes"
                                   preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       [NSTimer scheduledTimerWithTimeInterval:600.0
                                                                                        target:self
                                                                                      selector:@selector(enableResendbuttafter10min:)
                                                                                      userInfo:nil
                                                                                       repeats:NO];
                                                   }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        self.ReSendButtonView.enabled = NO;
        self.MyprogressIndicator.hidden = true;
        return;
    }
    
//    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:usermobilenumber
//                                            completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
    
    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:usermobilenumber
                                            UIDelegate:nil
                                            completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
                                                
                                                if (error) {
                                                    NSLog(@"VerifyViewController:Firebase Auth Error %@",[error localizedDescription]);
                                                    return;
                                                }
                                                
                                                VerficationID = verificationID;
                                                
                                                [[NSUserDefaults standardUserDefaults]
                                                 setValue:VerficationID forKey:@"authVID"];
                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                
                                                self.MyprogressIndicator.hidden = true;
                                                [self.MyprogressIndicator stopAnimating];
                                            }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ((textField.text.length < 1) && (string.length > 0))
    {
        
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        
        return NO;
        
    }else if ((textField.text.length >= 1) && (string.length > 0)){
        //FOR MAXIMUM 1 TEXT
        
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        //  textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        UITextField *thenexttxt = [textField.superview viewWithTag:nextTag];
        thenexttxt.text = string;
        
        if (nextTag > 5)
        {
            [thenexttxt resignFirstResponder];
        }
        
        return NO;
    }
    else if ((textField.text.length >= 1) && (string.length == 0)){
        // on deleteing value from Textfield
        
        NSInteger prevTag = textField.tag - 1;
        // Try to find prev responder
        UIResponder* prevResponder = [textField.superview viewWithTag:prevTag];
        if (! prevResponder){
            //  [textField resignFirstResponder];
            prevResponder = [textField.superview viewWithTag:1];
        }
        //textField.text = string;
        textField.text = @"";
        if (prevResponder)
            // Found next responder, so set it.
            [prevResponder becomeFirstResponder];
        return NO;
    }
    
    return YES;
}

//This is to delete using backspace
- (BOOL)keyboardInputShouldDelete:(UITextField *)textField {
    BOOL shouldDelete = YES;
    
    if ([textField.text length] == 0 && [textField.text isEqualToString:@""]) {
        long tagValue = textField.tag - 1;
        UITextField *txtField = (UITextField*) [self.view viewWithTag:tagValue];
        
        [txtField becomeFirstResponder];
    }
    return shouldDelete;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)enableResendbuttafter10min:(NSTimer*)timer
{
    NSLog(@"ViewConteroller:enableResendbuttafter10min() RESEND Button is enabled");
    sendbtncount = 0;
    self.ReSendButtonView.enabled = YES;
    
}

@end
