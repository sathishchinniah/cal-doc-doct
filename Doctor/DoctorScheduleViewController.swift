import UIKit
import Toast_Swift

class DoctorScheduleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var weekTableView: UITableView!
    @IBOutlet weak var bottomMenu: UIView!
    @IBOutlet weak var daysContainerWeekView: UIView!
    @IBOutlet weak var mondayWVBtn: UIButton!
    @IBOutlet weak var tuesdayWVBtn: UIButton!
    @IBOutlet weak var wednesdayWVBtn: UIButton!
    @IBOutlet weak var thursdayWVBtn: UIButton!
    @IBOutlet weak var fridayWVBtn: UIButton!
    @IBOutlet weak var satudayWVBtn: UIButton!
    @IBOutlet weak var sundayWVBtn: UIButton!
    @IBOutlet weak var dayViewButton: UIButton!
    @IBOutlet weak var weekViewButton: UIButton!
    @IBOutlet weak var todayDaysLabel: UILabel!
    @IBOutlet weak var previousBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var daysContainerView: UIView!
    @IBOutlet weak var mondayBtn: UIButton!
    @IBOutlet weak var tuesdayBtn: UIButton!
    @IBOutlet weak var wednesdayBtn: UIButton!
    @IBOutlet weak var thursdayBtn: UIButton!
    @IBOutlet weak var fridaydayBtn: UIButton!
    @IBOutlet weak var saturdaydayBtn: UIButton!
    @IBOutlet weak var sundaydayBtn: UIButton!
    @IBOutlet weak var copyBtn: UIButton!
    @IBOutlet weak var pasteBtn: UIButton!
    @IBOutlet weak var SchSwitchOnOffView: UISwitch!
    @IBOutlet weak var MaskSchedularred: UIView!
    @IBOutlet weak var docScheduleFirstTableView: UITableView!
    @IBOutlet weak var secondTableView: UITableView!
    @IBOutlet weak var thirdTableView: UITableView!
    
//    let wTVHrValueArray = [
//        "07",
//        "09",
//        "11",
//        "01",
//        "03",
//        "05",
//        "07",
//        "09",
//        "11",
//        "01",
//        "03",
//        "05"
//    ]
    
    let wTVHrValueArray = [
        "00",
        "02",
        "04",
        "06",
        "08",
        "10",
        "12",
        "02",
        "04",
        "06",
        "08",
        "10"
    ]
    
//    let amPmStringValueWVArray = [
//        "AM",
//        "AM",
//        "AM",
//        "PM",
//        "PM",
//        "PM",
//        "PM",
//        "PM",
//        "PM",
//        "AM",
//        "AM",
//        "AM"
//    ]
    
    let amPmStringValueWVArray = [
        "AM",
        "AM",
        "AM",
        "AM",
        "AM",
        "AM",
        "PM",
        "PM",
        "PM",
        "PM",
        "PM",
        "PM"
    ]

    var showPopup = false
    let weakViewCellReuseIdentifier = "TableCell"
    var isWeekViewSelected = false
    var amPmStringValueFTV = "AM"
    var amPmStringValueSTV = "PM"
    var amPmStringValueTTV = "PM"
    var nav: UINavigationController?
    let cellReuseIdentifier = "FirstTableViewCell"
    var fTVCellHeight = 0
    var fTVCellWidth = 0
    var fTVHrValue = 07
    var sTVCellHeight = 0
    var sTVCellWidth = 0
    var sTVHrValue = 03
    var tTVCellHeight = 0
    var tTVCellWidth = 0
    var tTVHrValue = 11
    var isCopy = false
    var isPaste = false
    var nextDay = false
    var previousDay = false
    var isDaysBtnSelect = false
    var todayDay: Int!
    var currentDay: Int!
    var copiedDataDay: Int!
    
    var previousIndexOfFirstTableView = 0
    var previousIndexOfSecondTableView = 0
    var previousIndexOfThirdTableView = 0
    
    var firstTableViewDataArray = NSMutableSet()
    var secondTableViewDataArray = NSMutableSet()
    var thirdTableViewDataArray = NSMutableSet()
    
    var mCopyFirstTableViewDataArray = NSMutableSet()
    var mCopySecondTableViewDataArray = NSMutableSet()
    var mCopyThirdTableViewDataArray = NSMutableSet()
    
    var mondaySelectedData = [String:NSMutableSet]()
    var tuesdaySelectedData = [String:NSMutableSet]()
    var wednesdaySelectedDate = [String:NSMutableSet]()
    var thursdaySelectedData = [String:NSMutableSet]()
    var fridaySelectedData = [String:NSMutableSet]()
    var saturdaySelectedData = [String:NSMutableSet]()
    var sundaySelectedData = [String:NSMutableSet]()
    
    var isdataChangesMade = false
    var mondayArray = [Int]()
    var tuesdayArray = [Int]()
    var wednesdayArray = [Int]()
    var thursdayArray = [Int]()
    var fridayArray = [Int]()
    var saturdayArray = [Int]()
    var sundayArray = [Int]()
    
    let Key_First_Table = "first_table"
    let Key_Second_Table = "second_table"
    let Key_Third_Table = "third_table"

    static let defaultArray = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
    
    var MondayarrayOfBool = defaultArray
    var TuesdayarrayOfBool = defaultArray
    var WednesdayarrayOfBool = defaultArray
    var ThursdayarrayOfBool = defaultArray
    var FridayarrayOfBool = defaultArray
    var SaturdayarrayOfBool = defaultArray
    var SundayarrayOfBool = defaultArray
    
    struct Device {
        // iDevice detection code
        static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
        static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
        static let IS_RETINA           = UIScreen.main.scale >= 2.0
        static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
        static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
        static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
        static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
        static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
        static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
        static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
        static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
        static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        daysContainerView.isHidden = false
        daysContainerWeekView.isHidden  = true
        
        mondaySelectedData = [Key_First_Table: NSMutableSet() , Key_Second_Table: NSMutableSet(), Key_Third_Table: NSMutableSet()]
        tuesdaySelectedData = [Key_First_Table: NSMutableSet() , Key_Second_Table: NSMutableSet(), Key_Third_Table: NSMutableSet()]
        wednesdaySelectedDate = [Key_First_Table: NSMutableSet() , Key_Second_Table: NSMutableSet(), Key_Third_Table: NSMutableSet()]
        thursdaySelectedData = [Key_First_Table: NSMutableSet() , Key_Second_Table: NSMutableSet(), Key_Third_Table: NSMutableSet()]
        fridaySelectedData = [Key_First_Table: NSMutableSet() , Key_Second_Table: NSMutableSet(), Key_Third_Table: NSMutableSet()]
        saturdaySelectedData = [Key_First_Table: NSMutableSet() , Key_Second_Table: NSMutableSet(), Key_Third_Table: NSMutableSet()]
        sundaySelectedData = [Key_First_Table: NSMutableSet() , Key_Second_Table: NSMutableSet(), Key_Third_Table: NSMutableSet()]
        
        weekViewButton.setTitleColor(UIColor.lightText, for: .normal)
        
        weekTableView.isHidden = true
        todayDaysLabel.isHidden = false
        docScheduleFirstTableView.isHidden = false
        secondTableView.isHidden = false
        thirdTableView.isHidden = false
        previousBtn.isHidden = false
        nextBtn.isHidden = false

        if showPopup {
            showPopUpToStartConsulting()
        }
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        
        bottomMenu.layer.shadowColor = UIColor.lightGray.cgColor
        bottomMenu.layer.shadowOpacity = 0.5
        bottomMenu.layer.shadowRadius = 3.0
        bottomMenu.layer.shadowOffset = .zero
        bottomMenu.layer.shouldRasterize = true
        bottomMenu.layer.masksToBounds = false
        
        SchSwitchOnOffView.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        MaskSchedularred.layer.contents = UIImage(named:"bgOfflineMask")?.cgImage
        GlobalDocData.maskschedularview = UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF)
        
        if(GlobalDocData.maskschedularview) {   // true means scheduler is off
            self.MaskSchedularred.isHidden = false
            SchSwitchOnOffView.setOn(false, animated: true)  // schedular is OFF
            SchSwitchOnOffView.layer.cornerRadius = 16
            SchSwitchOnOffView.onTintColor = UIColor(red:0.88, green:0.18, blue:0.2, alpha:1) //.red
            SchSwitchOnOffView.backgroundColor = UIColor(red:0.88, green:0.18, blue:0.2, alpha:1) //.red
        } else {
            self.MaskSchedularred.isHidden = true
            SchSwitchOnOffView.setOn(true, animated: true)    // schedular is ON
            SchSwitchOnOffView.layer.cornerRadius = 16
            SchSwitchOnOffView.onTintColor = UIColor(red:0.23, green:0.64, blue:0.34, alpha:1) //.green
            SchSwitchOnOffView.backgroundColor = UIColor(red:0.23, green:0.64, blue:0.34, alpha:1) //.green
        }
        
//        mondayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
//        mondayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
//        mondayBtn.setTitleColor(UIColor.white, for: .normal)
        
        let weekdays = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Satudrday,"
        ]
        let date = Date()
        let calendar = Calendar.current
        let weekdayComponent = calendar.component(.weekday, from: date)
        todayDaysLabel.text = weekdays[weekdayComponent-1]//weekdays[1]
        todayDay = weekdayComponent - 2
        copiedDataDay = todayDay
        currentDay = weekdayComponent
        crearDaysBackground()
        drowEmptyCircleOfCurrentDay(weekDay: weekdayComponent)
        copyBtn.isHidden = false
        pasteBtn.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(firstTableViewTapGestrue))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.cancelsTouchesInView = false
        docScheduleFirstTableView?.isUserInteractionEnabled = true
        docScheduleFirstTableView.addGestureRecognizer(tapGesture)
        
        let sTapGesture = UITapGestureRecognizer(target: self, action: #selector(secondTableViewTapGestrue))
        sTapGesture.numberOfTapsRequired = 1
        sTapGesture.cancelsTouchesInView = false
        secondTableView?.isUserInteractionEnabled = true
        secondTableView.addGestureRecognizer(sTapGesture)
        
        let tTapGesture = UITapGestureRecognizer(target: self, action: #selector(thirdTableViewTapGestrue))
        tTapGesture.numberOfTapsRequired = 1
        tTapGesture.cancelsTouchesInView = false
        thirdTableView?.isUserInteractionEnabled = true
        thirdTableView.addGestureRecognizer(tTapGesture)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(firstTableViewPanGesture))
        docScheduleFirstTableView?.isUserInteractionEnabled = true
        docScheduleFirstTableView.addGestureRecognizer(panGesture)
        
        let sPanGesture = UIPanGestureRecognizer(target: self, action: #selector(secondTableViewPanGesture))
        secondTableView?.isUserInteractionEnabled = true
        secondTableView.addGestureRecognizer(sPanGesture)
        
        let tPanGesture = UIPanGestureRecognizer(target: self, action: #selector(thirdTableViewPanGesture))
        thirdTableView?.isUserInteractionEnabled = true
        thirdTableView.addGestureRecognizer(tPanGesture)
        
        //Back Button
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.getSchedularDatafromBackend()
        
    }
    
    func getSchedularDatafromBackend() {

        if let mondayArrayOfBools = UserDefaults.standard.value(forKey: Constants.mondaySchedularData) as? [Bool] {
            self.mondaySelectedData = self.populateRetrivedData(array: mondayArrayOfBools)
        }

        if let tuesdayArrayOfBools = UserDefaults.standard.value(forKey: Constants.tuesdaySchedularData) as? [Bool] {
            self.tuesdaySelectedData = self.populateRetrivedData(array: tuesdayArrayOfBools)
        }

        if let wednesdayArrayOfBools = UserDefaults.standard.value(forKey: Constants.wednesdaySchedularData) as? [Bool] {
            self.wednesdaySelectedDate = self.populateRetrivedData(array: wednesdayArrayOfBools)
        }

        if let thursdayArrayOfBools = UserDefaults.standard.value(forKey: Constants.thursdaySchedularData) as? [Bool] {
            self.thursdaySelectedData = self.populateRetrivedData(array: thursdayArrayOfBools)
        }

        if let fridayArrayOfBools = UserDefaults.standard.value(forKey: Constants.fridaySchedularData) as? [Bool] {
            self.fridaySelectedData = self.populateRetrivedData(array: fridayArrayOfBools)
        }

        if let saturdayArrayOfBools = UserDefaults.standard.value(forKey: Constants.saturdaySchedularData) as? [Bool] {
            self.saturdaySelectedData = self.populateRetrivedData(array: saturdayArrayOfBools)
        }

        if let sundayArrayOfBools = UserDefaults.standard.value(forKey: Constants.sundaySchedularData) as? [Bool] {
            self.sundaySelectedData = self.populateRetrivedData(array: sundayArrayOfBools)
        }

        restoreDayViewValues()

        self.previousNextDaySelection()

    }

    private func restoreDayViewValues() {
        self.fTVHrValue = 12
        self.sTVHrValue = 8
        self.tTVHrValue = 4

        self.amPmStringValueFTV = "AM"
        self.amPmStringValueSTV = "AM"
        self.amPmStringValueTTV = "PM"
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        if isdataChangesMade {
            let alert = UIAlertController(title: nil, message: "You have made changes to your online schedule.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "SAVE", style: .default, handler: { (action) in
                self.saveMyScheduleData()
                self.navigateToHomeScreen()
            }))
            alert.addAction(UIAlertAction(title: "DISCARD", style: .default, handler: { (action) in
                self.isdataChangesMade = false
                self.isCopy = false
                self.viewDidLoad()
                //self.navigateToHomeScreen()
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            navigateToHomeScreen()
        }
    }
    
    func drowEmptyCircleOfCurrentDay(weekDay:Int) {
        if weekDay == 1 {
            currentDayCircle(sender: sundaydayBtn)
            currentDayCircle(sender: sundayWVBtn)
        } else if weekDay == 2 {
            currentDayCircle(sender: mondayBtn)
            currentDayCircle(sender: mondayWVBtn)
        } else if weekDay == 3 {
            currentDayCircle(sender: tuesdayBtn)
            currentDayCircle(sender: tuesdayWVBtn)
        } else if weekDay == 4 {
            currentDayCircle(sender: wednesdayBtn)
            currentDayCircle(sender: wednesdayWVBtn)
        } else if weekDay == 5 {
            currentDayCircle(sender: thursdayBtn)
            currentDayCircle(sender: thursdayWVBtn)
        } else if weekDay == 6 {
            currentDayCircle(sender: fridaydayBtn)
            currentDayCircle(sender: fridayWVBtn)
        } else if weekDay == 7 {
            currentDayCircle(sender: saturdaydayBtn)
            currentDayCircle(sender: sundayWVBtn)
        }
    }
    
    func currentDayCircle(sender: UIButton)  {
        sender.layer.cornerRadius = sender.frame.size.width/2
        sender.layer.borderWidth = 1
        sender.layer.borderColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1).cgColor
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }
    
    override func viewDidLayoutSubviews() {
        
        let mx = mondayBtn.frame.origin.x
        let tx = thursdayBtn.frame.origin.x
        let sx = sundaydayBtn.frame.origin.x

        docScheduleFirstTableView.frame.size = docScheduleFirstTableView.contentSize
        fTVCellWidth = Int(docScheduleFirstTableView.contentSize.width)
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        if (Device.IS_IPHONE_X) {
            fTVCellHeight = Int(height - (self.navigationController!.navigationBar.frame.size.height + 260))/16
        } else {
            fTVCellHeight = Int(height - (self.navigationController!.navigationBar.frame.size.height + 200))/16
        }
        
        secondTableView.frame.size = secondTableView.contentSize
        sTVCellWidth = Int(secondTableView.contentSize.width)
        let sBounds = UIScreen.main.bounds
        let sHeight = sBounds.size.height
        
        if (Device.IS_IPHONE_X) {
            sTVCellHeight = Int(sHeight - (self.navigationController!.navigationBar.frame.size.height + 260))/16
        } else {
            sTVCellHeight = Int(sHeight - (self.navigationController!.navigationBar.frame.size.height + 200))/16
        }
        
        thirdTableView.frame.size = thirdTableView.contentSize
        tTVCellWidth = Int(thirdTableView.contentSize.width)
        if (Device.IS_IPHONE_X) {
            tTVCellHeight = Int(sHeight - (self.navigationController!.navigationBar.frame.size.height + 260))/16
        } else {
            tTVCellHeight = Int(sHeight - (self.navigationController!.navigationBar.frame.size.height + 200))/16
        }
        
        if isCopy {
            if copiedDataDay == todayDay {
                pasteBtn.isHidden = true
            } else {
                pasteBtn.isHidden = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navBarColor = navigationController!.navigationBar
        navBarColor.topItem?.title = "My Schedule"
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    
    //MARK: GestureRecognizer
    
    @objc func firstTableViewTapGestrue(rec:UITapGestureRecognizer){
        isdataChangesMade = true
        let beginLocation =  rec.location(in: docScheduleFirstTableView)
        let indexPath = docScheduleFirstTableView.indexPathForRow(at: beginLocation)
        if (indexPath != nil) {
            let cell = docScheduleFirstTableView.cellForRow(at: indexPath!) as! DocScheduleFirstTableViewCell
            if cell.button1.backgroundColor == UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1){
                cell.button1.backgroundColor = UIColor.clear
                firstTableViewDataArray.remove(indexPath!.row)
            } else {
                cell.button1.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
                firstTableViewDataArray.add(indexPath!.row)
            }
        }
    }
    
    @objc func secondTableViewTapGestrue(rec:UITapGestureRecognizer){
        isdataChangesMade = true
        let beginLocation =  rec.location(in: secondTableView)
        let indexPath = secondTableView.indexPathForRow(at: beginLocation)
        if (indexPath != nil) {
            let cell = secondTableView.cellForRow(at: indexPath!) as! DocScheduleSecondTableViewCell
            if cell.button.backgroundColor == UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1){
                cell.button.backgroundColor = UIColor.clear
                secondTableViewDataArray.remove(indexPath!.row)
            } else {
                cell.button.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
                secondTableViewDataArray.add(indexPath!.row)
            }
        }
    }
    
    @objc func thirdTableViewTapGestrue(rec:UITapGestureRecognizer){
        isdataChangesMade = true
        let beginLocation =  rec.location(in: thirdTableView)
        let indexPath = thirdTableView.indexPathForRow(at: beginLocation)
        if (indexPath != nil) {
            let cell = thirdTableView.cellForRow(at: indexPath!) as! DocScheduleThirdTableViewCell
            if cell.button.backgroundColor == UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1){
                cell.button.backgroundColor = UIColor.clear
                thirdTableViewDataArray.remove(indexPath!.row)
            } else {
                cell.button.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
                thirdTableViewDataArray.add(indexPath!.row)
            }
        }
    }
    
    @objc func firstTableViewPanGesture(rec:UIPanGestureRecognizer){
        isdataChangesMade = true
        let beginLocation =  rec.location(in: docScheduleFirstTableView)
        let indexPath = docScheduleFirstTableView.indexPathForRow(at: beginLocation)
        if (indexPath != nil) {
            let cell = docScheduleFirstTableView.cellForRow(at: indexPath!) as! DocScheduleFirstTableViewCell
            
            if indexPath?.row != previousIndexOfFirstTableView {
                previousIndexOfFirstTableView = (indexPath?.row)!
                if cell.button1.backgroundColor == UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1){
                    cell.button1.backgroundColor = UIColor.clear
                    firstTableViewDataArray.remove(indexPath!.row)
                } else {
                    cell.button1.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
                    firstTableViewDataArray.add(indexPath!.row)
                }
            } else {
                print("in else")
            }
        }
    }
    
    @objc func secondTableViewPanGesture(rec:UIPanGestureRecognizer){
        isdataChangesMade = true
        let beginLocation =  rec.location(in: secondTableView)
        let indexPath = secondTableView.indexPathForRow(at: beginLocation)
        if (indexPath != nil){
            let cell = secondTableView.cellForRow(at: indexPath!) as! DocScheduleSecondTableViewCell
            if indexPath?.row != previousIndexOfSecondTableView {
                previousIndexOfSecondTableView = (indexPath?.row)!
                if cell.button.backgroundColor == UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1){
                    cell.button.backgroundColor = UIColor.clear
                    secondTableViewDataArray.remove(indexPath!.row)
                } else {
                    cell.button.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
                    secondTableViewDataArray.add(indexPath!.row)
                }
            }
        }
    }
    
    @objc func thirdTableViewPanGesture(rec:UIPanGestureRecognizer){
        isdataChangesMade = true
        let beginLocation =  rec.location(in: thirdTableView)
        let indexPath = thirdTableView.indexPathForRow(at: beginLocation)
        if (indexPath != nil) {
            let cell = thirdTableView.cellForRow(at: indexPath!) as! DocScheduleThirdTableViewCell
            if indexPath?.row != previousIndexOfThirdTableView {
                previousIndexOfThirdTableView = (indexPath?.row)!
                if cell.button.backgroundColor == UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1){
                    cell.button.backgroundColor = UIColor.clear
                    thirdTableViewDataArray.remove(indexPath!.row)
                } else {
                    cell.button.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
                    thirdTableViewDataArray.add(indexPath!.row)
                }
            }
        }
    }
    
    //MARK: UITableView methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == weekTableView {
            return 12
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == weekTableView {
            return 4
        }
        return 16
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let date = NSDate()
        let calendar = Calendar.current
        var day = calendar.component(.weekday, from: date as Date)
        let hour = calendar.component(.hour, from: date as Date)
        let minutes = calendar.component(.minute, from: date as Date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a" // for specifying the change to format hour:minute am/pm.
        let dateInString = dateFormatter.string(from: date as Date)
        var tableview = 0
        var slot = 0
        if hour < 8 {
           tableview = 1
            if minutes > 30 {
                slot = (hour * 2) + 1
            } else {
                slot = hour * 2
            }
        } else if hour < 16 {
            tableview = 2
            if minutes > 30 {
                slot = ((hour * 2) - 16 ) + 1
            } else {
                slot = (hour * 2) - 16
            }
        } else {
            tableview = 3
            if minutes > 30 {
                slot = ((hour * 2) - 32 ) + 1
            } else {
                slot = (hour * 2) - 32
            }
        }
        
        if day == 1 {
            day = 6
        } else {
            day = day - 2
        }
        
        //12AM to 7AM TV1, 8AM to 3PM TV2, 4PM to 11PM TV3
        
        
        if tableView == docScheduleFirstTableView {
            let cell  = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! DocScheduleFirstTableViewCell
            
            var btnFrame = cell.button1.frame
            btnFrame.size.height = CGFloat(fTVCellHeight - 3)
            btnFrame.origin.y = 0
            btnFrame.origin.x = CGFloat(fTVCellWidth/2)
            btnFrame.size.width = CGFloat(fTVCellWidth/2)
            
            cell.button1.frame = btnFrame
            cell.button1.layer.cornerRadius = 5
            
            if tableview == 1 && slot == indexPath.row && day == todayDay {
                cell.button1.layer.borderWidth = 1
                cell.button1.layer.borderColor = UIColor.black.cgColor
            } else {
                cell.button1.layer.borderWidth = 0
            }
            
            cell.button1.backgroundColor = UIColor.clear
            if firstTableViewDataArray.contains(indexPath.row) {
                cell.button1.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
            }
//            if isCopy {
//                cell.button1.backgroundColor = UIColor.clear
//            }
            if isPaste && firstTableViewDataArray.contains(indexPath.row) {
                cell.button1.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
            }
            
            var grayView = cell.grayView.frame
            grayView.origin.x = 0
            grayView.origin.y = CGFloat(fTVCellHeight - 1)
            grayView.size.width = 15
            cell.grayView.frame = grayView
            
            var  sepatatorView = cell.sepatatorView.frame
            sepatatorView.origin.x = 0
            sepatatorView.origin.y = CGFloat(fTVCellHeight - 1)
            sepatatorView.size.width = CGFloat(fTVCellWidth)
            cell.sepatatorView.frame = sepatatorView
            
            let row = indexPath.row%2

            if row != 0 {
                cell.sepatatorView.isHidden = false
                cell.grayView.isHidden = true
                cell.hrLabel.isHidden = true
                cell.amPMLabel.isHidden = true
            } else {
                cell.grayView.isHidden = false
                cell.sepatatorView.isHidden = true
                cell.hrLabel.isHidden = false
                cell.amPMLabel.isHidden = false
                
                cell.hrLabel.text = String(format: "%02d", fTVHrValue)
                if fTVHrValue == 12 {
                    fTVHrValue = 0;
                }
                cell.amPMLabel.text = amPmStringValueFTV
                fTVHrValue+=1
            }
            return cell

        } else if tableView == secondTableView {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "SecondTableViewCell") as! DocScheduleSecondTableViewCell
            var btnFrame = cell.button.frame
            btnFrame.size.height = CGFloat(sTVCellHeight - 3)
            btnFrame.origin.y = 0
            btnFrame.origin.x = CGFloat(sTVCellWidth/2)
            btnFrame.size.width = CGFloat(sTVCellWidth/2)
            cell.button.frame = btnFrame
            cell.button.layer.cornerRadius = 5
            
            if tableview == 2 && slot == indexPath.row && day == todayDay {
                cell.button.layer.borderWidth = 1
                cell.button.layer.borderColor = UIColor.black.cgColor
            } else {
                cell.button.layer.borderWidth = 0
            }
            
            cell.button.backgroundColor = UIColor.clear
            if secondTableViewDataArray.contains(indexPath.row){
                cell.button.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
            }
//            if isCopy {
//                cell.button.backgroundColor = UIColor.clear
//            }
            if isPaste && secondTableViewDataArray.contains(indexPath.row){
                cell.button.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
            }
            
            var grayView = cell.grayView.frame
            grayView.origin.x = 0
            grayView.origin.y = CGFloat(sTVCellHeight - 1)
            grayView.size.width = 15
            cell.grayView.frame = grayView
            
            var  sepatatorView = cell.sepatatorView.frame
            sepatatorView.origin.x = 0
            sepatatorView.origin.y = CGFloat(sTVCellHeight - 1)
            sepatatorView.size.width = CGFloat(sTVCellWidth)
            cell.sepatatorView.frame = sepatatorView
            
            let row = indexPath.row%2

            if row != 0 {
                cell.sepatatorView.isHidden = false
                cell.grayView.isHidden = true
                cell.hrLabel.isHidden = true
                cell.amPMLabel.isHidden = true
            } else {
                cell.grayView.isHidden = false
                cell.sepatatorView.isHidden = true
                cell.hrLabel.isHidden = false
                cell.amPMLabel.isHidden = false
                
                cell.hrLabel.text = String(format: "%02d", sTVHrValue)
                if sTVHrValue == 12 {
                    sTVHrValue = 0
                    amPmStringValueSTV = "PM"
                }
                cell.amPMLabel.text = amPmStringValueSTV
                sTVHrValue+=1
            }
            return cell
            
            
        } else if tableView == thirdTableView {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "ThirdTableViewCell") as! DocScheduleThirdTableViewCell
            var btnFrame = cell.button.frame
            btnFrame.size.height = CGFloat(tTVCellHeight - 3)
            btnFrame.origin.y = 0
            btnFrame.origin.x = CGFloat(tTVCellWidth/2)
            btnFrame.size.width = CGFloat(tTVCellWidth/2)
            cell.button.frame = btnFrame
            cell.button.layer.cornerRadius = 5
            
            if tableview == 3 && slot == indexPath.row && day == todayDay {
                cell.button.layer.borderWidth = 1
                cell.button.layer.borderColor = UIColor.black.cgColor
            } else {
                cell.button.layer.borderWidth = 0
            }
            
            cell.button.backgroundColor = UIColor.clear
            if thirdTableViewDataArray.contains(indexPath.row) {
                cell.button.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
            }
//            if isCopy {
//                cell.button.backgroundColor = UIColor.clear
//            }
            if isPaste && thirdTableViewDataArray.contains(indexPath.row) {
                cell.button.backgroundColor = UIColor.init(red: 0/255, green: 171/255, blue: 89/255, alpha: 1)
            }
            
            var grayView = cell.grayView.frame
            grayView.origin.x = 0
            grayView.origin.y = CGFloat(tTVCellHeight - 1)
            grayView.size.width = 15
            cell.grayView.frame = grayView
            
            var  sepatatorView = cell.sepatatorView.frame
            sepatatorView.origin.x = 0
            sepatatorView.origin.y = CGFloat(tTVCellHeight - 1)
            sepatatorView.size.width = CGFloat(tTVCellWidth)
            cell.sepatatorView.frame = sepatatorView
            
            let row = indexPath.row%2
            if row != 0 {
                cell.sepatatorView.isHidden = false
                cell.grayView.isHidden = true
                
                cell.hrLabel.isHidden = true
                cell.amPMLabel.isHidden = true
            } else {
                cell.grayView.isHidden = false
                cell.sepatatorView.isHidden = true
                cell.hrLabel.isHidden = false
                cell.amPMLabel.isHidden = false
                
                cell.hrLabel.text = String(format: "%02d", tTVHrValue)
                cell.amPMLabel.text = amPmStringValueTTV
                tTVHrValue+=1
            }
            return cell

        } else {

            /**************** WeekTableView ******************/
            let cell = tableView.dequeueReusableCell(withIdentifier: weakViewCellReuseIdentifier,for: indexPath) as! WeekTableViewCell
            
            if indexPath.row == 0 && indexPath.section != 0 {
                cell.SPView1.isHidden = false
                cell.SPView2.isHidden = false
                cell.SPView3.isHidden = false
                cell.SPView4.isHidden = false
                cell.SPView5.isHidden = false
                cell.SPView6.isHidden = false
                cell.SPView7.isHidden = false
            } else {
                cell.SPView1.isHidden = true
                cell.SPView2.isHidden = true
                cell.SPView3.isHidden = true
                cell.SPView4.isHidden = true
                cell.SPView5.isHidden = true
                cell.SPView6.isHidden = true
                cell.SPView7.isHidden = true
            }
            
            clearCircularDots(cell: cell)
            
            showCircularDots(array: mondayArray, view: cell.circuleView1, indexPath: indexPath)
            showCircularDots(array: tuesdayArray, view: cell.circuleView2, indexPath: indexPath)
            showCircularDots(array: wednesdayArray, view: cell.circuleView3, indexPath: indexPath)
            showCircularDots(array: thursdayArray, view: cell.circuleView4, indexPath: indexPath)
            showCircularDots(array: fridayArray, view: cell.circuleView5, indexPath: indexPath)
            showCircularDots(array: saturdayArray, view: cell.circuleView6, indexPath: indexPath)
            showCircularDots(array: sundayArray, view: cell.circuleView7, indexPath: indexPath)
            
            if indexPath.row == 0 {
                cell.hrLabel.isHidden = false
                cell.amPmLabel.isHidden = false
                cell.hrLabel.text = wTVHrValueArray[indexPath.section]
                cell.amPmLabel.text = amPmStringValueWVArray[indexPath.section]
            } else {
                cell.hrLabel.isHidden = true
                cell.amPmLabel.isHidden = true
            }
            
            return cell
            /**************** WeekTableView End ******************/
        }
        
    }
    
    func showCircularDots(array: Array<Int>, view: UIView, indexPath: IndexPath) {
        for item in array {
            if  Int(floor(Double(item/4))) == indexPath.section  {
                if indexPath.row == item % 4 {
                    view.isHidden = false
                }
            }
        }
    }
    
    func clearCircularDots(cell : WeekTableViewCell) {
        cell.circuleView1.isHidden = true
        cell.circuleView2.isHidden = true
        cell.circuleView3.isHidden = true
        cell.circuleView4.isHidden = true
        cell.circuleView5.isHidden = true
        cell.circuleView6.isHidden = true
        cell.circuleView7.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == docScheduleFirstTableView {
            return "MORNING"
        } else if tableView == secondTableView {
            return "DAY TIME"//"EVENING"
        } else if tableView == thirdTableView {
            return "EVENING"//"NIGHT"
        } else {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = UIColor.clear
        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 9, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font =  UIFont.boldSystemFont(ofSize: 15)
        headerLabel.textColor = UIColor.init(red: 171/255, green: 196/255, blue: 204/255, alpha: 1)
        headerLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == weekTableView {
            return 2
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == docScheduleFirstTableView {
            return CGFloat(fTVCellHeight)
        } else if tableView == secondTableView {
            return CGFloat(sTVCellHeight)
        } else if tableView == thirdTableView {
            return CGFloat(tTVCellHeight)
        } else if tableView == weekTableView {
            return 18
        } else {
            return 0
        }
    }
    
    //MARK: Copy Paste Methods
    
    @IBAction func copyBtnAction(_ sender: Any) {
        copiedDataDay = todayDay
        isCopy = true
        isPaste = false
        pasteBtn.isHidden = true
        isdataChangesMade = true
        mCopyFirstTableViewDataArray = NSMutableSet(set: firstTableViewDataArray)
        mCopySecondTableViewDataArray = NSMutableSet(set: secondTableViewDataArray)
        mCopyThirdTableViewDataArray = NSMutableSet(set: thirdTableViewDataArray)
    }
    
    @IBAction func pasteBtnAction(_ sender: Any) {
        isPaste = true
        
        restoreDayViewValues()
        
        nextDay = false
        previousDay = false
        isDaysBtnSelect = false
        
        firstTableViewDataArray = mCopyFirstTableViewDataArray
        secondTableViewDataArray = mCopySecondTableViewDataArray
        thirdTableViewDataArray = mCopyThirdTableViewDataArray
        
        restoreDayViewValues()
        
        nextDay = false
        previousDay = false
        isDaysBtnSelect = false
        
        docScheduleFirstTableView.reloadData()
        secondTableView.reloadData()
        thirdTableView.reloadData()
        
    }
    
    
    //MARK: Days methods
    @IBAction func daysBtnAction(_ sender: UIButton) {
        setBlackColorToDaysButtonTitle()
        //pasteBtn.isHidden = false
        sender.setTitleColor(UIColor.white, for: .normal)
        updateWeeklyData()
        
        todayDay = sender.tag
        previousNextDaySelection()
        
        if nextDay {
            nextDay = false
        }
        if previousDay {
            previousDay = false
        }
        if isDaysBtnSelect {
            isDaysBtnSelect = false
        }

        restoreDayViewValues()
        
        setTodayLabel(btn: sender)
        
        crearDaysBackground()
        if !isWeekViewSelected {
            sender.layer.cornerRadius = sender.frame.size.width/2
            sender.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
        }
        
        isDaysBtnSelect = true
        docScheduleFirstTableView.reloadData()
        secondTableView.reloadData()
        thirdTableView.reloadData()
    }
    
    func setTodayLabel(btn:UIButton) {
        let weekdays = [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Satudrday",
            "Sunday"
        ]
        todayDaysLabel.text = weekdays[btn.tag]
    }
    
    func setBlackColorToDaysButtonTitle()  {
        mondayBtn.setTitleColor(UIColor.black, for: .normal)
        tuesdayBtn.setTitleColor(UIColor.black, for: .normal)
        wednesdayBtn.setTitleColor(UIColor.black, for: .normal)
        thursdayBtn.setTitleColor(UIColor.black, for: .normal)
        fridaydayBtn.setTitleColor(UIColor.black, for: .normal)
        saturdaydayBtn.setTitleColor(UIColor.black, for: .normal)
        sundaydayBtn.setTitleColor(UIColor.black, for: .normal)
    }
    
    func crearDaysBackground() {
        mondayBtn.backgroundColor = UIColor.clear
        tuesdayBtn.backgroundColor = UIColor.clear
        wednesdayBtn.backgroundColor = UIColor.clear
        thursdayBtn.backgroundColor = UIColor.clear
        fridaydayBtn.backgroundColor = UIColor.clear
        saturdaydayBtn.backgroundColor = UIColor.clear
        sundaydayBtn.backgroundColor = UIColor.clear
    }
    
    @IBAction func nextDayBtnAction(_ sender: UIButton) {
        if nextDay{
            nextDay = false
        }
        if previousDay{
            previousDay = false
        }
        if isDaysBtnSelect{
            isDaysBtnSelect = false
        }
        if !isWeekViewSelected {
            if isCopy {
                isPaste = true
            }
        }
        
        restoreDayViewValues()
        
        nextDay = true
        docScheduleFirstTableView.reloadData()
        secondTableView.reloadData()
        thirdTableView.reloadData()
        
        crearDaysBackground()
        updateWeeklyData()
        todayDay = todayDay + 1
        previousNextDaySelection()
    }
    
    
    
    @IBAction func previousDayBtnAction(_ sender: UIButton) {
        
        if nextDay{
            nextDay = false
        }

        if previousDay{
            previousDay = false
        }

        if isDaysBtnSelect{
            isDaysBtnSelect = false
        }

        if !isWeekViewSelected {
            if isCopy {
                isPaste = true
            }
        }
        
        restoreDayViewValues()

        previousDay = true

        crearDaysBackground()
        updateWeeklyData()
        todayDay = todayDay - 1
        previousNextDaySelection()
        
    }
    
    
    func previousNextDaySelection() {
        setBlackColorToDaysButtonTitle()
        if todayDay == 0 {
            mondayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
            mondayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            todayDay = 0
            mondayBtn.setTitleColor(UIColor.white, for: .normal)
            todayDaysLabel.text = "Monday"
            if let firstTableDataSet = mondaySelectedData[Key_First_Table], let secondTableDataSet = mondaySelectedData[Key_Second_Table], let thirdTableDataSet = mondaySelectedData[Key_Third_Table] {
                firstTableViewDataArray = firstTableDataSet
                secondTableViewDataArray = secondTableDataSet
                thirdTableViewDataArray = thirdTableDataSet
            }
            
        }
        
        if todayDay == 1 {
            tuesdayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
            tuesdayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            tuesdayBtn.setTitleColor(UIColor.white, for: .normal)
            todayDaysLabel.text = "Tuesday"
            if let firstTableDataSet = tuesdaySelectedData[Key_First_Table], let secondTableDataSet = tuesdaySelectedData[Key_Second_Table], let thirdTableDataSet = tuesdaySelectedData[Key_Third_Table] {
                firstTableViewDataArray = firstTableDataSet
                secondTableViewDataArray = secondTableDataSet
                thirdTableViewDataArray = thirdTableDataSet
            }
        }
        
        if todayDay == 2 {
            wednesdayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
            wednesdayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            wednesdayBtn.setTitleColor(UIColor.white, for: .normal)
            todayDaysLabel.text = "Wednesday"
            if let firstTableDataSet = wednesdaySelectedDate[Key_First_Table], let secondTableDataSet = wednesdaySelectedDate[Key_Second_Table], let thirdTableDataSet = wednesdaySelectedDate[Key_Third_Table] {
                firstTableViewDataArray = firstTableDataSet
                secondTableViewDataArray = secondTableDataSet
                thirdTableViewDataArray = thirdTableDataSet
            }
        }
        
        if todayDay == 3 {
            thursdayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
            thursdayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            thursdayBtn.setTitleColor(UIColor.white, for: .normal)
            todayDaysLabel.text = "Thursday"
            if let firstTableDataSet = thursdaySelectedData[Key_First_Table], let secondTableDataSet = thursdaySelectedData[Key_Second_Table], let thirdTableDataSet = thursdaySelectedData[Key_Third_Table] {
                firstTableViewDataArray = firstTableDataSet
                secondTableViewDataArray = secondTableDataSet
                thirdTableViewDataArray = thirdTableDataSet
            }
        }
        
        if todayDay == 4 {
            fridaydayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
            fridaydayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            fridaydayBtn.setTitleColor(UIColor.white, for: .normal)
            todayDaysLabel.text = "Friday"
            if let firstTableDataSet = fridaySelectedData[Key_First_Table], let secondTableDataSet = fridaySelectedData[Key_Second_Table], let thirdTableDataSet = fridaySelectedData[Key_Third_Table] {
                firstTableViewDataArray = firstTableDataSet
                secondTableViewDataArray = secondTableDataSet
                thirdTableViewDataArray = thirdTableDataSet
            }
        }
        
        if todayDay == 5 {
            saturdaydayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
            saturdaydayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            saturdaydayBtn.setTitleColor(UIColor.white, for: .normal)
            todayDaysLabel.text = "Saturday"
            if let firstTableDataSet = saturdaySelectedData[Key_First_Table], let secondTableDataSet = saturdaySelectedData[Key_Second_Table], let thirdTableDataSet = saturdaySelectedData[Key_Third_Table] {
                firstTableViewDataArray = firstTableDataSet
                secondTableViewDataArray = secondTableDataSet
                thirdTableViewDataArray = thirdTableDataSet
            }
        }
        
        if todayDay == 6 {
            sundaydayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
            sundaydayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            todayDay = 6
            sundaydayBtn.setTitleColor(UIColor.white, for: .normal)
            todayDaysLabel.text = "Sunday"
            if let firstTableDataSet = sundaySelectedData[Key_First_Table], let secondTableDataSet = sundaySelectedData[Key_Second_Table], let thirdTableDataSet = sundaySelectedData[Key_Third_Table] {
                firstTableViewDataArray = firstTableDataSet
                secondTableViewDataArray = secondTableDataSet
                thirdTableViewDataArray = thirdTableDataSet
            }
        }
        
        if todayDay > 6 {
            todayDay = 0
            mondayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
            mondayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            mondayBtn.setTitleColor(UIColor.white, for: .normal)
            todayDaysLabel.text = "Monday"
            if let firstTableDataSet = mondaySelectedData[Key_First_Table], let secondTableDataSet = mondaySelectedData[Key_Second_Table], let thirdTableDataSet = mondaySelectedData[Key_Third_Table] {
                firstTableViewDataArray = firstTableDataSet
                secondTableViewDataArray = secondTableDataSet
                thirdTableViewDataArray = thirdTableDataSet
            }
        }
        
        if todayDay < 0 {
            todayDay = 6
            sundaydayBtn.layer.cornerRadius = mondayBtn.frame.size.width/2
            sundaydayBtn.backgroundColor = UIColor.init(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
            sundaydayBtn.setTitleColor(UIColor.white, for: .normal)
            todayDaysLabel.text = "Sunday"
            if let firstTableDataSet = sundaySelectedData[Key_First_Table], let secondTableDataSet = sundaySelectedData[Key_Second_Table], let thirdTableDataSet = sundaySelectedData[Key_Third_Table] {
                firstTableViewDataArray = firstTableDataSet
                secondTableViewDataArray = secondTableDataSet
                thirdTableViewDataArray = thirdTableDataSet
            }
        }
        docScheduleFirstTableView.reloadData()
        secondTableView.reloadData()
        thirdTableView.reloadData()
    }
    
    func updateWeeklyData() {
        
        switch todayDay {
        case 0:
            mondaySelectedData = [Key_First_Table: firstTableViewDataArray, Key_Second_Table: secondTableViewDataArray, Key_Third_Table: thirdTableViewDataArray]
            
        case 1:
            tuesdaySelectedData = [Key_First_Table: firstTableViewDataArray, Key_Second_Table: secondTableViewDataArray, Key_Third_Table: thirdTableViewDataArray]
            
        case 2:
            wednesdaySelectedDate = [Key_First_Table: firstTableViewDataArray, Key_Second_Table: secondTableViewDataArray, Key_Third_Table: thirdTableViewDataArray]
            
        case 3:
            thursdaySelectedData = [Key_First_Table: firstTableViewDataArray, Key_Second_Table: secondTableViewDataArray, Key_Third_Table: thirdTableViewDataArray]
            
        case 4:
            fridaySelectedData = [Key_First_Table: firstTableViewDataArray, Key_Second_Table: secondTableViewDataArray, Key_Third_Table: thirdTableViewDataArray]
            
        case 5:
            saturdaySelectedData = [Key_First_Table: firstTableViewDataArray, Key_Second_Table: secondTableViewDataArray, Key_Third_Table: thirdTableViewDataArray]
            
        case 6:
            sundaySelectedData = [Key_First_Table: firstTableViewDataArray, Key_Second_Table: secondTableViewDataArray, Key_Third_Table: thirdTableViewDataArray]
            
        default:
            break
        }
        
        firstTableViewDataArray = NSMutableSet()
        secondTableViewDataArray = NSMutableSet()
        thirdTableViewDataArray = NSMutableSet()
        
    }
    
    @IBAction func dayViewBtnAction(_ sender: Any) {

        restoreDayViewValues()
        
        dayViewButton.setTitleColor(UIColor.white, for: .normal)
        weekViewButton.setTitleColor(UIColor.lightText, for: .normal)
        previousNextDaySelection()
        copyBtn.isHidden = false
        pasteBtn.isHidden = false
        isWeekViewSelected = false
        weekTableView.isHidden = true
        todayDaysLabel.isHidden = false
        docScheduleFirstTableView.isHidden = false
        secondTableView.isHidden = false
        thirdTableView.isHidden = false
        previousBtn.isHidden = false
        nextBtn.isHidden = false
        daysContainerView.isHidden = false
        daysContainerWeekView.isHidden  = true
    }
    
    @IBAction func weekViewBtnAction(_ sender: Any) {
        if isdataChangesMade {
            let alert = UIAlertController(title: nil, message: "You have made changes to your online schedule.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "SAVE", style: .default, handler: { (action) in
                self.saveMyScheduleData()
                self.showWeekView()
            }))
            alert.addAction(UIAlertAction(title: "DISCARD", style: .default, handler: { (action) in
                self.showWeekView()
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.showWeekView()
        }
    }
    
    func showWeekView() {
        isdataChangesMade = false
        docScheduleFirstTableView.reloadData()
        secondTableView.reloadData()
        thirdTableView.reloadData()
        
        mondayArray = getSelectedDataArray(selectedData: mondaySelectedData)
        tuesdayArray = getSelectedDataArray(selectedData: tuesdaySelectedData)
        wednesdayArray = getSelectedDataArray(selectedData: wednesdaySelectedDate)
        thursdayArray = getSelectedDataArray(selectedData: thursdaySelectedData)
        fridayArray = getSelectedDataArray(selectedData: fridaySelectedData)
        saturdayArray = getSelectedDataArray(selectedData: saturdaySelectedData)
        sundayArray = getSelectedDataArray(selectedData: sundaySelectedData)
        
        dayViewButton.setTitleColor(UIColor.lightText, for: .normal)
        weekViewButton.setTitleColor(UIColor.white, for: .normal)
        copyBtn.isHidden = true
        pasteBtn.isHidden = true
        isWeekViewSelected = true
        weekTableView.isHidden = false
        todayDaysLabel.isHidden = true
        docScheduleFirstTableView.isHidden = true
        secondTableView.isHidden = true
        thirdTableView.isHidden = true
        previousBtn.isHidden = true
        nextBtn.isHidden = true
        daysContainerView.isHidden = true
        daysContainerWeekView.isHidden  = false
        weekTableView.reloadData()
    }
    
    
    @IBAction func SchedularOnOffSwitch(_ sender: UISwitch) {
        if isdataChangesMade {
            let alert = UIAlertController(title: nil, message: "You have made changes to your online schedule.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "SAVE", style: .default, handler: { (action) in
                self.saveMyScheduleData()
                self.handleSchedulerOnOff(sender: sender)
            }))
            alert.addAction(UIAlertAction(title: "DISCARD", style: .default, handler: { (action) in
                self.handleSchedulerOnOff(sender: sender)
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler:  { (action) in
                if sender.isOn {
                    sender.setOn(false, animated: true)
                } else {
                    sender.setOn(true, animated: true)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            handleSchedulerOnOff(sender: sender)
        }
    }
    
    func handleSchedulerOnOff(sender: UISwitch) {
        if (sender.isOn == true) {
            
            MaskSchedularred.isHidden = true
            UserDefaults.standard.set(false, forKey: Constants.KEY_SCHEDULE_ON_OFF)
            self.SchSwitchOnOffView.layer.cornerRadius = 16
            self.SchSwitchOnOffView.onTintColor = UIColor(red:0.23, green:0.64, blue:0.34, alpha:1) //.green
            SchSwitchOnOffView.backgroundColor = UIColor(red:0.23, green:0.64, blue:0.34, alpha:1) //.green
            self.updateSchedularBackendforONOFFSwitch(switchval: false)
        } else {
            self.SchSwitchOnOffView.layer.cornerRadius = 16
            self.SchSwitchOnOffView.onTintColor = UIColor(red:0.88, green:0.18, blue:0.2, alpha:1) //.red
            SchSwitchOnOffView.backgroundColor = UIColor(red:0.88, green:0.18, blue:0.2, alpha:1) //.red
            let schoffonViewController = self.storyboard!.instantiateViewController(withIdentifier: "AutoSchedularOnOffView") as! AutoSchedularOnOffViewController
            self.nav = UINavigationController(rootViewController: schoffonViewController)
            self.present(self.nav!, animated: true, completion: nil)
        }
    }
    
    func saveMyScheduleData() {
        //Handle saving the data to backend
        updateWeeklyData()
        isdataChangesMade = false
        //The way to retreive Data and get it in Array form. Below code to be removed once the backend is Handled
        if let mondaydataSetOfFirstTable = mondaySelectedData[Key_First_Table], let mondayDataSetOfSecondTable = mondaySelectedData[Key_Second_Table], let mondayDataSetOfThirdTable = mondaySelectedData[Key_Third_Table] {
            print(Array(mondaydataSetOfFirstTable),Array(mondayDataSetOfSecondTable),Array(mondayDataSetOfThirdTable))
        }
        
        // lets save data to backend
        mondayArray = getSelectedDataArray(selectedData: mondaySelectedData)
        for day in mondayArray {
            MondayarrayOfBool[day] = true
        }
        print(MondayarrayOfBool)

        tuesdayArray = getSelectedDataArray(selectedData: tuesdaySelectedData)
        for day in tuesdayArray {
            TuesdayarrayOfBool[day] = true
        }
        print(TuesdayarrayOfBool)
        
        wednesdayArray = getSelectedDataArray(selectedData: wednesdaySelectedDate)
        for day in wednesdayArray {
            WednesdayarrayOfBool[day] = true
        }
        print(WednesdayarrayOfBool)
        
        thursdayArray = getSelectedDataArray(selectedData: thursdaySelectedData)
        for day in thursdayArray {
            ThursdayarrayOfBool[day] = true
        }
        print(ThursdayarrayOfBool)
        
        fridayArray = getSelectedDataArray(selectedData: fridaySelectedData)
        for day in fridayArray {
            FridayarrayOfBool[day] = true
        }
        print(FridayarrayOfBool)
        
        saturdayArray = getSelectedDataArray(selectedData: saturdaySelectedData)
        for day in saturdayArray {
            SaturdayarrayOfBool[day] = true
        }
        print(SaturdayarrayOfBool)
        
        sundayArray = getSelectedDataArray(selectedData: sundaySelectedData)
        for day in sundayArray {
            SundayarrayOfBool[day] = true
        }
        print(SundayarrayOfBool)
        
        self.updateSchedularBackend(arrayofboolmonday: MondayarrayOfBool, arrayofbooltuesday: TuesdayarrayOfBool, arrayofboolwednesday: WednesdayarrayOfBool, arrayofboolthursday: ThursdayarrayOfBool, arrayofboolfirday: FridayarrayOfBool, arrayofboolsaturday: SaturdayarrayOfBool, arrayofboolsunday: SundayarrayOfBool)
    }
    
    func navigateToHomeScreen() {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    func getSelectedDataArray(selectedData:[String:NSMutableSet]) -> Array<Int> {
        if let firstSet = selectedData[Key_First_Table], let secondSet = selectedData[Key_Second_Table], let thirdSet = selectedData[Key_Third_Table] {
            
            let firstArray = Array(firstSet) as! [Int]
            
            let SecondArray =  Array(secondSet)
            
            var modifiedSecondArray = [Int]()
            for item in SecondArray {
                let mItem = (item as! Int) + 16
                modifiedSecondArray.append(mItem)
            }
            
            let thirdArray =  Array(thirdSet)
            var modifiedthirdArray = [Int]()
            for item in thirdArray {
                let mItem = (item as! Int) + 32
                modifiedthirdArray.append(mItem)
            }
            
            let finalArray = (firstArray + modifiedSecondArray + modifiedthirdArray).sorted()
            
            return finalArray
        }
        
        return []
    }
    
    func updateSchedularBackend(arrayofboolmonday: Array<Bool>, arrayofbooltuesday: Array<Bool>, arrayofboolwednesday: Array<Bool>, arrayofboolthursday: Array<Bool>, arrayofboolfirday: Array<Bool>, arrayofboolsaturday: Array<Bool>, arrayofboolsunday: Array<Bool>) {
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"
        object.fields["Monday"] = arrayofboolmonday
        object.fields["Tuesday"] = arrayofbooltuesday
        object.fields["Wednesday"] = arrayofboolwednesday
        object.fields["Thursday"] = arrayofboolthursday
        object.fields["Friday"] = arrayofboolfirday
        object.fields["Saturday"] = arrayofboolsaturday
        object.fields["Sunday"] = arrayofboolsunday
        
        object.id = UserDefaults.standard.string(forKey: "customobjIDSchedular")!      //record id

        UserDefaults.standard.set(arrayofboolmonday, forKey: Constants.mondaySchedularData)
        UserDefaults.standard.set(arrayofbooltuesday, forKey: Constants.tuesdaySchedularData)
        UserDefaults.standard.set(arrayofboolwednesday, forKey: Constants.wednesdaySchedularData)
        UserDefaults.standard.set(arrayofboolthursday, forKey: Constants.thursdaySchedularData)
        UserDefaults.standard.set(arrayofboolfirday, forKey: Constants.fridaySchedularData)
        UserDefaults.standard.set(arrayofboolsaturday, forKey: Constants.saturdaySchedularData)
        UserDefaults.standard.set(arrayofboolsunday, forKey: Constants.sundaySchedularData)

        QBRequest.update(object, successBlock: { (response, contributors) in
            print("DocScheduleViewController:CustomObject: Successfully Updated for Schedular!")
            DoctorScheduleHelper.updateScheduleUserDefaultsValue(object: contributors)
        }) { (response) in
            print("DocScheduleViewController:CustomObject Schedular: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func updateSchedularBackendforONOFFSwitch(switchval: Bool) {
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"
        object.fields["SchedularStatus"] = !switchval
        object.id = UserDefaults.standard.string(forKey: "customobjIDSchedular")!      //record id
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("DocScheduleViewController:updateSchedularBackendforONOFFSwitch() successfull")
        }) { (response) in
            //Handle Error
            print("DocScheduleViewController:updateSchedularBackendforONOFFSwitch Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func populateRetrivedData(array: Array<Bool>) -> [String: NSMutableSet] {
        var selectedDataArray = [Int]()
        let firstSet = NSMutableSet()
        let secondSet = NSMutableSet()
        let thirdSet = NSMutableSet()
        
        for (index,item) in array.enumerated() {
            if item {
                selectedDataArray.append(index)
            }
        }
        
        for data in selectedDataArray {
            switch data {
            case 0...15:
                firstSet.add(data)
            case 16...31:
                secondSet.add(data - 16)
            default:
                thirdSet.add(data - 32)
            }
        }
        
        return [Key_First_Table: firstSet, Key_Second_Table: secondSet, Key_Third_Table: thirdSet]
        
    }

    func showPopUpToStartConsulting() {
        var style = ToastStyle()
        style.messageColor = .white
        ToastManager.shared.style = style
        

        if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) {
            self.view.makeToast("To start consulting, switch your scheduler on.", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
        } else if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_OFFLINE) {
            let alert = UIAlertController(title: "To start consulting, turn your availability for the current time slot.", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            //self.view.makeToast("To start consulting, turn your availability for the current time slot.", duration: 4.0, position: .bottom, title: nil, image: nil, style: style, completion: nil)
        }
    }
    
}
