import UIKit

class PrescriptionCell8: UITableViewCell {

    @IBOutlet var signatureimg: UIImageView!
    @IBOutlet var btnSignature: UIButton!
    @IBOutlet var doctornamelbl: UILabel!
    @IBOutlet var doctordegreelbl: UILabel!
    @IBOutlet var mcinumlbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureUI(data: QBCOCustomObject) {
        let signatureurl = data.fields.value(forKey: "docsignatureUID") as! String
        signatureimg.sd_setImage(with: URL(string: signatureurl), placeholderImage: UIImage(named: "Signature"), options: .cacheMemoryOnly)
        doctornamelbl.text = "Dr. " + "\(data.fields.value(forKey: "docfullname") as! String)"
        doctordegreelbl.text = data.fields.value(forKey: "docdegrees") as? String
        mcinumlbl.text = "Reg No. " + "\(data.fields.value(forKey: "docmci") as! String)"
    }

}
