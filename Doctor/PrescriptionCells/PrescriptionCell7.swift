import UIKit

class PrescriptionCell7: UITableViewCell {

    @IBOutlet var followuplbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureUI(data: QBCOCustomObject) {
        followuplbl.text = data.fields.value(forKey: "followup") as? String
    }

}
