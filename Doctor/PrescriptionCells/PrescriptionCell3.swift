import UIKit

class PrescriptionCell3: UITableViewCell {

    @IBOutlet var provisionaldiagnosislbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureUI(data: QBCOCustomObject) {
        provisionaldiagnosislbl.text = data.fields.value(forKey: "prodiag") as? String
    }

}
