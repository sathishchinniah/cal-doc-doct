import UIKit

class PrescriptionCell9: UITableViewCell {

    @IBOutlet weak var consultationChargesLabel: UILabel!
    @IBOutlet weak var callDocChargesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureUI(data: QBCOCustomObject) {
        consultationChargesLabel.text = "\((data.fields.value(forKey: "doctoramount") as? Int) ?? 0)"
        callDocChargesLabel.text = "\((data.fields.value(forKey: "cdcharges") as? Int) ?? 0)"
    }

}
