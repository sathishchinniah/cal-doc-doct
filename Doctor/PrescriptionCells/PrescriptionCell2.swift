import UIKit

class PrescriptionCell2: UITableViewCell {

    @IBOutlet var skipmedicaladvicelbl: UILabel!
    @IBOutlet var symptomdlbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureUI(data: QBCOCustomObject) {
        if data.fields.value(forKey: "skipadvice") as! Bool {
            skipmedicaladvicelbl.text = "Medical Advice Skipped."
            symptomdlbl.text = data.fields.value(forKey: "reason") as? String
        } else {
            symptomdlbl.text = data.fields.value(forKey: "symptoms") as? String
        }
    }

}
