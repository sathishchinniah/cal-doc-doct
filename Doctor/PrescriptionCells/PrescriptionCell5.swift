import UIKit
import SnapKit

class PrescriptionCell5: UITableViewCell {

    @IBOutlet weak var dxItemsView: UIView!
    @IBOutlet weak var dxItemsViewHeightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureDxView(dxMedicines: [DxMedicalAdviceData]) {
        dxItemsView.subviews.forEach { $0.removeFromSuperview() }
        dxItemsViewHeightConstraint.constant = CGFloat(145 * dxMedicines.count)

        for (i, medicine) in dxMedicines.enumerated() {

            let dxView = DXConfirmMedicineView()
            dxItemsView.addSubview(dxView)

            dxView.snp.makeConstraints { (make) in
                make.height.equalTo(145)
                make.leading.equalToSuperview()
                make.trailing.equalToSuperview()
                make.top.equalTo(i * 145)
            }

            dxView.configureView(dxData: medicine)
        }
    }

}
