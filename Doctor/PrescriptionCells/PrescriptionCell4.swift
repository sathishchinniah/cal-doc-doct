import UIKit

class PrescriptionCell4: UITableViewCell {

    @IBOutlet weak var rxItemsView: UIView!
    @IBOutlet weak var rxItemsViewHeightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureRxView(rxMedicines: [RxMedicalAdviceData]) {
            rxItemsView.subviews.forEach { $0.removeFromSuperview() }
            rxItemsViewHeightConstraint.constant = CGFloat(180 * rxMedicines.count)

            for (i, medicine) in rxMedicines.enumerated() {

                let rxView = RXConfirmMedicineView()
                rxItemsView.addSubview(rxView)

                rxView.snp.makeConstraints { (make) in
                    make.height.equalTo(160)
                    make.leading.equalToSuperview()
                    make.trailing.equalToSuperview()
                    make.top.equalTo(i * 160)
                }

                rxView.configureView(rxData: medicine)
            }
    }

}
