import UIKit

class PrescriptionCell1: UITableViewCell {

    @IBOutlet var patientnamelbl: UILabel!
    @IBOutlet var patientgenderlbl: UILabel!
    @IBOutlet var patientagelbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureUI(data: QBCOCustomObject) {
        patientnamelbl.text =  data.fields.value(forKey: "patfullname") as? String
        patientgenderlbl.text = data.fields.value(forKey: "patgender") as? String
        patientagelbl.text = data.fields.value(forKey: "patage") as? String
    }

}
