import UIKit
import Foundation
import Contacts
import ContactsUI
import DropDown

class AddDoctoCOTViewController: UIViewController, UITextFieldDelegate, CNContactPickerDelegate, UINavigationControllerDelegate {

    @IBOutlet var firstnametxt: UITextField!
    @IBOutlet var lastnametxt: UITextField!
    @IBOutlet var mobiletxt: UITextField!
    @IBOutlet var countrytxt: UITextField!
    @IBOutlet var addaspecificdocview: UIView!
    @IBOutlet var countrydropview: UIView!
    @IBOutlet var addaspecificdocButtonview: UIButton!
    @IBOutlet var searchforspecificdocview: UIView!
    @IBOutlet var searchforspecificdocButtonview: UIButton!
    @IBOutlet var NSConstraintsearchdocbuttonY: NSLayoutConstraint!
    
    var nav: UINavigationController?
    var specialitystringpassed = ""
    var theposition: UInt8 = 0
    var teamswitch: Bool = false
    let CountryDropDown = DropDown()
    var doctorname = ""
    var inviteeprofile = ""
    var inviteespeciality = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        firstnametxt.delegate = self
        lastnametxt.delegate = self
        mobiletxt.delegate = self
        countrytxt.delegate = self
        firstnametxt.tintColor = UIColor.blue
        lastnametxt.tintColor = UIColor.blue
        mobiletxt.tintColor = UIColor.blue
        
        let helpButton = UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self,  action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpButton;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if (textField == countrytxt) {

            hideKeyboardWhenTappedAround()
            textField.resignFirstResponder()
            textField.endEditing(true)
            
            CountryDropDown.anchorView = countrydropview
            CountryDropDown.dataSource = ["+91 India", "+44 UK", "+61 Singapore", "+1 United States"]
            CountryDropDown.show()
            CountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                var ccode = ""

                var components = item.components(separatedBy: " ")
                if(components.count > 0) {
                    ccode = components.removeFirst()
                }
                self.countrytxt.text = ccode
                GlobalDocData.countrycode = item
            }
        }
    }
    
    /**
     * Called when the user click on the view's (outside the UITextField).
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mobiletxt {
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            
            if length == 0 || length == 10 {
                
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                let index = 0 as Int
                let formattedString = NSMutableString()
                
                let remainder = decimalString.substring(from: index)
                formattedString.append(remainder)
                textField.text = formattedString as String
                
                mobiletxt.resignFirstResponder()
                return (newLength >= 10) ? false : true
            }

            let index = 0 as Int
            let formattedString = NSMutableString()
            
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
            
        } else {
            return true
        }
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    @IBAction func AddressBookButton(_ sender: UIButton) {
        let entityType = CNEntityType.contacts
        let authStatus = CNContactStore.authorizationStatus(for: entityType)
        
        if authStatus == CNAuthorizationStatus.notDetermined {
            
            let contactStore = CNContactStore.init()
            contactStore.requestAccess(for: entityType, completionHandler: { (success, nil) in
                if success {
                    self.openContacts()
                } else {
                    print("Addressboon Not Authorised")
                }
            })
        } else if authStatus == CNAuthorizationStatus.authorized {
            self.openContacts()
        }
        
    }
    
    func openContacts() {
        let contactPicker = CNContactPickerViewController.init()
        contactPicker.delegate = self
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true) {
            
        }
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        var phoneNo = "Not Available"
        let phoneString = ((((contact.phoneNumbers[0] as  AnyObject).value(forKey: "labelValuePair") as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "stringValue"))
        phoneNo = (phoneString as? String) ?? ""
        var formattedPhoneNo = phoneNo.replacingOccurrences(of: " ", with: "")
        formattedPhoneNo = formattedPhoneNo.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")//formattedPhoneNo.replacingOccurrences(of: "-", with: "")
        self.mobiletxt.text = formattedPhoneNo
    }

    @IBAction func InviteSMSButton(_ sender: UIButton) {
        
        if (firstnametxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter First Name!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in

            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        
        if (lastnametxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Last Name!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in

            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        
        if (mobiletxt.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Valid Mobile Number!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                return
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        
        self.checkifUserAlreadyExist()

    }
    
    // This is to check if user is already registered in CalDoc, if registered then check RelSpeciality Table else send SMS
    func checkifUserAlreadyExist() {
        var totalmobilenumber = ""
        totalmobilenumber = self.countrytxt.text! + mobiletxt.text!
        let stringWithoutpluswith_d: String = totalmobilenumber.replacingOccurrences(of: "+", with: "d_")
        LoadingIndicatorView.show("")

        QBRequest.users(withLogins: [stringWithoutpluswith_d], page: QBGeneralResponsePage(currentPage: 1, perPage: 10), successBlock: {(_ response: QBResponse, _ page: QBGeneralResponsePage, _ users: [Any]) -> Void in
            if users.count > 0 {
                let selectedUser = users[0] as? QBUUser
                self.doctorname = (selectedUser?.fullName)!
                self.fetchInviteeDoctordetails(inviteedocuserid: (selectedUser?.id)!)

            } else {
                LoadingIndicatorView.show("Send SMS Invite....")
                self.CheckInviteTableforEntry(invitoruid: (QBSession.current.currentUser?.id)!, inviteemb: stringWithoutpluswith_d)
            }
        }, errorBlock: {(_ response: QBResponse) -> Void in
            LoadingIndicatorView.hide()
        })
    }
    
    func CheckInviteTableforEntry(invitoruid: UInt, inviteemb: String) {
        // Before making entry first check if there is already entry in Invite Table
        let getRequest = NSMutableDictionary()
        getRequest["invitoruserid"] = invitoruid
        getRequest["inviteemobilenum"] = inviteemb
        QBRequest.objects(withClassName: "InviteTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if (contributors?.count)! > 0 {
                // There is already entry
                let alert = UIAlertController(title: "Alert", message: "Doctor has been invited Already!", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    LoadingIndicatorView.hide()
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: true, completion: nil)
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                self.MakeInviteTableentrybeforeSMSInvite(invitoruid: (QBSession.current.currentUser?.id)!, inviteemb: inviteemb)
            }
        }) { (errorresponse) in
            print("AddDoctorsCOTViewConteroller:fetchInviteeDoctordetails(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func MakeInviteTableentrybeforeSMSInvite(invitoruid: UInt, inviteemb: String) {
        let object = QBCOCustomObject()
        object.className = "InviteTable"
        object.fields["invitoruserid"] = invitoruid
        object.fields["inviteemobilenum"] = inviteemb
        object.fields["invitorfullname"] = GlobalDocData.gdocname
        object.fields["notificationread"] = "false"
        object.fields["calldoccode"] = GlobalVariables.gcalldoccode
        object.fields["profileUID"] =  GlobalDocData.gprofileURL
        object.fields["invitortype"] = "doctor"
        object.fields["invitorspeciality"] = GlobalVariables.gdocspeciality
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            LoadingIndicatorView.hide()
            self.SendSMSInvitetoDoctor()
        }) { (response) in
            LoadingIndicatorView.hide()
        }
    }
    
    func SendSMSInvitetoDoctor() {
        self.SendSMSusingMSG91API()
    }
    
    func fetchInviteeDoctordetails(inviteedocuserid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = inviteedocuserid
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            self.inviteespeciality = (contributors![0].fields?.value(forKey: "specialisation") as? String)!
            self.inviteeprofile = (contributors![0].fields?.value(forKey: "profileUID") as? String)!
            
            // here we are checking from where we are coming , I mean if we are coming from COT screen or from My team screen
            if self.specialitystringpassed == "MYTeam" {
                //first check if invitor and invitee speciality is matching
                if (self.inviteespeciality == GlobalDocData.gdocspeciality) {
                    self.CheckRelDocTeamTable(invitoruid: (QBSession.current.currentUser?.id)!, inviteeuid: inviteedocuserid) // check RelDocTeam table if there is already invited team member exist ??
                } else {
                    let alert = UIAlertController(title: "Alert", message: "Invitee Doctor Speciality not matching with yours hence you can not add him to your team!", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                        LoadingIndicatorView.hide()
                        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                        self.nav = UINavigationController(rootViewController: homeViewController)
                        self.present(self.nav!, animated: true, completion: nil)
                    })
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                }
            } else { // check for COT
                //first check if invitor and invitee speciality is matching then it can not invite same speciality
                if (self.inviteespeciality == GlobalDocData.gdocspeciality) {
                    let alert = UIAlertController(title: "Alert", message: "Invitee Doctor Speciality is same as yours hence can not add to Circle of trust", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                        // go back to Home
                        LoadingIndicatorView.hide()
                        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                        self.nav = UINavigationController(rootViewController: homeViewController)
                        self.present(self.nav!, animated: true, completion: nil)
                    })
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    //check for COT -  if user has any entry in RelSpecality table ? it means that user already has been invited
                    self.CheckRelSpecialityTable(invitoruid: (QBSession.current.currentUser?.id)!, inviteeuid: inviteedocuserid)
                }
            }
            
        }) { (errorresponse) in
            print("AddDoctorsCOTViewConteroller:fetchInviteeDoctordetails(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    // check if there is entry of doctor and doctor for COT relation in RelSpeciality table
    func CheckRelSpecialityTable(invitoruid: UInt, inviteeuid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["inviteedocuserid"] =  inviteeuid
        getRequest["invitordocuserid"] = invitoruid
        
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                print("(AddDoctorsCOTViewConteroller::CheckRelSpecialityTable():Match Entry Found  !")
                let alert = UIAlertController(title: "Alert", message: "User Already Connected on COT!", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    // go back to Home
                    LoadingIndicatorView.hide()
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: true, completion: nil)
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                self.checkInviteTableforCOTInvite(invitoruid: invitoruid, inviteeuid: inviteeuid, typeofinvite: "COT" )
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsCOTViewConteroller::CheckRelSpecialityTable(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func checkInviteTableforCOTInvite(invitoruid: UInt, inviteeuid: UInt, typeofinvite: String ) {
        let getRequest = NSMutableDictionary()
        getRequest["inviteeuserid"] =  inviteeuid
        getRequest["invitoruserid"] = invitoruid
        getRequest["typeofinvite"] = typeofinvite
        QBRequest.objects(withClassName: "InviteTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                let alert = UIAlertController(title: "Alert", message: "Doctor Already Invited to Circle Of Trust!", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    // go back to Home
                    LoadingIndicatorView.hide()
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: true, completion: nil)
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                LoadingIndicatorView.hide()
                let alert = UIAlertController(title: "", message: "Would you like to Invite Dr. \(self.doctorname)  \(self.inviteespeciality) to your Circle of Trust ??", preferredStyle: .alert)
                let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
                imageView.layer.cornerRadius = imageView.frame.size.width / 2;
                imageView.clipsToBounds = true;
                imageView.layer.borderWidth = 0.2
                imageView.layer.borderColor = UIColor.lightGray.cgColor
                imageView.sd_setImage(with: URL(string: self.inviteeprofile), placeholderImage: UIImage(named: "profile"))
                alert.view.addSubview(imageView)
                let ok = UIAlertAction(title: "YES", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    LoadingIndicatorView.hide()
                    self.AddNewEntrytoInviteTable(invitoruid: invitoruid, inviteeuid: inviteeuid, typeofinvite: "COT" )
                })
                let no = UIAlertAction(title: "NO", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    LoadingIndicatorView.hide()
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: true, completion: nil)
                })
                alert.addAction(ok)
                alert.addAction(no)
                self.present(alert, animated: true, completion: nil)
                
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsCOTViewConteroller::checkInviteTableforCOTInvite(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func AddNewEntrytoInviteTable(invitoruid: UInt, inviteeuid: UInt, typeofinvite: String) {
        let object = QBCOCustomObject()
        object.className = "InviteTable"
        object.fields["invitoruserid"] = invitoruid
        object.fields["inviteeuserid"] = inviteeuid
        object.fields["invitorfullname"] = GlobalDocData.gdocname
        object.fields["notificationread"] = "false"
        object.fields["typeofinvite"] = typeofinvite
        object.fields["calldoccode"] = GlobalVariables.gcalldoccode
        object.fields["profileUID"] =  GlobalDocData.gprofileURL
        object.fields["invitortype"] = "doctor"
        object.fields["invitorspeciality"] = GlobalVariables.gdocspeciality
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("AddDoctorCOTViewController:AddNewEntrytoInviteTable: Successfully created New Entry !")
            LoadingIndicatorView.hide()
            let alert = UIAlertController(title: "Alert", message: "Doctor has been invited Successfully!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                // go back to Home
                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                self.nav = UINavigationController(rootViewController: homeViewController)
                self.present(self.nav!, animated: true, completion: nil)
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            
        }) { (response) in
            LoadingIndicatorView.hide()
        }
        
    }

    /////////////////////////////// fromhere logic for My Team /////////////////////////////////////////////////

    func CheckRelDocTeamTable(invitoruid: UInt, inviteeuid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["inviteedocuserid"] =  inviteeuid
        getRequest["invitordocuserid"] = invitoruid

        QBRequest.objects(withClassName: "RelDocTeam", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                print("(AddDoctorsCOTViewConteroller::CheckRelDocTeamTable():Match Entry Found  !")
                let alert = UIAlertController(title: "Alert", message: "Doctor Already Connected at your Team!", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    // go back to Home
                    LoadingIndicatorView.hide()
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: true, completion: nil)
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                print("(AddDoctorsCOTViewConteroller::CheckRelDocTeamTable(): Relation Match NOT Found  !")
                self.checkInviteTableforTeamInvite(invitoruid: invitoruid, inviteeuid: inviteeuid, typeofinvite: "TEAM" )
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsCOTViewConteroller::CheckRelDocTeamTable(): Response error: \(String(describing: errorresponse.error?.description))")
        }

    }
    
    func checkInviteTableforTeamInvite(invitoruid: UInt, inviteeuid: UInt, typeofinvite: String) {
        let getRequest = NSMutableDictionary()
        getRequest["inviteeuserid"] =  inviteeuid
        getRequest["invitoruserid"] = invitoruid
        getRequest["typeofinvite"] = typeofinvite
        QBRequest.objects(withClassName: "InviteTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                print("(AddDoctorsCOTViewConteroller::checkInviteTableforTeamInvite():Match Entry Found  !")
                let alert = UIAlertController(title: "Alert", message: "Doctor was already Invited to your Team!", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    // go back to Home
                    LoadingIndicatorView.hide()
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: true, completion: nil)
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "", message: "Would you like to Invite Dr. \(self.doctorname)  \(self.inviteespeciality) to your Team ?", preferredStyle: .alert)
                let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
                imageView.layer.cornerRadius = imageView.frame.size.width / 2;
                imageView.clipsToBounds = true;
                imageView.layer.borderWidth = 0.2
                imageView.layer.borderColor = UIColor.lightGray.cgColor
                imageView.sd_setImage(with: URL(string: self.inviteeprofile), placeholderImage: UIImage(named: "profile"))
                alert.view.addSubview(imageView)
                let ok = UIAlertAction(title: "YES", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    self.AddNewEntrytoInviteTable(invitoruid: invitoruid, inviteeuid: inviteeuid, typeofinvite: "TEAM" )
                })
                let no = UIAlertAction(title: "NO", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    LoadingIndicatorView.hide()
                    let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: true, completion: nil)
                })
                alert.addAction(ok)
                alert.addAction(no)
                self.present(alert, animated: true, completion: nil)
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsCOTViewConteroller::checkInviteTableforTeamInvite(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }

    func  SendSMSusingMSG91API() {
        let mobilenumber = mobiletxt.text!
        let docname = GlobalDocData.gdocname
        let docnameparts = docname.components(separatedBy: " ")
        var docfirstname = ""
        var doclastname = ""
        docfirstname = docnameparts[0]
        doclastname = docnameparts[1]
        let calldoccode = GlobalVariables.gcalldoccode
        
        let headers = [
            "authkey": "191254A7N7Dkz1Y5a4db3e0",
            "content-type": "application/json"
        ]
        let parameters = [
            "sender": "CALDOC",
            "route": "4",
            "country": "91",
            "sms": [
                [
                    "message": "Hi \(firstnametxt.text!) \(lastnametxt.text!). you are invited by Dr. \(docfirstname) \(doclastname).  Use CallDoc Code \(calldoccode) to install/Register 'https://www.calldoc.com' ",
                    "to": [mobilenumber]
                ]
            ]
            ] as [String : Any]
        do {
            let postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let request = NSMutableURLRequest(url: NSURL(string: "http://api.msg91.com/api/v2/sendsms")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    print("AddDoctorsTableViewConteroller:SendSMSusingMSG91API():SMS Sent ERROR")
                    print(error!)
                    return
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print("AddDoctorsTableViewConteroller:SendSMSusingMSG91API():SMS Sent Successfully")
                    print(httpResponse!)
                    DispatchQueue.main.async {
                        self.SMSSendshowAlert(mobilenum: mobilenumber)
                    }
                }
            })
            dataTask.resume()
            
        } catch let error as NSError {
            print("AddDoctorsTableViewConteroller:SendSMSusingMSG91API():JSON error Failed to load: \(error.localizedDescription)")
            return
        }
    }
    
    func SMSSendshowAlert(mobilenum: String) {
        // create the alert
        let alert = UIAlertController(title: "Alert", message: "Congrats ! The Invite SMS has been send to \(mobilenum)", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            self.ActiononOK()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func ActiononOK() {
        LoadingIndicatorView.hide()
        self.navigationController?.isNavigationBarHidden = false
        // go back to Home
        let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }
}
