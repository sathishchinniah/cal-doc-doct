


import UIKit

class InvoicesCell: UITableViewCell {

    @IBOutlet weak var backView: UIView! {
        didSet {
            backView.layer.masksToBounds = false
            backView.layer.shadowColor = UIColor.gray.cgColor
            backView.layer.shadowOpacity = 1
            backView.layer.shadowOffset = CGSize(width: -1, height: 1)
            backView.layer.shadowRadius = 3

            backView.layer.shadowPath = UIBezierPath(rect: backView.bounds).cgPath
            backView.layer.shouldRasterize = true
            backView.layer.rasterizationScale = UIScreen.main.scale // : 1
        }
    }
    @IBOutlet weak var invoiceNumberLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var consultationChargesLabel: UILabel!
    @IBOutlet weak var convenienceFeesLabel: UILabel!
    @IBOutlet weak var totalChargesLabel: UILabel!
    @IBOutlet weak var signatureImageView: UIImageView!
    @IBOutlet weak var degreeLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureView(data: QBCOCustomObject) {
        patientNameLabel.text =  data.fields.value(forKey: "patfullname") as? String
        ageLabel.text = getAgeAndSex(data: data)
        consultationChargesLabel.text = "\((data.fields.value(forKey: "doctoramount") as? Int) ?? 0)"
        convenienceFeesLabel.text = "\((data.fields.value(forKey: "cdcharges") as? Int) ?? 0)"
        totalChargesLabel.text = "\(Int64(consultationChargesLabel.text!)! + Int64(convenienceFeesLabel.text!)!)"
        let signatureurl = data.fields.value(forKey: "docsignatureUID") as! String
        signatureImageView.sd_setImage(with: URL(string: signatureurl), placeholderImage: UIImage(named: "Signature"), options: .cacheMemoryOnly)
        degreeLabel.text = data.fields.value(forKey: "docdegrees") as? String
        numberLabel.text = "Reg No. " + "\(data.fields.value(forKey: "docmci") as! String)"
    }

    func getAgeAndSex(data: QBCOCustomObject) -> String {
        if let age = data.fields.value(forKey: "patage") as? String, let gender = data.fields.value(forKey: "patgender") as? String {
            if let ageNum = age.components(separatedBy: " ").first {
                if gender == "MALE" {
                    return "\(ageNum)M"
                } else {
                    return "\(ageNum)F"
                }
            }
        }
        return ""
    }

}
