import UIKit

class AutoSchedularOnOffViewController: UIViewController {
    
    var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.85, green:0.15, blue:0.17, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let helpItem : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self, action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpItem;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        GlobalDocData.maskschedularview = UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF)
        
        self.view.layer.contents = UIImage(named:"bgOffline")?.cgImage
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Help Bar icon pressed")
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
        let schViewController = self.storyboard!.instantiateViewController(withIdentifier: "DoctorScheduleView") as! DoctorScheduleViewController
        self.nav = UINavigationController(rootViewController: schViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    @IBAction func ConfirmButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "Allow my patients to consult with my team when i'm offline.", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO", style: .default, handler: { (action) in
            self.confirmSchedulingOff()
        }))
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
            self.confirmSchedulingOff()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func CancelButton(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: Constants.KEY_SCHEDULE_ON_OFF)
        let schViewController = self.storyboard!.instantiateViewController(withIdentifier: "DoctorScheduleView") as! DoctorScheduleViewController
        self.nav = UINavigationController(rootViewController: schViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    func confirmSchedulingOff() {
        UserDefaults.standard.set(true, forKey: Constants.KEY_SCHEDULE_ON_OFF)
        self.updateSchedularBackendforONOFFSwitchON(switchval: true)
    }
    
    func updateSchedularBackendforONOFFSwitchON(switchval: Bool) {
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"
        
        object.fields["SchedularStatus"] = !switchval
        object.id = UserDefaults.standard.string(forKey: "customobjIDSchedular")!      //record id
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: true, completion: nil)
        }) { (response) in
            print("AutoSchedularOnOffViewController:updateSchedularBackendforONOFFSwitch Response error: \(String(describing: response.error?.description))")
        }
    }
    
}
