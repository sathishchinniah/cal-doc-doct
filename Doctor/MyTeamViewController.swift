import UIKit
import Toast_Swift

class MyTeamViewController: UIViewController {

    @IBOutlet var toplabel: UILabel!
    @IBOutlet var docimg: UIImageView!
    @IBOutlet var docimg1: UIImageView!
    @IBOutlet var docimg2: UIImageView!
    @IBOutlet var docimg3: UIImageView!
    @IBOutlet var docimg4: UIImageView!
    @IBOutlet var docimg5: UIImageView!
    @IBOutlet var docimg6: UIImageView!
    
    @IBOutlet var myteamButtonview: UIButton!
    @IBOutlet var teamdoc1Buttonview: UIButton!
    @IBOutlet var teamdoc2Buttonview: UIButton!
    @IBOutlet var teamdoc3Buttonview: UIButton!
    @IBOutlet var teamdoc4Buttonview: UIButton!
    @IBOutlet var teamdoc5Buttonview: UIButton!
    @IBOutlet var teamdoc6Buttonview: UIButton!
    
    @IBOutlet weak var digonalline1: UIView!
    @IBOutlet weak var digonalline2: UIView!
    @IBOutlet weak var digonalline3: UIView!
    @IBOutlet weak var digonalline4: UIView!
    @IBOutlet weak var line5: UIView!
    @IBOutlet weak var line6: UIView!
    
    @IBOutlet weak var namelabel1: UILabel!
    @IBOutlet weak var namelabel2: UILabel!
    @IBOutlet weak var namelabel3: UILabel!
    @IBOutlet weak var namelabel4: UILabel!
    @IBOutlet weak var namelabel5: UILabel!
    @IBOutlet weak var namelabel6: UILabel!
    
    @IBOutlet weak var offlb: UILabel!
    @IBOutlet weak var onlb: UILabel!
    @IBOutlet weak var SwitchOnOffView: UISwitch!
    @IBOutlet weak var hrlinehightYConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var docimg1contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg1contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg2contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg2contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg3contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg3contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg4contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg4contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg5contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg5contraintsY: NSLayoutConstraint!
    @IBOutlet weak var docimg6contraintsX: NSLayoutConstraint!
    @IBOutlet weak var docimg6contraintsY: NSLayoutConstraint!
    
    @IBOutlet weak var toplbYconstraint: NSLayoutConstraint!
    var onoffswitchbit:  Bool = false
    var nav: UINavigationController?
    
    var teamswitchonoffbit = false
    var DocCounttablerecordID = ""
    var receivedrecordcount = 0
    var finalArray:[QBCOCustomObject] = []
    var position = 0
    var firstname = ""
    var lastname = ""
    var doc1profileaction: Bool = false,  doc2profileaction = false, doc3profileaction = false, doc4profileaction = false, doc5profileaction = false, doc6profileaction = false
    var doc1fullname: String = "", doc2fullname = "", doc3fullname = "", doc4fullname = "", doc5fullname = "", doc6fullname = ""
    var doc1imgURL: String = "", doc2imgURL = "", doc3imgURL = "", doc4imgURL = "", doc5imgURL = "", doc6imgURL = ""
    var doc1userid: Int = 0, doc2userid = 0, doc3userid = 0, doc4userid = 0, doc5userid = 0, doc6userid = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navBarColor.topItem?.title = "Trusted Team"
        
        namelabel1.layer.cornerRadius = 8
        namelabel1.layer.masksToBounds = true
        namelabel2.layer.cornerRadius = 8
        namelabel2.layer.masksToBounds = true
        namelabel3.layer.cornerRadius = 8
        namelabel3.layer.masksToBounds = true
        namelabel4.layer.cornerRadius = 8
        namelabel4.layer.masksToBounds = true
        namelabel5.layer.cornerRadius = 8
        namelabel5.layer.masksToBounds = true
        namelabel6.layer.cornerRadius = 8
        namelabel6.layer.masksToBounds = true
        
        namelabel2.sizeToFit()
        
        UserDefaults.standard.set(GlobalVariables.teamoffon, forKey: "teamswitch")
        UserDefaults.standard.synchronize()

        if (self.teamswitchonoffbit) {
            SwitchOnOffView.setOn(true, animated: true)
            self.SwitchOnOffView.layer.cornerRadius = 16
            SwitchOnOffView.onTintColor = UIColor(red:0.23, green:0.64, blue:0.34, alpha:1) //.green
            SwitchOnOffView.backgroundColor = UIColor(red:0.23, green:0.64, blue:0.34, alpha:1) //.green
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteam_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteam"), for: .normal)
            }
            
        } else {

            SwitchOnOffView.setOn(false, animated: true)
            self.SwitchOnOffView.layer.cornerRadius = 16
            self.SwitchOnOffView.onTintColor = UIColor(red:0.88, green:0.18, blue:0.2, alpha:1) //.red
            SwitchOnOffView.backgroundColor = UIColor(red:0.88, green:0.18, blue:0.2, alpha:1) //.red
            
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteamred_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamred"), for: .normal)
            }
        }
        
        self.updateTheView()
        
        if phone && maxLength == 568 {
            //iphone 5
            docimg.layer.cornerRadius = 32.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docimg6contraintsY.constant = 10
            docimg4contraintsY.constant = 45
            toplbYconstraint.constant = -3
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            docimg.layer.cornerRadius = 37.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docimg6contraintsY.constant = 20
            toplbYconstraint.constant = 5
            docimg1contraintsY.constant = -20
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            docimg.layer.cornerRadius = 40.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docimg6contraintsY.constant = 20
            docimg1contraintsY.constant = -20
        }
        
        if phone && maxLength == 812 {
            //iphone x
            toplabel.font =  UIFont(name: "Rubik-Regular", size: 15)
            docimg.layer.cornerRadius = 38.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            
            docimg5contraintsY.constant = 65
            docimg4contraintsY.constant = 65
            docimg2contraintsX.constant = -20
            docimg4contraintsX.constant = -95
            docimg2contraintsY.constant = 95
            docimg3contraintsX.constant = -90
            docimg5contraintsX.constant = 15
            
            docimg6contraintsY.constant = 43
            docimg1contraintsY.constant = -50
        }
        
        if ipad && maxLength == 1024 {
            
            //iPad
            docimg.layer.cornerRadius = 77.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            hrlinehightYConstraint.constant = 5
            toplbYconstraint.constant = 3
            docimg6contraintsY.constant = -45
            docimg1contraintsY.constant = 35
            docimg5contraintsY.constant = 105
            docimg5contraintsX.constant = 25
            docimg2contraintsY.constant = 95
            docimg2contraintsX.constant = -45
            docimg3contraintsY.constant = 75
            docimg3contraintsX.constant = -170
            docimg4contraintsY.constant = 75
            docimg4contraintsX.constant = -195

        }
        
        LoadProfileImage()
        let isonline = UserDefaults.standard.object(forKey: "doctoronline") as! Bool
        
        if(isonline) {
            docimg.layer.borderColor = UIColor.green.cgColor
        } else {
            docimg.layer.borderColor = UIColor.orange.cgColor
        }

        let helpButton = UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self,  action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpButton;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white

    }
    
    @IBAction func TeamOnOffSwitch(_ sender: UISwitch) {
        
        if (sender.isOn == true) {
            self.SwitchOnOffView.layer.cornerRadius = 16
            SwitchOnOffView.onTintColor = UIColor(red:0.23, green:0.64, blue:0.34, alpha:1) //.green
            SwitchOnOffView.backgroundColor = UIColor(red:0.23, green:0.64, blue:0.34, alpha:1) //.green
            //team is on
            GlobalVariables.teamoffon = true
            UserDefaults.standard.set(true, forKey: "teamswitch")
            UserDefaults.standard.synchronize()
            
            //here we need to update DocCountTable teamStatus using recrod ID we have got from COT
            self.UpdateTeamStatus(teamstatus: true)
            
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteam_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteam"), for: .normal)
            }
        } else {
            self.SwitchOnOffView.layer.cornerRadius = 16
            self.SwitchOnOffView.onTintColor = UIColor(red:0.88, green:0.18, blue:0.2, alpha:1)
            SwitchOnOffView.backgroundColor = UIColor(red:0.88, green:0.18, blue:0.2, alpha:1)

            self.UpdateTeamStatus(teamstatus: false)
            
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteamred_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamred"), for: .normal)
            }
        }
        
    }
    
    func LoadProfileImage() {
        self.docimg.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
    }
    
    func UpdateTeamStatus(teamstatus: Bool) {
        //here we are updating DocCountTable for teamStatus value
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        object.fields["teamStatus"] =  teamstatus
        object.id = self.DocCounttablerecordID      //record id of DocCountTable
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            var msg = ""
            if teamstatus {
                msg = "You have switched ON your team Successfully"
                GlobalVariables.teamoffon = true
                UserDefaults.standard.set(true, forKey: "teamswitch")
                UserDefaults.standard.synchronize()
            } else {
                msg = "You have switched OFF your team Successfully"
                GlobalVariables.teamoffon = false
                UserDefaults.standard.set(false, forKey: "teamswitch")
                UserDefaults.standard.synchronize()
            }
            print("MyTeamViewController:CustomObject: UpdateTeamStatus !")
            let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            
        }) { (response) in
            //Handle Error
            print("MyTeamViewController:UpdateTeamStatus: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    
    func updateTheView() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelDocTeam Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = QBSession.current.currentUser?.id
        getRequest["status"] = true
        QBRequest.objects(withClassName: "RelDocTeam", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            print("MyTeamViewController:updateTheView(): Success ")
            if contributors?.count == 0 {
                recordcount = 0
            } else {
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: contributors!)
            }
        }) { (response) in
            print("MyTeamViewController:updateTheView(): Response error: \(String(describing: response.error?.description))")
        }
        
        
        if recordcount == 0 {
            digonalline1.isHidden = false
            docimg1.isHidden = false
            teamdoc1Buttonview.isHidden = false
            namelabel1.isHidden = true
            digonalline2.isHidden = true
            docimg2.isHidden = true
            teamdoc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            digonalline3.isHidden = true
            docimg3.isHidden = true
            teamdoc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            digonalline4.isHidden = true
            docimg4.isHidden = true
            teamdoc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            teamdoc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            teamdoc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }

    func PopulateData(recordcount: Int, recordArray: [QBCOCustomObject]) {
        print("MyTeamViewController:PopulateData()")
        self.receivedrecordcount = recordArray.count
        for i in 0..<self.receivedrecordcount {
            
            if  let username = (recordArray[i].fields?.value(forKey: "inviteedocfullname") as? String)  {
                if i == 0 { // for first position
                    self.doc1profileaction = true
                    self.namelabel1.text = "Dr. " + username
                    self.doc1fullname =   self.namelabel1.text!
                    self.docimg1.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg1.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg1.layer.borderWidth = 0.5
                    self.docimg1.layer.cornerRadius = self.docimg1.bounds.size.height / 2
                    self.docimg1.layer.masksToBounds = true
                    //passing values to next viewcontroller
                    doc1imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!)
                    doc1userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    self.digonalline2.isHidden = false
                    self.namelabel1.isHidden = false
                    self.docimg2.isHidden = false
                    self.teamdoc2Buttonview.isHidden = false
                    self.namelabel2.isHidden = true
                    self.digonalline3.isHidden = true
                    self.docimg3.isHidden = true
                    self.teamdoc3Buttonview.isHidden = true
                    self.namelabel3.isHidden = true
                    self.digonalline4.isHidden = true
                    self.docimg4.isHidden = true
                    self.teamdoc4Buttonview.isHidden = true
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.teamdoc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.teamdoc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }

                if i == 1 { // for second position
                    self.doc2profileaction = true
                    self.namelabel2.text = "Dr. " + username
                    self.doc2fullname =  self.namelabel2.text!
                    self.docimg2.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg2.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg2.layer.borderWidth = 0.5
                    self.docimg2.layer.cornerRadius = self.docimg2.bounds.size.height / 2
                    self.docimg2.layer.masksToBounds = true
                    //passing values to next viewcontroller
                    doc2imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!)
                    doc2userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    self.namelabel2.isHidden = false
                    self.digonalline3.isHidden = false
                    self.docimg3.isHidden = false
                    self.teamdoc3Buttonview.isHidden = false
                    self.namelabel3.isHidden = true
                    self.digonalline4.isHidden = true
                    self.docimg4.isHidden = true
                    self.teamdoc4Buttonview.isHidden = true
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.teamdoc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.teamdoc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }

                if i == 2 { // for third position
                    self.doc3profileaction = true
                    self.namelabel3.text = "Dr. " + username
                    self.doc3fullname =   self.namelabel3.text!
                    self.docimg3.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg3.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg3.layer.borderWidth = 0.5
                    self.docimg3.layer.cornerRadius = self.docimg3.bounds.size.height / 2
                    self.docimg3.layer.masksToBounds = true
                    //passing values to next viewcontroller
                    doc3imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!)
                    doc3userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    self.namelabel3.isHidden = false
                    self.digonalline4.isHidden = false
                    self.docimg4.isHidden = false
                    self.teamdoc4Buttonview.isHidden = false
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.teamdoc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.teamdoc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }

                if i == 3 { // for forth position
                    self.doc4profileaction = true
                    self.namelabel4.text = "Dr. " + username
                    self.doc4fullname =  self.namelabel4.text!
                    self.docimg4.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg4.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg4.layer.borderWidth = 0.5
                    self.docimg4.layer.cornerRadius = self.docimg4.bounds.size.height / 2
                    self.docimg4.layer.masksToBounds = true
                    //passing values to next viewcontroller
                    doc4imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!)
                    doc4userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    self.namelabel4.isHidden = false
                    self.line5.isHidden = false
                    self.docimg5.isHidden = false
                    self.teamdoc5Buttonview.isHidden = false
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.teamdoc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }

                if i == 4 { // for fifth position
                    self.doc5profileaction = true
                    self.namelabel5.text = "Dr. " + username
                    self.doc5fullname =  self.namelabel5.text!
                    self.docimg5.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg5.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg5.layer.borderWidth = 0.5
                    self.docimg5.layer.cornerRadius = self.docimg5.bounds.size.height / 2
                    self.docimg5.layer.masksToBounds = true
                    //passing values to next viewcontroller
                    doc5imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!)
                    doc5userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    self.namelabel5.isHidden = false
                    self.line6.isHidden = false
                    self.docimg6.isHidden = false
                    self.teamdoc6Buttonview.isHidden = false
                    self.namelabel6.isHidden = true
                }

                if i == 5 { // for sixth position
                    self.doc6profileaction = true
                    self.namelabel6.text = "Dr. " + username
                    self.doc6fullname =  self.namelabel6.text!
                    self.docimg6.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg6.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg6.layer.borderWidth = 0.5
                    self.docimg6.layer.cornerRadius = self.docimg6.bounds.size.height / 2
                    self.docimg6.layer.masksToBounds = true
                    //passing values to next viewcontroller
                    doc6imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileURL") as? String)!)
                    doc6userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    self.namelabel6.isHidden = false
                }

            }
            
        }
    }
    
    func ToastNotication() {
        var style = ToastStyle()
        style.messageColor = .lightGray
        ToastManager.shared.style = style
        ToastManager.shared.isTapToDismissEnabled = true
        self.view.makeToast("Hi Doc you have already added 6 doctors to your Team", duration: 1000.0, position: .bottom, title: "Info", image: UIImage(named: "toast.png"), style: style, completion: {(_ didTap: Bool) -> Void in
            if didTap {
                print("Toast completion from tap")
                istoastbar = false
            }
            else {
                print("Toast completion without tap")
                istoastbar = false
            }
        })
    }

    // to check if key is present for persistance key value storage
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
//        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
//        self.nav = UINavigationController(rootViewController: homeViewController)
//        self.present(self.nav!, animated: false, completion: nil)
        let transition = CATransition.init()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromRight
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func BacktoCOTButton(_ sender: UIButton) {
        let transition = CATransition.init()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromRight
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func Addteamdoc1Button(_ sender: UIButton) {
        if doc1profileaction {
            print("Time to go to doc 1 profile")
            let vc = COTProfileViewController.storyboardInstance()
            vc.docfullname = doc1fullname
            vc.imageURLpassed = doc1imgURL
            vc.docuserid = doc1userid
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            
//            let docprofileviewController = self.storyboard!.instantiateViewController(withIdentifier: "OtherDoctorProfileView") as! OtherDoctorsProfileViewController
//            docprofileviewController.docnamestringpassed = doc1fullname
//            docprofileviewController.imageURLpassed = doc1imgURL
//            docprofileviewController.useridpassed = doc1userid
//            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = "MYTeam"
            adddoctocotviewController.theposition = 1
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Addteamdoc2Button(_ sender: UIButton) {
        if doc2profileaction {
            print("Time to go to doc 2 profile")
//            let docprofileviewController = self.storyboard!.instantiateViewController(withIdentifier: "OtherDoctorProfileView") as! OtherDoctorsProfileViewController
//            docprofileviewController.docnamestringpassed = doc2fullname
//            docprofileviewController.imageURLpassed = doc2imgURL
//            docprofileviewController.useridpassed = doc2userid
//            self.navigationController?.pushViewController(docprofileviewController, animated: true)
            
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel2.text!
            docprofileviewController.imageURLpassed = doc2imgURL
            docprofileviewController.docuserid = doc2userid
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
            
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = "MYTeam"
            adddoctocotviewController.theposition = 2
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Addteamdoc3Button(_ sender: UIButton) {
        if doc3profileaction {
            print("Time to go to doc 3 profile")
//            let docprofileviewController = self.storyboard!.instantiateViewController(withIdentifier: "OtherDoctorProfileView") as! OtherDoctorsProfileViewController
//            docprofileviewController.docnamestringpassed = doc3fullname
//            docprofileviewController.imageURLpassed = doc3imgURL
//            docprofileviewController.useridpassed = doc3userid
//            self.navigationController?.pushViewController(docprofileviewController, animated: true)
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel3.text!
            docprofileviewController.imageURLpassed = doc3imgURL
            docprofileviewController.docuserid = doc3userid
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = "MYTeam"
            adddoctocotviewController.theposition = 3
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Addteamdoc4Button(_ sender: UIButton) {
        if doc4profileaction {
            print("Time to go to doc 4 profile")
//            let docprofileviewController = self.storyboard!.instantiateViewController(withIdentifier: "OtherDoctorProfileView") as! OtherDoctorsProfileViewController
//            docprofileviewController.docnamestringpassed = doc4fullname
//            docprofileviewController.imageURLpassed = doc4imgURL
//            docprofileviewController.useridpassed = doc4userid
//            self.navigationController?.pushViewController(docprofileviewController, animated: true)
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel4.text!
            docprofileviewController.imageURLpassed = doc4imgURL
            docprofileviewController.docuserid = doc4userid
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = "MYTeam"
            adddoctocotviewController.theposition = 4
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Addteamdoc5Button(_ sender: UIButton) {
        if doc5profileaction {
            print("Time to go to doc 5 profile")
//            let docprofileviewController = self.storyboard!.instantiateViewController(withIdentifier: "OtherDoctorProfileView") as! OtherDoctorsProfileViewController
//            docprofileviewController.docnamestringpassed = doc5fullname
//            docprofileviewController.imageURLpassed = doc5imgURL
//            docprofileviewController.useridpassed = doc5userid
//            self.navigationController?.pushViewController(docprofileviewController, animated: true)
            print("Time to go to doc 5 profile")
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel5.text!
            docprofileviewController.imageURLpassed = doc5imgURL
            docprofileviewController.docuserid = doc5userid
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = "MYTeam"
            adddoctocotviewController.theposition = 5
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
    @IBAction func Addteamdoc6Button(_ sender: UIButton) {
        if doc1profileaction {
            print("Time to go to doc 1 profile")
//            let docprofileviewController = self.storyboard!.instantiateViewController(withIdentifier: "OtherDoctorProfileView") as! OtherDoctorsProfileViewController
//            docprofileviewController.docnamestringpassed = doc6fullname
//            docprofileviewController.imageURLpassed = doc6imgURL
//            docprofileviewController.useridpassed = doc6userid
//            self.navigationController?.pushViewController(docprofileviewController, animated: true)
            let docprofileviewController = COTProfileViewController.storyboardInstance()
            docprofileviewController.docfullname = self.namelabel6.text!
            docprofileviewController.imageURLpassed = doc6imgURL
            docprofileviewController.docuserid = doc6userid
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(docprofileviewController, animated: true)
        } else {
            let adddoctocotviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddDoctoCOT") as! AddDoctoCOTViewController
            adddoctocotviewController.specialitystringpassed = "MYTeam"
            adddoctocotviewController.theposition = 6
            self.navigationController?.pushViewController(adddoctocotviewController, animated: true)
        }
    }
    
}
