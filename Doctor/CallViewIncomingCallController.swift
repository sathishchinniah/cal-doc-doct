import UIKit
import Quickblox
import QuickbloxWebRTC
import SVProgressHUD
import Toast_Swift

private let kRefreshTimeInterval = 1.0 as? TimeInterval

class CallViewIncomingCallController: UIViewController, QBRTCClientDelegate {

    let defRect = CGRect.init(x: 0, y: 0, width: 48, height: 48)
    let defBgClr = UIColor.init(red: 0.8118, green: 0.8118, blue: 0.8118, alpha: 1.0)
    let defSlctClr = UIColor.init(red: 0.3843, green: 0.3843, blue: 0.3843, alpha: 1.0)
    
    
    @IBOutlet weak var toolBar: Toolbar!
    @IBOutlet weak var opponentVideoView: QBRTCRemoteVideoView!
    @IBOutlet weak var localVideoView: UIView!
    @IBOutlet weak var locallablel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    var nav: UINavigationController?
    
    open var opponets: [QBUUser]?
    open var currentUser: QBUUser?
    
    var views: [UIView] = []
    var videoCapture: QBRTCCameraCapture!
    var session: QBRTCSession?
    var callTimer: Timer?
    var timeDuration = TimeInterval()
    
    var callingnumber: UInt = 0
    var topView: UIView!
    var profileImageView: UIImageView!
    var profilePic: UIImageView!
    var videoCallButton: UIButton!
    
    // this userInfo is coming from InComingCallViewController:func AcceptCall
    var userInfo:Dictionary <String, String> = [:]
    
    @IBAction func chatInCallButtonTapped(_ sender: Any) {
        
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"
        
        object.fields["resumeConsult"] = true//!switchval
        object.id = UserDefaults.standard.string(forKey: "customobjIDSchedular")!      //record id
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("Call:updateResumeStatusBackend() successfull")
        }) { (response) in
            print("Resume Consult Response error: \(String(describing: response.error?.description))")
        }
        
        let calldoccode = userInfo["calldoccode"]
        let patfullname = userInfo["patientname"]
        let patgender = userInfo["gender"]
        let patage = userInfo["age"]
        
        let consultId = userInfo["consultrecordID"] ?? userInfo["consultTableId"] ?? ""//userInfo["consultrecordID"] ?? ""
        
        
        if (!consultId.isEmpty) {
            let user = QBUUser()
            user.id =  callingnumber
            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
            chatDialog.occupantIDs = ([NSNumber(value: Int(userInfo["patId"]!)!)]) as [NSNumber]//[user.id] as [NSNumber]
            
            QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let chatvc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController
                chatvc?.ConsultRecordID = consultId
                chatvc?.dialog = createdDialog
                chatvc?.profileimageurl = self.userInfo["patprofileid"] ?? ""//self.profileImage
                //let VC1 = self.storyboard!.instantiateViewControllerWithIdentifier("MyViewController") as! ViewController
                let navController = UINavigationController(rootViewController: chatvc!) // Creating a navigation controller with VC1 at the root of the navigation stack.
                self.present(navController, animated:true, completion: nil)
                //self.navigationController?.pushViewController(chatvc, animated: true)
            }, errorBlock: {(response: QBResponse!) in
                print("Error response + \(String(describing: response))")
            })
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("VIEWDIDLOAD")
        QBRTCClient.initializeRTC()
        QBRTCClient.instance().add(self)
        
        cofigureVideo()
        configureAudio()
        navigationController?.isNavigationBarHidden = true
        self.navigationItem.setHidesBackButton(true, animated:true)
        toolBar.isHidden = true
        let profPic = GlobalDocData.gprofileURL
        profilePic = UIImageView()
        profilePic.sd_setImage(with: URL(string: profPic), placeholderImage: UIImage(named: "profile"))// = UIImageView(image: #imageLiteral(resourceName: "logo"))
        
        
        configureToolbar()
        configureTopScreen()
        self.session?.localMediaStream.videoTrack.isEnabled = false
        QBRTCConfig.setStatsReportTimeInterval(5)
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"
        
        object.fields["resumeConsult"] = false//!switchval
        object.id = UserDefaults.standard.string(forKey: "customobjIDSchedular")!      //record id
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("Call:updateResumeStatusBackend() successfull")
        }) { (response) in
            print("Resume Consult Response error: \(String(describing: response.error?.description))")
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("VIEWDIDAPPEAR")
        self.resumeVideoCapture()
    }
    
    func configureTopScreen() {
        
        topView = UIView(frame: self.view.frame)
        profileImageView = UIImageView(frame: CGRect(x: (self.view.frame.width/2)-60, y: (self.view.frame.height/2)-60, width: 120.0, height: 120.0))
        profileImageView.sd_setImage(with: URL(string: (userInfo["patprofileid"] ?? "")), placeholderImage: UIImage(named: "profile"))//#imageLiteral(resourceName: "logo")
        videoCallButton = UIButton(frame: CGRect(x: (self.view.frame.width/2)+40, y: (self.view.frame.height/2)-40, width: 80.0, height: 80.0))
        videoCallButton.setImage(#imageLiteral(resourceName: "callblk"), for: .normal)
        videoCallButton.addTarget(self, action: #selector(indicateVideo(_:)), for: .touchUpInside)
        
        self.view.addSubview(videoCallButton)
        topView.addSubview(profileImageView)
        topView.backgroundColor = UIColor.white
     
        self.opponentVideoView.addSubview(topView)
        
        self.localVideoView.layer.addSublayer(profilePic.layer)
    }
    
    @objc func indicateVideo(_ sender: UIButton) {
        print("VIDEO CALL TAPPED")
        
        let payload = NSMutableDictionary()
        
        payload.setValue("request", forKey: "title")
        payload.setValue(100, forKey: "code")
        payload.setValue("5", forKey: "ios_badge")
        payload.setValue("mysound.wav", forKey: "ios_sound")
        payload.setValue(String(Int((self.session?.currentUserID) ?? 0)), forKey: "user_id")
        payload.setValue("10", forKey: "thread_id")
        payload.setValue("type", forKey: "type")
        
        

        let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted)
       
        let message = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = userInfo["patId"]
        event.type = QBMEventType.oneShot
        event.message = message
        
        var style = ToastStyle()
        style.messageColor = .white
        ToastManager.shared.style = style
        
        QBRequest.createEvent(event, successBlock: { (_, _) in
            print("EVENT CREATED SUCCESSFULLY")
            self.view.makeToast("Notification sent to the user", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
        }) { (error:QBResponse?) in
            print("ERROR: \(error)")
            self.view.makeToast("Couldn't notify patient. Please try again later.", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
        }
        
    }
    
    //MARK: WebRTC configuration
    
    func cofigureVideo() {
        print("CONFIGUREVIDEO")

        QBRTCConfig.mediaStreamConfiguration().videoCodec = .H264
        
        let videoFormat = QBRTCVideoFormat.init()
        videoFormat.frameRate = 21
        videoFormat.pixelFormat = .format420f
        videoFormat.width = 640
        videoFormat.height = 480
        
        self.videoCapture = QBRTCCameraCapture.init(videoFormat: videoFormat, position: .front)
        self.videoCapture.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.videoCapture.startSession {
            
            self.localVideoView.layer.cornerRadius = self.localVideoView.frame.size.width / 2;
            self.localVideoView.clipsToBounds = true;
            self.localVideoView.layer.borderWidth = 0.2
            self.localVideoView.layer.borderColor = UIColor.lightGray.cgColor
            
            if self.session?.localMediaStream.videoTrack.isEnabled == true {
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localVideoView.bounds
                self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
            } else {
                self.configureTopScreen()
            }
            
            
            let media = QBRTCMediaStreamConfiguration()
            
        }
    }
    
    func configureAudio() {
        print("PRINTAUDIO")
        QBRTCConfig.mediaStreamConfiguration().audioCodec = .codecOpus
        //Save current audio configuration before start call or accept call
        QBRTCAudioSession.instance().initialize()
        QBRTCAudioSession.instance().currentAudioDevice = .speaker
        //OR you can initialize audio session with a specific configuration
        QBRTCAudioSession.instance().initialize { (configuration: QBRTCAudioSessionConfiguration) -> () in
            
            var options = configuration.categoryOptions
            if #available(iOS 10.0, *) {
                options = options.union(AVAudioSessionCategoryOptions.allowBluetoothA2DP)
                options = options.union(AVAudioSessionCategoryOptions.allowAirPlay)
            } else {
                options = options.union(AVAudioSessionCategoryOptions.allowBluetooth)
            }
            
            configuration.categoryOptions = options
            configuration.mode = AVAudioSessionModeVideoChat
        }
        
    }
    
    //MARK: Helpers
    
    func configureToolbar() {
        print("CONFIGURETOOLBAR")
        let audioEnable = defButton()
        audioEnable.iconView = iconView(normalImage: "unmute", selectedImage: "mute")
        self.toolBar.addButton(button: audioEnable) { (button) in
            self.session?.localMediaStream.audioTrack.isEnabled = !(self.session?.localMediaStream.audioTrack.isEnabled)!
        }

        let videoEnable = defButton()
       /* videoEnable.iconView = iconView(normalImage: "videoOff", selectedImage: "videoOn")
        self.toolBar.addButton(button: videoEnable) { (button) in
            self.session?.localMediaStream.videoTrack.isEnabled = !(self.session?.localMediaStream.videoTrack.isEnabled)!
            if (self.session?.localMediaStream.videoTrack.isEnabled)! {
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.localVideoView.isHidden = false
                print("TOOLBAR: LOCALVIDEOVIEWSHOWN")
            } else {
                self.session?.localMediaStream.videoTrack.videoCapture = nil
                self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.localVideoView.isHidden = true
                print("TOOLBAR: LOCALVIDEOHIDDEN")
            }
        } */
        videoEnable.iconView = iconView(normalImage: "videoOn", selectedImage: "videoOff")
        self.toolBar.addButton(button: videoEnable) { (button) in
            
            self.session?.localMediaStream.videoTrack.isEnabled = !(self.session?.localMediaStream.videoTrack.isEnabled)!
            if self.session?.localMediaStream.videoTrack.isEnabled == true {
                
                //self.session?.localMediaStream.videoTrack.isEnabled = true
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                
                for subView in self.opponentVideoView.subviews {
                    if subView != self.opponentVideoView.subviews.first {
                        subView.removeFromSuperview()
                    }
                }
                self.topView.removeFromSuperview()
                
//                for layer in self.localVideoView.layer.sublayers! {
//                    layer.removeFromSuperlayer()
//                }
                
              //  self.profilePic.layer.removeFromSuperlayer()
                
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localVideoView.bounds
                self.localVideoView.layer.addSublayer(self.videoCapture!.previewLayer)
                
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
                self.session?.localMediaStream.videoTrack.isEnabled = true
                
                
            } else {
                
                self.session?.localMediaStream.videoTrack.videoCapture = nil
                self.configureTopScreen()
                
            }
        }
        
        let dynamicEnable = defButton()
        dynamicEnable.iconView = iconView(normalImage: "speakeron", selectedImage: "speakerOff")
        self.toolBar.addButton(button: dynamicEnable) { (button) in
            let device = QBRTCAudioSession.instance().currentAudioDevice;
            
            if device == .speaker {
                QBRTCAudioSession.instance().currentAudioDevice = .receiver
            } else {
                QBRTCAudioSession.instance().currentAudioDevice = .speaker
            }
        }
        
        let endthecall = defButton()
        endthecall.iconView = iconView(normalImage: "disconnect", selectedImage: "disconnect")
        self.toolBar.addButton(button: endthecall) { (button) in
            self.calldisconnectedlocallyByDoctor()
        }
        
        self.toolBar.updateItems()
        
        if (gsession != nil) {
            print("CallViewIncomingCallController:with gsession:")
            handleIncomingCall()
        }
    }
    
    func iconView(normalImage: String, selectedImage: String) -> UIImageView {
        let icon = UIImage.init(named: normalImage)
        let selectedIcon = UIImage.init(named: selectedImage)
        let iconView = UIImageView.init(image: icon, highlightedImage: selectedIcon)
        iconView.contentMode = .scaleAspectFit
        return iconView
    }
    
    func defButton() -> ToolbarButton {
        let defButton = ToolbarButton.init(frame: defRect)
        defButton.backgroundColor = defBgClr
        defButton.selectedColor = defSlctClr
        defButton.isPushed = true
        return defButton
    }
    
    @IBAction func cameraSwitchButton(_ sender: UIButton) {
        let position = self.videoCapture.position
        if position == .back {
            self.videoCapture.position = .front
        } else {
            self.videoCapture.position = .back
        }
    }
    
    func resumeVideoCapture() {
        print("RESUMECAPTURE")
        // ideally you should always stop capture session
        // when you are leaving controller in any way
        // here we should get its running state back
        if self.videoCapture != nil && !self.videoCapture.hasStarted {
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.videoCapture.startSession(nil)
        }
        /*if self.videoCapture != nil && !self.videoCapture.hasStarted {
         self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
         self.videoCapture.startSession(nil)
         self.localvideoView.isHidden = true
         //self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = false
         } else {
         self.localvideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
         self.localvideoView.isHidden = false
         //self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
         } */
    }
    
    func relayout(with views: [UIView]) {
        print("RELAYOUT")
        self.stackView.removeAllArrangedSubviews()
        
        for v in views {
            
            if self.stackView.arrangedSubviews.count > 1 {
                let i = views.index(of: v)! % 2
                let s = self.stackView.arrangedSubviews[i] as! UIStackView
                s.addArrangedSubview(v)
            }
            else {
                let hStack = UIStackView()
                hStack.axis = .horizontal
                hStack.distribution = .fillEqually
                hStack.spacing = 5
                hStack.addArrangedSubview(v)
                self.stackView.addArrangedSubview(hStack)
            }
        }
    }
    
    func removeRemoteView(with userID: UInt) {
        
        print("REMOVEREMOTEVIEW")

    }
    
    func calldisconnectedlocallyByDoctor() {
        
        print("CallViewIncomingCallController: calldisconnectedlocallyByDoctor()")
        
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"
        
        object.fields["resumeConsult"] = true//!switchval
        object.id = UserDefaults.standard.string(forKey: "customobjIDSchedular")!      //record id
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("Call:updateResumeStatusBackend() successfull")
        }) { (response) in
            print("Resume Consult Response error: \(String(describing: response.error?.description))")
        }
        
        if self.session != nil {
            print("CallViewIncomingCallController: calldisconnectedlocallyByDoctor()-> hangUp")
            self.session?.hangUp(nil)
            gsession = nil
            self.callTimer?.invalidate()
        }
        let localuser = QBUUser()
        localuser.id = (session?.initiatorID as? UInt) ?? (0 as UInt)
        
        let patfullname = userInfo["patientname"]
        let patprofileurl = userInfo["patprofileid"]
        let consultID = userInfo["consultrecordID"] ?? userInfo["consultTableId"] ?? ""
        if consultID != "" {//userInfo["consultrecordID"] {
            // call came as existing Consult
            let alert = UIAlertController(title: "", message: "Your Consult with Patient  \(patfullname ?? " ") is completed, Do you want to generate Prescription/Bill ??", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
            imageView.layer.cornerRadius = imageView.frame.size.width / 2;
            imageView.clipsToBounds = true;
            imageView.layer.borderWidth = 0.2
            imageView.layer.borderColor = UIColor.lightGray.cgColor
            imageView.sd_setImage(with: URL(string: patprofileurl!), placeholderImage: UIImage(named: "profile"))
            alert.view.addSubview(imageView)
            let ok = UIAlertAction(title: "YES", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
                // this userInfo is coming from InComingCallViewController:func AcceptCall
                self.UpdateConsultTableforconsult(userID:localuser.id, userInfo: self.userInfo)
                
            })
            
            let no = UIAlertAction(title: "NO", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                // go to home
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                guard let homeViewController = storyboard.instantiateViewController(withIdentifier: "homeviewVC") as? HomeViewController else {
                    self.dismiss(animated: true, completion: nil)
                    fatalError("Error")
                }
                self.nav = UINavigationController(rootViewController: homeViewController)
                self.present(self.nav!, animated: true, completion: nil)
            })
            alert.addAction(ok)
            alert.addAction(no)
//            DispatchQueue.main.async {
//                self.present(alert, animated: true, completion: nil)
//            }
            self.present(alert, animated: true, completion: nil)
            
            
        } else {
            
            let alert = UIAlertController(title: "", message: "Your Consult with Patient  \(patfullname ?? " ") is completed, Do you want to generate New Prescription/Bill ??", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
            imageView.layer.cornerRadius = imageView.frame.size.width / 2;
            imageView.clipsToBounds = true;
            imageView.layer.borderWidth = 0.2
            imageView.layer.borderColor = UIColor.lightGray.cgColor
            imageView.sd_setImage(with: URL(string: patprofileurl!), placeholderImage: UIImage(named: "profile"))
            alert.view.addSubview(imageView)
            let ok = UIAlertAction(title: "YES", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
                // this userInfo is coming from InComingCallViewController:func AcceptCall
                //self.CreateNewEntrytoConsultTable(userID:localuser.id, userInfo: self.userInfo)
                self.UpdateConsultTableforconsult(userID:localuser.id, userInfo: self.userInfo)
            })
            
            let no = UIAlertAction(title: "NO", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                // go to home
                
                //DispatchQueue.main.async {
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    guard let homeViewController = storyboard.instantiateViewController(withIdentifier: "homeviewVC") as? HomeViewController else {
                        self.dismiss(animated: true, completion: nil)
                        fatalError("Error")
                    }
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: true, completion: nil)
                //}
            })
            alert.addAction(ok)
            alert.addAction(no)
//            DispatchQueue.main.async {
//                self.present(alert, animated: true, completion: nil)
//            }
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func calldisconnectedbyremoteuser() {
        print("CallViewIncomingCallController:calldisconnectedbyremoteuser()")
        
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"
        
        object.fields["resumeConsult"] = true//!switchval
        object.id = UserDefaults.standard.string(forKey: "customobjIDSchedular")!      //record id
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("Call:updateResumeStatusBackend() successfull")
        }) { (response) in
            print("Resume Consult Response error: \(String(describing: response.error?.description))")
        }
        
        if self.session != nil {
            self.session?.hangUp(nil)
            gsession = nil
            self.callTimer?.invalidate()
        }
        let localuser = QBUUser()
        localuser.id = session?.initiatorID as! UInt
        
        let patfullname = userInfo["patientname"]
        let patprofileurl = userInfo["patprofileid"]
        var consultidstr: String = ""
        let consultId = userInfo["consultrecordID"] ?? userInfo["consultTableId"] ?? ""
        if consultId != "" {
            consultidstr = consultId
        } else {
            // problem with consultrecordIDstring
            print("CallViewIncomingCallController: problem with consultrecordIDstring")
        }
        
        if consultidstr.isEmpty {
            // call came as NEW Consult
            let alert = UIAlertController(title: "", message: "Your Consult with Patient  \(patfullname ?? " ") is completed, Do you want to generate New Prescription/Bill ??", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
            imageView.layer.cornerRadius = imageView.frame.size.width / 2;
            imageView.clipsToBounds = true;
            imageView.layer.borderWidth = 0.2
            imageView.layer.borderColor = UIColor.lightGray.cgColor
            imageView.sd_setImage(with: URL(string: patprofileurl!), placeholderImage: UIImage(named: "profile"))
            alert.view.addSubview(imageView)
            let ok = UIAlertAction(title: "YES", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
                // this userInfo is coming from InComingCallViewController:func AcceptCall
                //self.CreateNewEntrytoConsultTable(userID:localuser.id, userInfo: self.userInfo)
                self.UpdateConsultTableforconsult(userID:localuser.id, userInfo: self.userInfo)
                
            })
            
            let no = UIAlertAction(title: "NO", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                // go to home
                
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    guard let homeViewController = storyboard.instantiateViewController(withIdentifier: "homeviewVC") as? HomeViewController else {
                        fatalError("Error")
                    }
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: true, completion: nil)
                }
                
            })
            alert.addAction(ok)
            alert.addAction(no)
//            DispatchQueue.main.async {
//                self.present(alert, animated: true, completion: nil)
//            }
            self.present(alert, animated: true, completion: nil)

        } else {
            
            // call came as existing Consult
            let alert = UIAlertController(title: "", message: "Your Consult with Patient  \(patfullname ?? " ") is completed, Do you want to generate Prescription/Bill ??", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
            imageView.layer.cornerRadius = imageView.frame.size.width / 2;
            imageView.clipsToBounds = true;
            imageView.layer.borderWidth = 0.2
            imageView.layer.borderColor = UIColor.lightGray.cgColor
            imageView.sd_setImage(with: URL(string: patprofileurl!), placeholderImage: UIImage(named: "profile"))
            alert.view.addSubview(imageView)
            let ok = UIAlertAction(title: "YES", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
                // this userInfo is coming from InComingCallViewController:func AcceptCall
                self.UpdateConsultTableforconsult(userID:localuser.id, userInfo: self.userInfo)
                
            })
            
            let no = UIAlertAction(title: "NO", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                // go to home
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                guard let homeViewController = storyboard.instantiateViewController(withIdentifier: "homeviewVC") as? HomeViewController else {
                    self.dismiss(animated: true)
                    fatalError("Error")
                }
                self.nav = UINavigationController(rootViewController: homeViewController)
                self.present(self.nav!, animated: true, completion: nil)
            })
            alert.addAction(ok)
            alert.addAction(no)
//            DispatchQueue.main.async {
//                self.present(alert, animated: true, completion: nil)
//            }
            self.present(alert, animated: true, completion: nil)

        }
    }
    
    func session(_ session: QBRTCBaseSession, connectedToUser userID: NSNumber) {
        print("SESSION: connectedToUser")
        if (session as! QBRTCSession).id == self.session?.id {
            if session.conferenceType == QBRTCConferenceType.video {
                
            }
            
            if callTimer == nil {
                callTimer = Timer.scheduledTimer(timeInterval: kRefreshTimeInterval!, target: self, selector: #selector(self.refreshCallTime), userInfo: nil, repeats: true)
            }
        }
    }
    func session(_ session: QBRTCBaseSession, disconnectedFromUser userID: NSNumber) {
        //code
    }
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        print("CallViewIncomingCallController: hungUpByUser")
        if session.id == self.session?.id {
//
//            //self.removeRemoteView(with: userID.uintValue)
//            if userID == session.initiatorID {
//                self.session?.hangUp(nil)
//                print("CallViewIncomingCallController: hungUpByUser --> calling calldisconnectedbyremoteuser()")
//                self.calldisconnectedbyremoteuser()
//                self.removeRemoteView(with: userID.uintValue)
//            }
            print("CallViewIncomingCallController: hungUpByUser --> calling calldisconnectedbyremoteuser()")
            self.calldisconnectedbyremoteuser()
            self.removeRemoteView(with: userID.uintValue)
        }
    }
    
    func session(_ session: QBRTCBaseSession, updatedStatsReport report: QBRTCStatsReport, forUserID userID: NSNumber) {
        let audioReceivedBitrate = report.audioReceivedBitrateTracker.bitrate
        let videoReceivedBitrate = report.videoReceivedBitrateTracker.bitrate
        
        
        print("REMOTEVIDEOTRACK:\(session.remoteVideoTrack(withUserID: userID))")
        print("audioReceivedBitrate\(audioReceivedBitrate)")
        print("videoReceivedBitrate\(videoReceivedBitrate)")
    }
    
    
    
    func session(_ session: QBRTCBaseSession, receivedRemoteVideoTrack videoTrack: QBRTCVideoTrack, fromUser userID: NSNumber) {
        print("SESSION: receivedRemoteVideoTrack")
        print("VIDEOTRACK: \(videoTrack)")
        
        if (session as! QBRTCSession).id == self.session?.id {
            print("SESSION: receivedRemoteVideoTrack: sameSession")
            self.opponentVideoView.setVideoTrack(videoTrack)
//            let remoteView :QBRTCRemoteVideoView = QBRTCRemoteVideoView()
//            remoteView.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
//            remoteView.clipsToBounds = true
//            remoteView.setVideoTrack(videoTrack)
//            remoteView.tag = userID.intValue
//            self.views.append(remoteView)
//            self.relayout(with: self.views)
            
        } else {
            print("SESSION: receivedRemoteVideoTrack: diffSession")
        }
    }
    //**************remotestreamlistener
    
//    private func session(session: QBRTCSession!, didChangeState state: QBRTCSessionState!) {
//        print("Session did change state to \(state)")
//    }
//    private func session(session: QBRTCSession!, didChangeConnectionState state: QBRTCConnectionState!, forUser userID: NSNumber!) {
//        print("Session did change state to \(state) for userID \(userID)")
//    }
    
    func session(_ session: QBRTCBaseSession, didChange state: QBRTCSessionState) {
        print("Session did change state to \(state.rawValue)")
    }
    
    func session(_ session: QBRTCBaseSession, didChange state: QBRTCConnectionState, forUser userID: NSNumber) {
        print("Session did change state to \(state) for userID \(userID)")
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        if session.id == self.session?.id {
            gsession = nil
            self.session = nil
        }
    }

    func handleIncomingCall() {
        self.session = gsession
        self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
        self.session?.acceptCall(nil)
        toolBar.isHidden = false
    }

    @objc func refreshCallTime(_ sender: Timer) {
        timeDuration += kRefreshTimeInterval!
        locallablel.text =  gowithTimeDuration(timeDuration:timeDuration)
    }
    
    func gowithTimeDuration( timeDuration: TimeInterval) -> String {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        formatter.allowedUnits = [ .minute, .second ]
        formatter.zeroFormattingBehavior = [ .pad ]
        let formattedDuration = formatter.string(from: timeDuration)
        return formattedDuration!
    }


    func CreateNewEntrytoConsultTable(userID: UInt, userInfo:Dictionary <String, String>) {
        print("CallViewIncomingCallController: CreateNewEntrytoConsultTable() Creating New new entry in  Consult Table")
        
        let calldoccode = userInfo["calldoccode"]
        let relation = userInfo["relation"]
        let patfullname = userInfo["patientname"]
        let patprofileid = userInfo["patprofileid"]
        let patgender = userInfo["gender"]
        let patage = userInfo["age"]
        
        let object = QBCOCustomObject()
        object.className = "ConsultsTable"
        object.fields["patientuserid"] =  userID
        object.fields["docfullname"] = GlobalDocData.gdocname
        object.fields["patfullname"] = patfullname
        object.fields["patrelation"] = relation
        object.fields["patcalldoccode"] = calldoccode
        object.fields["patprofileUID"] = patprofileid
        object.fields["prescriptionid"] = ""
        object.fields["amount"] =  GlobalVariables.gDoctorFee
        
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            GlobalVariables.recordIDConsulttable = (contributors?.id)!  // this record id will be used later to update the record for
            // prescription related information
            print("CallViewIncomingCallController:CreateNewEntrytoConsultTable(): Successfully created Consult Details !")
            let storyboard = UIStoryboard(name: "MedicalStoryboard", bundle: nil)
            let medViewController = storyboard.instantiateViewController(withIdentifier: "MedicalAdviceBaseViewController") as! MedicalAdviceBaseViewController
            medViewController.ConsultTablerecordIDpassed  = (contributors?.id)!
            medViewController.patientnamepassed = patfullname!
            medViewController.patientgenderpassed = patgender!
            medViewController.patientagepassed = patage!
            medViewController.patientcalldoccodepassed = calldoccode!
            medViewController.patientpatuseridpassed = userID
            medViewController.consultType = "NEW"
            
            self.nav = UINavigationController(rootViewController: medViewController)
            self.present(self.nav!, animated: false, completion: nil)
        }) { (response) in
            //Handle Error
            print("CallViewIncomingCallController:CreateNewEntrytoConsultTable: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func UpdateConsultTableforconsult(userID: UInt, userInfo:Dictionary <String, String>) {
        print("CallViewIncomingCallController: UpdateConsultTableforconsult() Update existing Consult Table")
        let calldoccode = userInfo["calldoccode"]
        let patfullname = userInfo["patientname"]
        let patgender = userInfo["gender"]
        let patage = userInfo["age"]
        let isFirstConsult = userInfo["isFirstConsult"]
        
        GlobalVariables.recordIDConsulttable = userInfo["consultrecordID"] ?? ""/* ?? userInfo["consultTableId"] ?? userInfo["consultRecordID"] ?? "" */
        
        
        // Here we should read consult table for the given consultrecordID and get prescriptionid as string value which we will pass to prescription generation for comma seperated values of prescription id's
        let getRequest = NSMutableDictionary()
        getRequest["_id"] =  GlobalVariables.recordIDConsulttable //userInfo["consultrecordID"] ?? userInfo["consultTableId"] ?? userInfo["consultRecordID"] ?? ""
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                let prescriptionIDstring = (contributors![0].fields?.value(forKey: "prescriptionid") as? String) ?? ""
                // prescription related information
                print("CallViewIncomingCallController:UpdateConsultTableforconsult(): Success")
                let storyboard = UIStoryboard(name: "MedicalStoryboard", bundle: nil)
                let medViewController = storyboard.instantiateViewController(withIdentifier: "MedicalAdviceBaseViewController") as! MedicalAdviceBaseViewController
                medViewController.ConsultTablerecordIDpassed  = GlobalVariables.recordIDConsulttable
                medViewController.existingprescriptionIDpassed = prescriptionIDstring
                medViewController.patientnamepassed = patfullname!
                medViewController.patientgenderpassed = patgender!
                medViewController.patientagepassed = patage!
                medViewController.patientcalldoccodepassed = calldoccode!
                medViewController.patientpatuseridpassed = userID
                medViewController.isFirstConsult = isFirstConsult ?? ""
                medViewController.consultType = "EXISTING"
                self.nav = UINavigationController(rootViewController: medViewController)
                self.present(self.nav!, animated: false, completion: nil)
            } else {
                print("CallViewIncomingCallController:UpdateConsultTableforconsult(): contributors count Zero")
                var style = ToastStyle()
                style.messageColor = .white
                ToastManager.shared.style = style
                self.view.makeToast("Cannot generate prescription. Please try later.", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
                //self.CreateNewEntrytoConsultTable(userID: userID, userInfo: userInfo)
                //self.present(self.nav!, animated: false, completion: nil)
            }
            
        }) { (response) in
            //Error handling
            print("CallViewIncomingCallController:UpdateConsultTableforconsult: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func handleVideoRequestCallback(withcode: Int) {
        
        //accepted
        if withcode == 101 {
            let msg = "Video session with patient started"//"Dr. \(doctorname) is requesting for your video."
            let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "DENY", style: .default, handler: { (action) in
//                //self.showCOTConfirmationAlert()
//                //VIDEO_ALLOW_CODE = 101;
//
//            }))
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                //self.showCOTConfirmationAlert()
                //VIDEO_REJECT_CODE = 102;
//                for subView in self.opponentVideoView.subviews {
//                    if subView != self.opponentVideoView.subviews.first {
//                        subView.removeFromSuperview()
//                    }
//                }
//                self.topView.removeFromSuperview()
//
//                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
//                self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
//
//            }))
//            self.present(alert, animated: true, completion: nil)
                for subView in self.opponentVideoView.subviews {
                    if subView != self.opponentVideoView.subviews.first {
                        subView.removeFromSuperview()
                    }
                }
                self.topView.removeFromSuperview()
                
//                for layer in self.localVideoView.layer.sublayers! {
//                    layer.removeFromSuperlayer()
//                }
                
//                self.videoCallButton.removeFromSuperview()
//                self.videoCallButton.isEnabled = false
//                self.videoCallButton.isHidden = true
                
                //self.view.addSubview(videoCallButton)
//                for view in self.view.subviews {
//                    if view == self.videoCallButton {
//                        view.removeFromSuperview()
//                    }
//                }
                
                DispatchQueue.main.async {
                    self.videoCallButton.removeFromSuperview()
                }
                
                for subview in self.view.subviews {
                    if subview == self.videoCallButton {
                        subview.removeFromSuperview()
                    }
                }
                
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localVideoView.bounds
                self.localVideoView.layer.addSublayer(self.videoCapture!.previewLayer)
                
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
                self.session?.localMediaStream.videoTrack.isEnabled = true
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        //rejected
        if withcode == 102 {
            let msg = "Patient refused to turn on their camera"//"Dr. \(doctorname) is requesting for your video."
            let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
           
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

extension UIStackView {
    
    func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
