//
//  VerifyViewController.h
//  Doctor
//
//  Created by Manish Verma on 11/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyViewController : UIViewController<UITextFieldDelegate>

@end
