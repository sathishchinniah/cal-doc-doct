import UIKit

var latestdatelist = ["18 Sep 2017", "06 Aug 2016", "10 Apr 2017", "21 Mar 2017", "10 Mar 2017", "19 Feb 2017", "25th April 2017", "5 Jan 2016", "10 Feb 2017", "11 Oct 2017" ]
var lastdatelist = ["Consult close on", " ", " ", " ", " ", " "," "," "," "," "]

var thelastdateonly = ["23rd Sep @ 6.45 PM", "", "", "", "", "", "", "", "" ,""]
var datebadgelist = ["3","1","2","4","2","3","1","4","3","5","0"]
var extendlist = ["EXTEND", "", "", "", "", "","","","",""]
var patientagelist = ["F 26", "F 22", "F 20", "F 23", "F 27", "F 24","F 21","F 28","F 18","F 17"]
var PatientConsultsdetailsArray : [QBCOCustomObject] = []

//These variable is made global here because it will be used in consultsdetails to fetch details of that Consult

var theuserid: UInt = 0
var theindexval: Int = 0
var thedateval = ""

class PatientDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, QMChatServiceDelegate, QMChatConnectionDelegate, QMAuthServiceDelegate {

    @IBOutlet var patientImage: UIImageView!
    @IBOutlet var patientnamelb: UILabel!
    @IBOutlet var patientAgelb: UILabel!
    @IBOutlet var consulttableView: UITableView!
    @IBOutlet var sharedconsultstableView: UITableView!
    @IBOutlet var profileImageViewContainer: UIView!
    @IBOutlet var profileView: UIView!
    @IBOutlet var BottomBarView: UIView!
    @IBOutlet weak var profileImagetopdistanceConstraint: NSLayoutConstraint!
    @IBOutlet weak var consultsBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!

    var dataPointer = GData.shared()
    var thePatientName = ""
    var patientindex = 0
    var patientuseridpassed: UInt = 0
    var patientcalldoccodepassed = ""
    var nav: UINavigationController?

    override func awakeFromNib() {
        super.awakeFromNib()
        if (QBChat.instance.isConnected) {
            self.getDialogs()
        }
    }

    func getDialogs() {

        if let lastActivityDate = ServicesManager.instance().lastActivityDate {
            ServicesManager.instance().chatService.fetchDialogsUpdated(from: lastActivityDate as Date, andPageLimit: kDialogsPageLimit, iterationBlock: { (response, dialogObjects, dialogsUsersIDs, stop) -> Void in
            }, completionBlock: { (response) -> Void in
                if (response.isSuccess) {
                    ServicesManager.instance().lastActivityDate = NSDate()
                }
            })
        } else {
            ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in

            }, completion: { (response: QBResponse?) -> Void in

                guard response != nil && response!.isSuccess else {
                    SVProgressHUD.showError(withStatus: "SA_STR_FAILED_LOAD_DIALOGS".localized)
                    return
                }

                SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
                ServicesManager.instance().lastActivityDate = NSDate()
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navBarColor = navigationController!.navigationBar
        navBarColor.topItem?.title = "Consult History"
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        title = "Consult History"
        // this removes navigation bar horizental line
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        BottomBarView.backgroundColor = UIColor(patternImage: UIImage(named: "bottombarbg")!)
        //make cell lines full end to end
        consulttableView.separatorInset = .zero
        consulttableView.layoutMargins = .zero
        sharedconsultstableView.separatorInset = .zero
        sharedconsultstableView.layoutMargins = .zero

        profileView.isHidden = true
        sharedconsultstableView.isHidden = true
        consultsBtn.isHighlighted = true
        patientnamelb.text = thePatientName
        
        // These two lines removes extra white space from top of table view
        self.consulttableView.contentInset = UIEdgeInsetsMake(-70, 0, 0, 0)
        self.sharedconsultstableView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
        
        patientImage.sd_setImage(with: URL(string: (MyAllConsultsUniqueArray[patientindex].fields?.value(forKey: "patprofileUID") as! String)), placeholderImage: UIImage(named: "profile"))
        print("PatientDetailsViewController: patientuserid = \(patientuseridpassed) and calldoccode = \(patientcalldoccodepassed)")
        patientImage.layer.cornerRadius = patientImage.frame.size.width / 2
        patientImage.clipsToBounds = true
        patientImage.layer.borderWidth = 0.5
        patientImage.layer.borderColor = UIColor.clear.cgColor
        
        profileImageViewContainer.layer.cornerRadius = profileImageViewContainer.frame.size.width / 2;
        profileImageViewContainer.clipsToBounds = true;
        profileImageViewContainer.layer.borderWidth = 2.0
        profileImageViewContainer.layer.borderColor =  UIColor.clear.cgColor

        self.ReadyConsultsDetails()
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white

    }

    @objc func leftButtonAction(sender: UIBarButtonItem) {
        let myclinicController = self.storyboard!.instantiateViewController(withIdentifier: "myClinic") as! myClinicViewController
        self.nav = UINavigationController(rootViewController: myclinicController)
        self.present(self.nav!, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == consulttableView {
            return PatientConsultsdetailsArray.count
        } else {
            return 10
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == consulttableView) {
            let cell = consulttableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PatientDetailsConsultCell

            cell.selectionStyle = .none

            let currenttime: Int = Int(NSDate().timeIntervalSince1970)
            let createdat  = PatientConsultsdetailsArray[indexPath.row].createdAt! as NSDate
            let createdatepochtime = Int(createdat.timeIntervalSince1970)
            let diff = currenttime - createdatepochtime
            let hourslasped = diff/3600

            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"    //"MMM dd YYYY hh:mm a"
            let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
            let fulldate = dateString.components(separatedBy: "|")
            let lastdatetimelb = fulldate[0]
            let lastdatebtn = fulldate[1]
            cell.latestDateBtn.setTitle(lastdatebtn, for: .normal)
            cell.latestDateTimelb.text = lastdatetimelb
            //cell.extendconsultButton.isHidden = true
            cell.consultstatuslb.layer.masksToBounds = true
            cell.consultstatuslb.layer.cornerRadius = 5
            
            if indexPath.row == 0 {
                if ( hourslasped < 72 ) && ( hourslasped >= 0) {
                    if ( hourslasped <= 1 ) {
                        cell.consultstatuslb.text = "Open"
                        cell.consultstatuslb.backgroundColor = openColor
                    } else {
                        //cell.extendconsultButton.isHidden = false
                        let thehours = 72 - hourslasped
                        cell.consultstatuslb.text = "closes in " + String(thehours) +  " hrs"
                        cell.consultstatuslb.backgroundColor = closesColor
                        cell.consultstatuslbwithConstraint.constant = 100
                    }
                } else {
                    cell.consultstatuslb.text = "Closed"
                    cell.consultstatuslb.backgroundColor = closedColor
                }
            } else {
                if ( hourslasped <= 72 ) && ( hourslasped >= 0) {
                    //cell.extendconsultButton.isHidden = false
                    let thehours = 72 - hourslasped
                    cell.consultstatuslb.text = "closes in " + String(thehours) +  " hrs"
                    cell.consultstatuslb.backgroundColor = closesColor
                    cell.consultstatuslbwithConstraint.constant = 100
                } else {
                    cell.consultstatuslb.text = "Closed"
                    cell.consultstatuslb.backgroundColor = closedColor
                }

            }
            return(cell)
        } else {
            let sharedconsultscell = sharedconsultstableView.dequeueReusableCell(withIdentifier: "SharedConsultsCell", for: indexPath) as! SharedConsultsTableViewCell
            
            sharedconsultscell.selectionStyle = .none
            sharedconsultscell.doctornamelb.text = "Dr. Manish Verma"
            sharedconsultscell.doctorspecialitylb.text = "ENT"
            sharedconsultscell.lockbuttonview.tag = indexPath.row
            sharedconsultscell.lockbuttonview.addTarget(self,action:#selector(LockButtonAction), for:.touchUpInside)
            
            return(sharedconsultscell)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if ( tableView == consulttableView) {
            return 85
        } else {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ( tableView == consulttableView) {
            let vc = ConsultDetailsViewController.storyboardInstance()
            vc.patientnamepassed = thePatientName
            vc.imageurlpassed = (MyAllConsultsUniqueArray[patientindex].fields?.value(forKey: "patprofileUID") as! String)
            
            let currenttime: Int = Int(NSDate().timeIntervalSince1970)
            let createdat  = PatientConsultsdetailsArray[indexPath.row].createdAt! as NSDate
            let createdatepochtime = Int(createdat.timeIntervalSince1970)
            let diff = currenttime - createdatepochtime
            let hourslasped = diff/3600

            if (hourslasped > 72) {
                vc.consultONOFF = false
            } else {
                vc.consultONOFF = true
            }

            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"    //"MMM dd YYYY hh:mm a"
            let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
            let fulldate = dateString.components(separatedBy: "|")
            let lastdatetimelb = fulldate[0]
            let lastdatebtn = fulldate[1]
            vc.ConsultRecordID = PatientConsultsdetailsArray[indexPath.row].id! // pasing consult record ID
            vc.patientuseridpassed = patientuseridpassed
            vc.patientindex = patientindex
            vc.datepassed = lastdatebtn
            vc.timepassed = lastdatetimelb
            vc.tblindexpassed = indexPath.row
            theindexval = Int(indexPath.row)
            thedateval = lastdatebtn
            
            // this line will hide back button title on next viewcontroller navigation bar
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    @objc func LockButtonAction(sender: UIButton) {
        //sender.tag
    }
    
    @IBAction func ConsultButtonClicked(_ sender: UIButton) {
        consultsBtn.isHighlighted = false
        consulttableView.isHidden = false
        profileView.isHidden = true
        profileBtn.isHighlighted = true
        sharedconsultstableView.isHidden = true
    }

    @IBAction func ProfileButtonClicked(_ sender: UIButton) {
        profileBtn.isHighlighted = false
        consulttableView.isHidden = true
        profileView.isHidden = false
        consultsBtn.isHighlighted = true
        sharedconsultstableView.isHidden = false
    }
    
    @IBAction func MakeCall(_ sender: UIButton) {
        let mycallviewController = self.storyboard!.instantiateViewController(withIdentifier: "makeCall") as! CallViewControllerswft
        mycallviewController.callingnumber = patientuseridpassed
        mycallviewController.patientname = thePatientName
        self.navigationController?.pushViewController(mycallviewController, animated: true)
    }
    
    @IBAction func ChatButtonAction(_ sender: UIButton!) {
        let user = QBUUser()
        user.id =  patientuseridpassed
        let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
        chatDialog.occupantIDs = [user.id] as [NSNumber]
        
        QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
            print("sucess + \(String(describing: response))")
            let chatvc = self.storyboard!.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            chatvc.dialog = createdDialog
            chatvc.profileimageurl = (MyAllConsultsUniqueArray[self.patientindex].fields?.value(forKey: "patprofileUID") as! String)
            self.navigationController?.pushViewController(chatvc, animated: true)
        }, errorBlock: {(response: QBResponse!) in
            print("Error response + \(response)")
        })
    }
    
    func ReadyConsultsDetails() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        getRequest["patientuserid"] = patientuseridpassed
        getRequest["patcalldoccode"] = patientcalldoccodepassed
        
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                print("PatientDetailsViewController:ReadyConsultsDetails: PatientConsultsdetailsArray not empty")
                PatientConsultsdetailsArray = contributors!
                PatientConsultsdetailsArray.reverse()
            } else {
                PatientConsultsdetailsArray = contributors!
            }
            
            self.consulttableView.reloadData()
            
        }) { (response) in
            print("PatientDetailsViewController:ReadyConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
    }
    
}
