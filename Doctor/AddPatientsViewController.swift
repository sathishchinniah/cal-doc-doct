import UIKit
import DropDown
import Contacts
import ContactsUI
import Toast_Swift

class AddPatientsViewController: UIViewController, UITextFieldDelegate, CNContactPickerDelegate {

    @IBOutlet var mobilenumbertxt: UITextField!
    @IBOutlet var invitetextlb: UILabel!
    @IBOutlet var Mobilenumlb: UILabel!
    @IBOutlet var userinfolb: UILabel!
    @IBOutlet var countrycodedropdownView: UIView!
    @IBOutlet var countrycodetxt: UITextField!
    @IBOutlet weak var toplogotopmargin: NSLayoutConstraint!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    let dropDownCountryCode = DropDown()
    var nav: UINavigationController?
    var patientname = ""
    var objectforreldocpat = AddPatientsBEClass()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        
        progressIndicator.isHidden = true
        countrycodetxt.attributedPlaceholder = NSAttributedString(string: "+91",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        countrycodetxt.text = "+91"
        
        if ipad && maxLength == 1024 {
            Mobilenumlb.font = UIFont(name: "Rubik", size: 18)
            invitetextlb.font = UIFont(name: "Rubik", size: 22)
            userinfolb.font = UIFont(name: "Rubik", size: 22)
            mobilenumbertxt.font = UIFont(name: "Rubik", size: 22)
        }
        
        if phone && maxLength == 736 {
            toplogotopmargin.constant = 55
        }
        
        self.mobilenumbertxt.tintColor = UIColor.white
        self.countrycodetxt.tintColor = UIColor.white
        mobilenumbertxt.delegate = self
        countrycodetxt.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func leftButtonAction(sender: UIBarButtonItem) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ sender: UITextField) {
        
        if (sender == self.countrycodetxt) {
            sender.resignFirstResponder()
            dropDownCountryCode.anchorView = countrycodedropdownView
            dropDownCountryCode.dataSource = ["India +91", "United Kingdon +44", "United States +1", "Singapore +64", "Australia +61"]
            dropDownCountryCode.show()
            dropDownCountryCode.selectionAction = { [unowned self] (index: Int, item: String) in
                let snippet = item
                if let range = snippet.range(of: "+") {
                    let code = snippet[range.upperBound...]
                    self.countrycodetxt.text = "+" + code
                    self.mobilenumbertxt.becomeFirstResponder()
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == mobilenumbertxt {

            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)

            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            
            if length == 0 || length == 10 {

                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                let index = 0 as Int
                let formattedString = NSMutableString()
                
                let remainder = decimalString.substring(from: index)
                formattedString.append(remainder)
                textField.text = formattedString as String
                
                mobilenumbertxt.resignFirstResponder()
                return (newLength >= 10) ? false : true
            }
            let index = 0 as Int
            let formattedString = NSMutableString()

            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
        } else {
            return true
        }
    }
    
    @IBAction func AddressBookButton(_ sender: UIButton) {
        let entityType = CNEntityType.contacts
        let authStatus = CNContactStore.authorizationStatus(for: entityType)
        
        if authStatus == CNAuthorizationStatus.notDetermined {
            
            let contactStore = CNContactStore.init()
            contactStore.requestAccess(for: entityType, completionHandler: { (success, nil) in
                
                if success {
                    self.openContacts()
                } else {
                    print("Addressboon Not Authorised")
                }
            })
        } else if authStatus == CNAuthorizationStatus.authorized {
            self.openContacts()
        }
    }
    
    func openContacts() {
        let contactPicker = CNContactPickerViewController.init()
        contactPicker.delegate = self
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true) {
        }
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        var phoneNo = "NA"
        let phoneString = ((((contact.phoneNumbers[0] as  AnyObject).value(forKey: "labelValuePair") as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "stringValue"))
        //phoneNo = phoneString! as! String
        //let phoneString = ((((contact.phoneNumbers[0] as  AnyObject).value(forKey: "labelValuePair") as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "stringValue"))
        phoneNo = (phoneString as? String) ?? ""
        var formattedPhoneNo = phoneNo.replacingOccurrences(of: " ", with: "")
        formattedPhoneNo = formattedPhoneNo.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")//formattedPhoneNo.replacingOccurrences(of: "-", with: "")
        self.mobilenumbertxt.text = formattedPhoneNo
//        if phoneNo.count > 10 {
//            let substring = phoneNo.substring(from: phoneNo.index(phoneNo.startIndex, offsetBy: 3))
//            self.mobilenumbertxt.text = substring
//        } else {
//
//            self.mobilenumbertxt.text = phoneNo
//        }
    }
    
    @IBAction func BackButton(_ sender: UIButton) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    @IBAction func InviteButton(_ sender: UIButton) {
        
        if (mobilenumbertxt.text?.count)! < 10 {
            let alert = UIAlertController(title: "Alert", message: "Please Entre 10 Digit Mobile Number!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
                LoadingIndicatorView.hide()
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }

        self.checkifUserAlreadyExist()
    }
    
    // First check if Patient exists in calldoc database
    func checkifUserAlreadyExist() {
        
        LoadingIndicatorView.show("Send Invite....")
        var totalmobilenumber = ""
        totalmobilenumber = countrycodetxt.text! + mobilenumbertxt.text!
        let stringWithoutpluswith_p: String = totalmobilenumber.replacingOccurrences(of: "+", with: "p_")
        QBRequest.users(withLogins: [stringWithoutpluswith_p], page: QBGeneralResponsePage(currentPage: 1, perPage: 10), successBlock: {(_ response: QBResponse, _ page: QBGeneralResponsePage, _ users: [Any]) -> Void in
            if users.count > 0 {
                let selectedUser = users[0] as? QBUUser
                self.patientname = (selectedUser?.fullName)!
                self.progressIndicator.isHidden = false
                self.progressIndicator.stopAnimating()
                self.ReadCallDoctable_RelDocPat(invitoruid: (QBSession.current.currentUser?.id)!, inviteeuid: (selectedUser?.id)!)
            } else {
                self.SendSMSInvitetoPatients()
                
            }
        }, errorBlock: {(_ response: QBResponse) -> Void in
            LoadingIndicatorView.hide()
        })
    }
    
    // now check if there is entry in RelDocPat table for doctor Patient relation
    func ReadCallDoctable_RelDocPat(invitoruid: UInt, inviteeuid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["doctoruserid"] =   invitoruid
        getRequest["patientuserid"] =  inviteeuid
        QBRequest.objects(withClassName: "RelDocPat", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                LoadingIndicatorView.hide()
                let alert = UIAlertController(title: "Alert", message: "User Already Connected !", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    // go back to Home
                    let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: false, completion: nil)
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                self.readPatientdetails(doctoruserid: invitoruid, patientuserid: inviteeuid)
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
        }
    }

    func readPatientdetails(doctoruserid: UInt, patientuserid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = patientuserid
        QBRequest.objects(withClassName: "PatPrimaryDetailsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            if ((contributors?.count)! > 0) {
                var patientprofileurl = contributors![0].fields?.value(forKey: "profileimageURL") as? String
                if (patientprofileurl == nil || patientprofileurl!.isEmpty) {
                    patientprofileurl = "none"
                }
                self.createEntryinRelDocPatTable(invitoruid: doctoruserid, inviteeuid: patientuserid, patientprofileurl: patientprofileurl! )
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
        }
    }
    
    func createEntryinRelDocPatTable(invitoruid: UInt, inviteeuid: UInt, patientprofileurl: String ) {
        print("AddPaitentsViewConteroller: createEntryinRelDocPatTable(): Creating new entry on RelDocPatable")
        let object = QBCOCustomObject()
        object.className = "RelDocPat"
        let doctoruserid = invitoruid
        object.fields["doctoruserid"] = doctoruserid
        object.fields["patientuserid"] = inviteeuid
        object.fields["doctorfullname"] = QBSession.current.currentUser!.fullName
        object.fields["lock"] = "true"
        object.fields["doctorspeciality"] = GlobalVariables.gdocspeciality
        object.fields["doctorprofileimageURL"] =  GlobalDocData.gprofileURL
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            LoadingIndicatorView.hide()
            let alert = UIAlertController(title: "Congratulations", message: " Notification has been sent to your Patient   \(self.patientname )", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
            imageView.layer.cornerRadius = imageView.frame.size.width / 2;
            imageView.clipsToBounds = true;
            imageView.layer.borderWidth = 0.2
            imageView.layer.borderColor = UIColor.lightGray.cgColor
            imageView.sd_setImage(with: URL(string: patientprofileurl), placeholderImage: UIImage(named: "profile"))
            alert.view.addSubview(imageView)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                self.sendPushToPatients(inviteeuid: inviteeuid, patientprofileurl:patientprofileurl )
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }) { (response) in
            LoadingIndicatorView.hide()
            print("AddPaitentsViewConteroller:createEntryinRelDocPatTable(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func sendPushToPatients(inviteeuid: UInt, patientprofileurl: String) {
        
        let currentUserLogin = QBSession.current.currentUser!.fullName
        let userid = "\(inviteeuid)"

        QBRequest.sendPush(withText:  "\("Dr " + currentUserLogin! ) Invited you", toUsers: userid, successBlock: { (response: QBResponse, event:[QBMEvent]?) in
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: true, completion: nil)
        }) { (error:QBError?) in
            self.ToastNotificationsendPushToPatientsfailed(patientname: self.patientname)
        }
    }
    
    func ToastNotificationsendPushToPatientsfailed(patientname: String) {
        // create a new style for Toast Notifiction
        istoastbar = true
        var style = ToastStyle()
        style.messageColor = .lightGray
        ToastManager.shared.style = style
        ToastManager.shared.isTapToDismissEnabled = true
        self.view.makeToast("Invite has been sent t Patient \(patientname) ", duration: 500.0, position: .center, title: "", image: UIImage(named: "toast.png"), style: style, completion: {(_ didTap: Bool) -> Void in
            if didTap {
                istoastbar = false
                let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                self.nav = UINavigationController(rootViewController: homeViewController)
                self.present(self.nav!, animated: true, completion: nil)
            } else {
                print("AddPaitentsViewConteroller:Toast completion without tap")
                istoastbar = false
                let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
                self.nav = UINavigationController(rootViewController: homeViewController)
                self.present(self.nav!, animated: true, completion: nil)
            }
        })
    }
    
    func showAlert(msg: String) {
        let cntrl = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        cntrl.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(cntrl, animated: true, completion: nil)
    }
    
    func sendPushToOpponentsAboutNewInvite(inviteeuid: UInt) {
        let currentUserLogin = QBSession.current.currentUser!.fullName
        let userid = "\(inviteeuid)"
        QBRequest.sendPush(withText: "\("Dr " + currentUserLogin! ) Invited you", toUsers: userid, successBlock: { (response: QBResponse, event:[QBMEvent]?) in
            print("sendPushToOpponentsAboutNewInvite: Push sent!")
            let pushinvitecontroller = self.storyboard?.instantiateViewController(withIdentifier: "PushInvite") as! PushInviteViewController
            pushinvitecontroller.profilenamepassed = self.patientname
            self.present(pushinvitecontroller, animated: true, completion: nil)
            
        }) { (error:QBError?) in
            print("sendPushToOpponentsAboutNewInvite:Can not send push: \(error!))")
        }
    }
    
    func SendSMSInvitetoPatients() {
        self.SendSMSusingMSG91API()
    }
    
    func  SendSMSusingMSG91API() {
        let mobilenumber = mobilenumbertxt.text!
        let docname = GlobalDocData.gdocname
        let docnameparts = docname.components(separatedBy: " ")
        var docfirstname = ""
        var doclastname = ""
        GlobalDocData.gthefinalcalldoccode = (QBSession.current.currentUser?.customData)!
        let calldoccode = GlobalDocData.gthefinalcalldoccode
        docfirstname = docnameparts[0]
        doclastname = docnameparts[1]
        let headers = [
            "authkey": "191254A7N7Dkz1Y5a4db3e0",
            "content-type": "application/json"
        ]
        let parameters = [
            "sender": "CALDOC",
            "route": "4",
            "country": "91",
            "sms": [
                [
                    "message": "Hi \(mobilenumber). You are invited by Dr. \(docfirstname) \(doclastname). Use CallDoc Code \(calldoccode) to install/Register 'https://www.calldoc.com' ",
                    "to": [mobilenumber]
                ]
            ]
            ] as [String : Any]
        
        do {
            let postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let request = NSMutableURLRequest(url: NSURL(string: "http://api.msg91.com/api/v2/sendsms")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    print("AddPatientsViewController:SendSMSusingMSG91API():SMS Sent ERROR")
                    print(error!)
                    return
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print("AddPatientsViewController:SendSMSusingMSG91API():SMS Sent Successfully")
                    print(httpResponse!)
                    DispatchQueue.main.async {
                        self.SMSSendshowAlert(mobilenum: mobilenumber)
                    }
                }
            })
            dataTask.resume()
            
        } catch let error as NSError {
            print("AddPatientsViewController:SendSMSusingMSG91API():JSON error Failed to load: \(error.localizedDescription)")
            return
        }
    }
    
    func SMSSendshowAlert(mobilenum: String) {
        let alert = UIAlertController(title: "Alert", message: "Congrats ! The Invite SMS has been send to \(mobilenum)", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            self.ActiononOK()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func ActiononOK() {
        LoadingIndicatorView.hide()
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }
    
}
