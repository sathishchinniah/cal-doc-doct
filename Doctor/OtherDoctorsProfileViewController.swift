import UIKit

class OtherDoctorsProfileViewController: UIViewController {

    @IBOutlet weak var specialityib: UILabel!
    @IBOutlet weak var profileimageviewcontainer: UIView!
    @IBOutlet weak var docimg: UIImageView!
    @IBOutlet weak var certificatetxt: UITextView!
    @IBOutlet weak var detailstxt: UITextView!
    @IBOutlet weak var topview: UIView!
    
    var nav: UINavigationController?
    var docnamestringpassed = ""
    var imageURLpassed = ""
    var useridpassed = 0
    var docspeciality = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        title =  docnamestringpassed
        self.specialityib.text = docspeciality
        LoadProfileImage()
        
        docimg.layer.cornerRadius = docimg.frame.size.width / 2;
        docimg.clipsToBounds = true;
        docimg.layer.borderWidth = 4.0
        docimg.layer.borderColor = UIColor.clear.cgColor
        
        profileimageviewcontainer.layer.cornerRadius = profileimageviewcontainer.frame.size.width / 2;
        profileimageviewcontainer.clipsToBounds = true;
        profileimageviewcontainer.layer.borderWidth = 2.0
        profileimageviewcontainer.layer.borderColor =  UIColor.clear.cgColor
        
        let isonline = UserDefaults.standard.object(forKey: "doctoronline") as! Bool
        
        if(isonline) {
            profileimageviewcontainer.layer.borderColor = UIColor.init(red: 0/255, green: 177/255, blue: 100/255, alpha: 1).cgColor
        } else {
            profileimageviewcontainer.layer.borderColor = UIColor.orange.cgColor
        }

        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        topview.backgroundColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
    }

    func LoadProfileImage() {
        self.docimg.sd_setImage(with: URL(string: imageURLpassed), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
