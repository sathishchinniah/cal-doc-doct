import UIKit

class SMSInviteDismissViewController: UIViewController {

    @IBOutlet weak var Mobilenumber: UILabel!

    var nav: UINavigationController?
    var mobilenumberpassed = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        Mobilenumber.text = mobilenumberpassed
        
        navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
            // Your code with delay
            print("InviteDismissViewController:timeToMoveOn after delay")
            self.navigationController?.isNavigationBarHidden = false
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: true, completion: nil)
        }
    }

    func timeToMoveOn() {
        navigationController?.isNavigationBarHidden = false
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }
}
