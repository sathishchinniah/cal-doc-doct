import UIKit

class DXConfirmMedicineView: UIView {

    override init (frame : CGRect) {
        super.init(frame : frame)
    }

    convenience init () {
        self.init(frame:CGRect.zero)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }

    func configureView(dxData: DxMedicalAdviceData) {

        let testNameLabel = UILabel()
        let testNameValueLabel = UILabel()
        let testNameLineView = UIView()
        let instructionsLabel = UILabel()
        let instructionsValueLabel = UILabel()
        let instructionsLineView = UIView()
        
        self.addSubview(testNameLabel)
        self.addSubview(testNameValueLabel)
        self.addSubview(testNameLineView)
        self.addSubview(instructionsLabel)
        self.addSubview(instructionsValueLabel)
        self.addSubview(instructionsLineView)

        testNameLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin)
            make.top.equalTo(self.snp.topMargin)
        }

        testNameLabel.text = "Test Name"
        testNameLabel.textColor = UIColor.gray
        testNameLabel.font = UIFont(name: "Rubik-Regular", size: 10.0)

        testNameValueLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin)
            make.trailing.equalTo(self.snp.trailingMargin)
            make.top.equalTo(testNameLabel.snp.bottom).offset(10)
        }

        testNameValueLabel.font = UIFont(name: "Rubik-Regular", size: 15.0)
        testNameValueLabel.text = dxData.testName

        testNameLineView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.top.equalTo(testNameValueLabel.snp.bottom).offset(10)
            make.leading.equalTo(testNameValueLabel.snp.leading)
            make.trailing.equalTo(testNameValueLabel.snp.trailing)
        }

        testNameLineView.backgroundColor = UIColor(red: 170.0/255, green: 170.0/255, blue: 170.0/255, alpha: 1.0)

        instructionsLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin)
            make.top.equalTo(testNameLineView.snp.bottom).offset(10)
        }

        instructionsLabel.text = "Instructions"
        instructionsLabel.textColor = UIColor.gray
        instructionsLabel.font = UIFont(name: "Rubik-Regular", size: 10.0)

        instructionsValueLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin)
            make.trailing.equalTo(self.snp.trailingMargin)
            make.top.equalTo(instructionsLabel.snp.bottom).offset(10)
        }

        instructionsValueLabel.font = UIFont(name: "Rubik-Regular", size: 15.0)
        instructionsValueLabel.text = dxData.instructions

        instructionsLineView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.top.equalTo(instructionsValueLabel.snp.bottom).offset(10)
            make.leading.equalTo(instructionsValueLabel.snp.leading)
            make.trailing.equalTo(instructionsValueLabel.snp.trailing)
        }

        instructionsLineView.backgroundColor = UIColor(red: 170.0/255, green: 170.0/255, blue: 170.0/255, alpha: 1.0)

    }

}
