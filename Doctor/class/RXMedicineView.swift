import UIKit
import SnapKit

class RXMedicineView: UIView {
    
    var rxdelegate: RXMedicineViewDelegate?
    
    override init (frame : CGRect) {
        super.init(frame : frame)
    }
    
    convenience init () {
        self.init(frame:CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func configureView (rxData: RxMedicalAdviceData, rxNo:String){
        
        let descLabel = UILabel()
        let lineView = UIView()
        let cancelButton = UIButton()
        
        let MARGIN = 20
        
        self.addSubview(descLabel)
        self.addSubview(cancelButton)
        self.addSubview(lineView)
        
        descLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin).offset(MARGIN)
            make.trailing.equalTo(cancelButton.snp.leading).offset(10)
            make.centerY.equalToSuperview()
        }
        
        descLabel.font = UIFont(name: "Rubik-Regular", size: 15.0)
        descLabel.textColor = UIColor.black
        descLabel.numberOfLines = 3
        
        cancelButton.snp.makeConstraints { (make) in
            make.height.width.equalTo(30)
            make.centerY.equalToSuperview()
            make.trailing.equalTo(self.snp.trailingMargin).offset(-MARGIN)
        }
        
        cancelButton.setImage(#imageLiteral(resourceName: "minus"), for: .normal)
        cancelButton.tag = Int(rxNo)! - 1
        cancelButton.addTarget(self, action: #selector(cancelBtnClicked(_:)), for: UIControlEvents.touchUpInside)
        
        lineView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.bottom.equalTo(self.snp.bottom).offset(-3)
            make.leading.equalTo(descLabel.snp.leading)
            make.trailing.equalTo(cancelButton.snp.trailing)
        }
        
        lineView.backgroundColor = UIColor(red: 170.0/255, green: 170.0/255, blue: 170.0/255, alpha: 1.0)
        
        descLabel.text = getDescString(rxData: rxData, rxNo: rxNo)
    }
    
    func getDescString(rxData: RxMedicalAdviceData, rxNo: String) -> String {
        return "\(rxNo). \(rxData.medicineName), \(rxData.form), \(rxData.dosage)\(rxData.strength), \(rxData.frequency) x \(rxData.duration) \(rxData.durationType), \(rxData.afterFood ? "After Food": "Before Food")"
    }
    
    @objc func cancelBtnClicked(_ sender: UIButton) {
        rxdelegate?.rxCancelBtnClicked(index: sender.tag)
    }
    
}
