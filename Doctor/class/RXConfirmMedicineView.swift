import UIKit
import SnapKit

class RXConfirmMedicineView: UIView {

    override init (frame : CGRect) {
        super.init(frame : frame)
    }

    convenience init () {
        self.init(frame:CGRect.zero)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }

    func configureView(rxData: RxMedicalAdviceData) {
        
        let topView = UIView()
        let bottomView = UIView()

        let medicineLabel = UILabel()
        let genericNameLabel = UILabel()
        let medicineDetailsLabel = UILabel()

        let frequencyLabel = UILabel()
        let remarksLabel = UILabel()
        let durationLabel = UILabel()

        topView.addSubview(medicineLabel)
        topView.addSubview(genericNameLabel)
        topView.addSubview(medicineDetailsLabel)

        bottomView.addSubview(frequencyLabel)
        bottomView.addSubview(remarksLabel)
        bottomView.addSubview(durationLabel)

        self.addSubview(topView)
        self.addSubview(bottomView)

        topView.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin)
            make.trailing.equalTo(self.snp.trailingMargin)
            make.top.equalTo(self.snp.topMargin)
            make.height.equalTo(70)
        }

        topView.backgroundColor = UIColor(red: 238/255, green: 246/255, blue: 250/255, alpha: 1.0)

        bottomView.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin)
            make.trailing.equalTo(self.snp.trailingMargin)
            make.top.equalTo(topView.snp.bottom)
            make.height.equalTo(70)
        }

        bottomView.backgroundColor = UIColor(red: 156/255, green: 213/255, blue: 230/255, alpha: 1.0)

        medicineLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().offset(3)
            make.top.equalTo(topView.snp.topMargin).offset(3)
        }

        medicineLabel.font = UIFont(name: "Rubik-Medium", size: 13.0)
        medicineLabel.text = rxData.medicineName

        genericNameLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().offset(3)
            make.top.equalTo(medicineLabel.snp.bottom).offset(5)
        }

        genericNameLabel.font = UIFont(name: "Rubik-Regular", size: 11.0)
        genericNameLabel.textColor = UIColor(red: 112/255, green: 124/255, blue: 132/255, alpha: 1.0)
        genericNameLabel.text = rxData.genericName

        medicineDetailsLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().offset(3)
            make.top.equalTo(genericNameLabel.snp.bottom).offset(5)
        }

        medicineDetailsLabel.font = UIFont(name: "Rubik-Regular", size: 11.0)
        medicineDetailsLabel.textColor = UIColor(red: 107/255, green: 117/255, blue: 124/255, alpha: 1.0)
        medicineDetailsLabel.text = getMedicineDetails(rxData: rxData)

        frequencyLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().offset(3)
            make.top.equalTo(bottomView.snp.topMargin).offset(5)
        }

        frequencyLabel.font = UIFont(name: "Rubik-Medium", size: 13.0)
        frequencyLabel.text = rxData.frequency

        remarksLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().offset(3)
            make.top.equalTo(frequencyLabel.snp.bottom).offset(5)
        }

        remarksLabel.font = UIFont(name: "Rubik-Regular", size: 11.0)
        remarksLabel.textColor = UIColor(red: 112/255, green: 124/255, blue: 132/255, alpha: 1.0)
        remarksLabel.text = rxData.specialRemarks ?? "No special remarks."

        durationLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().offset(3)
            make.top.equalTo(remarksLabel.snp.bottom).offset(5)
        }

        durationLabel.font = UIFont(name: "Rubik-Regular", size: 11.0)
        durationLabel.textColor = UIColor(red: 107/255, green: 117/255, blue: 124/255, alpha: 1.0)
        durationLabel.text = getMedicineDetails(rxData: rxData)

    }

    func getMedicineDetails(rxData: RxMedicalAdviceData) -> String {
        return "\(rxData.dosage) \(rxData.form)  ●  \(rxData.strength)  ●  \(rxData.afterFood ? "After Food" : "Before Food")"
    }
}
