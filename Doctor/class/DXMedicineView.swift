import UIKit
import SnapKit

class DXMedicineView: UIView, UITextFieldDelegate {

    var testNameLabel = UILabel()
    var testNameTextField = UITextField()
    var testNameLineView = UIView()
    var instructionsLabel = UILabel()
    var instructionsTextField = UITextField()
    var instructionsLineView = UIView()
    var cancelButton = UIButton()
    var dxdelegate: DXMedicineViewDelegate?

    override init (frame : CGRect) {
        super.init(frame : frame)
    }

    convenience init () {
        self.init(frame:CGRect.zero)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }

    func configureView(dxData: DxMedicalAdviceData, index: Int) {

        let MARGIN = 20

        self.addSubview(testNameLabel)
        self.addSubview(testNameTextField)
        self.addSubview(testNameLineView)
        self.addSubview(instructionsLabel)
        self.addSubview(instructionsTextField)
        self.addSubview(instructionsLineView)
        self.addSubview(cancelButton)

        testNameLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin).offset(MARGIN)
            make.top.equalTo(self.snp.topMargin).offset(10)
        }

        testNameLabel.text = "Test Name"
        testNameLabel.textColor = UIColor.gray
        testNameLabel.font = UIFont(name: "Rubik-Regular", size: 10.0)

        testNameTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin).offset(MARGIN)
            make.trailing.equalTo(self.snp.trailingMargin).offset(-MARGIN)
            make.top.equalTo(testNameLabel.snp.bottom).offset(10)
        }

        testNameTextField.font = UIFont(name: "Rubik-Regular", size: 15.0)
        testNameTextField.borderStyle = .none
        testNameTextField.delegate = self
        testNameTextField.placeholder = "Enter Test Name."

        testNameLineView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.top.equalTo(testNameTextField.snp.bottom).offset(10)
            make.leading.equalTo(testNameTextField.snp.leading)
            make.trailing.equalTo(cancelButton.snp.leading).offset(-10)
        }

        testNameLineView.backgroundColor = UIColor(red: 170.0/255, green: 170.0/255, blue: 170.0/255, alpha: 1.0)

        cancelButton.snp.makeConstraints { (make) in
            make.height.width.equalTo(30)
            make.centerY.equalTo(testNameLineView.snp.centerY)
            make.trailing.equalTo(self.snp.trailingMargin).offset(-MARGIN)
        }

        cancelButton.setImage(#imageLiteral(resourceName: "minus"), for: .normal)
        cancelButton.tag = index
        cancelButton.addTarget(self, action: #selector(cancelBtnClicked(_:)), for: UIControlEvents.touchUpInside)

        instructionsLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin).offset(MARGIN)
            make.top.equalTo(testNameLineView.snp.bottom).offset(10)
        }

        instructionsLabel.text = "Instructions"
        instructionsLabel.textColor = UIColor.gray
        instructionsLabel.font = UIFont(name: "Rubik-Regular", size: 10.0)

        instructionsTextField.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leadingMargin).offset(MARGIN)
            make.trailing.equalTo(self.snp.trailingMargin).offset(-MARGIN)
            make.top.equalTo(instructionsLabel.snp.bottom).offset(10)
        }

        instructionsTextField.font = UIFont(name: "Rubik-Regular", size: 15.0)
        instructionsTextField.borderStyle = .none
        instructionsTextField.delegate = self
        instructionsTextField.placeholder = "Enter Instructions."

        instructionsLineView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.top.equalTo(instructionsTextField.snp.bottom).offset(10)
            make.leading.equalTo(instructionsTextField.snp.leading)
            make.trailing.equalTo(instructionsTextField.snp.trailing)
        }

        instructionsLineView.backgroundColor = UIColor(red: 170.0/255, green: 170.0/255, blue: 170.0/255, alpha: 1.0)

    }

    //MARK:- UITextFieldDelegate methods

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @objc func cancelBtnClicked(_ sender: UIButton) {
        dxdelegate?.dxCancelBtnClicked(index: sender.tag)
    }

}
