//
//  InCallViewController.swift
//  Patient
//
//  Created by Manish Verma on 08/06/18.
//  Copyright © 2018 calldoc. All rights reserved.
//

import UIKit

class InComingCallViewController: UIViewController, QBRTCClientDelegate {

    @IBOutlet weak var callStatusLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    var nav: UINavigationController?
    var session: QBRTCSession?
    
    // this userInfo is coming from AppDelegate: func handleIncomingCall(userInfo:Dictionary <String, String>)
    var userInfo:Dictionary <String, String> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let localuser = QBUUser()
        localuser.id = session?.initiatorID as! UInt
        
        QBRequest.user(withID: localuser.id, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
            

            let calltype: String = self.session?.conferenceType == QBRTCConferenceType.video ? "Video call from:  " : "Audio call from: "
            
           
            if (self.userInfo["relation"]?.lowercased() ?? "") == "self" {
                self.callStatusLabel.text = "\(calltype)  \(self.userInfo["patientname"] ?? "Patient")"
            } else {
                let relation = self.userInfo["relation"] ?? ""
                self.callStatusLabel.text = "\(calltype)  \(self.userInfo["patientname"] ?? "Patient") \(relation) of \(self.userInfo["primaryUser"] ?? "")"
            }
            
            
            let patimgURL = self.userInfo["patprofileid"]
            self.profileImageView.sd_setImage(with: URL(string: patimgURL!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
            self.profileImageView.layer.borderColor = UIColor.lightGray.cgColor
            self.profileImageView.layer.borderWidth = 0.2
            self.profileImageView.layer.cornerRadius =  self.profileImageView.bounds.size.height / 2
            self.profileImageView.layer.masksToBounds = true
        }, errorBlock: {(_ response: QBResponse) -> Void in
            print("InComingCallViewController: QBRequest() Error")
        })

        QMSoundManager.playRingtoneSound()
        dialignTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(), target: self, selector: #selector(self.dialing), userInfo: nil, repeats: true)
    }


    @objc func dialing(_ timer: Timer) {
        QMSoundManager.playRingtoneSound()
    }
    
    @IBAction func AcceptCall(_ sender: UIButton!) {
        dialignTimer?.invalidate()
        dialignTimer = nil
        QMSoundManager.instance().stopAllSounds()
        let localuser = QBUUser()
        localuser.id = session?.initiatorID as! UInt
        let inCall = self.storyboard!.instantiateViewController(withIdentifier: "ReceiveCall") as! CallViewIncomingCallController
        inCall.userInfo = userInfo
        //inCall.CreateNewEntrytoConsultTable(userID: localuser.id, userInfo: userInfo)
        self.nav = UINavigationController(rootViewController: inCall)
        //self.nav?.pushViewController(inCall, animated: true)
        self.present(self.nav!, animated: false, completion: nil)
        
    }
    
    @IBAction func RejectCall(_ sender: UIButton!) {
        self.cleanUp()
        self.session?.rejectCall(nil)
        calldisconnected()
    }
    
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        
        print("InComingCallViewController:hungUpByUser")
        session.hangUp(nil)
        gsession = nil
        
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        
        if session.id == self.session?.id {
            gsession = nil
            self.session = nil
            self.calldisconnected()
        }
    }
    
    func calldisconnected() {
        print("IncomingCallViewController: Disconnecting the call...")
        if self.session != nil {
            self.session?.hangUp(nil)
            gsession = nil
        }
        
        QBRTCClient.instance().remove(self)
        dialignTimer?.invalidate()
        dialignTimer = nil
        QBRTCClient.instance().remove(self)
        QMSoundManager.instance().stopAllSounds()
        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    func cleanUp() {
        dialignTimer?.invalidate()
        dialignTimer = nil
        QBRTCClient.instance().remove(self)
        QMSoundManager.instance().stopAllSounds()
    }
    
}
