//
//  HeaderViewWithImage.h
//  Example
//
//  Created by Marek Serafin on 13/10/14.
//  Copyright (c) 2014 Marek Serafin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HeaderViewWithImage : UIView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatordocimg;
@property (weak, nonatomic) IBOutlet UIButton *profileimgButtonview;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelUserRpleType;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfileIcon;
@property (weak, nonatomic) IBOutlet UIButton *circle1;
@property (weak, nonatomic) IBOutlet UIButton *circle2;
@property (weak, nonatomic) IBOutlet UIButton *circle3;
@property (weak, nonatomic) IBOutlet UIView *line1;
@property (weak, nonatomic) IBOutlet UIView *line2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profileimageYConstraint;
+ (instancetype)instantiateFromNib;

@end
