import UIKit

class InvoicesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var invoicesTableView: UITableView!
    var invoiceDetailsArray : [QBCOCustomObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        let prescriptionid: String =  ((/*PatientConsultsdetailsArray*/MyAllConsultsArray[theindexval].fields?.value(forKey: "prescriptionid") as? String) ?? "")

        let charset = CharacterSet(charactersIn: ",")
        if prescriptionid.rangeOfCharacter(from: charset) != nil {
            self.readPrescriptionDetails(prescriptionid: prescriptionid)
        } else {
            self.readSinglePrescriptionDetails(prescriptionid: prescriptionid)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- UItableView delegate and datasource methods

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoiceDetailsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoicesCell", for: indexPath) as! InvoicesCell
        cell.configureView(data: invoiceDetailsArray[indexPath.row])
        return cell
    }

    //MARK:- Storyboard Instance

    static func storyboardInstance() -> InvoicesViewController {
        let storyboard = UIStoryboard(name: "Consult", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "InvoicesViewController") as! InvoicesViewController
    }

    //MARK:- API methods

    func readPrescriptionDetails(prescriptionid: String) {
        QBRequest.objects(withClassName: "PrescriptionTable", ids: [prescriptionid], successBlock: { response, contributors in
            self.invoiceDetailsArray = contributors as! [QBCOCustomObject]
            self.invoicesTableView.reloadData()
        }) { (response) in
            print("PrescriptionTableViewController:ReadyConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
    }

    func readSinglePrescriptionDetails(prescriptionid: String) {
        let getRequest = NSMutableDictionary()
        getRequest["_id"] = prescriptionid

        QBRequest.objects(withClassName: "PrescriptionTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in

            if let contributors = contributors, contributors.count > 0 {
                self.invoiceDetailsArray = contributors
            }

            self.invoicesTableView.reloadData()

        }) { (response) in
            print("PrescriptionTableViewController:ReadyConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
    }

}
