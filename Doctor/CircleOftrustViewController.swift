import UIKit

struct COTglobalVariables {
    
    static var specialityString = ""

}

class CircleOftrustViewController: UIViewController {

    @IBOutlet var label: UILabel!
    @IBOutlet var docimg: UIImageView!
    @IBOutlet var myteamButtonview: UIButton!

    @IBOutlet var cardioButtonview: UIButton!
    @IBOutlet var dermatoioButtonview: UIButton!
    @IBOutlet var dieticianButtonview: UIButton!
    @IBOutlet var endocrinoButtonview: UIButton!
    @IBOutlet var ENTButtonview: UIButton!
    @IBOutlet var GPButtonview: UIButton!
    @IBOutlet var GetroButtonview: UIButton!
    @IBOutlet var GynoButtonview: UIButton!
    @IBOutlet var NephroButtonview: UIButton!
    @IBOutlet var NeuroButtonview: UIButton!
    @IBOutlet var OncologyButtonview: UIButton!
    @IBOutlet var OrthoButtonview: UIButton!
    @IBOutlet var PaediatricButtonview: UIButton!
    @IBOutlet var PsychButtonview: UIButton!
    @IBOutlet var PolmonoButtonview: UIButton!
    @IBOutlet var SexologyButtonview: UIButton!
    @IBOutlet var AltMedButtonview: UIButton!
    
    @IBOutlet var myteamLabel: UILabel!
    @IBOutlet var DermatoLabel: UILabel!
    @IBOutlet var DieticianLabel: UILabel!
    @IBOutlet var EndocrinoLabel: UILabel!
    @IBOutlet var GpLabel: UILabel!
    @IBOutlet var GestroLabel: UILabel!
    @IBOutlet var GynaecoLabel: UILabel!
    @IBOutlet var NephroLabel: UILabel!
    @IBOutlet var NeuroLabel: UILabel!
    @IBOutlet var OncoloLabel: UILabel!
    @IBOutlet var PaediatricLabel: UILabel!
    @IBOutlet var PsychiatristLabel: UILabel!
    @IBOutlet var PulmonoLabel: UILabel!
    @IBOutlet var SexoLabel: UILabel!
    @IBOutlet var AltMedLabel: UILabel!
    @IBOutlet var CardioLabel: UILabel!
    @IBOutlet var EntLabel: UILabel!
    @IBOutlet var OrthoLabel: UILabel!

    @IBOutlet weak var cardiologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var cardiologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var DermatologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var DermatologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var dieticiancontraintsX: NSLayoutConstraint!
    @IBOutlet weak var dieticiancontraintsY: NSLayoutConstraint!
    @IBOutlet weak var endocrinologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var endocrinologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var ENTcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var ENTcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var GPcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var GPcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var GestrocontraintsX: NSLayoutConstraint!
    @IBOutlet weak var GestrocontraintsY: NSLayoutConstraint!
    @IBOutlet weak var GynocontraintsX: NSLayoutConstraint!
    @IBOutlet weak var GynocontraintsY: NSLayoutConstraint!
    @IBOutlet weak var NephrologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var NephrologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var NeurologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var NeurologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var OncologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var OncologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var OrthocontraintsX: NSLayoutConstraint!
    @IBOutlet weak var OrthocontraintsY: NSLayoutConstraint!
    @IBOutlet weak var PaediatriciancontraintsX: NSLayoutConstraint!
    @IBOutlet weak var PaediatriciancontraintsY: NSLayoutConstraint!
    @IBOutlet weak var PsychiatristcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var PsychiatristcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var PolmonoligistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var PolmonoligistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var SexologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var SexologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var AltMedcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var AltMedcontraintsY: NSLayoutConstraint!
    
    // @IBOutlet weak var bottonlabelYConstraint: NSLayoutConstraint!
    @IBOutlet weak var toplabelYConstraint: NSLayoutConstraint!
    @IBOutlet weak var myteamhrzlinewidthConstraint: NSLayoutConstraint!
    
    var nav: UINavigationController?
    var myteamonoffbit:  Bool = false
    var DocCountTableRecordID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.myteamonoffbit = UserDefaults.standard.bool(forKey: "teamswitch")

        cardioButtonview.setTitle("    Cardiologist",for: .normal)
        dermatoioButtonview.setTitle("    Dermatologist",for: .normal)
        dieticianButtonview.setTitle("    Dietician", for: .normal)
        endocrinoButtonview.setTitle("    Endocrinologist", for: .normal)
        ENTButtonview.setTitle("    E.N.T", for: .normal)
        GPButtonview.setTitle("    G.P.", for: .normal)
        GetroButtonview.setTitle("    Gestroentrologist", for: .normal)
        GynoButtonview.setTitle("    Gynaecologist", for: .normal)
        NephroButtonview.setTitle("    Nephrologist", for: .normal)
        NeuroButtonview.setTitle("    Neurologist", for: .normal)
        OncologyButtonview.setTitle("    Oncologist", for: .normal)
        OrthoButtonview.setTitle("    Orthopaedic", for: .normal)
        PaediatricButtonview.setTitle("    Paediatrician", for: .normal)
        PsychButtonview.setTitle("    Psychiatrist", for: .normal)
        PolmonoButtonview.setTitle("    Pulmonoligist", for: .normal)
        SexologyButtonview.setTitle("    Sexologist", for: .normal)
        AltMedButtonview.setTitle("    Alt Medicine", for: .normal)
        
        
        if (self.myteamonoffbit) {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteam_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamcotbg"), for: .normal)
            }
        } else {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteamred_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamredcotbg"), for: .normal)
            }
        }
        if phone && maxLength == 568 {
            //iphone 5
            docimg.layer.cornerRadius = 32.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            toplabelYConstraint.constant = 3
            NephrologistcontraintsY.constant = 33
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            docimg.layer.cornerRadius = 37.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            toplabelYConstraint.constant = 12
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            docimg.layer.cornerRadius = 40.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            toplabelYConstraint.constant = 22
            label.font =  UIFont(name: "Rubik-Medium", size: 14.7)
        }
        
        if phone && maxLength == 812 {
            //iphone x
            docimg.layer.cornerRadius = 38.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            toplabelYConstraint.constant = 22
            GestrocontraintsY.constant = 60
            NephrologistcontraintsY.constant = 50
            label.font =  UIFont(name: "Rubik-Medium", size: 13)
        }
        if ipad && maxLength == 1024 {
            //iPad
            docimg.layer.cornerRadius = 77.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            myteamhrzlinewidthConstraint.constant = 5
            toplabelYConstraint.constant = 25
            cardiologistcontraintsY.constant = 10
            GestrocontraintsY.constant = 110
            NephrologistcontraintsY.constant = 91
            dermatoioButtonview.frame.size = CGSize(width: 50, height: 50)
            
        }
        
        self.docimg.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profilenonedit"), options: .cacheMemoryOnly)

        let isonline = UserDefaults.standard.object(forKey: "doctoronline") as! Bool
        
        if(isonline) {
            docimg.layer.borderColor = UIColor.green.cgColor
        } else {
            docimg.layer.borderColor = UIColor.orange.cgColor
        }

        self.samespecialityButtondisabeling()
        
        let helpButton = UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self,  action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpButton;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.addbadgebuttoncounts()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UserDefaults.standard.set(GlobalVariables.teamoffon, forKey: "teamswitch")  // global variable has been read in HomeViewController when reading COTtable
        self.myteamonoffbit = UserDefaults.standard.bool(forKey: "teamswitch")
        UserDefaults.standard.synchronize()
        
        if (self.myteamonoffbit) {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteam_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamcotbg"), for: .normal)
            }
        } else {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteamred_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamredcotbg"), for: .normal)
            }
        }
    }
    
    func LoadProfileImage() {
        let defaults: UserDefaults? = UserDefaults.standard
        var fetchPersistantValUrl: URL? = defaults?.url(forKey: "keyforProfileurl")
        if fetchPersistantValUrl != nil {
            let theURL: String? = fetchPersistantValUrl?.absoluteString
            GlobalDocData.gprofileURL = theURL!
            
            self.docimg.sd_setImage(with: URL(string: theURL!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
            fetchPersistantValUrl = nil
        }
    }

    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    @IBAction func MyTeamButton(_ sender: UIButton) {
        let transition = CATransition.init()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromLeft
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        let myteamviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTeamView") as! MyTeamViewController
        myteamviewController.teamswitchonoffbit = self.myteamonoffbit
        myteamviewController.DocCounttablerecordID = self.DocCountTableRecordID
        self.navigationController?.pushViewController(myteamviewController, animated: true)
    }
    
    @IBAction func DoctorProfileButton(_ sender: UIButton) {
        let vc = SelfProfileViewController.storyboardInstance()
        vc.imageURLpassed = GlobalDocData.gprofileURL
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func HeartButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Cardiologist"
        GlobalDocData.cotspecialityselected = "Cardiologist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func AlternateMedicineButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Alt Medicine"
        GlobalDocData.cotspecialityselected = "Alt Medicine"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }

    @IBAction func DermatologistButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Dermatologist"
        GlobalDocData.cotspecialityselected = "Dermatologist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func GastroentrologistButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Gastroentrologist"
        GlobalDocData.cotspecialityselected = "Gastroentrologist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func ENTButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "E.N.T."
        GlobalDocData.cotspecialityselected = "E.N.T."
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func EndocrinologistButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Endocrinologist"
        GlobalDocData.cotspecialityselected = "Endocrinologist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func PulmonoligistButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Pulmonoligist"
        GlobalDocData.cotspecialityselected = "Pulmonoligist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func PediatricianButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Pediatrician"
        GlobalDocData.cotspecialityselected = "Pediatrician"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func UrinaryButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Nephrologist"
        GlobalDocData.cotspecialityselected = "Nephrologist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func GeneralHealthButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "G.P."
        GlobalDocData.cotspecialityselected = "G.P."
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func GynaecologistButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Gynaecologist"
        GlobalDocData.cotspecialityselected = "Gynaecologist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func NeurologistButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Neurologist"
        GlobalDocData.cotspecialityselected = "Neurologist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func DieticianButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Dietician"
        GlobalDocData.cotspecialityselected = "Dietician"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func PsychiatristButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Psychiatrist"
        GlobalDocData.cotspecialityselected = "Psychiatrist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func OncologistButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Oncologist"
        GlobalDocData.cotspecialityselected = "Oncologist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }

    @IBAction func OrthopedicButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Orthopedic"
        GlobalDocData.cotspecialityselected = "Orthopedic"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }

    @IBAction func SexologistButton(_ sender: UIButton) {
        let mytrustedviewController = self.storyboard!.instantiateViewController(withIdentifier: "MyTrustedNWView") as! DocTrustedNetworkViewController
        mytrustedviewController.stringpassed = "Sexologist"
        GlobalDocData.cotspecialityselected = "Sexologist"
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    func samespecialityButtondisabeling() {
        if GlobalVariables.gdocspeciality == "cardiologist" {
            cardioButtonview.isEnabled = false
            cardioButtonview.backgroundColor = UIColor.white
            cardioButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "sexologist" {
            SexologyButtonview.isEnabled = false
            SexologyButtonview.backgroundColor = UIColor.white
            SexologyButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "orthopaedic" {
            OrthoButtonview.isEnabled = false
            OrthoButtonview.backgroundColor = UIColor.white
            OrthoButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "oncologist" {
            OncologyButtonview.isEnabled = false
            OncologyButtonview.backgroundColor = UIColor.white
            OncologyButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "psychiatrist" {
            PsychButtonview.isEnabled = false
            PsychButtonview.backgroundColor = UIColor.white
            PsychButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "dietician" {
            dieticianButtonview.isEnabled = false
            dieticianButtonview.backgroundColor = UIColor.white
            dieticianButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "nephrologist" {
            NephroButtonview.isEnabled = false
            NephroButtonview.backgroundColor = UIColor.white
            NephroButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "neurologist" {
            NeuroButtonview.isEnabled = false
            NeuroButtonview.backgroundColor = UIColor.white
            NeuroButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "gynaecologist" {
            GynoButtonview.isEnabled = false
            GynoButtonview.backgroundColor = UIColor.white
            GynoButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "general" {
            GPButtonview.isEnabled = false
            GPButtonview.backgroundColor = UIColor.white
            GPButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "paediatrician" {
            PaediatricButtonview.isEnabled = false
            PaediatricButtonview.backgroundColor = UIColor.white
            PaediatricButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "pulmonoligist" {
            PolmonoButtonview.isEnabled = false
            PolmonoButtonview.backgroundColor = UIColor.white
            PolmonoButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "endocrinologist" {
            endocrinoButtonview.isEnabled = false
            endocrinoButtonview.backgroundColor = UIColor.white
            endocrinoButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "ent" {
            ENTButtonview.isEnabled = false
            ENTButtonview.backgroundColor = UIColor.white
            ENTButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "gastroentrologist" {
            GetroButtonview.isEnabled = false
            GetroButtonview.backgroundColor = UIColor.white
            GetroButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "dermatologist" {
            dermatoioButtonview.isEnabled = false
            dermatoioButtonview.backgroundColor = UIColor.white
            dermatoioButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        } else if GlobalVariables.gdocspeciality == "altmedicine" {
            AltMedButtonview.isEnabled = false
            AltMedButtonview.backgroundColor = UIColor.white
            AltMedButtonview.setTitleColor(UIColor.lightGray, for: .disabled)
        }
        
    }
    
    func addbadgebuttoncounts() {
        //Read DocCount Table
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("NotificationTableViewController:addbadgebuttoncounts(): Reading DocCount Table !")
            let invitorRecordID = (contributors![0].id)!
            self.DocCountTableRecordID = invitorRecordID
            let dermatologist =  contributors![0].fields?.value(forKey: "dermatologist") as? Int
            let dietician =  contributors![0].fields?.value(forKey: "dietician") as? Int
            let cardiologist =  contributors![0].fields?.value(forKey: "cardiologist") as? Int
            let ent =  contributors![0].fields?.value(forKey: "ent") as? Int
            let general =  contributors![0].fields?.value(forKey: "general") as? Int
            let endocrinologist =  contributors![0].fields?.value(forKey: "endocrinologist") as? Int
            let gynaecologist =  contributors![0].fields?.value(forKey: "gynaecologist") as? Int
            let nephrologist =  contributors![0].fields?.value(forKey: "nephrologist") as? Int
            let neurologist =  contributors![0].fields?.value(forKey: "neurologist") as? Int
            let oncologist =  contributors![0].fields?.value(forKey: "oncologist") as? Int
            let gastroentrologist =  contributors![0].fields?.value(forKey: "gastroentrologist") as? Int
            let paediatrician =  contributors![0].fields?.value(forKey: "paediatrician") as? Int
            let psychiatrist =  contributors![0].fields?.value(forKey: "psychiatrist") as? Int
            let orthopaedic =  contributors![0].fields?.value(forKey: "orthopaedic") as? Int
            let sexologist =  contributors![0].fields?.value(forKey: "sexologist") as? Int
            let altmedicine =  contributors![0].fields?.value(forKey: "altmedicine") as? Int
            let pulmonoligist =  contributors![0].fields?.value(forKey: "pulmonoligist") as? Int
            let myteam = contributors![0].fields?.value(forKey: "myTeam") as? Int
            let teamstatus = contributors![0].fields?.value(forKey: "teamStatus") as? Bool
            
            self.updateDocCountOnCOTView(myteam: myteam!, teamstatus: teamstatus!, dermatologist: dermatologist!, dietician: dietician!, cardiologist: cardiologist!, ent: ent!, general: general!, endocrinologist: endocrinologist!,  gynaecologist: gynaecologist!, nephrologist: nephrologist!, neurologist: neurologist!, oncologist: oncologist!, gastroentrologist: gastroentrologist!, paediatrician: paediatrician!, psychiatrist: psychiatrist!, orthopaedic: orthopaedic!, sexologist: sexologist!, altmedicine: altmedicine!, pulmonoligist: pulmonoligist!)
            
        }) { (errorresponse) in
            print("NotificationTableViewController:addbadgebuttoncounts() Response error: \(String(describing: errorresponse.error?.description))")
        }

    }

    func updateDocCountOnCOTView(myteam: Int, teamstatus: Bool,  dermatologist: Int, dietician: Int, cardiologist: Int, ent: Int, general: Int, endocrinologist: Int,  gynaecologist: Int, nephrologist: Int, neurologist: Int, oncologist: Int, gastroentrologist: Int, paediatrician: Int, psychiatrist: Int, orthopaedic: Int, sexologist: Int, altmedicine: Int, pulmonoligist: Int) {
        // team button color for ON/Off of team button

        if (teamstatus) {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteam_ipad"), for: .normal)
                    self.myteamonoffbit = true
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamcotbg"), for: .normal)
                self.myteamonoffbit = true
            }
        } else {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteamred_ipad"), for: .normal)
                    self.myteamonoffbit = false
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamredcotbg"), for: .normal)
                self.myteamonoffbit = false
            }
        }
        myteamLabel.layer.borderColor = UIColor.clear.cgColor
        myteamLabel.layer.borderWidth = 1
        myteamLabel.layer.cornerRadius = myteamLabel.bounds.size.height / 2
        myteamLabel.textAlignment = .center
        myteamLabel.layer.masksToBounds = true
        myteamLabel.textColor = .black
        myteamLabel.font = label.font.withSize(11)
        myteamLabel.backgroundColor = .white
        myteamLabel.text = String(myteam)

        CardioLabel.layer.borderColor = UIColor.clear.cgColor
        CardioLabel.layer.borderWidth = 1
        CardioLabel.layer.cornerRadius = CardioLabel.bounds.size.height / 2
        CardioLabel.textAlignment = .center
        CardioLabel.layer.masksToBounds = true
        CardioLabel.textColor = .black
        CardioLabel.font = label.font.withSize(11)
        CardioLabel.backgroundColor = .white
        CardioLabel.text = String(cardiologist)

        DermatoLabel.layer.borderColor = UIColor.clear.cgColor
        DermatoLabel.layer.borderWidth = 1
        DermatoLabel.layer.cornerRadius = DermatoLabel.bounds.size.height / 2
        DermatoLabel.textAlignment = .center
        DermatoLabel.layer.masksToBounds = true
        DermatoLabel.textColor = .black
        DermatoLabel.font = label.font.withSize(11)
        DermatoLabel.backgroundColor = .white
        DermatoLabel.text = String(dermatologist)

        DieticianLabel.layer.borderColor = UIColor.clear.cgColor
        DieticianLabel.layer.borderWidth = 1
        DieticianLabel.layer.cornerRadius = DieticianLabel.bounds.size.height / 2
        DieticianLabel.textAlignment = .center
        DieticianLabel.layer.masksToBounds = true
        DieticianLabel.textColor = .black
        DieticianLabel.font = label.font.withSize(11)
        DieticianLabel.backgroundColor = .white
        DieticianLabel.text = String(dietician)

        GpLabel.layer.borderColor = UIColor.clear.cgColor
        GpLabel.layer.borderWidth = 1
        GpLabel.layer.cornerRadius = GpLabel.bounds.size.height / 2
        GpLabel.textAlignment = .center
        GpLabel.layer.masksToBounds = true
        GpLabel.textColor = .black
        GpLabel.font = label.font.withSize(11)
        GpLabel.backgroundColor = .white
        GpLabel.text = String(general)

        EndocrinoLabel.layer.borderColor = UIColor.clear.cgColor
        EndocrinoLabel.layer.borderWidth = 1
        EndocrinoLabel.layer.cornerRadius = EndocrinoLabel.bounds.size.height / 2
        EndocrinoLabel.textAlignment = .center
        EndocrinoLabel.layer.masksToBounds = true
        EndocrinoLabel.textColor = .black
        EndocrinoLabel.font = label.font.withSize(11)
        EndocrinoLabel.backgroundColor = .white
        EndocrinoLabel.text = String(endocrinologist)

        GynaecoLabel.layer.borderColor = UIColor.clear.cgColor
        GynaecoLabel.layer.borderWidth = 1
        GynaecoLabel.layer.cornerRadius = GynaecoLabel.bounds.size.height / 2
        GynaecoLabel.textAlignment = .center
        GynaecoLabel.layer.masksToBounds = true
        GynaecoLabel.textColor = .black
        GynaecoLabel.font = label.font.withSize(11)
        GynaecoLabel.backgroundColor = .white
        GynaecoLabel.text = String(gynaecologist)

        NephroLabel.layer.borderColor = UIColor.clear.cgColor
        NephroLabel.layer.borderWidth = 1
        NephroLabel.layer.cornerRadius = NephroLabel.bounds.size.height / 2
        NephroLabel.textAlignment = .center
        NephroLabel.layer.masksToBounds = true
        NephroLabel.textColor = .black
        NephroLabel.font = label.font.withSize(11)
        NephroLabel.backgroundColor = .white
        NephroLabel.text = String(nephrologist)

        NeuroLabel.layer.borderColor = UIColor.clear.cgColor
        NeuroLabel.layer.borderWidth = 1
        NeuroLabel.layer.cornerRadius = NeuroLabel.bounds.size.height / 2
        NeuroLabel.textAlignment = .center
        NeuroLabel.layer.masksToBounds = true
        NeuroLabel.textColor = .black
        NeuroLabel.font = label.font.withSize(11)
        NeuroLabel.backgroundColor = .white
        NeuroLabel.text = String(neurologist)

        OncoloLabel.layer.borderColor = UIColor.clear.cgColor
        OncoloLabel.layer.borderWidth = 1
        OncoloLabel.layer.cornerRadius = OncoloLabel.bounds.size.height / 2
        OncoloLabel.textAlignment = .center
        OncoloLabel.layer.masksToBounds = true
        OncoloLabel.textColor = .black
        OncoloLabel.font = label.font.withSize(11)
        OncoloLabel.backgroundColor = .white
        OncoloLabel.text = String(oncologist)

        GestroLabel.layer.borderColor = UIColor.clear.cgColor
        GestroLabel.layer.borderWidth = 1
        GestroLabel.layer.cornerRadius = GestroLabel.bounds.size.height / 2
        GestroLabel.textAlignment = .center
        GestroLabel.layer.masksToBounds = true
        GestroLabel.textColor = .black
        GestroLabel.font = label.font.withSize(11)
        GestroLabel.backgroundColor = .white
        GestroLabel.text = String(gastroentrologist)

        PaediatricLabel.layer.borderColor = UIColor.clear.cgColor
        PaediatricLabel.layer.borderWidth = 1
        PaediatricLabel.layer.cornerRadius = PaediatricLabel.bounds.size.height / 2
        PaediatricLabel.textAlignment = .center
        PaediatricLabel.layer.masksToBounds = true
        PaediatricLabel.textColor = .black
        PaediatricLabel.font = label.font.withSize(11)
        PaediatricLabel.backgroundColor = .white
        PaediatricLabel.text = String(paediatrician)

        PsychiatristLabel.layer.borderColor = UIColor.clear.cgColor
        PsychiatristLabel.layer.borderWidth = 1
        PsychiatristLabel.layer.cornerRadius = PsychiatristLabel.bounds.size.height / 2
        PsychiatristLabel.textAlignment = .center
        PsychiatristLabel.layer.masksToBounds = true
        PsychiatristLabel.textColor = .black
        PsychiatristLabel.font = label.font.withSize(11)
        PsychiatristLabel.backgroundColor = .white
        PsychiatristLabel.text = String(psychiatrist)

        SexoLabel.layer.borderColor = UIColor.clear.cgColor
        SexoLabel.layer.borderWidth = 1
        SexoLabel.layer.cornerRadius = SexoLabel.bounds.size.height / 2
        SexoLabel.textAlignment = .center
        SexoLabel.layer.masksToBounds = true
        SexoLabel.textColor = .black
        SexoLabel.font = label.font.withSize(11)
        SexoLabel.backgroundColor = .white
        SexoLabel.text = String(sexologist)

        AltMedLabel.layer.borderColor = UIColor.clear.cgColor
        AltMedLabel.layer.borderWidth = 1
        AltMedLabel.layer.cornerRadius = AltMedLabel.bounds.size.height / 2
        AltMedLabel.textAlignment = .center
        AltMedLabel.layer.masksToBounds = true
        AltMedLabel.textColor = .black
        AltMedLabel.font = label.font.withSize(11)
        AltMedLabel.backgroundColor = .white
        AltMedLabel.text = String(altmedicine)

        PulmonoLabel.layer.borderColor = UIColor.clear.cgColor
        PulmonoLabel.layer.borderWidth = 1
        PulmonoLabel.layer.cornerRadius = PulmonoLabel.bounds.size.height / 2
        PulmonoLabel.textAlignment = .center
        PulmonoLabel.layer.masksToBounds = true
        PulmonoLabel.textColor = .black
        PulmonoLabel.font = label.font.withSize(11)
        PulmonoLabel.backgroundColor = .white
        PulmonoLabel.text = String(pulmonoligist)

        EntLabel.layer.borderColor = UIColor.clear.cgColor
        EntLabel.layer.borderWidth = 1
        EntLabel.layer.cornerRadius = EntLabel.bounds.size.height / 2
        EntLabel.textAlignment = .center
        EntLabel.layer.masksToBounds = true
        EntLabel.textColor = .black
        EntLabel.font = label.font.withSize(11)
        EntLabel.backgroundColor = .white
        EntLabel.text = String(ent)

        OrthoLabel.layer.borderColor = UIColor.clear.cgColor
        OrthoLabel.layer.borderWidth = 1
        OrthoLabel.layer.cornerRadius = OrthoLabel.bounds.size.height / 2
        OrthoLabel.textAlignment = .center
        OrthoLabel.layer.masksToBounds = true
        OrthoLabel.textColor = .black
        OrthoLabel.font = label.font.withSize(11)
        OrthoLabel.backgroundColor = .white
        OrthoLabel.text = String(orthopaedic)

    }
    
}
