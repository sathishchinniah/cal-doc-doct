import UIKit
import Toast_Swift

class PrescriptionTableViewController: UITableViewController {
    
    @IBOutlet weak var prescriptiontbl: UITableView!

    var confirmMedicalAdviceHeight: [CGFloat] = [69, 71, 60, 60, 50, 180, 60, 145, 60, 61, 140, 122]
    var skipRxConfirmMedicalAdviceHeight: [CGFloat] = [69, 71, 60, 0, 0, 0, 0, 0, 0, 0, 140, 122]
    var prescriptionDetailsArray : [QBCOCustomObject] = []
    var isSkip: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let prescriptionidstring  =  /*PatientConsultsdetailsArray*/MyAllConsultsArray[theindexval].fields?.value(forKey: "prescriptionid") as? String {
            
            let charset = CharacterSet(charactersIn: ",")
            if prescriptionidstring.rangeOfCharacter(from: charset) != nil {
                self.ReadPrescriptionDetails(prescriptionid: prescriptionidstring)
            } else {
                self.ReadSinglePrescriptionDetails(prescriptionid: prescriptionidstring)
            }

        } else {

            isSkip = true
            skipRxConfirmMedicalAdviceHeight = [69, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            self.prescriptiontbl.reloadData()

            var style = ToastStyle()
            style.messageColor = .lightGray
            ToastManager.shared.style = style
            ToastManager.shared.isTapToDismissEnabled = true
            /*
            self.view.makeToast("Prescription ID is nil", duration: 1000.0, position: .center, title: "", image: UIImage(named: "toastWarning"), style: style) { (didTap) in
                if didTap {
                    print("Toast completion from tap")
                } else {
                    print("Toast completion without tap")
                }
            } */

        }

    }

    // MARK: - Table view Datasource and Delegate methods

    override func numberOfSections(in tableView: UITableView) -> Int {
        if prescriptionDetailsArray.count > 10 {
            return 10  // Currently restricted max prescriptions to 10.
        }
        return prescriptionDetailsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell0
            cell.configureUI(dateVal: thedateval)
            return cell
        }

        if indexPath.row == 1 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell1
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 2 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell2
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 3 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell3
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 5 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell4
            let rxdatastring  =  prescriptionDetailsArray[indexPath.section].fields.value(forKey: "rxdata") as? String
            cell.configureRxView(rxMedicines: decodeRxData(rxText: rxdatastring))
            return cell
        }

        if indexPath.row == 7 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell5
            let dxdatastring  =  prescriptionDetailsArray[indexPath.section].fields.value(forKey: "dxdata") as? String
            cell.configureDxView(dxMedicines: decodeDXData(dxText: dxdatastring))
            return cell
        }

        if indexPath.row == 8 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell6
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 9 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell7
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 10 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell8
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 11 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell9
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as UITableViewCell
        return cell

    }

    override func tableView(_ prescriptiontbl: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (isSkip) {
            return skipRxConfirmMedicalAdviceHeight[indexPath.row]
        }

        let rxdatastring  =  prescriptionDetailsArray[indexPath.section].fields.value(forKey: "rxdata") as? String
        let rxMedicines = decodeRxData(rxText: rxdatastring)

        let dxdatastring  =  prescriptionDetailsArray[indexPath.section].fields.value(forKey: "dxdata") as? String
        let dxMedicines = decodeDXData(dxText: dxdatastring)

        if indexPath.row == 4 {
            if rxMedicines.count == 0 {
                return 0
            }
        }
        
        if indexPath.row == 5 {
            return CGFloat(180 * rxMedicines.count)
        }
        
        if indexPath.row == 6 {
            if dxMedicines.count == 0 {
                return 0
            }
        }
        
        if indexPath.row == 7 {
            return CGFloat(145 * dxMedicines.count)
        }
        
        return confirmMedicalAdviceHeight[indexPath.row]
    }

    // MARK: - Custom methods

    func ReadPrescriptionDetails(prescriptionid: String) {
        QBRequest.objects(withClassName: "PrescriptionTable", ids: [prescriptionid], successBlock: { response, objects in
            self.prescriptionDetailsArray = objects as! [QBCOCustomObject]
            self.prescriptiontbl.reloadData()
        }, errorBlock: { response in
            if let aDescription = response.error?.description {
                print("Response error: \(aDescription)")
            }
        })
    }
    
    func ReadSinglePrescriptionDetails(prescriptionid: String) {
        
        let getRequest = NSMutableDictionary()
        // getRequest["user_id"] = QBSession.current.currentUser?.id
        getRequest["_id"] = prescriptionid
        
        QBRequest.objects(withClassName: "PrescriptionTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("PrescriptionTableViewController:ReadSinglePrescriptionDetails: Successfully Read Prescription from Server !")
            if ((contributors?.count)! > 0)
            {
                print("PrescriptionTableViewController:ReadSinglePrescriptionDetails: PrescriptiondetailsArray not empty")
                self.prescriptionDetailsArray = contributors!
            }
            else
            {
                print("PrescriptionTableViewController:ReadSinglePrescriptionDetails: PrescriptiondetailsArray is empty")
                self.prescriptionDetailsArray = contributors!
            }
            
            self.prescriptiontbl.reloadData()
            
        }) { (response) in
            //Error handling
            print("PrescriptionTableViewController:ReadyConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
    }

    private func decodeRxData(rxText: String?) -> [RxMedicalAdviceData] {
        var rxDatas = [RxMedicalAdviceData]()

        if let rxStrings = rxText?.components(separatedBy: "<>") {
            for rx in rxStrings {
                if rx.count > 3 {
                    let rxContents = rx.components(separatedBy: "|")
                    if rxContents.count == 10 {
                        var rxData = RxMedicalAdviceData()
                        rxData.medicineName = rxContents[0]
                        rxData.genericName = rxContents[1]
                        rxData.dosage = rxContents[2]
                        rxData.form = rxContents[3]
                        rxData.strength = rxContents[4]
                        rxData.afterFood = "\(rxContents[5])" == "After Food" ? true : false
                        rxData.frequency = rxContents[6]
                        rxData.specialRemarks = rxContents[7]
                        rxData.duration = rxContents[8]
                        rxData.durationType = rxContents[9]
                        rxDatas.append(rxData)
                    }
                }
            }
        }

        return rxDatas
    }

    private func decodeDXData(dxText: String?) -> [DxMedicalAdviceData] {
        var dxDatas = [DxMedicalAdviceData]()

        if let dxStrings = dxText?.components(separatedBy: "<>") {
            for dx in dxStrings {
                if dx.count > 3 {
                    let dxContents = dx.components(separatedBy: "|")
                    if dxContents.count == 2 {
                        var dxData = DxMedicalAdviceData()
                        dxData.testName = dxContents[0]
                        dxData.instructions = dxContents[1]
                        dxDatas.append(dxData)
                    }
                }
            }
        }

        return dxDatas
    }
    
}
