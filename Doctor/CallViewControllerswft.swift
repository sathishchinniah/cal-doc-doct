import UIKit
import Quickblox
import QuickbloxWebRTC
import SVProgressHUD
import AVFoundation
import Toast_Swift

private let kRefreshTimeInterval = 1.0 as? TimeInterval

class CallViewControllerswft: UIViewController, QBRTCClientDelegate {

    let defRect = CGRect.init(x: 0, y: 0, width: 48, height: 48)
    let defBgClr = UIColor.init(red: 0.8118, green: 0.8118, blue: 0.8118, alpha: 1.0)
    let defSlctClr = UIColor.init(red: 0.3843, green: 0.3843, blue: 0.3843, alpha: 1.0)
    
    @IBOutlet weak var calllabel: UILabel!
    @IBOutlet weak var toolBar: Toolbar!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var calltimelb: UILabel!
    @IBOutlet weak var oponentVideoView: QBRTCRemoteVideoView!
    @IBOutlet weak var localvideoView: UIView!
    
    var nav: UINavigationController?
    open var opponets: [QBUUser]?
    open var currentUser: QBUUser?
    
    var views: [UIView] = []
    var videoCapture: QBRTCCameraCapture!
    var session: QBRTCSession?
    var callTimer: Timer?
    var timeDuration = TimeInterval()
    
    var callingnumber: UInt = 0
    var beepTimer: Timer?
    var patientname = ""
    var ConsultRecordID = ""
    
    var topView: UIView!
    var profileImageView: UIImageView!
    var profilePic: UIImageView!
    var videoCallButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        QBRTCClient.initializeRTC()
        QBRTCClient.instance().add(self)
        cofigureVideo()
        configureAudio()
        self.oponentVideoView.backgroundColor = UIColor.white
        self.oponentVideoView.layer.backgroundColor = UIColor.red.cgColor
        let topView = UIView(frame: CGRect(x: self.oponentVideoView.frame.origin.x, y: self.oponentVideoView.frame.origin.y, width: self.oponentVideoView.frame.width, height: self.oponentVideoView.frame.height))
        topView.backgroundColor = UIColor.darkGray
        self.oponentVideoView.addSubview(topView)
        navigationController?.isNavigationBarHidden = true
        self.navigationItem.setHidesBackButton(true, animated:true)
        toolBar.isHidden = true
        configureToolbar()
        self.toolBar.isHidden = false
        self.calllabel.text = "Calling : " + patientname
        
        beepTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(), target: self, selector: #selector(self.playCallingSound), userInfo: nil, repeats: true)
        playCallingSound((Any).self)
        let remotenumber: NSNumber
        remotenumber = NSNumber(value: callingnumber) // 14566
        self.session = QBRTCClient.instance().createNewSession(withOpponents: [remotenumber] as [NSNumber],
                                                               with: .video)
        self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
        self.session?.localMediaStream.videoTrack.isEnabled = false
        configureTopScreen()
        
        self.session?.startCall(["info" : "user info"])
        sendPushToOpponentsAboutNewCall()
    }

    
    func configureTopScreen() {
        
        topView = UIView(frame: self.view.frame)
        profileImageView = UIImageView(frame: CGRect(x: (self.view.frame.width/2)-60, y: (self.view.frame.height/2)-60, width: 120.0, height: 120.0))
        profileImageView.image = #imageLiteral(resourceName: "logo")
        videoCallButton = UIButton(frame: CGRect(x: (self.view.frame.width/2)+40, y: (self.view.frame.height/2)-40, width: 80.0, height: 80.0))
        videoCallButton.setImage(#imageLiteral(resourceName: "callblk"), for: .normal)
        videoCallButton.addTarget(self, action: #selector(indicateVideo(_:)), for: .touchUpInside)
        
        self.view.addSubview(videoCallButton)
        topView.addSubview(profileImageView)
        topView.backgroundColor = UIColor.white
        
        self.oponentVideoView.addSubview(topView)
        if profilePic != nil {
                self.localvideoView?.layer.addSublayer((profilePic.layer))
        }
        
    }
    
    @objc func indicateVideo(_ sender: UIButton) {
        print("VIDEO CALL TAPPED")
        
        let payload = NSMutableDictionary()
        
        payload.setValue("Request to turn on the video", forKey: "title")
        payload.setValue(100, forKey: "code")
        payload.setValue("5", forKey: "ios_badge")
        payload.setValue("mysound.wav", forKey: "ios_sound")
        payload.setValue(String(Int((self.session?.currentUserID) ?? 0)), forKey: "user_id")
        payload.setValue("10", forKey: "thread_id")
        
        
        
        let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted)
        
        let message = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = (self.session?.opponentsIDs as NSArray?)?.componentsJoined(by: ",")//userInfo["patId"]
        event.type = QBMEventType.oneShot
        event.message = message
        
        var style = ToastStyle()
        style.messageColor = .white
        ToastManager.shared.style = style
        
        QBRequest.createEvent(event, successBlock: { (_, _) in
            print("EVENT CREATED SUCCESSFULLY")
            self.view.makeToast("Notification sent to the user", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
        }) { (error:QBResponse?) in
            print("ERROR: \(error)")
            self.view.makeToast("Couldn't notify patient. Please try again later.", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
        }
        
    }
    
    //MARK: WebRTC configuration
    
    func cofigureVideo() {
        
        /*QBRTCConfig.mediaStreamConfiguration().videoCodec = .H264
        
        let videoFormat = QBRTCVideoFormat.init()
        videoFormat.frameRate = 21
        videoFormat.pixelFormat = .format420f
        videoFormat.width = 640
        videoFormat.height = 480
        
        self.videoCapture = QBRTCCameraCapture.init(videoFormat: videoFormat, position: .front)
        self.videoCapture.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.videoCapture.startSession {
        if !((self.session?.localMediaStream.videoTrack.isEnabled) ?? false) {
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.videoCapture!.previewLayer.frame = self.localvideoView.bounds
            self.localvideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
            self.localvideoView.isHidden = false
        } else {
            self.localvideoView.isHidden = true
            
        }
            
            self.localvideoView.layer.cornerRadius = self.localvideoView.frame.size.width / 2;
            self.localvideoView.clipsToBounds = true;
            self.localvideoView.layer.borderWidth = 0.2
            self.localvideoView.layer.borderColor = UIColor.lightGray.cgColor
        } */
        print("CONFIGUREVIDEO")
        
        QBRTCConfig.mediaStreamConfiguration().videoCodec = .H264
        
        let videoFormat = QBRTCVideoFormat.init()
        videoFormat.frameRate = 21
        videoFormat.pixelFormat = .format420f
        videoFormat.width = 640
        videoFormat.height = 480
        
        self.videoCapture = QBRTCCameraCapture.init(videoFormat: videoFormat, position: .front)
        self.videoCapture.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.videoCapture.startSession {
            
            self.localvideoView.layer.cornerRadius = self.localvideoView.frame.size.width / 2;
            self.localvideoView.clipsToBounds = true;
            self.localvideoView.layer.borderWidth = 0.2
            self.localvideoView.layer.borderColor = UIColor.lightGray.cgColor
            
            if self.session?.localMediaStream.videoTrack.isEnabled == true {
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localvideoView.bounds
                self.localvideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
            } else {
                self.configureTopScreen()
            }
            
            
            let media = QBRTCMediaStreamConfiguration()
            
        }
    }
    
    func configureAudio() {
        
        QBRTCConfig.mediaStreamConfiguration().audioCodec = .codecOpus
        //Save current audio configuration before start call or accept call
        QBRTCAudioSession.instance().initialize()
        QBRTCAudioSession.instance().currentAudioDevice = .speaker
        //OR you can initialize audio session with a specific configuration
        QBRTCAudioSession.instance().initialize { (configuration: QBRTCAudioSessionConfiguration) -> () in
            
            var options = configuration.categoryOptions
            if #available(iOS 10.0, *) {
                options = options.union(AVAudioSessionCategoryOptions.allowBluetoothA2DP)
                options = options.union(AVAudioSessionCategoryOptions.allowAirPlay)
            } else {
                options = options.union(AVAudioSessionCategoryOptions.allowBluetooth)
            }
            configuration.categoryOptions = options
            configuration.mode = AVAudioSessionModeVideoChat
        }
    }
    
    //MARK: Helpers
    
    func configureToolbar() {
        
        let audioEnable = defButton()
        audioEnable.iconView = iconView(normalImage: "unmute", selectedImage: "mute")
        self.toolBar.addButton(button: audioEnable) { (button) in
            self.session?.localMediaStream.audioTrack.isEnabled = !(self.session?.localMediaStream.audioTrack.isEnabled)!
        }
        
        let videoEnable = defButton()
        /*videoEnable.iconView = iconView(normalImage: "videoOff", selectedImage: "videoOn")
        self.toolBar.addButton(button: videoEnable) { (button) in
            self.session?.localMediaStream.videoTrack.isEnabled = !(self.session?.localMediaStream.videoTrack.isEnabled)!
            if ((self.session?.localMediaStream.videoTrack.isEnabled) ?? false) {
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.localvideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.localvideoView.isHidden = false
            } else {
                self.session?.localMediaStream.videoTrack.videoCapture = nil
                self.localvideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.localvideoView.isHidden = true
            }
        }*/
        videoEnable.iconView = iconView(normalImage: "videoOn", selectedImage: "videoOff")
        self.toolBar.addButton(button: videoEnable) { (button) in
            
            self.session?.localMediaStream.videoTrack.isEnabled = !(self.session?.localMediaStream.videoTrack.isEnabled)!
            if self.session?.localMediaStream.videoTrack.isEnabled == true {
                
                //self.session?.localMediaStream.videoTrack.isEnabled = true
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                
                for subView in self.oponentVideoView.subviews {
                    if subView != self.oponentVideoView.subviews.first {
                        subView.removeFromSuperview()
                    }
                }
                self.topView.removeFromSuperview()
                
                for layer in self.localvideoView.layer.sublayers! {
                    layer.removeFromSuperlayer()
                }
                
                self.profilePic?.layer.removeFromSuperlayer()
                
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localvideoView.bounds
                self.localvideoView.layer.addSublayer(self.videoCapture!.previewLayer)
                
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
                self.session?.localMediaStream.videoTrack.isEnabled = true
                
                
            } else {
                
                self.session?.localMediaStream.videoTrack.videoCapture = nil
                self.configureTopScreen()
                
            }
        }

        
        let dynamicEnable = defButton()
        dynamicEnable.iconView = iconView(normalImage: "speakeron", selectedImage: "speakerOff")
        self.toolBar.addButton(button: dynamicEnable) { (button) in
            let device = QBRTCAudioSession.instance().currentAudioDevice;
            
            if device == .speaker {
                QBRTCAudioSession.instance().currentAudioDevice = .receiver
            } else {
                QBRTCAudioSession.instance().currentAudioDevice = .speaker
            }
        }
        let endthecall = defButton()
        endthecall.iconView = iconView(normalImage: "disconnect", selectedImage: "disconnect")
        self.toolBar.addButton(button: endthecall) { (button) in
            self.calldisconnected()
        }

        self.toolBar.updateItems()
    }
    
    func iconView(normalImage: String, selectedImage: String) -> UIImageView {
        let icon = UIImage.init(named: normalImage)
        let selectedIcon = UIImage.init(named: selectedImage)
        let iconView = UIImageView.init(image: icon, highlightedImage: selectedIcon)
        iconView.contentMode = .scaleAspectFit // .scaleAspectFill     //.scaleAspectFit
        return iconView
    }
    
    func defButton() -> ToolbarButton {
        let defButton = ToolbarButton.init(frame: defRect)
        defButton.backgroundColor = defBgClr
        defButton.selectedColor = defSlctClr
        defButton.isPushed = true
        return defButton
    }

    @IBAction func cameraSwitchButton(_ sender: UIButton) {
        let position = self.videoCapture.position
        if position == .back {
            self.videoCapture.position = .front
        } else {
            self.videoCapture.position = .back
        }
    }
    
    func resumeVideoCapture() {
        // ideally you should always stop capture session
        // when you are leaving controller in any way
        // here we should get its running state back
        if self.videoCapture != nil && !self.videoCapture.hasStarted {
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.videoCapture.startSession(nil)
            self.localvideoView.isHidden = true
        } else {
            self.localvideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
            self.localvideoView.isHidden = false
        }
    }
    
    func removeRemoteView(with userID: UInt) {

    }

    @objc func playCallingSound(_ sender: Any) {
        QMSoundManager.playCallingSound()
    }
    
    @IBAction func didPressendButton(_ sender: UIButton) {
        if self.session != nil {
            self.session?.hangUp(nil)
            self.callTimer?.invalidate()
        }
    }

    func relayout(with views: [UIView]) {
        self.stackView.removeAllArrangedSubviews()
        
        for v in views {
            
            if self.stackView.arrangedSubviews.count > 1 {
                let i = views.index(of: v)! % 2
                let s = self.stackView.arrangedSubviews[i] as! UIStackView
                s.addArrangedSubview(v)
            } else {
                let hStack = UIStackView()
                hStack.axis = .horizontal
                hStack.distribution = .fillEqually
                hStack.spacing = 5
                hStack.addArrangedSubview(v)
                self.stackView.addArrangedSubview(hStack)
            }
        }
    }
    
    func calldisconnected() {
        print("Disconnecting the call...")
        if self.session != nil {
            self.session?.hangUp(nil)
            
            self.callTimer?.invalidate()
        }
        
        // We will come here after the call is disconnted by doctor. this is graceful disconnection of the call
        // so first thing we will do it we create the entry in Consult Table for this valid consult
        if (ConsultRecordID.isEmpty) { // as doctor made a call from existing prescription hence there always going to be ConsultRecordID
            self.CreateNewEntrytoConsultTable()
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }

    }
    
    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        if self.session == nil {
            self.session = session
        }
    }
    
    
    func session(_ session: QBRTCBaseSession, connectedToUser userID: NSNumber) {
        
        if (session as! QBRTCSession).id == self.session?.id {
            
            self.calllabel.isHidden = true
            if beepTimer != nil {
                beepTimer?.invalidate()
                beepTimer = nil
                QMSoundManager.instance().stopAllSounds()
            }

            if session.conferenceType == QBRTCConferenceType.video {
                
            }
            
            if callTimer == nil {
                callTimer = Timer.scheduledTimer(timeInterval: kRefreshTimeInterval!, target: self, selector: #selector(self.refreshCallTime), userInfo: nil, repeats: true)
            }
        }
    }
    
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        
        if session.id == self.session?.id {
            
            self.removeRemoteView(with: userID.uintValue)
            if userID == session.initiatorID {
                self.session?.hangUp(nil)
            }
            self.calldisconnected()
        }
    }
    
    func session(_ session: QBRTCBaseSession, receivedRemoteVideoTrack videoTrack: QBRTCVideoTrack, fromUser userID: NSNumber) {
        
        if (session as! QBRTCSession).id == self.session?.id {
            if (session as! QBRTCSession).id == self.session?.id {
                let remoteView :QBRTCRemoteVideoView = QBRTCRemoteVideoView()
                remoteView.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
                remoteView.clipsToBounds = true
                remoteView.setVideoTrack(videoTrack)
                remoteView.tag = userID.intValue
                self.views.append(remoteView)
                self.relayout(with: self.views)
            }
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        
        if session.id == self.session?.id {
            self.session = nil
            if beepTimer != nil {
                beepTimer?.invalidate()
                beepTimer = nil
                QMSoundManager.instance().stopAllSounds()
            }
        }
    }
    
    
    func handleIncomingCall() {
        
        let alert = UIAlertController.init(title: "Incoming video call", message: "Accept ?", preferredStyle: .actionSheet)
        
        let accept = UIAlertAction.init(title: "Accept", style: .default) { action in
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.session?.acceptCall(nil)
            self.toolBar.isHidden = false
        }

        let reject = UIAlertAction.init(title: "Reject", style: .default) { action in
            self.session?.rejectCall(nil)
        }
        
        alert.addAction(accept)
        alert.addAction(reject)
        self.present(alert, animated: true)
    }
    
    @objc func refreshCallTime(_ sender: Timer) {
        timeDuration += kRefreshTimeInterval!
        // let extraTitle = " "
        calltimelb.text =  gowithTimeDuration(timeDuration:timeDuration)
    }
    
    func gowithTimeDuration( timeDuration: TimeInterval) -> String {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        formatter.allowedUnits = [ .minute, .second ]
        formatter.zeroFormattingBehavior = [ .pad ]
        let formattedDuration = formatter.string(from: timeDuration)
        return formattedDuration!
    }
    
    func sendPushToOpponentsAboutNewCall() {
        let opponentsIDs = (self.session!.opponentsIDs as NSArray).componentsJoined(by: ",")
        let currentUserLogin = QBSession.current.currentUser!.fullName
        QBRequest.sendPush(withText: "\("Dr " + currentUserLogin! ) is calling you", toUsers: opponentsIDs, successBlock: { (response: QBResponse, event:[QBMEvent]?) in
            print("sendPushToOpponentsAboutNewCall: Push sent!")
        }) { (error:QBError?) in
            print("sendPushToOpponentsAboutNewCall:Can not send push: \(error!))")
        }
    }
    
    
    func CreateNewEntrytoConsultTable() {
        print("CallViewController: CreateNewEntrytoConsultTable() Creating New new entry in  Consult Table")
        let object = QBCOCustomObject()
        object.className = "ConsultsTable"
        object.fields["patientuserid"] =  QBSession.current.currentUser?.id
        object.fields["docfullname"] = GlobalDocData.gdocname

        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            // we are creating the record ID for Consult table , this record = recordIDConsulttable will be used later to update record
            UserDefaults.standard.setValue((contributors?.id)!, forKey: "recordIDConsulttable")
            UserDefaults.standard.synchronize()
            print("CallViewController:CreateNewEntrytoConsultTable(): Successfully created Consult Details !")
            let storyboard = UIStoryboard.init(name: "MedicalStoryboard", bundle: nil)
            let medicalAdviceVC = storyboard.instantiateViewController(withIdentifier: "MedicalAdviceBaseViewController")
            // these lines of code removes left and right animation
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            let animation = CATransition()
            animation.type = kCATransitionFade
            self.navigationController?.view.layer.add(animation, forKey: "someAnimation")
            self.nav = UINavigationController(rootViewController: medicalAdviceVC)
            _ = self.present(self.nav!, animated: false, completion: nil)
            CATransaction.commit()
        }) { (response) in
            //Handle Error
            print("CallViewController:CustomObject: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func handleVideoRequestCallback(withcode: Int) {
        
        //accepted
        if withcode == 101 {
            let msg = "Video session with patient started"//"Dr. \(doctorname) is requesting for your video."
            let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
            //            alert.addAction(UIAlertAction(title: "DENY", style: .default, handler: { (action) in
            //                //self.showCOTConfirmationAlert()
            //                //VIDEO_ALLOW_CODE = 101;
            //
            //            }))
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                //self.showCOTConfirmationAlert()
                //VIDEO_REJECT_CODE = 102;
                //                for subView in self.opponentVideoView.subviews {
                //                    if subView != self.opponentVideoView.subviews.first {
                //                        subView.removeFromSuperview()
                //                    }
                //                }
                //                self.topView.removeFromSuperview()
                //
                //                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                //                self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                //
                //            }))
                //            self.present(alert, animated: true, completion: nil)
                for subView in self.oponentVideoView.subviews {
                    if subView != self.oponentVideoView.subviews.first {
                        subView.removeFromSuperview()
                    }
                }
                self.topView.removeFromSuperview()
                
                //                for layer in self.localVideoView.layer.sublayers! {
                //                    layer.removeFromSuperlayer()
                //                }
                
                //                self.videoCallButton.removeFromSuperview()
                //                self.videoCallButton.isEnabled = false
                //                self.videoCallButton.isHidden = true
                
                //self.view.addSubview(videoCallButton)
                //                for view in self.view.subviews {
                //                    if view == self.videoCallButton {
                //                        view.removeFromSuperview()
                //                    }
                //                }
                
                DispatchQueue.main.async {
                    self.videoCallButton.removeFromSuperview()
                }
                
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localvideoView.bounds
                self.localvideoView.layer.addSublayer(self.videoCapture!.previewLayer)
                
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
                self.session?.localMediaStream.videoTrack.isEnabled = true
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        //rejected
        if withcode == 102 {
            let msg = "Patient refused to turn on their camera"//"Dr. \(doctorname) is requesting for your video."
            let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
