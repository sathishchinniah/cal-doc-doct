import UIKit
import KDCircularProgress
import SwiftCharts
import MIBadgeButton_Swift
import Firebase
import FirebaseAuth
import Toast_Swift

var phone:Bool { return UIDevice.current.userInterfaceIdiom == .phone }
var ipad:Bool { return UIDevice.current.userInterfaceIdiom == .pad }
var width:CGFloat { return UIScreen.main.bounds.size.width }
var height:CGFloat { return UIScreen.main.bounds.size.height }
var maxLength:CGFloat { return max(width, height) }
var isSlideMenuHidden = true
var istoastbar = false
var iscameasexistinguser = false

var PatientArray : [QBUUser] = []
var NotificationArray : [QBCOCustomObject] = []
var MyAllConsultsArray : [QBCOCustomObject] = []
var MyAllConsultsUniqueArray : [QBCOCustomObject] = []

let greenProfileBorder = UIColor(red: 39/255, green: 134/255, blue: 57/255, alpha: 1).cgColor
let orangeProfileBorder = UIColor(red: 238/255, green: 79/255, blue: 7/255, alpha: 1).cgColor
let redProfileBorder = UIColor(red: 200/255, green: 24/255, blue: 39/255, alpha: 1).cgColor

struct GlobalVariables {
    static var globalString = "MyString"
    static var PatientArray: NSArray = []
    static var patientsimagearray = [Any]()
    static var gCurrentUser = QBUUser()
    static var gchargevalueint = 0
    static var gdocspeciality = ""
    static var gcalldoccode = ""
    static var recordIDConsulttable = ""
    static var gDoctorFee = 0

    // for COT
    static var cardiocount :Int = 0, dermatocount = 0, deietcount = 0, endocrinocount = 0, entcount = 0, gpcount = 0, gestrocount = 0, gynococount = 0,
    nephrocount = 0, neurocount = 0, oncologcount = 0, orthocount = 0, paediatriccount = 0, psychcount = 0, polmonocount = 0, sexologcount = 0,
    altmedcount = 0, teamcount = 0
    
    //Add Doctors to trusted doctorss Network
    static var docposition : UInt8  = 0
    
    // for Myteam
    static var teamoffon: Bool = false
}

class HomeViewController: UIViewController, UIScrollViewDelegate, QBRTCClientDelegate, QBChatDelegate {
    
    let cotbutton   = UIButton(type: UIButtonType.custom) as UIButton
    let adlabel1 = UILabel()
    let adlabel2 = UILabel()
    let homeViewHelper = HomeViewHelper.sharedInstance

    weak var mysession: QBRTCSession?
    var nav: UINavigationController?
    var beepTimer: Timer!
    var dataPointer = GData.shared()
    var badgeCount = Int()
    var COTController : CircleOftrustViewController?
    var schedularVC : DoctorScheduleViewController?
    var consultpressed = false

    let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
    let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
    let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
    let slide4:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
    let slide5:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide

    @IBOutlet weak var optionsview: UIView!
    @IBOutlet weak var optionsviewConstraint: NSLayoutConstraint!
    @IBOutlet weak var optionsdocnamelb: UILabel!
    @IBOutlet var slideScrollView: UIScrollView!
    @IBOutlet var bottumBarView: UIView!
    @IBOutlet var topBarView: UIView!
    @IBOutlet var scheduleCtrlBtnview: UIButton!
    @IBOutlet var docImg: UIImageView! {
        didSet {
            docImg.layer.cornerRadius = docImg.frame.size.width / 2;
            docImg.clipsToBounds = true;
            docImg.layer.borderWidth = 2.0
            docImg.layer.borderColor = UIColor.white.cgColor
        }
    }
    @IBOutlet var docImageViewContainer: UIView! {
        didSet {
            docImageViewContainer.layer.cornerRadius = docImageViewContainer.frame.size.width / 2;
            docImageViewContainer.clipsToBounds = true;
            docImageViewContainer.layer.borderWidth = 2.0
            docImageViewContainer.layer.borderColor =  UIColor.green.cgColor
        }
    }
    
    @IBOutlet var statustextlb: UILabel!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var profileImg: UIImageView!
    @IBOutlet weak var profileImageViewContainer: UIView!
    @IBOutlet var progressIndicator: UIActivityIndicatorView!
    @IBOutlet var myClinicBttn: MIBadgeButton!
    @IBOutlet var addPatientBtn: UIButton!
    @IBOutlet var editschduleBtn: UIButton!
    @IBOutlet var ViewBehildOptionview:  UIView!
    @IBOutlet var ViewBehildOptionsButtonview: UIButton!
    @IBOutlet weak var scheduletxtverticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var optionsviewhrlineVdistanceConstraint: NSLayoutConstraint!
    @IBOutlet weak var schedulelabel_1: UILabel!
    @IBOutlet weak var schedulelabel_2: UILabel!
    @IBOutlet weak var profileimagevieContainerYConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(refreshHomeScreen), name: Notification.Name(Constants.REFRESH_HOME_SCREEN), object: nil)

        self.HomeReadPrimaryDetailsFromServer()
        
        self.badgeCount = 0
        self.ReadAllNotifications()
        self.ReadNotificationCounts()
        self.setMyClinicBadgeCount()
        
        if isKeyPresentInUserDefaults(key: "charge") {
            GlobalVariables.gchargevalueint = UserDefaults.standard.integer(forKey: "charge")
        } else {
            UserDefaults.standard.set(0, forKey: "charge")
            UserDefaults.standard.synchronize()
            GlobalVariables.gchargevalueint = 0   // setting default value
        }
        
        self.HomeReadCustomObjectforDocConsultCharges()  // get doctor fee what he has entered at the time of registration
        
        if phone && maxLength == 568 {
            //iphone 5
            scheduletxtverticalConstraint.constant = 10 // to centre allign next schedule , tomorrow text
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            scheduletxtverticalConstraint.constant = 15 // to centre allign next schedule , tomorrow text
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            scheduletxtverticalConstraint.constant = 19 // to centre allign next schedule , tomorrow text
            profileimagevieContainerYConstraint.constant = 55
        }
        
        if phone && maxLength == 812 {
            //iphone x
            profileimagevieContainerYConstraint.constant = 55
        }
        
        if ipad && maxLength == 1024 {
            //ipad
            scheduletxtverticalConstraint.constant = 36 // to centre allign next schedule , tomorrow text
            optionsviewhrlineVdistanceConstraint.constant = 260
        }

        alignoptionsview() // hiding the side options
        
        self.getSchedularSwitchstatusfromBackend()

        let isexistinguser = UserDefaults.standard.object(forKey: "existinguser") as! Bool
        
        if(isexistinguser) {
            iscameasexistinguser = true
            
            if let existingusername = UserDefaults.standard.string(forKey: "existinguserfullname")  {
                GlobalDocData.gdocname = existingusername
            }

            if let existinguseremail = UserDefaults.standard.string(forKey: "existinguseremail")  {
                GlobalDocData.gdocemail = existinguseremail
            }

            if let existinguserphone = UserDefaults.standard.string(forKey: "existinguserphopne")  {
                GlobalDocData.gdocmobile = existinguserphone
            }
            
            if let existingusercustomdata = UserDefaults.standard.string(forKey: "usercustomdata")  {
                GlobalDocData.gusercustomdata = existingusercustomdata
            }
        } else { // this is we are coming after registration hence all values we get

            if  GlobalDocData.firsttimeview {
                print("HomeViewController: coming after Registration ")
                GlobalDocData.guserid = UserDefaults.standard.integer(forKey: "userid")
                GlobalDocData.gdocname = UserDefaults.standard.string(forKey: "fullname")!
                GlobalDocData.gdocemail = UserDefaults.standard.string(forKey: "useremail")!
                GlobalDocData.gdocmobile = UserDefaults.standard.string(forKey: "userphonenumber")!
                GlobalDocData.gdocage = UserDefaults.standard.string(forKey: "age") ?? ""
                GlobalDocData.gdocspeciality =  UserDefaults.standard.string(forKey: "specialisation") ?? ""
                GlobalDocData.gdocgender =  UserDefaults.standard.string(forKey: "gender") ?? ""
                GlobalDocData.gmcinum = UserDefaults.standard.string(forKey: "mcinumber") ?? ""
                GlobalDocData.gdegree = UserDefaults.standard.string(forKey: "degree") ?? ""
                
                self.UpdateCustomObjectforCOT(param: "", count: 0) // this is just to update COT table with Speciality
            }
        }

        let navBarColor = navigationController?.navigationBar
        navBarColor?.barTintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()

        optionsdocnamelb.text = "Dr. " + GlobalDocData.gdocname
        
        //disable buttons
        myClinicBttn.isEnabled = true
        addPatientBtn.isEnabled = true
        editschduleBtn.isEnabled = true

        self.myClinicBttn.badgeString = ""
        self.myClinicBttn.badgeBackgroundColor = .black
        let isLoggedin = UserDefaults.standard.object(forKey: "isLoggedIn") as! Bool
        self.dataPointer = GData.shared()
        
        if(isLoggedin) {
            VerifiedUserLoginNow()
        } else {
            progressIndicator.isHidden = true
            progressIndicator.stopAnimating()
            //video specific
            QBRTCClient.initializeRTC()
            QBRTCClient.instance().add(self)
        }

        slideScrollView.delegate = self

        view.bringSubview(toFront: optionsview)
        
        checkStatusOFSchedular()
        
        self.CheckConsultTablefornullPrescriptionID()

        homeViewHelper.getDoctorStatus { (currentSchedule, nextSchedule) in
            self.statustextlb.text = UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) ? "Automatic scheduler switched off" : currentSchedule
            self.schedulelabel_2.lineBreakMode = .byWordWrapping
            self.schedulelabel_2.numberOfLines = 3
            //let scheduletext = UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) ? ("\(nextSchedule) Automatic scheduler switched off") : nextSchedule
            self.schedulelabel_2.text = UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) ? "Automatic scheduler switched off" : nextSchedule//nextSchedule
            self.checkDoctorStatus()
        }
        
        var style = ToastStyle()
        style.messageColor = .white
        ToastManager.shared.style = style
        
        if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) {
            self.view.makeToast("Team members will be consulted when offline.", duration: 4.0, position: .bottom, title: nil, image: nil, style: style, completion: nil)
        }
    }
    
    func  CreateCustomObjectforCOT() {
        // Create Custom Object COT here which is empty table with ID, UserID, ParentID as predefined fields and user defined fields as

        let object = QBCOCustomObject()
        object.className = "DocCOTTable"
        
        object.fields["docspeciality"] =  GlobalDocData.gdocspeciality
        object.fields["cardiologistcount"] = "0"
        object.fields["dermatologistcount"] = "0"
        object.fields["dieticiancount"] = "0"
        object.fields["endocrinologistcount"] = "0"
        object.fields["entcount"] = "0"
        object.fields["gpcount"] = "0"
        object.fields["gastroentrologistcount"] = "0"
        object.fields["gynaecologistcount"] = "0"
        object.fields["nephrologistcount"] = "0"
        object.fields["neurologistcount"] = "0"
        object.fields["oncologistcount"] =  "0"
        object.fields["orthopaediccount"] = "0"
        object.fields["paediatriciancount"] = "0"
        object.fields["psychiatristcount"] = "0"
        object.fields["pulmonoligistcount"] = "0"
        object.fields["sexologistcount"] = "0"
        object.fields["altmedicinecount"] = "0"
        object.fields["myteamcount"] = "0"
        object.fields["teamoffon"] = false
        
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            // we are creating the record ID for doctors COT table , this record = customobjIDCOT will be used later to update COT record
            UserDefaults.standard.setValue((contributors?.id)!, forKey: "customobjIDCOT")
            UserDefaults.standard.synchronize()
            print("HomeViewController:CustomObject: Successfully created COT !")
        }) { (response) in
            //Handle Error
            print("HomeViewController:CustomObject: COT Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func createEntryIntoDocCountTable() {
        
        let object = QBCOCustomObject()
        object.className = "DocCountTable"
        object.fields["dermatologist"] = 0
        object.fields["dietician"] = 0
        object.fields["cardiologist"] = 0
        object.fields["ent"] = 0
        object.fields["general"] = 0
        object.fields["endocrinologist"] = 0
        object.fields["gynaecologist"] = 0
        object.fields["nephrologist"] = 0
        object.fields["neurologist"] = 0
        object.fields["oncologist"] = 0
        object.fields["gastroentrologist"] = 0
        object.fields["paediatrician"] = 0
        object.fields["psychiatrist"] = 0
        object.fields["orthopaedic"] = 0
        object.fields["sexologist"] = 0
        object.fields["altmedicine"] = 0
        object.fields["pulmonoligist"] = 0
        object.fields["myTeam"] = 0
        object.fields["teamStatus"] = "false"
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            UserDefaults.standard.setValue((contributors?.id)!, forKey: "RecordIDDocCount")
            UserDefaults.standard.synchronize()
        }) { (response) in
            print("PrimaryDetailsDocViewController: CreateNewEntrytoDocCountTable(): Response error: \(String(describing: response.error?.description))")
        }
        
        ReadCustomObjectforCOT()
        
    }
    
    func  UpdateCustomObjectforCOT(param: String, count: Int) {

        let object = QBCOCustomObject()
        object.className = "DocCOTTable"
        object.fields["docspeciality"] =  GlobalDocData.gdocspeciality

        if param == "Cardiologist" {

            object.fields["cardiologistcount"] = count // put actual values

        } else if param == "Dermatologist" {

            object.fields["dermatologistcount"] = count

        } else if param == "Dietician" {

            object.fields["dieticiancount"] = count

        } else if param == "Endocrinologist" {

            object.fields["endocrinologistcount"] = count

        } else if param == "E.N.T." {

            object.fields["entcount"] = count

        } else if param == "G.P." {

            object.fields["gpcount"] = count

        } else if param == "Gastroentrologist" {

            object.fields["gastroentrologistcount"] = count

        } else if param == "Gynaecologist" {

            object.fields["gynaecologistcount"] = count

        } else if param == "Nephrologist" {

            object.fields["nephrologistcount"] = count

        } else if param == "Neurologist" {

            object.fields["neurologistcount"] = count

        } else if param == "Oncologist" {

            object.fields["oncologistcount"] =  count

        } else if param == "Orthopaedic" {

            object.fields["orthopaediccount"] = count

        } else if param == "Paediatrician" {

            object.fields["paediatriciancount"] = count

        } else if param == "Psychiatrist" {

            object.fields["psychiatristcount"] = count

        } else if param == "Pulmonoligist" {

            object.fields["pulmonoligistcount"] = count

        } else if param == "Sexologist" {

            object.fields["sexologistcount"] = count

        } else if param == "Altmedicine" {

            object.fields["altmedicinecount"] = count

        } else if param == "MyTeam" {

            object.fields["myteamcount"] = count

        } else if param == "teamoffon" {
            if count == 0 {
                object.fields["teamoffon"] = false
            } else {
                object.fields["teamoffon"] = true
            }
        }
        
        if let checkCOTID = UserDefaults.standard.string(forKey: "customobjIDCOT") {
            object.id = checkCOTID
        } else {
            return
        }

        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("HomeViewController:CustomObject: Successfully Updated for COT !")
        }) { (response) in
            //Handle Error
            print("HomeViewController:CustomObject COT: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func  ReadCustomObjectforCOT() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        //DocCountTable //DocCOTTable
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                UserDefaults.standard.setValue((contributors![0].id)!, forKey: "customobjIDCOT")
                GlobalVariables.cardiocount = (contributors?[0].fields?.value(forKey: "cardiologistcount") as? Int ?? 0 )
                GlobalVariables.altmedcount = (contributors![0].fields?.value(forKey: "altmedicinecount") as? Int ?? 0 )
                GlobalVariables.deietcount =  (contributors![0].fields?.value(forKey: "dieticiancount") as? Int ?? 0 )
                GlobalVariables.dermatocount = (contributors![0].fields?.value(forKey: "dermatologistcount") as? Int ?? 0 )
                GlobalVariables.endocrinocount = (contributors![0].fields?.value(forKey: "endocrinologistcount") as? Int ?? 0 )
                GlobalVariables.entcount = (contributors![0].fields?.value(forKey: "entcount") as? Int ?? 0 )
                GlobalVariables.gestrocount = (contributors![0].fields?.value(forKey: "gastroentrologistcount") as? Int ?? 0 )
                GlobalVariables.gpcount = (contributors![0].fields?.value(forKey: "gpcount") as? Int ?? 0 )
                GlobalVariables.gynococount = (contributors![0].fields?.value(forKey: "gynaecologistcount") as? Int ?? 0 )
                GlobalVariables.nephrocount = (contributors![0].fields?.value(forKey: "nephrologistcount") as? Int ?? 0 )
                GlobalVariables.neurocount = (contributors![0].fields?.value(forKey: "neurologistcount") as? Int ?? 0 )
                GlobalVariables.oncologcount = (contributors![0].fields?.value(forKey: "oncologistcount") as? Int ?? 0 )
                GlobalVariables.orthocount = (contributors![0].fields?.value(forKey: "orthopaediccount") as? Int ?? 0 )
                GlobalVariables.paediatriccount = (contributors![0].fields?.value(forKey: "paediatriciancount") as? Int ?? 0 )
                GlobalVariables.polmonocount = (contributors![0].fields?.value(forKey: "pulmonoligistcount") as? Int ?? 0 )
                GlobalVariables.psychcount = (contributors![0].fields?.value(forKey: "psychiatristcount") as? Int ?? 0 )
                GlobalVariables.sexologcount = (contributors![0].fields?.value(forKey: "sexologistcount") as? Int ?? 0 )
                GlobalVariables.teamcount = (contributors![0].fields?.value(forKey: "myteamcount") as? Int ?? 0 )
                GlobalVariables.teamoffon = (contributors![0].fields?.value(forKey: "teamoffon") as? Bool ?? false )

                let cotViewController = self.storyboard!.instantiateViewController(withIdentifier: "CircleOfTrust") as! CircleOftrustViewController
                
                // these lines of code removes left and right animation
                CATransaction.begin()
                CATransaction.setDisableActions(true)
                let animation = CATransition()
                animation.type = kCATransitionFade
                self.navigationController?.view.layer.add(animation, forKey: "someAnimation")
                self.nav = UINavigationController(rootViewController: cotViewController)
                _ = self.present(self.nav!, animated: false, completion: nil)
                CATransaction.commit()
            } else {
                print("HomeViewController:ReadCustomObjectforCOT()  error: No contributors")
                self.createEntryIntoDocCountTable()
            }
        }) { (errorresponse) in
            print("HomeViewController:ReadCustomObjectforCOT() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.navigationBar.isHidden = true
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        //DocCountTable //DocCOTTable
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                UserDefaults.standard.setValue((contributors![0].id)!, forKey: "customobjIDCOT")
            }
        }) { (errorresponse) in
            print("HomeViewController:viewWillAppear() Response error: \(String(describing: errorresponse.error?.description))")
        }
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.black
        checkDoctorStatus()
    }
    
    
    // to check if key is present for persistance key value storage
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func checkStatusOFSchedular() {
        if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) {
            scheduleCtrlBtnview.setTitle("STOP CONSULTING", for: UIControlState.normal)
            schedulelabel_2.text = "Automatic Scheduler Off"
        }
    }

    func setUpBadgeCountAndBarButton() {
        // badge label
        let label = UILabel(frame: CGRect(x: 16, y: -09, width: 15, height: 15))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 1
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.textColor = .white
        label.font = label.font.withSize(8)
        label.backgroundColor = .black
        label.text = "\(self.badgeCount)"
        
        // Bell Bar button
        let rightBellButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        rightBellButton.setBackgroundImage(UIImage(named: "bellicon"), for: .normal)
        rightBellButton.addTarget(self, action: #selector(didTapBellButton), for: .touchUpInside)
        rightBellButton.addSubview(label)
        // Bar button item
        let rightBarBellButtomItem = UIBarButtonItem(customView: rightBellButton)
        
        // Help Bar button
        let rightHelpButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        rightHelpButton.setBackgroundImage(UIImage(named: "helpblk"), for: .normal)
        rightHelpButton.addTarget(self, action: #selector(didTapHelpButton), for: .touchUpInside)
        // Bar button item
        let rightBarHelpButtomItem = UIBarButtonItem(customView: rightHelpButton)

        navigationItem.rightBarButtonItems = [rightBarHelpButtomItem, rightBarBellButtomItem]
        
    }

    @objc func didTapHelpButton(sender: UIBarButtonItem) {
        print("didTapHelpButton Bar icon pressed")
    }
    
    @objc func didTapBellButton(sender: UIBarButtonItem) {
        print("didTapBellButton Bar icon pressed")
        let notificationbellViewController = self.storyboard!.instantiateViewController(withIdentifier: "NotificationsView") as! NotificationsTableViewController
        self.nav = UINavigationController(rootViewController: notificationbellViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }
    
    func LoadProfileImage() {
        self.docImg.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profile"))
    }

    func alignoptionsview() {

        if phone && maxLength == 568 { // iphone 5S
            optionsviewConstraint.constant = -253
            isSlideMenuHidden = true
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
        }

        if phone && maxLength == 667 {
            //iphone 6
            optionsviewConstraint.constant = -296.5
            isSlideMenuHidden = true
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
        }

        if phone && maxLength == 736 {
            //iphone 6 plus
            optionsviewConstraint.constant = -327
            isSlideMenuHidden = true
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
        }

        if phone && maxLength == 812 {
            //iphone X
            optionsviewConstraint.constant = -296.67
            isSlideMenuHidden = true
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
        }

        if ipad && maxLength == 1024 {
            //iPad
            optionsviewConstraint.constant = -640.0
            isSlideMenuHidden = true
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
        }
    }
    
    func checkDoctorStatus() {
        if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) || UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_OFFLINE) {
            updateDoctorWhenSchedulerOff()
        } else {
            editschduleBtn.isHidden = false

            if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_ONLINE) {
                if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_PAUSED) {
                    updateDoctorPausedStatus()
                } else {
                    updateDoctorOnlineStatus()
                }
            }
        }
    }
    
    func updateDoctorOnlineStatus() {
        docImageViewContainer.layer.borderColor = greenProfileBorder
        bottumBarView.backgroundColor = UIColor(red:0.33, green:0.76, blue:0.44, alpha:0.60)
        self.scheduleCtrlBtnview.setTitleColor(UIColor.black, for: .normal)
        scheduleCtrlBtnview.setTitle("PAUSE CONSULTING", for: UIControlState.normal)

        //        statustextlb.text = "Online till 06:30 PM"
        navigationItem.title = "ONLINE"
        LoadProfileImageforoptions()
        self.view.layer.contents = #imageLiteral(resourceName: "bg_green").cgImage

        consultpressed = false;  // making sure working of consulting button press

        DrawtheBarChartGreen()
        let slides:[Slide] = createSlidesGreen()
        setupSlideScrollView(slides: slides)
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0

        slide1.circleProgressBar.progressColors = [UIColor(red:0.35, green:0.8, blue:0.47, alpha:1)]
        slide2.circleProgressBar.progressColors = [UIColor(red:0.35, green:0.8, blue:0.47, alpha:1)]
        slide3.circleProgressBar.progressColors = [UIColor(red:0.35, green:0.8, blue:0.47, alpha:1)]
        slide4.circleProgressBar.progressColors = [UIColor(red:0.35, green:0.8, blue:0.47, alpha:1)]

        slide1.circleProgressBar.trackColor = UIColor(red:0.16, green:0.49, blue:0.25, alpha:1)
        slide2.circleProgressBar.trackColor = UIColor(red:0.16, green:0.49, blue:0.25, alpha:1)
        slide3.circleProgressBar.trackColor = UIColor(red:0.16, green:0.49, blue:0.25, alpha:1)
        slide4.circleProgressBar.trackColor = UIColor(red:0.16, green:0.49, blue:0.25, alpha:1)

        slide1.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide2.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide3.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide4.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)

    }
    
    func updateDoctorPausedStatus() {
        docImageViewContainer.layer.borderColor  = orangeProfileBorder
        bottumBarView.backgroundColor = UIColor(red:0.98, green:0.61, blue:0, alpha:0.40)
        self.scheduleCtrlBtnview.setTitleColor(UIColor.black, for: .normal)
        self.scheduleCtrlBtnview.setTitle("RESUME CONSULTING", for: .normal)

        cotbutton.setImage(#imageLiteral(resourceName: "COTOrange"), for: .normal)
        navigationItem.title = "PAUSED"
        self.view.layer.contents = #imageLiteral(resourceName: "bg_orange").cgImage

        consultpressed = true

        DrawtheBarChartOrange()
        let slides:[Slide] = createSlidesOrange()
        setupSlideScrollView(slides: slides)
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubview(toFront: pageControl)

        slide1.circleProgressBar.progressColors = [UIColor(red:1, green:0.7, blue:0.36, alpha:1)]
        slide2.circleProgressBar.progressColors = [UIColor(red:1, green:0.7, blue:0.36, alpha:1)]
        slide3.circleProgressBar.progressColors = [UIColor(red:1, green:0.7, blue:0.36, alpha:1)]
        slide4.circleProgressBar.progressColors = [UIColor(red:1, green:0.7, blue:0.36, alpha:1)]
        slide1.circleProgressBar.trackColor = UIColor(red:0.76, green:0.36, blue:0, alpha:1)
        slide2.circleProgressBar.trackColor = UIColor(red:0.76, green:0.36, blue:0, alpha:1)
        slide3.circleProgressBar.trackColor = UIColor(red:0.76, green:0.36, blue:0, alpha:1)
        slide4.circleProgressBar.trackColor = UIColor(red:0.76, green:0.36, blue:0, alpha:1)
        slide1.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide2.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide3.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide4.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        
    }

    func updateDoctorWhenSchedulerOff() {
        editschduleBtn.isHidden = false//true
        docImageViewContainer.layer.borderColor  = redProfileBorder
        
        bottumBarView.backgroundColor = UIColor(red:250/255, green:55/255, blue:70/255, alpha:0.40)
        self.scheduleCtrlBtnview.setTitleColor(UIColor.black, for: .normal)
        self.scheduleCtrlBtnview.setTitle("START CONSULTING", for: .normal)
        
        cotbutton.setImage(#imageLiteral(resourceName: "COT Red"), for: .normal)
        
        navigationItem.title = "OFFLINE"
        //        statustextlb.text = "This Session ends at 06:30 PM"
        self.view.layer.contents = #imageLiteral(resourceName: "bgOffline").cgImage
        
        consultpressed = true
        
        DrawtheBarChartRed()
        
        let slides:[Slide] = createSlidesRed()
        setupSlideScrollView(slides: slides)
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubview(toFront: pageControl)
        
        slide1.circleProgressBar.progressColors = [UIColor(red:220/255, green:122/255, blue:125/255, alpha:1)]
        slide2.circleProgressBar.progressColors = [UIColor(red:220/255, green:122/255, blue:125/255, alpha:1)]
        slide3.circleProgressBar.progressColors = [UIColor(red:220/255, green:122/255, blue:125/255, alpha:1)]
        slide4.circleProgressBar.progressColors = [UIColor(red:220/255, green:122/255, blue:125/255, alpha:1)]
        slide1.circleProgressBar.trackColor = UIColor(red:162/255, green:0, blue:0, alpha:1)
        slide2.circleProgressBar.trackColor = UIColor(red:162/255, green:0, blue:0, alpha:1)
        slide3.circleProgressBar.trackColor = UIColor(red:162/255, green:0, blue:0, alpha:1)
        slide4.circleProgressBar.trackColor = UIColor(red:162/255, green:0, blue:0, alpha:1)
        slide1.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide2.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide3.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide4.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
    }
    
    func checkupdateschedular() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocSchedularDayView", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("HomeViewController:checkupdateschedular(): Successfully  !")
            
            if (contributors?.count == 0) {
                self.createdefaultschedular()
            } else {
                UserDefaults.standard.setValue((contributors?[0].id)!, forKey: "customobjIDSchedular")
            }
        }) { (errorresponse) in
            print("HomeViewController: checkupdateschedular() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func createdefaultschedular() {
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"

        let defaultArray = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false]//[false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]

        object.fields["Monday"] = defaultArray
        object.fields["Tuesday"] = defaultArray
        object.fields["Wednesday"] = defaultArray
        object.fields["Thursday"] = defaultArray
        object.fields["Friday"] = defaultArray
        object.fields["Saturday"] = defaultArray
        object.fields["Sunday"] = defaultArray

        object.fields["SchedularStatus"] = true//false
        object.fields["resumeConsult"] = true
        
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success

            self.homeViewHelper.setUserDefaultsForSchedular()

            UserDefaults.standard.setValue((contributors?.id)!, forKey: "customobjIDSchedular")
            UserDefaults.standard.synchronize()
            print("HomeViewController:createdefaultschedular(): Successfully created COT !")
            
        }) { (response) in
            //Handle Error
            print("HomeViewController:CustomObject: COT Response error: \(String(describing: response.error?.description))")
        }
        
    }
    
    func getSchedularSwitchstatusfromBackend() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocSchedularDayView", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("HomeViewController:getSchedularSwitchstatusfromBackend(): Successfully  !")
            
            if ((contributors?.count)! > 0) {
                let schedularVal = !((contributors![0].fields?.value(forKey: "SchedularStatus") as? Bool) ?? true)
                UserDefaults.standard.set(/*contributors![0].fields?.value(forKey: "SchedularStatus") as! Bool?*/schedularVal, forKey: Constants.KEY_SCHEDULE_ON_OFF)
                let resumeValue = !((contributors![0].fields?.value(forKey: "resumeConsult") as? Bool) ?? true)
                UserDefaults.standard.set(/*contributors![0].fields?.value(forKey: "resumeConsult") as! Bool?*/resumeValue, forKey: Constants.KEY_DOCTOR_PAUSED)
                self.checkDoctorStatus()
            } else {
                print("HomeViewController: getSchedularSwitchstatusfromBackend() NO data from schedular found")
                self.checkDoctorStatus()
            }
        }) { (errorresponse) in
            print("HomeViewController: getSchedularSwitchstatusfromBackend() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func createSlidesGreen() -> [Slide] {

        slide1.label.text = "Circle of Trust"
        slide2.callnums.text = "10"
        slide2.label.text = "Daily"
        slide2.callcval.text = "15"
        slide2.hourslb.text = "Today"
        slide2.billedval.text = "100"
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                slide2.circleProgressBar.frame = CGRect(x: 150, y: 0, width: 400, height: 400)
                slide2.circleProgressBar.startAngle = -90
                slide2.circleProgressBar.progressThickness = 0.35
                slide2.circleProgressBar.trackThickness = 0.35
                slide2.circleProgressBar.clockwise = true
                slide2.circleProgressBar.gradientRotateSpeed = 150
                slide2.circleProgressBar.roundedCorners = true
                //                slide2.circleProgressBar.glowMode = .forward
                slide2.circleProgressBar.glowAmount = 0.1
                slide2.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
                slide2.circleProgressBar.set(colors: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1))
                
                //slide 3
                slide3.circleProgressBar.frame = CGRect(x: 150, y: 0, width: 400, height: 400)
                slide3.circleProgressBar.startAngle = -90
                slide3.circleProgressBar.progressThickness = 0.35
                slide3.circleProgressBar.trackThickness = 0.35
                slide3.circleProgressBar.clockwise = true
                slide3.circleProgressBar.gradientRotateSpeed = 150
                slide3.circleProgressBar.roundedCorners = true
                //                slide3.circleProgressBar.glowMode = .forward
                slide3.circleProgressBar.glowAmount = 0.1
                slide3.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
                slide3.circleProgressBar.set(colors: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1))
                
                //slide 3
                slide4.circleProgressBar.frame = CGRect(x: 150, y: 0, width: 400, height: 400)
                slide4.circleProgressBar.startAngle = -90
                slide4.circleProgressBar.progressThickness = 0.35
                slide4.circleProgressBar.trackThickness = 0.35
                slide4.circleProgressBar.clockwise = true
                slide4.circleProgressBar.gradientRotateSpeed = 150
                slide4.circleProgressBar.roundedCorners = true
                //                slide4.circleProgressBar.glowMode = .forward
                slide4.circleProgressBar.glowAmount = 0.1
                slide4.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
                slide4.circleProgressBar.set(colors: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1))
            }

        } else {

            slide2.circleProgressBar.startAngle = -90
            slide2.circleProgressBar.progressThickness = 0.5
            slide2.circleProgressBar.trackThickness = 0.5
            slide2.circleProgressBar.clockwise = true
            slide2.circleProgressBar.gradientRotateSpeed = 150
            slide2.circleProgressBar.roundedCorners = true
            //            slide2.circleProgressBar.glowMode = .forward
            slide2.circleProgressBar.glowAmount = 0.1
            slide2.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
            slide2.circleProgressBar.set(colors: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1))
            
        }
        
        slide3.callnums.text = "70"
        slide3.circleProgressBar.angle = 265.0
        slide3.label.text = "Weekly"
        slide3.callcval.text = "95"
        slide3.hourslb.text = "This Week"
        slide3.billedval.text = "3150"
        slide3.circleProgressBar.set(colors: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1))
        
        slide4.callnums.text = "330"
        slide4.circleProgressBar.angle = 285.0
        slide4.label.text = "Monthly"
        slide4.callcval.text = "380"
        slide4.hourslb.text = "This Month"
        slide4.billedval.text = "12600"
        slide4.circleProgressBar.set(colors: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1))

        slide5.label.font = UIFont(name: "Rubik", size: 12.0)
        slide5.label.text = "Designed by Doctors, for Doctors"
        slide5.barchartView.isHidden = true
        slide5.circleProgressBar.isHidden = true
        
        return [slide1, slide2, slide3, slide4,slide5]
    }
    
    func createSlidesOrange() -> [Slide] {
        
        slide1.label.text = "Circle of Trust"
        slide2.callnums.text = "10"
        slide2.label.text = "Daily"
        slide2.callcval.text = "15"
        slide2.hourslb.text = "Today"
        slide2.billedval.text = "100"
        slide2.circleProgressBar.startAngle = -90
        slide2.circleProgressBar.progressThickness = 0.5
        slide2.circleProgressBar.trackThickness = 0.5
        slide2.circleProgressBar.clockwise = true
        slide2.circleProgressBar.gradientRotateSpeed = 150
        slide2.circleProgressBar.roundedCorners = true
        //        slide2.circleProgressBar.glowMode = .forward
        slide2.circleProgressBar.glowAmount = 0.1
        slide2.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide2.circleProgressBar.set(colors: UIColor(red:1, green:0.7, blue:0.36, alpha:1))
        
        slide3.callnums.text = "70"
        slide3.circleProgressBar.angle = 265.0
        slide3.label.text = "Weekly"
        slide3.callcval.text = "95"
        slide3.hourslb.text = "This Week"
        slide3.billedval.text = "3150"
        slide3.circleProgressBar.set(colors: UIColor(red:1, green:0.7, blue:0.36, alpha:1))
        
        slide4.callnums.text = "330"
        slide4.circleProgressBar.angle = 285.0
        slide4.label.text = "Monthly"
        slide4.callcval.text = "380"
        slide4.hourslb.text = "This Month"
        slide4.billedval.text = "12600"
        slide4.circleProgressBar.set(colors: UIColor(red:1, green:0.7, blue:0.36, alpha:1))
        
        slide5.label.text = "Designed by Doctors, for Doctors"
        slide5.barchartView.isHidden = true
        slide5.circleProgressBar.isHidden = true
        return [slide1, slide2, slide3, slide4,slide5]
        
    }
    
    func createSlidesRed() -> [Slide] {
        
        slide1.label.text = "Circle of Trust"
        
        slide2.callnums.text = "10"
        slide2.label.text = "Daily"
        slide2.callcval.text = "15"
        slide2.hourslb.text = "Today"
        slide2.billedval.text = "100"
        
        slide2.circleProgressBar.startAngle = -90
        slide2.circleProgressBar.progressThickness = 0.5
        slide2.circleProgressBar.trackThickness = 0.5
        slide2.circleProgressBar.clockwise = true
        slide2.circleProgressBar.gradientRotateSpeed = 150
        slide2.circleProgressBar.roundedCorners = true
        //        slide2.circleProgressBar.glowMode = .forward
        slide2.circleProgressBar.glowAmount = 0.1
        slide2.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
        slide2.circleProgressBar.set(colors: UIColor(red:220/255, green:122/255, blue:125/255, alpha:1))
        
        slide3.callnums.text = "70"
        slide3.circleProgressBar.angle = 265.0
        slide3.label.text = "Weekly"
        slide3.callcval.text = "95"
        slide3.hourslb.text = "This Week"
        slide3.billedval.text = "3150"
        slide3.circleProgressBar.set(colors: UIColor(red:220/255, green:122/255, blue:125/255, alpha:1))
        
        slide4.callnums.text = "330"
        slide4.circleProgressBar.angle = 285.0
        slide4.label.text = "Monthly"
        slide4.callcval.text = "380"
        slide4.hourslb.text = "This Month"
        slide4.billedval.text = "12600"
        slide4.circleProgressBar.set(colors: UIColor(red:220/255, green:122/255, blue:125/255, alpha:1))
        
        slide5.label.text = "Designed by Doctors, for Doctors"
        slide5.barchartView.isHidden = true
        slide5.circleProgressBar.isHidden = true
        
        return [slide1, slide2, slide3, slide4,slide5]
        
    }

    @objc func cotButtonTapped() {
        ReadCustomObjectforCOT()
    }

    func setupSlideScrollView(slides:[Slide]) {

        slideScrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        slideScrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: 1)
        slideScrollView.isPagingEnabled = true;
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: slides[i].frame.width * CGFloat(i), y: 0, width: slides[i].frame.width, height: slides[i].frame.height)
            
            if phone && maxLength == 568 {
                //iphone5
                slides[i].frame = CGRect(x: slideScrollView.frame.width * CGFloat(i), y: 0, width: slideScrollView.frame.width, height: slideScrollView.frame.height)
                
                if( i  == 0) { // Circle of trust
                    //slideScrollView.frame.origin.y
                    slides[i].frame = CGRect(x: slideScrollView.frame.width * CGFloat(i), y: -45, width: slideScrollView.frame.width, height: slideScrollView.frame.height)
                    //slides[i].label.bounds = CGRect(x: 30, y: slideScrollView.frame.origin.y, width: 200, height: 130)
                    //slide1.label.frame = CGRect(x: 30, y: slideScrollView.frame.origin.y, width: 200, height: 130)
                    slides[i].label.frame = CGRect(x: 60, y: slideScrollView.frame.origin.y+10, width: 200, height: 130)
                    cotbutton.frame = CGRect(x: 60, y: 95, width: 200, height: 130)

                    if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) || UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_OFFLINE){
                        cotbutton.setImage(#imageLiteral(resourceName: "COT Red"), for: .normal)
                    } else {
                        if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_ONLINE) {
                            if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_PAUSED) {
                                cotbutton.setImage(#imageLiteral(resourceName: "COTOrange"), for: .normal)
                            } else {
                                cotbutton.setImage(#imageLiteral(resourceName: "COT"), for: .normal)
                            }
                        }
                    }
                    cotbutton.backgroundColor = UIColor.clear
                    cotbutton.addTarget(self, action: #selector(cotButtonTapped), for: .touchUpInside)
                    slide1.addSubview(cotbutton)
                }

                if( i  == 1) {
                    slides[i].frame = CGRect(x: slideScrollView.frame.width * CGFloat(i), y: -45 , width: slideScrollView.frame.width, height: slideScrollView.frame.height)
                }

                if( i  == 2) {
                    slides[i].frame = CGRect(x: slideScrollView.frame.width * CGFloat(i), y: -45, width: slideScrollView.frame.width, height: slideScrollView.frame.height)
                }

                if( i  == 3) {
                    slides[i].frame = CGRect(x: slideScrollView.frame.width * CGFloat(i), y: -45, width: slideScrollView.frame.width, height: slideScrollView.frame.height)
                }
                if( i  == 4) {
                    slides[i].frame = CGRect(x: slideScrollView.frame.width * CGFloat(i), y: -45, width: slideScrollView.frame.width, height: slideScrollView.frame.height)
                    
                    slide5.label.font = UIFont(name: "Rubik", size: 16)
                    slide5.line1.isHidden = false
                    slide5.adlabel1.text = "Help us make CallDoc better"
                    slide5.adlabel2.text = "Click to let us know what you’d like improved or what features you’d like added"
                    slide5.adbuttonclick.addTarget(self, action: #selector(self.adBtnPressed), for: .touchUpInside)
                }

            } else if phone && maxLength == 667 {

                //iphone6 this is hard code fix for slide view: Manish:3/11/17
                if( i  == 0) {
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                    slides[i].label.frame = CGRect(x: 60, y: slideScrollView.frame.origin.y+5, width: 200, height: 130)
                    cotbutton.frame = CGRect(x: 90, y: 70, width: 200, height: 130)
                    if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) || UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_OFFLINE) {
                        cotbutton.setImage(#imageLiteral(resourceName: "COT Red"), for: .normal)
                    } else {
                        if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_ONLINE) {
                            if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_PAUSED) {
                                cotbutton.setImage(#imageLiteral(resourceName: "COTOrange"), for: .normal)
                            } else {
                                cotbutton.setImage(#imageLiteral(resourceName: "COT"), for: .normal)
                            }
                        }
                    }
                    cotbutton.backgroundColor = UIColor.clear
                    cotbutton.addTarget(self, action: #selector(cotButtonTapped), for: .touchUpInside)
                    slide1.addSubview(cotbutton)
                }
                
                if( i  == 1) { //day
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }

                if( i  == 2) { //week
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }

                if( i  == 3) { //month
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                }
                
                if( i  == 4) { //add banner
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                    slide5.label.font = UIFont(name: "Rubik", size: 18)
                    slide5.line1.isHidden = false
                    slide5.adlabel1.text = "Help us make CallDoc better"
                    slide5.adlabel2.text = "Click to let us know what you’d like improved or what features you’d like added"
                    slide5.adbuttonclick.addTarget(self, action: #selector(self.adBtnPressed), for: .touchUpInside)
                }

            } else if phone && maxLength == 736 {
                //iphone6plus
                
                if( i  == 0) {
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 100, width: view.frame.width, height: slideScrollView.frame.height)
                    cotbutton.frame = CGRect(x: 100, y: -10, width: 200, height: 130)
                    slide1.addSubview(cotbutton)

                    if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) || UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_OFFLINE) {
                        cotbutton.setImage(#imageLiteral(resourceName: "COT Red"), for: .normal)
                    } else {
                        if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_ONLINE) {
                            if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_PAUSED) {
                                cotbutton.setImage(#imageLiteral(resourceName: "COTOrange"), for: .normal)
                            } else {
                                cotbutton.setImage(#imageLiteral(resourceName: "COT"), for: .normal)
                            }
                        }
                    }
                    cotbutton.backgroundColor = UIColor.clear
                    cotbutton.addTarget(self, action: #selector(cotButtonTapped), for: .touchUpInside)
                    
                }
                
                if( i  == 1) { //Today
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 100, width: view.frame.width, height: view.frame.height)
                }

                if( i  == 2) { // this week
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 100, width: view.frame.width, height: view.frame.height)
                }

                if( i  == 3) { // this month
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 100, width: view.frame.width, height: view.frame.height)
                }
                
                if( i  == 4) { // Add Banner
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 100, width: view.frame.width, height: view.frame.height)
                    
                    slide5.label.font = UIFont(name: "Rubik", size: 20)
                    slide5.line1.isHidden = false
                    slide5.adlabel1.text = "Help us make CallDoc better"
                    slide5.adlabel2.text = "Click to let us know what you’d like improved or what features you’d like added"
                    slide5.adbuttonclick.addTarget(self, action: #selector(self.adBtnPressed), for: .touchUpInside)
                }

            } else if phone && maxLength == 812 {

                //iphone X
                if( i  == 0) {
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: view.frame.height)
                    cotbutton.frame = CGRect(x: 95, y: 80, width: 200, height: 150)
                    
                    if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) || UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_OFFLINE) {
                        cotbutton.setImage(#imageLiteral(resourceName: "COT Red"), for: .normal)
                    } else {
                        if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_ONLINE) {
                            if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_PAUSED) {
                                cotbutton.setImage(#imageLiteral(resourceName: "COTOrange"), for: .normal)
                            } else {
                                cotbutton.setImage(#imageLiteral(resourceName: "COT"), for: .normal)
                            }
                        }
                    }
                    cotbutton.backgroundColor = UIColor.clear
                    cotbutton.addTarget(self, action: #selector(cotButtonTapped), for: .touchUpInside)
                    slide1.addSubview(cotbutton)
                }
                
                if( i  == 1) {
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: view.frame.height)
                }

                if( i  == 2) {
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: view.frame.height)
                }

                if( i  == 3) {
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: view.frame.height)
                }
                
                if( i  == 4) {
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: view.frame.height)
                    slide5.label.font = UIFont(name: "Rubik", size: 20)
                    slide5.line1.isHidden = false
                    
                    slide5.adlabel1.text = "Help us make CallDoc better"
                    slide5.adlabel2.text = "Click to let us know what you’d like improved or what features you’d like added"
                    slide5.adbuttonclick.addTarget(self, action: #selector(self.adBtnPressed), for: .touchUpInside)
                }
            }
            
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    
                    if( i  == 0) {
                        slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: slideScrollView.frame.height)
                        cotbutton.frame = CGRect(x: 280, y: 108, width: 200, height: 130)
                        
                        if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) || UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_OFFLINE) {
                            cotbutton.setImage(#imageLiteral(resourceName: "COTRed"), for: .normal)
                        } else {
                            if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_ONLINE) {
                                if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_PAUSED) {
                                    cotbutton.setImage(#imageLiteral(resourceName: "COTOrange"), for: .normal)
                                } else {
                                    cotbutton.setImage(#imageLiteral(resourceName: "COT"), for: .normal)
                                }
                            }
                        }
                        cotbutton.backgroundColor = UIColor.clear
                        cotbutton.addTarget(self, action: #selector(cotButtonTapped), for: .touchUpInside)
                        slide1.addSubview(cotbutton)
                    }
                    
                    if( i  == 1) {
                        slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: view.frame.height)
                        slide2.circleProgressBar.frame = CGRect(x: 150, y: 0, width: 400, height: 400)
                    }

                    if( i  == 2) {
                        slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: view.frame.height)
                    }

                    if( i  == 3) {
                        slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: view.frame.height)
                    }
                    
                    if( i  == 4) {
                        slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 30, width: view.frame.width, height: view.frame.height)
                        slide5.label.font = UIFont(name: "Rubik", size: 26)
                        slide5.line1.isHidden = false
                        slide5.adlabel1.text = "Help us make CallDoc better"
                        slide5.adlabel2.text = "Click to let us know what you’d like improved or what features you’d like added"
                        slide5.adbuttonclick.addTarget(self, action: #selector(self.adBtnPressed), for: .touchUpInside)
                    }
                }
            }
            slideScrollView.addSubview(slides[i])
        }
    }
    
    func DrawtheBarChartRed() {
        let chartConfig = BarsChartConfig(
            valsAxisConfig: ChartAxisConfig(from: 0, to: 7, by: 0.2)
        )
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                let frame = CGRect(x: -150, y: 0, width: 260, height: 240)
                let chart1 = BarsChart(
                    frame: frame,
                    chartConfig: chartConfig,
                    xTitle: "Days",
                    yTitle: "Amount",
                    bars: [
                        ("Mon", 2),
                        ("Tue", 4.5),
                        ("Wed", 3),
                        ("Thu", 5.1),
                        ("Fri", 5.8),
                        ("Sat", 1.5),
                        ("Sun", 1.0)
                    ],
                    color: UIColor(red:220/255, green:122/255, blue:125/255, alpha:1),
                    barWidth: 15
                )
                
                slide2.barchartView.addSubview(chart1.view)
            }
            
        } else {
            let frame = CGRect(x: -20, y: 0, width: 150, height: 160)
            
            let chart1 = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "Days",
                yTitle: "Amount",
                bars: [
                    ("Mon", 2),
                    ("Tue", 4.0),
                    ("Wed", 3),
                    ("Thu", 4.8),
                    ("Fri", 4.3),
                    ("Sat", 1.5),
                    ("Sun", 1.0)
                ],
                color: UIColor(red:220/255, green:122/255, blue:125/255, alpha:1),
                barWidth: 10
            )
            
            slide2.barchartView.addSubview(chart1.view)
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                let frame = CGRect(x: -150, y: 0, width: 260, height: 240)
                let chart3 = BarsChart(
                    frame: frame,
                    chartConfig: chartConfig,
                    xTitle: "Week",
                    yTitle: "Amount",
                    bars: [
                        ("Week1", 3.3),
                        ("Week2", 2.5),
                        ("Week3", 5.1),
                        ("Week4", 4.5)
                    ],
                    color: UIColor(red:220/255, green:122/255, blue:125/255, alpha:1),
                    barWidth: 15
                )
                
                slide3.barchartView.addSubview(chart3.view)
            }
            
        } else {
            let frame = CGRect(x: -20, y: 0, width: 150, height: 160)
            let chart3 = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "Week",
                yTitle: "Amount",
                bars: [
                    ("Week1", 3.3),
                    ("Week2", 2.5),
                    ("Week3", 5.1),
                    ("Week4", 4.5)
                ],
                color: UIColor(red:220/255, green:122/255, blue:125/255, alpha:1),
                barWidth: 10
            )
            
            slide3.barchartView.addSubview(chart3.view)
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                let frame = CGRect(x: -150, y: 0, width: 260, height: 240)
                let chart4 = BarsChart(
                    frame: frame,
                    chartConfig: chartConfig,
                    xTitle: "Months",
                    yTitle: "Amount",
                    bars: [
                        ("Month1", 4.3),
                        ("Month2", 2.9),
                        ("Month3", 3.4),
                        ("Month4", 4.7),
                        ("Month5", 5.4),
                        ("Month6", 4.2)
                    ],
                    color: UIColor(red:220/255, green:122/255, blue:125/255, alpha:1),
                    barWidth: 15
                )
                
                slide4.barchartView.addSubview(chart4.view)
            }
            
        } else {
            let frame = CGRect(x: -20, y: 0, width: 150, height: 160)
            let chart3 = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "Months",
                yTitle: "Y axis",
                bars: [
                    ("Month1", 4.3),
                    ("Month2", 2.9),
                    ("Month3", 3.4),
                    ("Month4", 4.7),
                    ("Month5", 5.4),
                    ("Month6", 4.2)
                ],
                color: UIColor(red:220/255, green:122/255, blue:125/255, alpha:1),
                barWidth: 10
            )
            slide4.barchartView.addSubview(chart3.view)
        }
    }

    
    @objc func adBtnPressed(_ sender: Any) {
        print("adBtnPressed Button pressed")
        UIApplication.shared.openURL(NSURL(string: "http://www.calldoc.com")! as URL)
    }
    
    func scrollViewDidScroll(_ scollview: UIScrollView) {
        let pageIndex = round(scollview.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
        if(pageIndex == 0) {
            slide1.barchartView.isHidden = true
            slide1.circleProgressBar.isHidden = true
        }

        if(pageIndex == 4) {
            slide5.line1.isHidden = false
            slide5.adlabel1.isHidden = false
            slide5.mycallslb.isHidden = true
            slide5.callcval.isHidden = true
            slide5.billedval.isHidden = true
            slide5.callslb.isHidden = true
            slide5.hourslb.isHidden = true
            slide5.billedlb.isHidden = true
        }
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func DrawtheBarChartGreen() {
        let chartConfig = BarsChartConfig(
            valsAxisConfig: ChartAxisConfig(from: 0, to: 7, by: 0.2)
        )
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                let frame = CGRect(x: -150, y: 0, width: 260, height: 240)
                let chart1 = BarsChart(
                    frame: frame,
                    chartConfig: chartConfig,
                    xTitle: "Days",
                    yTitle: "Amount",
                    bars: [
                        ("Mon", 2),
                        ("Tue", 4.5),
                        ("Wed", 3),
                        ("Thu", 5.1),
                        ("Fri", 5.8),
                        ("Sat", 1.5),
                        ("Sun", 1.0)
                    ],
                    color: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1),
                    barWidth: 15
                )
                
                slide2.barchartView.addSubview(chart1.view)
            }
            
        } else { // for all iphone devices

            var frame = CGRect(x: -20, y: 0, width: 150, height: 160)

            if phone && maxLength == 568 {
                //iphone 5
                frame = CGRect(x: -20, y: 0, width: 150, height: 160)
            }

            let chart1 = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "Days",
                yTitle: "Amount",
                bars: [
                    ("Mon", 2),
                    ("Tue", 4.0),
                    ("Wed", 3),
                    ("Thu", 4.8),
                    ("Fri", 4.3),
                    ("Sat", 1.5),
                    ("Sun", 1.0)
                ],
                color: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1),
                barWidth: 10
            )

            slide2.barchartView.addSubview(chart1.view)
        }
        

        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                let frame = CGRect(x: -150, y: 0, width: 260, height: 240)
                let chart1 = BarsChart(
                    frame: frame,
                    chartConfig: chartConfig,
                    xTitle: "Week",
                    yTitle: "Amount",
                    bars: [
                        ("Week1", 3.3),
                        ("Week2", 2.5),
                        ("Week3", 5.1),
                        ("Week4", 4.5)
                        
                    ],
                    color: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1),
                    barWidth: 15
                )
                
                slide3.barchartView.addSubview(chart1.view)
            }

        } else {
            
            let frame = CGRect(x: -20, y: 0, width: 150, height: 160)

            let chart2 = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "Week",
                yTitle: "Amount",
                bars: [
                    ("Week1", 3.3),
                    ("Week2", 2.5),
                    ("Week3", 5.1),
                    ("Week4", 4.5)
                ],
                color: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1),
                barWidth: 10
            )

            slide3.barchartView.addSubview(chart2.view)
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                let frame = CGRect(x: -150, y: 0, width: 260, height: 240)
                let chart1 = BarsChart(
                    frame: frame,
                    chartConfig: chartConfig,
                    xTitle: "Months",
                    yTitle: "Yaxis",
                    bars: [
                        ("Month1", 4.3),
                        ("Month2", 2.9),
                        ("Month3", 3.4),
                        ("Month4", 4.7),
                        ("Month5", 5.4),
                        ("Month6", 4.2)

                    ],
                    color: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1),
                    barWidth: 15
                )
                
                slide4.barchartView.addSubview(chart1.view)
            }
        } else {
            let frame = CGRect(x: -20, y: 0, width: 150, height: 160)
            let chart3 = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "Months",
                yTitle: "Y axis",
                bars: [
                    ("Month1", 4.3),
                    ("Month2", 2.9),
                    ("Month3", 3.4),
                    ("Month4", 4.7),
                    ("Month5", 5.4),
                    ("Month6", 4.2)
                ],
                color: UIColor(red:0.35, green:0.8, blue:0.47, alpha:1),
                barWidth: 10
            )
            slide4.barchartView.addSubview(chart3.view)
        }
    }
    
    
    func DrawtheBarChartOrange() {
        let chartConfig = BarsChartConfig(
            valsAxisConfig: ChartAxisConfig(from: 0, to: 7, by: 0.2)
        )
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                let frame = CGRect(x: -150, y: 0, width: 260, height: 240)
                let chart1 = BarsChart(
                    frame: frame,
                    chartConfig: chartConfig,
                    xTitle: "Days",
                    yTitle: "Amount",
                    bars: [
                        ("Mon", 2),
                        ("Tue", 4.5),
                        ("Wed", 3),
                        ("Thu", 5.1),
                        ("Fri", 5.8),
                        ("Sat", 1.5),
                        ("Sun", 1.0)
                    ],
                    color: UIColor(red:1, green:0.7, blue:0.36, alpha:1),
                    barWidth: 15
                )
                
                slide2.barchartView.addSubview(chart1.view)
            }
            
        } else { // for all iphone devices

            let frame = CGRect(x: -20, y: 0, width: 150, height: 160)

            let chart1 = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "Days",
                yTitle: "Amount",
                bars: [
                    ("Mon", 2),
                    ("Tue", 4.0),
                    ("Wed", 3),
                    ("Thu", 4.8),
                    ("Fri", 4.3),
                    ("Sat", 1.5),
                    ("Sun", 1.0)
                ],
                color: UIColor(red:1, green:0.7, blue:0.36, alpha:1),
                barWidth: 10
            )

            slide2.barchartView.addSubview(chart1.view)
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                let frame = CGRect(x: -150, y: 0, width: 260, height: 240)
                let chart3 = BarsChart(
                    frame: frame,
                    chartConfig: chartConfig,
                    xTitle: "Week",
                    yTitle: "Amount",
                    bars: [
                        ("Week1", 3.3),
                        ("Week2", 2.5),
                        ("Week3", 5.1),
                        ("Week4", 4.5)
                    ],
                    color: UIColor(red:1, green:0.7, blue:0.36, alpha:1),
                    barWidth: 15
                )
                
                slide3.barchartView.addSubview(chart3.view)
            }
            
        } else { // for all iphone devices

            let frame = CGRect(x: -20, y: 0, width: 150, height: 160)
            let chart3 = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "Week",
                yTitle: "Amount",
                bars: [
                    ("Week1", 3.3),
                    ("Week2", 2.5),
                    ("Week3", 5.1),
                    ("Week4", 4.5)
                ],
                color: UIColor(red:1, green:0.7, blue:0.36, alpha:1),
                barWidth: 10
            )

            slide3.barchartView.addSubview(chart3.view)
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
            if iOSDeviceScreenSize.height == 1024 {
                let frame = CGRect(x: -150, y: 0, width: 260, height: 240)
                let chart4 = BarsChart(
                    frame: frame,
                    chartConfig: chartConfig,
                    xTitle: "Months",
                    yTitle: "Amount",
                    bars: [
                        ("Month1", 4.3),
                        ("Month2", 2.9),
                        ("Month3", 3.4),
                        ("Month4", 4.7),
                        ("Month5", 5.4),
                        ("Month6", 4.2)
                    ],
                    color: UIColor(red:1, green:0.7, blue:0.36, alpha:1),
                    barWidth: 15
                )
                
                slide4.barchartView.addSubview(chart4.view)
            }
            
        } else { // for all iphone devices

            let frame = CGRect(x: -20, y: 0, width: 150, height: 160)
            let chart3 = BarsChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "Months",
                yTitle: "Y axis",
                bars: [
                    ("Month1", 4.3),
                    ("Month2", 2.9),
                    ("Month3", 3.4),
                    ("Month4", 4.7),
                    ("Month5", 5.4),
                    ("Month6", 4.2)
                ],
                color: UIColor(red:1, green:0.7, blue:0.36, alpha:1),
                barWidth: 10
            )
            slide4.barchartView.addSubview(chart3.view)
        }
    }

    @IBAction func prescriptionButtonClickedtemp(_ sender: UIButton) {
        let vc = SelfProfileViewController.storyboardInstance()
        vc.imageURLpassed = GlobalDocData.gprofileURL
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func editScheduleButtonClicked(_ sender: UIButton) {
        if GlobalDocData.profileVerified == true {
            let myclinicViewController = self.storyboard!.instantiateViewController(withIdentifier: "DoctorScheduleView") as! DoctorScheduleViewController
            myclinicViewController.showPopup = true
            self.nav = UINavigationController(rootViewController: myclinicViewController)
            self.present(self.nav!, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Alert", message: "Profile not verified.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                //self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func myClinicButton(_ sender: UIButton) {
        if GlobalDocData.profileVerified == false {
            let alert = UIAlertController(title: "Alert", message: "Profile not verified.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                //self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        self.HomeReadCustomObjectMyConsults()
    }

    @IBAction func addPatientsButton(_ sender: UIButton) {
        if GlobalDocData.profileVerified == false {
            let alert = UIAlertController(title: "Alert", message: "Profile not verified.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                //self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        let addpatientsviewController = self.storyboard!.instantiateViewController(withIdentifier: "AddPatientsview") as! AddPatientsViewController
        self.nav = UINavigationController(rootViewController: addpatientsviewController)
        self.present(self.nav!, animated: true, completion: nil)
    }
    
    // To get Hamberger button to slide out
    @IBAction func myOptionsClicked(_ sender: UIBarButtonItem) {

        profileImg.layer.cornerRadius = profileImg.frame.size.width / 2;
        profileImg.clipsToBounds = true;
        profileImg.layer.borderWidth = 2.0
        profileImg.layer.borderColor = UIColor.white.cgColor
        
        profileImageViewContainer.layer.cornerRadius = profileImageViewContainer.frame.size.width / 2;
        profileImageViewContainer.clipsToBounds = true;
        profileImageViewContainer.layer.borderWidth = 2.0

        LoadProfileImageforoptions()
        
        if UserDefaults.standard.bool(forKey: Constants.KEY_SCHEDULE_ON_OFF) || UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_OFFLINE) {
            profileImageViewContainer.layer.borderColor = redProfileBorder
        } else {
            if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_ONLINE) {
                if UserDefaults.standard.bool(forKey: Constants.KEY_DOCTOR_PAUSED) {
                    profileImageViewContainer.layer.borderColor = orangeProfileBorder
                } else {
                    profileImageViewContainer.layer.borderColor = greenProfileBorder
                }
            }
        }
        
        if phone && maxLength == 568 { // iphone 5S
            
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = 0
                ViewBehildOptionview.isHidden = false
                ViewBehildOptionsButtonview.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = -253
                ViewBehildOptionview.isHidden = true
                ViewBehildOptionsButtonview.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = 0
                ViewBehildOptionview.isHidden = false
                ViewBehildOptionsButtonview.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = -296.5
                ViewBehildOptionview.isHidden = true
                ViewBehildOptionsButtonview.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = 0
                ViewBehildOptionview.isHidden = false
                ViewBehildOptionsButtonview.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = -327
                ViewBehildOptionview.isHidden = true
                ViewBehildOptionsButtonview.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }

        if phone && maxLength == 812 {
            //iphone X
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = 0
                ViewBehildOptionview.isHidden = false
                ViewBehildOptionsButtonview.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = -296.67
                ViewBehildOptionview.isHidden = true
                ViewBehildOptionsButtonview.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            
        }
        
        if ipad && maxLength == 1024 {
            //iPad
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = 0
                ViewBehildOptionview.isHidden = false
                ViewBehildOptionsButtonview.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                optionsviewConstraint.constant = -620.0
                ViewBehildOptionview.isHidden = true
                ViewBehildOptionsButtonview.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @IBAction func ViewBehildOptionsButtonClicked(_ sender: UIButton) {

        if phone && maxLength == 568 {
            //iphone 5
            isSlideMenuHidden = true
            optionsviewConstraint.constant = -253
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            isSlideMenuHidden = true
            optionsviewConstraint.constant = -296.5
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            isSlideMenuHidden = true
            optionsviewConstraint.constant = -327
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        if phone && maxLength == 812 {
            //iphone X
            isSlideMenuHidden = true
            optionsviewConstraint.constant = -296.67
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        if ipad && maxLength == 1024 {
            //iPad
            isSlideMenuHidden = true
            optionsviewConstraint.constant = -620.0
            ViewBehildOptionview.isHidden = true
            ViewBehildOptionsButtonview.isHidden = true
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
    }
    
    func LoadProfileImageforoptions() {
        self.profileImg.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profile"))
    }
    
    @IBAction func consultingButton(_ sender: UIButton) {
        if GlobalDocData.profileVerified == false {
            let alert = UIAlertController(title: "Alert", message: "Profile not verified.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                //self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if self.scheduleCtrlBtnview.titleLabel?.text == "START CONSULTING" {
            let doctorScheduleVC = self.storyboard!.instantiateViewController(withIdentifier: "DoctorScheduleView") as! DoctorScheduleViewController
            doctorScheduleVC.showPopup = true
            self.nav = UINavigationController(rootViewController: doctorScheduleVC)
            self.present(self.nav!, animated: true, completion: nil)
            return
        }

        if !consultpressed {
            
            updateResumeStatusBackend(switchval: true)
            
            bottumBarView.backgroundColor = UIColor(red:0.98, green:0.61, blue:0, alpha:0.40)
            self.scheduleCtrlBtnview.setTitleColor(UIColor.black, for: .normal)
            self.scheduleCtrlBtnview.setTitle("RESUME CONSULTING", for: .normal)

            cotbutton.setImage(#imageLiteral(resourceName: "COTOrange"), for: .normal)
            
            navigationItem.title = "PAUSED"
            self.view.layer.contents = UIImage(named:"bg_orange")?.cgImage
            docImageViewContainer.layer.borderColor =  orangeProfileBorder
            DrawtheBarChartOrange()

            UserDefaults.standard.set(true, forKey: Constants.KEY_DOCTOR_PAUSED)

            slide1.circleProgressBar.progressColors = [UIColor(red:1, green:0.7, blue:0.36, alpha:1)]
            slide2.circleProgressBar.progressColors = [UIColor(red:1, green:0.7, blue:0.36, alpha:1)]
            slide3.circleProgressBar.progressColors = [UIColor(red:1, green:0.7, blue:0.36, alpha:1)]
            slide4.circleProgressBar.progressColors = [UIColor(red:1, green:0.7, blue:0.36, alpha:1)]
            slide1.circleProgressBar.trackColor = UIColor(red:0.76, green:0.36, blue:0, alpha:1)
            slide2.circleProgressBar.trackColor = UIColor(red:0.76, green:0.36, blue:0, alpha:1)
            slide3.circleProgressBar.trackColor = UIColor(red:0.76, green:0.36, blue:0, alpha:1)
            slide4.circleProgressBar.trackColor = UIColor(red:0.76, green:0.36, blue:0, alpha:1)
            slide1.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
            slide2.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
            slide3.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
            slide4.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
            consultpressed = true

        } else {
            updateResumeStatusBackend(switchval: false)
            bottumBarView.backgroundColor = UIColor(red:0.33, green:0.76, blue:0.44, alpha:0.60)
            self.scheduleCtrlBtnview.setTitleColor(UIColor.black, for: .normal)
            scheduleCtrlBtnview.setTitle("PAUSE CONSULTING", for: UIControlState.normal)
            cotbutton.setImage(#imageLiteral(resourceName: "COT"), for: .normal)
            navigationItem.title = "ONLINE"

            UserDefaults.standard.set(false, forKey: Constants.KEY_DOCTOR_PAUSED)

            LoadProfileImageforoptions()
            self.view.layer.contents = UIImage(named:"bg_green")?.cgImage
            docImageViewContainer.layer.borderColor = greenProfileBorder
            DrawtheBarChartGreen()
            slide1.circleProgressBar.progressColors = [UIColor(red:0.35, green:0.8, blue:0.47, alpha:1)]
            slide2.circleProgressBar.progressColors = [UIColor(red:0.35, green:0.8, blue:0.47, alpha:1)]
            slide3.circleProgressBar.progressColors = [UIColor(red:0.35, green:0.8, blue:0.47, alpha:1)]
            slide4.circleProgressBar.progressColors = [UIColor(red:0.35, green:0.8, blue:0.47, alpha:1)]

            slide1.circleProgressBar.trackColor = UIColor(red:0.16, green:0.49, blue:0.25, alpha:1)
            slide2.circleProgressBar.trackColor = UIColor(red:0.16, green:0.49, blue:0.25, alpha:1)
            slide3.circleProgressBar.trackColor = UIColor(red:0.16, green:0.49, blue:0.25, alpha:1)
            slide4.circleProgressBar.trackColor = UIColor(red:0.16, green:0.49, blue:0.25, alpha:1)
            
            slide1.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
            slide2.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
            slide3.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
            slide4.circleProgressBar.animate(fromAngle: 0, toAngle: 299, duration: 2, completion: nil)
            consultpressed = false
        }
    }
    
    
    func updateSchedularBackendforONOFFSwitchOFF(switchval: Bool) {
        //SchedularStatus
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"
        object.fields["SchedularStatus"] = switchval
        object.id = UserDefaults.standard.string(forKey: "customobjIDSchedular")!
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("HomeViewController:updateSchedularBackendforONOFFSwitchOFF() successfull")
        }) { (response) in
            print("HomeViewController:updateSchedularBackendforONOFFSwitchOFF Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func updateResumeStatusBackend(switchval: Bool) {
        let object = QBCOCustomObject()
        object.className = "DocSchedularDayView"
        
        object.fields["resumeConsult"] = !switchval
        object.id = UserDefaults.standard.string(forKey: "customobjIDSchedular")!      //record id
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            print("HomeViewController:updateResumeStatusBackend() successfull")
        }) { (response) in
            print("Resume Consult Response error: \(String(describing: response.error?.description))")
        }
    }

    @IBAction func OptionsMyProfileButton(_ sender: UIButton) {
        GlobalDocData.firsttimeview = false
        let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
        let docprimaryViewController = storyboard.instantiateViewController(withIdentifier: "PrimaryDetailsDocBaseViewController") as! PrimaryDetailsDocBaseViewController
        present(docprimaryViewController, animated: true, completion: nil)
    }
    
    @IBAction func OptionsMyAccountsDetailsButton(_ sender: UIButton) {
        isSlideMenuHidden = !isSlideMenuHidden
        GlobalDocData.firsttimeviewbankdetails = false
        let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
        let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorDetailsBaseViewController")
        present(docDeatilsTVC, animated: true, completion: nil)
    }
    
    @IBAction func OptionsMyConsultChargesButton(_ sender: UIButton) {
        isSlideMenuHidden = !isSlideMenuHidden
        GlobalDocData.firsttimeviewcharges = false
        let storyboard = UIStoryboard.init(name: "DoctorDetailsStoryboard", bundle: nil)
        let docDeatilsTVC = storyboard.instantiateViewController(withIdentifier: "DoctorConsultChargeBaseViewController")
        present(docDeatilsTVC, animated: true, completion: nil)
    }
    
    @IBAction func CallDocReferalcodeButton(_ sender: UIButton) {
        let calldoccodeviewcontroller:CallDocCodeViewController = self.storyboard?.instantiateViewController(withIdentifier: "CallDocCodeViewController") as! CallDocCodeViewController
        self.navigationController?.pushViewController(calldoccodeviewcontroller, animated: true)
    }
    
    @IBAction func OptionsFAQButton(_ sender: UIButton) {
        //        let message = "Hello Doc this is my Push Message!"
        //        var payload = [String: AnyObject]()
        //        var aps = [String: AnyObject]()
        //        aps[QBMPushMessageSoundKey] = "default" as AnyObject?
        //        aps[QBMPushMessageAlertKey] = message as AnyObject?
        //        payload[QBMPushMessageApsKey] = aps as AnyObject?
        //        let pushMessage = QBMPushMessage.init()
        //        pushMessage.payloadDict = NSMutableDictionary(dictionary: payload)
        //
        //        QBRequest.sendPush(pushMessage, toUsers: "33297", successBlock: { response, event in
        //            // Successful response with event
        //             print("Successfully Push sent!")
        //        }, errorBlock: { error in
        //             print("Can not send push: \(String(describing: error))")
        //        })
    }
    
    @IBAction func OptionsSupportButton(_ sender: UIButton) {
        //        let botViewController = self.storyboard!.instantiateViewController(withIdentifier: "BotWebView") as! BotWebViewController
        //        self.nav = UINavigationController(rootViewController: botViewController)
        //        self.present(self.nav!, animated: false, completion: { _ in })
    }
    
    @IBAction func OptionsLegalButton(_ sender: UIButton) {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        resetGlobalDocData()
        //GlobalDocData.firsttimeview = true
        //UserDefaults.standard.setValue(false, forKey: "isLoggedIn")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var ovc: UIViewController
        
        // show the onboarding screen
        ovc = (storyboard.instantiateViewController(withIdentifier: "OnBoardVC") as? UIViewController)!
        present(ovc, animated: true, completion: nil)
        
    }
    
    func resetGlobalDocData() {
        GlobalDocData.gdocname = ""
        GlobalDocData.guserid = 0
        GlobalDocData.gdocemail = ""
        GlobalDocData.gdocmobile = ""
        GlobalDocData.gusercustomdata = ""
        GlobalDocData.gdocage = ""
        GlobalDocData.gdocgender = ""
        GlobalDocData.gmcinum = ""
        GlobalDocData.gdegree = ""
        GlobalDocData.gdegree1 = ""
        GlobalDocData.gdocspeciality = ""
        GlobalDocData.gprofileURL = ""
        GlobalDocData.gMCIURL = ""
        GlobalDocData.gSignatureURL = ""
        GlobalDocData.gsamplesignatureURL = ""
        GlobalDocData.gaccounttype = ""
        GlobalDocData.firsttimeview = true
        GlobalDocData.firsttimeviewbankdetails = true
        GlobalDocData.firsttimeviewcharges = true
        GlobalDocData.gfullchargeval = 0
        GlobalDocData.ghalfchargeval = 0
        GlobalDocData.boolmytrustedteam = false
        GlobalDocData.gsmsinvitefirstname = ""
        GlobalDocData.gsmsinvitelastname = ""
        GlobalDocData.gwhichdocadded = 0    // this is to tell from my team which doctor has sent sms adding request ie 1,2,3,4,5
        GlobalDocData.cotspecialityselected = "" // this is to tell which speciality doctor want to add other doctors
        GlobalDocData.languagearray = []
        GlobalDocData.languagecounter = 0
        GlobalDocData.maskschedularview = false
        GlobalDocData.languagearrayipad = []
        GlobalDocData.languagecounteripad = 0
        GlobalDocData.gthefinalcalldoccode = ""
        GlobalDocData.regcalldoccode = ""
        GlobalDocData.countrycode = ""
        GlobalDocData.uploadspeciality = ""
        GlobalDocData.displayspeciality = ""
    }
    
    static func storyBoardInstance() -> HomeViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
    }
    
    // hiding options side menu when when touch other than options view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // let touch: UITouch? = touches.first
        //location is relative to the current view
        // do something with the touched point
        //        if touch?.view != optionsview {
        //            if isSlideMenuHidden {
        //
        //            }
        //            else
        //            {
        //                UIView.animate(withDuration: 0.9, animations: {
        //                self.view.layoutIfNeeded()
        //                })
        //                isSlideMenuHidden = !isSlideMenuHidden
        //                alignoptionsview()
        //            }
        //        }
    }
    
    func VerifiedUserLoginNow() {

        let userlogin = UserDefaults.standard.object(forKey: "userlogin") as! String
        let userpwd = UserDefaults.standard.object(forKey: "userpwd") as! String
        
        QBRequest.logIn(withUserLogin: userlogin, password: userpwd,  successBlock:
            { (response, user) in
                print("HomeViewController:VerifiedUserLoginNow():Success Login uring login: \(response)")
                let globalloginUser = QBUUser()
                GlobalVariables.gCurrentUser = globalloginUser
                globalloginUser.id = (user.id)
                globalloginUser.login = userlogin
                globalloginUser.password = userpwd
                globalloginUser.fullName = user.fullName

                if !QBChat.instance.isConnected {
                    QBChat.instance.connect(withUserID: user.id, password: userpwd, completion: { (error) in
                        if error != nil {
                            print(" HomeViewController: QB Chat ERROR")
                            print(error?.localizedDescription as Any)
                        }
                    })
                    
                    self.VerifiedUserDone()
                    self.UpdateUserData()
                } else {
                    self.VerifiedUserDone()  // Manish: 16/10/17 I have moved this just above at QB Chat Success
                }
        })
        { (errorResponse) in
            print("HomeViewController Login Error: \(errorResponse)")
        }
    }
    
    // This function updates User profile in user table of QB with user data for
    func UpdateUserData() {
        self.checkupdateschedular()
        if (GlobalDocData.firsttimeview) || (iscameasexistinguser) {
            let ver = versionBuild()
            let ver1 = ver.components(separatedBy: CharacterSet.decimalDigits.inverted)
                .joined()
            let version = "Ver" + ver1
            print("HomeViewController: UpdateUserData()- Updating  User data for first time")
            GlobalDocData.firsttimeview = false
            iscameasexistinguser = false
            let updateParameters = QBUpdateUserParameters()
            updateParameters.tags =  ["Doc1","IOS", version]
            updateParameters.fullName = GlobalDocData.gdocname

            QBRequest.updateCurrentUser(updateParameters, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
                user.password = QBSession.current.sessionDetails?.token
                QBChat.instance.connect(with: user) { err in
                    print("HomeViewController: Error in QBChat.instance.connect : \(String(describing: err))")
                }
                self.progressIndicator.isHidden = true
                self.progressIndicator.stopAnimating()

            }, errorBlock: {(_ response: QBResponse) -> Void in
                // Handle error
                print("HomeViewController: Error in User Update data")
                self.progressIndicator.isHidden = true
                self.progressIndicator.stopAnimating()
            })
        }
    }
    
    func getAllPatients() {
        QBRequest.users(withTags: ["Pat"], page: QBGeneralResponsePage(currentPage: 1, perPage: 10), successBlock: {(_ response: QBResponse, _ page: QBGeneralResponsePage, _ users: [QBUUser]) -> Void in
            PatientArray = users
            let myclinicViewController = self.storyboard!.instantiateViewController(withIdentifier: "myClinic") as! myClinicViewController
            self.nav = UINavigationController(rootViewController: myclinicViewController)
            self.present(self.nav!, animated: true, completion: nil)
            
        }, errorBlock: {(_ response: QBResponse) -> Void in
            // Handle error
            print("HomeViewController: getAllPatients():Error")
        })
    }
    
    func VerifiedUserDone() {
        progressIndicator.isHidden = true
        progressIndicator.stopAnimating()
        //enable buttons
        myClinicBttn.isEnabled = true
        addPatientBtn.isEnabled = true
        editschduleBtn.isEnabled = true
        self.registerForRemoteNotification()
    }
    
    // MARK: Remote notifications
    
    func registerForRemoteNotification() {
        // Register for push in iOS 8
        if #available(iOS 8.0, *) {
            let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            // Register for push in iOS 7
            UIApplication.shared.registerForRemoteNotifications(matching: [UIRemoteNotificationType.badge, UIRemoteNotificationType.sound, UIRemoteNotificationType.alert])
        }
    }
    
    func HomeReadPrimaryDetailsFromServer() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("HomeViewController: ReadPrimaryDetailsFromServer(): Successfully  !")
            if let contributors = contributors, contributors.count > 0  {
                UserDefaults.standard.setValue((contributors[0].id), forKey: "customobjIDPrimarydetails")
                UserDefaults.standard.synchronize()
                GlobalDocData.gprofileURL = (contributors[0].fields?.value(forKey: "profileUID") as? String) ?? ""
                GlobalDocData.gMCIURL = (contributors[0].fields?.value(forKey: "mciUID") as? String) ?? ""
                GlobalDocData.gdocgender = (contributors[0].fields?.value(forKey: "gender") as? String) ?? ""
                GlobalDocData.gdocage = (contributors[0].fields?.value(forKey: "experience") as? String) ?? ""
                GlobalDocData.gsamplesignatureURL = (contributors[0].fields?.value(forKey: "signatureUID") as? String) ?? ""
                GlobalVariables.gdocspeciality = (contributors[0].fields?.value(forKey: "specialisation") as? String) ?? ""
                self.convertbacktodisplayspeciality(speciality: GlobalVariables.gdocspeciality)
                GlobalVariables.gcalldoccode = (contributors[0].fields?.value(forKey: "calldoccode") as? String) ?? ""
                GlobalDocData.gdegree = (contributors[0].fields?.value(forKey: "degree") as? String) ?? ""
                GlobalDocData.gmcinum = (contributors[0].fields?.value(forKey: "mcinumber") as? String) ?? ""
                let languagestring = contributors[0].fields?.value(forKey: "languages") as? String
                GlobalDocData.languagearray = (languagestring?.components(separatedBy: ",")) ?? []
                GlobalDocData.profileVerified = (contributors[0].fields?.value(forKey: "profileverified") as? Bool) ?? false
            }
            self.LoadProfileImage()
        }) { (errorresponse) in
            print("HomeViewController: ReadPrimaryDetailsFromServer() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func ReadNotificationCounts() {
        let getRequest = NSMutableDictionary()
        getRequest["inviteeuserid"] = QBSession.current.currentUser?.id
        getRequest["notificationread"] = "false"
        QBRequest.objects(withClassName: "InviteTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            print("(HomeViewController:ReadNotificationCounts(): Successfully  !")
            if ((contributors?.count)! > 0) {
                // self.theprofileurl = (contributors![0].fields?.value(forKey: "profileimageURL") as? String)!
                self.badgeCount = (contributors?.count)!
                self.setUpBadgeCountAndBarButton()
            } else {
                self.badgeCount = (contributors?.count)!
                self.setUpBadgeCountAndBarButton()
            }
            
        }) { (errorresponse) in
            print("HomeViewController:ReadNotificationCounts() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func ReadAllNotifications() {
        let getRequest = NSMutableDictionary()
        getRequest["inviteeuserid"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "InviteTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            print("(HomeViewController:ReadAllNotifications(): Successfully  !")
            if ((contributors?.count)! > 0) {
                NotificationArray = contributors!
            } else {
                NotificationArray = contributors!
            }
            
        }) { (errorresponse) in
            print("HomeViewController:ReadAllNotifications() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func setMyClinicBadgeCount() {
        let getRequest = NSMutableDictionary()
        getRequest["doctoruserid"] = QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            
            if ((contributors?.count)! > 0) {
                self.myClinicBttn.badgeString = String((contributors?.count)!)
            } else {
                self.myClinicBttn.badgeString = String((contributors?.count)!)
            }
            
        }) { (response) in
            //Error handling
            print("HomeViewController:HomeReadCustomObjectforDocConsultCharges: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func HomeReadCustomObjectforDocConsultCharges() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocConsultationCharge", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("HomeViewController:HomeReadCustomObjectforDocConsultCharges(): Successfully  !")
            if (contributors?.count)! > 0 {
                UserDefaults.standard.setValue(((contributors![0].id) ?? ""), forKey: "customobjIDConsultationCharges")
                UserDefaults.standard.synchronize()
                let val =  contributors![0].fields?.value(forKey: "feeamount") as? Int
                GlobalVariables.gDoctorFee = val ?? 0
            }
            print("HomeViewController:HomeReadCustomObjectforDocConsultCharges: Successfully Read consult charges from Server !")
            
        }) { (response) in
            //Error handling
            print("HomeViewController:HomeReadCustomObjectforDocConsultCharges: Response error: \(String(describing: response.error?.description))")
        }
    }

    func HomeReadCustomObjectMyConsults() {
        let getRequest = NSMutableDictionary()
        getRequest["doctoruserid"] = QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in

            if ((contributors?.count)! > 0) {
                MyAllConsultsArray = contributors!
                MyAllConsultsUniqueArray = MyAllConsultsArray.filterDuplicate{ ($0.userID, $0.fields?.value(forKey: "patcalldoccode")) }
            } else {
                MyAllConsultsArray = contributors!
                MyAllConsultsUniqueArray = MyAllConsultsArray
            }
            MyAllConsultsArray = MyAllConsultsArray.sorted(by: { $0.createdAt! > $1.createdAt! }) //or updatedAt
            //self.updateConsultStatus()
            DispatchQueue.main.async {
                let myclinicViewController = self.storyboard!.instantiateViewController(withIdentifier: "myClinic") as! myClinicViewController
                self.nav = UINavigationController(rootViewController: myclinicViewController)
                self.present(self.nav!, animated: true, completion: nil)
            }
//            let myclinicViewController = self.storyboard!.instantiateViewController(withIdentifier: "myClinic") as! myClinicViewController
//            self.nav = UINavigationController(rootViewController: myclinicViewController)
//            self.present(self.nav!, animated: true, completion: nil)
            
        }) { (response) in
            //Error handling
            print("HomeViewController:HomeReadCustomObjectforDocConsultCharges: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func updateConsultStatus() {
        for consult in MyAllConsultsArray {
            
            let createdAt = consult.createdAt
            let extendedDuration = (consult.fields.value(forKey: "extendduration") as? Int) ?? 0
            let prescriptionId = (consult.fields.value(forKey: "prescriptionid") as? String) ?? ""
            //let currentTime = Date()
            //72hrs = 259200seconds
            let totalConsultPeriod = 259200 + extendedDuration
            let consultTimePassed = Int(Date().timeIntervalSince(createdAt!))
            let consultTimeRemaining = totalConsultPeriod - consultTimePassed
            let object = QBCOCustomObject()
            object.className = "ConsultsTable"
            if prescriptionId == nil || prescriptionId == "" {
                object.fields["status"] = "PENDING"
            } else if consultTimeRemaining <= 24 && consultTimeRemaining > 0 {
                //closing soon
                object.fields["status"] = "CLOSING SOON"
            } else if consultTimeRemaining <= 0 {
                //closed
                object.fields["status"] = "CLOSED"
            } else {
                //open
                object.fields["status"] = "OPEN"
            }
            //prescriptionid
            //object.fields["status"] = "open"
            object.id = consult.id//GlobalMedicalAdvicedata.ConsultTablerecordID
            QBRequest.update(object, successBlock: { (response, contributors) in
                    print("SUCCESS")
//                if consult == MyAllConsultsArray.last {
//                    DispatchQueue.main.async {
//                        let myclinicViewController = self.storyboard!.instantiateViewController(withIdentifier: "myClinic") as! myClinicViewController
//                        self.nav = UINavigationController(rootViewController: myclinicViewController)
//                        self.present(self.nav!, animated: true, completion: nil)
//                    }
//                }
            }) { (response) in
                
                print("ConfirmMedicalAdviceBaseViewController:UpdateConsultTableforPrescriptionrID: Response error: \(String(describing: response.error?.description))")
            }
            
//            if consult == MyAllConsultsArray.last {
//                DispatchQueue.main.async {
//                    let myclinicViewController = self.storyboard!.instantiateViewController(withIdentifier: "myClinic") as! myClinicViewController
//                    self.nav = UINavigationController(rootViewController: myclinicViewController)
//                    self.present(self.nav!, animated: true, completion: nil)
//                }
//            }
            
        }
    }
    
    func CheckConsultTablefornullPrescriptionID() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        getRequest["prescriptionid"] = ""
        
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                PatientConsultsdetailsArray = contributors!
            } else {
                print("HomeViewController: CheckConsultTablefornullPrescriptionID:No Nulll records found")
            }

        }) { (response) in
            //Error handling
            print("HomeViewController:CheckConsultTablefornullPrescriptionID(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func convertbacktodisplayspeciality(speciality: String) {
        var iitem = speciality
        GlobalDocData.gdocspeciality = iitem // this is used in rest of app logic as value same as what is stored in backend
        if iitem == "ent"{
            iitem = "E.N.T"
        } else if iitem == "general" {
            iitem = "G.P."
        } else if iitem == "altMedcine" {
            iitem = "Alt Medicine"
        }else if iitem == "cardiologist" {
            iitem = "Cardiologistc"
        }else if iitem == "dermatologist" {
            iitem = "Dermatologist"
        }else if iitem == "dietician" {
            iitem = "Dietician"
        }else if iitem == "endocrinologist" {
            iitem = "Endocrinologist"
        }else if iitem == "gastroentrologist" {
            iitem = "Gastroentrologist"
        }else if iitem == "gynaecologist" {
            iitem = "Gynaecologist"
        }else if iitem == "nephrologist" {
            iitem = "Nephrologist"
        }else if iitem == "neurologist" {
            iitem = "Neurologist"
        }else if iitem == "oncologist" {
            iitem = "Oncologist"
        }else if iitem == "orthopaedic" {
            iitem = "Orthopaedic"
        }else if iitem == "paediatrician" {
            iitem = "Paediatrician"
        }else if iitem == "psychiatrist" {
            iitem = "Psychiatrist"
        }else if iitem == "pulmonoligist" {
            iitem = "Pulmonoligist"
        }else if iitem == "sexologist" {
            iitem = "Sexologist"
        }
        
        GlobalDocData.displayspeciality = iitem
        
    }
    
    //MARK: Helpers
    
    func appVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    func build() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    func versionBuild() -> String {
        
        let version = self.appVersion()
        let build = self.build()
        var versionBuild = String(format:"v%@",version)
        if version != build {
            versionBuild = String(format:"%@(%@)", versionBuild, build )
        }
        return versionBuild
    }

    @objc func refreshHomeScreen() {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
}

extension Array {
    func filterDuplicate<T>(_ keyValue:(Element)->T) -> [Element] {
        var uniqueKeys = Set<String>()
        return filter{uniqueKeys.insert("\(keyValue($0))").inserted}
    }
}
