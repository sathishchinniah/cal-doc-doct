import UIKit

class PushInviteViewController: UIViewController {

    @IBOutlet weak var Profileimage: UIImageView!
    @IBOutlet weak var profilename: UILabel!
    
    var nav: UINavigationController?
    var profilenamepassed = ""
    var profileimageurl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Profileimage.sd_setImage(with: URL(string: profileimageurl), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
        profilename.text = profilenamepassed
    }
    
    @IBAction func OKButtonClick(_ sender :UIButton) {
        navigationController?.isNavigationBarHidden = false
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }
    
}
