//
//  ViewController.m
//  Doctor
//
//  Created by Manish Verma on 10/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import "GData.h"
#import "ViewController.h"
#import "VerifyViewController.h"
#import <sys/utsname.h>
@import FirebaseAuth;
//#import "Doctor-Swift.h"


#define USERPHONE @"userphonenumber"


@interface ViewController ()
{
    bool isLoggedin;
   // UIActivityIndicatorView *indicator;
    NSString *VerficationID;
     NSString *totalmobilenum; // country code + mobile number = 919845......
    NSString *fullname;
    double devtypehight;
}

@property (weak, nonatomic) IBOutlet UIView *bottombarView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *sendotpbuttonview;
@property (strong, nonatomic) UINavigationController *nav;
@property (weak, nonatomic) IBOutlet UITextField *mobilenumber;
@property (weak, nonatomic) IBOutlet UILabel *countrycodelb;
@property (weak, nonatomic) IBOutlet UITextField *countrycodetxt;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.progressIndicator.hidden = true;
   // [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    CGRect newFrame = CGRectMake( self.topView.frame.origin.x, self.topView.frame.origin.y, 200, 200);
    
    self.view.frame = newFrame;
    
     self.countrycodeData = [[NSArray alloc]initWithObjects:@"India +91", @"United Kingdom +44", @"Singapore +64", @"United States +1", @"Australia +61" ,nil];
    
    
    devtypehight = [self checkdevicetrype];
    
    if (devtypehight == 812)
    {
        
    }
    if (devtypehight == 736)
    {
         [_countrycodetxt setFont:[UIFont fontWithName:@"Rubik-Regular" size:17.0]];
        [_mobilenumber setFont:[UIFont fontWithName:@"Rubik-Regular" size:17.0]];
    }
    if (devtypehight == 667)
    {
        
    }
    if (devtypehight == 568)
    {
        
    }
    
   _countrycodetxt.text = @"+91"; // the default value
    
   
    
    self.bottombarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"schbottombarbg"]];
    
    //register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    self.mobilenumber.delegate= self;
    self.countrycodetxt.delegate=self;
    
    self.countrycodetableView.delegate = self;
    self.countrycodetableView.dataSource = self;
    self.countrycodetableView.hidden = true;
    
    self.mobilenumber.tintColor = [UIColor whiteColor];
    self.countrycodetxt.tintColor = [UIColor whiteColor];
    
    //back button
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"backBtn" style: UIBarButtonItemStyleBordered target:self action:@selector(BackButtonPressed)];
//    self.navigationItem.leftBarButtonItem = backButton;
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(20, 20, 44.0f, 30.0f)];
    [backButton setImage:[UIImage imageNamed:@"backBtn"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

- (void) BackButtonPressed
{
  //  [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// method to hide keyboard when user taps on a scrollview
-(void)hideKeyboard
{
    [self.mobilenumber resignFirstResponder];
    [self.countrycodetxt resignFirstResponder];
}


-(double)checkdevicetrype
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812)
        {
            NSLog(@"iPhone X");
            return screenSize.height;
        }
        
        if (screenSize.height == 736)
        {
            NSLog(@"iPhone 6 plus");
            return screenSize.height;
        }
        
        if (screenSize.height == 667)
        {
            NSLog(@"iPhone 6");
            return screenSize.height;
        }
        if (screenSize.height == 568)
        {
            NSLog(@"iPhone 5");
            return screenSize.height;
        }
    }
    return 0;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWasShown:(NSNotification *)aNotification
{
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationItem setHidesBackButton:YES];
}

//Dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"ViewConteroller:touchesBegan");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
    _countrycodetableView.hidden = true;
}



- (IBAction)textFieldDidBeginEditing:(UITextField *)textField {
    //sender.delegate = self;
    
    
    if (textField == _countrycodetxt)
    {
        [_countrycodetxt resignFirstResponder]; // this should hide KB
        [_mobilenumber resignFirstResponder];
        self.countrycodetableView.hidden = false;
    }
    else
    {
        _countrycodetableView.hidden = true;
    }
}

- (IBAction)textFieldDidEndEditing:(UITextField *)textField
{
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _mobilenumber)
    {
        [_mobilenumber becomeFirstResponder];
        _countrycodetableView.hidden = true;
    }
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
//    devtype = [self checkdevicetrype];
    
    // if it's the phone number textfield format it.
    if(textField.tag==10 ) {
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else if (range.location == 9) {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
            //totalmibilenum is = +91 plus entered mobile number
            totalmobilenum = [NSString stringWithFormat:@"%@%@",self.countrycodetxt.text, totalString];
            
            // use UIAlertController to Check Mobile number
            UIAlertController *alert= [UIAlertController
                                       alertControllerWithTitle:totalmobilenum
                                       message:@"Correct Mobile Number ?"
                                       preferredStyle:UIAlertControllerStyleAlert];
            
            
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           NSLog(@" Wrong  Mobile Number");
                                                           self.progressIndicator.hidden = true;
                                                           [self.progressIndicator stopAnimating];
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                           return;
                                                       }];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           
                                                           self.progressIndicator.hidden = false;
                                                           [self.progressIndicator startAnimating];
                                                           [self CheckifUserAlreadyExist];
                                                           //[ self CallFirebaseAuthentication];
                                                       }];
            [alert addAction:no];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    
    return YES;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    
    if (simpleNumber.length == 10 && deleteLastChar == NO) { [_mobilenumber resignFirstResponder]; }
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    return simpleNumber;
}


- (IBAction)mobilenumbertextdidend:(id)sender{
    
    if([self.mobilenumber.text length] < 10)
    {
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Your have not entered 10 digit Mobile Number!"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       
                                                   }];
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (IBAction)SendOTPButton:(UIButton *)sender {
    
    self.progressIndicator.hidden = false;
    [self.progressIndicator startAnimating];
    
    if([self.mobilenumber.text length] < 10)
    {
        
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Your have not entered 10 digit Mobile Number!"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                       self.progressIndicator.hidden = true;
                                                       [self.progressIndicator stopAnimating];
                                                       return;
                                                   }];
        
        [alert addAction:ok];
        self.progressIndicator.hidden = false;
        [self.progressIndicator startAnimating];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    //totalmibilenum is = +91 plus entered mobile number
    totalmobilenum = [NSString stringWithFormat:@"%@%@",self.countrycodetxt.text, self.mobilenumber.text];
    
    // use UIAlertController to Check Mobile number
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:totalmobilenum
                               message:@"Correct Mobile Number ?"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   
                                                   NSLog(@" Wrong  Mobile Number");
                                                   self.progressIndicator.hidden = true;
                                                   [self.progressIndicator stopAnimating];
                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                   return;
                                               }];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                                   self.progressIndicator.hidden = false;
                                                   [self.progressIndicator startAnimating];
                                                   [ self CheckifUserAlreadyExist];
                                               }];
    [alert addAction:no];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


-(void)CheckifUserAlreadyExist
{
    NSString *stringWithoutpluswith_d = [totalmobilenum
                                     stringByReplacingOccurrencesOfString:@"+" withString:@"d_"];
    
    [QBRequest usersWithLogins:@[stringWithoutpluswith_d] page:[QBGeneralResponsePage responsePageWithCurrentPage:1 perPage:10]
                  successBlock:^(QBResponse *response, QBGeneralResponsePage *page, NSArray *users) {
                      // Successful response with page information and users array
                      if (users.count > 0)
                      {
                         NSLog(@"ViewConteroller:CheckifUserAlreadyExist - yes user exists");
                          QBUUser *selectedUser = [users objectAtIndex:0] ;
                          [[NSUserDefaults standardUserDefaults]
                           setValue:selectedUser.fullName forKey:@"existinguserfullname"];
                          [[NSUserDefaults standardUserDefaults]
                           setValue:selectedUser.email forKey:@"existinguseremail"];
                          [[NSUserDefaults standardUserDefaults]
                           setValue:selectedUser.phone forKey:@"existinguserphone"];
                          [[NSUserDefaults standardUserDefaults]
                           setValue:selectedUser.customData forKey:@"usercustomdata"];
                          
                          [ self CallFirebaseAuthentication];
                      }
                      else
                      {
                          NSLog(@"ViewConteroller:CheckifUserAlreadyExist - NO user does not exists");
                          UIAlertController *alert= [UIAlertController
                                                     alertControllerWithTitle:@"User does not exists for this Mobile Number!!!!"
                                                     message:@"Please Register as New User !"
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        
                          UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * action){
                                                                         self.progressIndicator.hidden = true;
                                                                         [self.progressIndicator stopAnimating];
                                                                         return;
                                                                     }];
                          [alert addAction:ok];
                          [self presentViewController:alert animated:YES completion:nil];
                      }
                      
                  } errorBlock:^(QBResponse *response) {
                      // Handle error
                      NSLog(@"ViewConteroller:CheckifUserAlreadyExist - Response error");
                  }];
}


-(void)CallFirebaseAuthentication
{
    // Uncomment this when to test with firebase actuals
//        [[FIRPhoneAuthProvider provider] verifyPhoneNumber:totalmobilenum
//                                                completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
    
    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:totalmobilenum
                                            UIDelegate:nil
                                            completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
                                                    
                                                    if (error) {
                                                        NSLog(@"ViewController: Firebase Auth Error %@",[error localizedDescription]);
                                                        return;
                                                    }
                                                    VerficationID = verificationID;
                                                    [_progressIndicator stopAnimating];
                                                    [[NSUserDefaults standardUserDefaults]
                                                     setValue:VerficationID forKey:@"authVID"];
                                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                                    [[NSUserDefaults standardUserDefaults]
                                                     setValue:totalmobilenum forKey:USERPHONE];
                                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"existinguser"];
                                                        [[NSUserDefaults standardUserDefaults] synchronize];
    
                                                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"doctoronline"];
                                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                                    [self performSegueWithIdentifier:@"Verify" sender:nil];
    
                                                }];
// Uncomment this code when bypass actual Firebase
//    [[NSUserDefaults standardUserDefaults] setValue:totalmobilenum forKey:USERPHONE];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"existinguser"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"doctoronline"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [self performSegueWithIdentifier:@"Verify" sender:nil];
}



- (IBAction)RegisterButton:(UIButton *)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self presentViewController:VC animated:NO completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifer = @"SimpletableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifer];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifer];
    }
    cell.textLabel.text = [self.countrycodeData objectAtIndex:indexPath.row];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.countrycodeData count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.countrycodetableView cellForRowAtIndexPath:indexPath];
    NSString *str = cell.textLabel.text;
    NSArray *item = [str componentsSeparatedByString:@"+"];
    NSString *str2=[item objectAtIndex:1];
    NSString *myString = [NSString stringWithFormat:@"%@%@", @"+" , str2];
    _countrycodetxt.text = myString;
    _countrycodetableView.hidden = true;
    [_mobilenumber becomeFirstResponder];
}

@end

