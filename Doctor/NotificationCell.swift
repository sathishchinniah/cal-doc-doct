import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var docImageView: UIImageView!
    @IBOutlet weak var docNameLabel: UILabel!
    @IBOutlet weak var docDetailsLabel: UILabel!
    @IBOutlet weak var addDocSwitch: UISwitch!
    @IBOutlet weak var referPatientsSwitch: UISwitch!
    @IBOutlet weak var referPatientsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        addDocSwitch.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)
        referPatientsSwitch.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
