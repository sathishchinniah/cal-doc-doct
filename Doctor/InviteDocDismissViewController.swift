import UIKit

class InviteDocDismissViewController: UIViewController {

    @IBOutlet weak var congratslb: UILabel!
    
    var nav: UINavigationController?
    var specialitystring = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        congratslb.text = "You have added " + specialitystring + " Successfully !!"
        let timer = Timer.scheduledTimer(timeInterval: 2.3, target: self, selector: #selector(timeToMoveOn), userInfo: nil, repeats: false)
    }

    @objc func timeToMoveOn() {
        navigationController?.isNavigationBarHidden = false
        let cotViewController = self.storyboard!.instantiateViewController(withIdentifier: "CircleOfTrust") as! CircleOftrustViewController
        self.nav = UINavigationController(rootViewController: cotViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }

}
