//
//  GData.h
//  Patient
//
//  Created by Manish Verma on 15/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface GData : NSObject
{
    QBUUser *theCurrentUser;
    NSUInteger thecurrentuserID;
}

@property (nonatomic ,assign)BOOL isPatientFetched, ischatuserfound, isalreadyloggedin;
@property (nonatomic, retain) QBUUser *theCurrentUser;
@property (retain, nonatomic) CLLocation *glocationcurrent, *glocationdestination;
@property int scanidfor, chatdialogusersindex;
@property (nonatomic ,assign)NSUInteger thecurrentuserID;
@property NSString *PatientName, *incomingcallUserName, *calledUserName;
@property (nonatomic,retain) NSMutableArray *populatedArray, *patientslistArray, *chatDialoguserscontext;
@property (nonatomic,retain) QBRTCSession *theSession;
@property (weak, nonatomic) NSTimer *dialignTimer;


+ (GData *)sharedData;


@end
