import UIKit

class DoctorProfileViewController: UIViewController {

    @IBOutlet weak var spacilitylb: UILabel!
    @IBOutlet weak var agelb: UILabel!
    @IBOutlet weak var profileimageviewContainer: UIView!
    @IBOutlet weak var docimg: UIImageView!
    @IBOutlet weak var certificatetxt: UITextView!
    @IBOutlet weak var detailstxt: UITextView!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var docprobottombarview: UIView!
    
    var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // this removes navigation bar horizental line
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        title =   "Dr. " + GlobalDocData.gdocname
        
        if !GlobalDocData.gdocage.isEmpty {
            agelb.text = GlobalDocData.gdocage
        }
        
        if !GlobalDocData.gdocspeciality.isEmpty {
            spacilitylb.text = GlobalDocData.displayspeciality
        }

        LoadProfileImage()
        
        docimg.layer.cornerRadius = docimg.frame.size.width / 2;
        docimg.clipsToBounds = true;
        docimg.layer.borderWidth = 4.0
        docimg.layer.borderColor = UIColor.clear.cgColor
        
        profileimageviewContainer.layer.cornerRadius = profileimageviewContainer.frame.size.width / 2;
        profileimageviewContainer.clipsToBounds = true;
        profileimageviewContainer.layer.borderWidth = 2.0
        profileimageviewContainer.layer.borderColor =  UIColor.clear.cgColor
        
        let isonline = UserDefaults.standard.object(forKey: "doctoronline") as! Bool
        
        if(isonline) {
            profileimageviewContainer.layer.borderColor = UIColor.init(red: 0/255, green: 177/255, blue: 100/255, alpha: 1).cgColor
        } else {
            profileimageviewContainer.layer.borderColor = UIColor.orange.cgColor
        }

        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        topview.backgroundColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        docprobottombarview.backgroundColor = UIColor(patternImage: UIImage(named: "docprobbarbk")!)
    }

    func LoadProfileImage() {
        self.docimg.sd_setImage(with: URL(string: GlobalDocData.gprofileURL), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "homeviewVC") as! HomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }

}
