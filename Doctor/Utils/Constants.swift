import Foundation

let kChatPresenceTimeInterval:TimeInterval = 45
let kMessageContainerWidthPadding:CGFloat = 40.0

class Constants: NSObject {

    static let KEY_DOCTOR_ONLINE = "doctoronline"
    static let KEY_DOCTOR_OFFLINE = "doctoroffline"
    static let KEY_DOCTOR_PAUSED = "doctorpaused"
    static let KEY_SCHEDULE_ON_OFF = "schedularOnOFF"

    static let REFRESH_HOME_SCREEN = "refresh_home_screen"
    
    class var QB_USERS_ENVIROMENT: String {
        
        #if DEBUG
        return "dev"
        #elseif QA
        return "qbqa"
        #else
        assert(false, "Not supported build configuration")
        return ""
        #endif
        
    }

    static let mondaySchedularData = "monday_schedular_data"
    static let tuesdaySchedularData = "tuesday_schedular_data"
    static let wednesdaySchedularData = "wednesday_schedular_data"
    static let thursdaySchedularData = "thursday_schedular_data"
    static let fridaySchedularData = "friday_schedular_data"
    static let saturdaySchedularData = "saturday_schedular_data"
    static let sundaySchedularData = "sunday_schedular_data"

}

