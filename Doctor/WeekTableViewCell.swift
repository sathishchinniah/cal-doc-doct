//
//  WeekTableViewCell.swift
//  Docter
//
//  Created by Arjun Singh Baghel on 25/01/18.
//  Copyright © 2018 Arjun Singh Baghel. All rights reserved.
//

import UIKit

class WeekTableViewCell: UITableViewCell {

    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageView3: UIImageView!
    @IBOutlet weak var imageView4: UIImageView!
    @IBOutlet weak var imageView5: UIImageView!
    @IBOutlet weak var imageView6: UIImageView!
    @IBOutlet weak var imageView7: UIImageView!
    @IBOutlet weak var hrLabel: UILabel!
    @IBOutlet weak var amPmLabel: UILabel!
    
    @IBOutlet weak var circuleView1: UIView!
    @IBOutlet weak var circuleView2: UIView!
    @IBOutlet weak var circuleView3: UIView!
    @IBOutlet weak var circuleView4: UIView!
    @IBOutlet weak var circuleView5: UIView!
    @IBOutlet weak var circuleView6: UIView!
    @IBOutlet weak var circuleView7: UIView!

    @IBOutlet weak var SPView1: UIView!
    @IBOutlet weak var SPView2: UIView!
    @IBOutlet weak var SPView3: UIView!
    @IBOutlet weak var SPView4: UIView!
    @IBOutlet weak var SPView5: UIView!
    @IBOutlet weak var SPView6: UIView!
    @IBOutlet weak var SPView7: UIView!

    @IBOutlet weak var gapView2: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
